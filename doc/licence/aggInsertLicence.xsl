<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:cli="http://alexis.royer.free.fr/CLI"
                              xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
<xsl:output method="text" encoding="iso-8859-1"/>


<xsl:variable name="gePython"><xsl:text>ePython</xsl:text></xsl:variable>
<xsl:variable name="geCppC"><xsl:text>eCppC</xsl:text></xsl:variable>
<xsl:variable name="geXml"><xsl:text>eXml</xsl:text></xsl:variable>
<xsl:variable name="geShell"><xsl:text>eShell</xsl:text></xsl:variable>


<xsl:template name="insertCopyRight">
	<xsl:param name="aeTypeTarget"/>
   
	<xsl:call-template name="insertCopyRightStart">
		<xsl:with-param name="aeTypeTarget" select="$aeTypeTarget"/>
	</xsl:call-template>
	<xsl:call-template name="insertCopyRightBody">
		<xsl:with-param name="aeTypeTarget" select="$aeTypeTarget"/>
	</xsl:call-template>
	<xsl:call-template name="insertCopyRightEnd">
		<xsl:with-param name="aeTypeTarget" select="$aeTypeTarget"/>
	</xsl:call-template>
    
</xsl:template>


<xsl:template name="insertCopyRightStart">
	<xsl:param name="aeTypeTarget"/>

    <xsl:variable name="sStartLicenceLine">
        <xsl:choose>
            <xsl:when test="($gePython=$aeTypeTarget) or ($geShell=$aeTypeTarget)">
                <xsl:text>##</xsl:text>
            </xsl:when>
            <xsl:when test="$aeTypeTarget=$geCppC">
                <xsl:text>**</xsl:text>
            </xsl:when>
            <xsl:when test="$aeTypeTarget=$geXml">
                <xsl:text>--</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    
	<xsl:call-template name="repeatText">
		<xsl:with-param name="asText"><xsl:value-of select="$sStartLicenceLine"/></xsl:with-param>
		<xsl:with-param name="auiNbTimes" select="38"/>
	</xsl:call-template>
    <xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Contact: eti.laurent@gmail.com</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> This file is part of the Agglo project.</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> AGGLO_BEGIN_LICENSE</xsl:text><xsl:value-of select="$gsEndl"/>
    
</xsl:template>


<xsl:template name="insertCopyRightBody">
	<xsl:param name="aeTypeTarget"/>

    <xsl:variable name="sStartLicenceLine">
        <xsl:choose>
            <xsl:when test="($gePython=$aeTypeTarget) or ($geShell=$aeTypeTarget)">
                <xsl:text>##</xsl:text>
            </xsl:when>
            <xsl:when test="$aeTypeTarget=$geCppC">
                <xsl:text>**</xsl:text>
            </xsl:when>
            <xsl:when test="$aeTypeTarget=$geXml">
                <xsl:text>--</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Commercial License Usage</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Licensees holding valid commercial Agglo licenses may use this file in</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> accordance with the commercial license agreement provided with the</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Software or, alternatively, in accordance with the terms contained in</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> a written agreement between you and Plaisic.  For licensing terms and</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> conditions contact eti.laurent@gmail.com.</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> GNU General Public License Usage</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Alternatively, this file may be used under the terms of the GNU</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> General Public License version 3.0 as published by the Free Software</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> Foundation and appearing in the file LICENSE.GPL included in the</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> packaging of this file.  Please review the following information to</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> ensure the GNU General Public License version 3.0 requirements will be</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> met: http://www.gnu.org/copyleft/gpl.html.</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> In addition, the following conditions apply:</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>     * Redistributions in binary form must reproduce the above copyright</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>       notice, this list of conditions and the following disclaimer in</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>       the documentation and/or other materials provided with the</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>       distribution.</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>     * Neither the name of the Agglo project nor the names of its</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>       contributors may be used to endorse or promote products derived</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text>       from this software without specific prior written permission.</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</xsl:text><xsl:value-of select="$gsEndl"/>
    
</xsl:template>


<xsl:template name="insertCopyRightEnd">
	<xsl:param name="aeTypeTarget"/>

    <xsl:variable name="sStartLicenceLine">
        <xsl:choose>
            <xsl:when test="($gePython=$aeTypeTarget) or ($geShell=$aeTypeTarget)">
                <xsl:text>##</xsl:text>
            </xsl:when>
            <xsl:when test="$aeTypeTarget=$geCppC">
                <xsl:text>**</xsl:text>
            </xsl:when>
            <xsl:when test="$aeTypeTarget=$geXml">
                <xsl:text>--</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="$sStartLicenceLine"/><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> AGGLO_END_LICENSE</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sStartLicenceLine"/><xsl:text> </xsl:text><xsl:value-of select="$gsEndl"/>
	<xsl:call-template name="repeatText">
		<xsl:with-param name="asText"><xsl:value-of select="$sStartLicenceLine"/></xsl:with-param>
		<xsl:with-param name="auiNbTimes" select="38"/>
	</xsl:call-template>
    <xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>
    
</xsl:template>

</xsl:stylesheet>