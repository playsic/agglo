1.2.1
    - AggloTestBench
        - int�gration de can : int�gration dans les IO, primitives d'�mission r�ception
        - dump des IO pcap
    - AggloReq
    	- analyse tracabilite
    	- avancement projet
    - AggloUnitTest
        - saisie fiche de test xml avec par ihm xsd alexis royer
        - conversion fiche de test xml en script AUT et html
    - Agglo 
    	- passage python3.5

1.2.2
	- AggloCliServer
        - TODO : commentaire doxygen,
    - AggloCliClient
        - enrichissement du sample avec le multi cli (cli identique ou cli autre-debug ou monitoring ou ihm)
    - AggloTestBench
        - dump des IO CLI au format html, xml, csv, melted...
        - dump des IO scapy au format html, xml, csv, pcap, melted...
        - enrichissement du sample avec le multi cli (cli identique ou cli autre)
    - AggloUnitTest
        - r�cup�ration du code python ex�cut�
        - pas d'arret sur assert false
        - skip pytest de TestCase/TestSuite �volu� par le code
        - sort des test suites avec hook pytest_collection_modifyitems
        - campagne de test sp�cifi� par xml
        - correction AUT : bug si pas include, mark des test classe utilisable par root et test classe suivante, tester markage d'une suite en entier (ex mark(io), mark(tree)...), selecteur par rank de la test suite, bug le exclude root n'inclus plus personne
    - Agglo 
        - creation du namespace xsl agglo
        - mise en place des tickets
        - faire de la pub sur les forums

1.3
	- AggloCliClient
		- fifo des commandes
		- multithread
		- g�n�ration gui client python � partir du xml cli
        - reprise globale des TU
	- AggloCliServer
        - merge avec alexis royer
	- AggloUnitTest
        - merge avec pytest
	- AggloTestBench		

1.4
	- AggloMetrics
        - generation de graphes de dependance python
		- recuperation des metrics AggloReq (avancement exigences), AggloUT (couverture de test, test case OK/KO/skipped)
		- creation de rapport de metrics, avec graphe d'avancement
		- scalabilite des rapports � afficher
		- generation de metriques de couverture python
	- AggloToolKit
		- librairie cpp/python de gestion des droits d'acc�s
	- AggloCliClient
		- gestion des menus dynamique (cad non d�ductibles � partir du xml car g�n�r� dynamiquement par l'application server)
		- gestion des login (une entr�e suppl�mentaire est attendue, qui ne peut �tre d�duite du xml)
	- AggloTestBench
		- creation d'un sample de test qt
        - librairie crypto
	- AggloCliServer
        - gestion des droits d'acces par shell
		- generation d'un cli qt � partir du xml qt
		- merge de 2 cli (cli user et cli qt)
        - gestion de la fermeture des shells, et du forcage des menus des shells (logout...)
		- gestion de la pagination
        - commande system par cli
        - affichage couleur
        - telnet server mfc
        - unification IODevice FILE/FileDescriptor
        - redirect ssh par socat
        - remplacement de NonBlockable par BlockingModeMutator
        - test des evolutions 1.1
	- impl�mentation de tous les TODO
    - Agglo
        - faire de la pub sur les forums



2.0
	- AggloReq
		- detection des erreurs de tracabilite (doublon, non couvertes, non couvrantes, exigences inconnues...)
		- analyse d'impact
		- creation d'un sample avec exigences text/doc/pdf/html, code, TU, STD, STR, STR_TU
	- Agglo
		- mise sous AggloReq de l'ensemble du projet Agglo : creation des exigences, mise en place des commentaires, creation de la traca et des indicateurs
		- generation de la doc doxygen, avec generation des liens des sources entre elles (invocation de methode, utilisation de classe, include...)


2.1
	- AggloCliClient
		- creation de skin pour le client python
		- gestion des versions de cli
		- communication par zip
	- AggloCliServer
		- communication par zip
	- AggloMetrics
		- generation de metriques cpp de couverture (gcov), de profiling(gprof), de fuite memoire (valgrind)
		- ajout de metrics de qualite de code (commentaires, taille des methodes, taille des fichiers, nombre cyclometrique...)
		- gerer le renommage des metriques, de fa�on � ne pas perre un historique
	- AggloTestBench
		- r�cup�ration des metriques cli serveur (couverture, profiling, fuite memoire) durant les tests AggloTestBench

		
2.2
	- AggloIntegCont
		- scripts de mise en place de Jenkins, update source, creation job, lancement job....
		- xsl de conversion boost->junit
		- integration des metriques existantes
	- AggloVirtualMachine
		- script de cr�ation/configuration d'une VM par vagrant : d�marrage, cr�ation port ethernet/Serial/ssh, synchronisation/copie de fichier, lancement de scripts (compilation, d�marrage application....)
		- pilotage des VM par Jenkins: r�cup�ration des sources sur une VM, lancement compil, r�cup�ration des artefacts produits, lancement d'une campagne de test
		- sample
	- AggloCliClient
		- IODevice RS232
		- sample avec communication RS232
	- AggloCliServer
		- modification du sample pour ping entre 2 target
	- AggloTestBench
		- sample de test avec target sur VM, cli telnet/rs232/ssh, comm r�seau sur plusieurs interfaces


2.3
	- AggloBug
		- modification du flow de gestion des bugs
		- ajout/modification (descr, bugnote, attached file, etat...) d'un bug par script
	- AggloTestBench
		- ajout de lien vers la base de bug dans les rapports de test html, pour sauvegarder les pcaps/IO pcap dans les bugs, ou cr�er de nouveaux  bugs
		- creation d'un xsl de conversion xml fichier de test -> script python. Utiliser le projet schema de alexis
	- AggloReq
		- customisation par l'utilisateur sur les regexpr/tags xml a generer pour le parsing des docs
		- mecanisme de generation evolu� de l'id des exigences extraites par le parsing
		- creation de matrice de tracabilite transverses (entre 2 niveaux non reli�s directement)


2.4
	- AggloReq
		- gestion des version d'exigences
		- generation de lien hypertexte vers les fichiers d'exigence(si possible avec anchor vers les exigences) dans les rapports
		- gestion du planning : avancement previsionnel, avancement effectu�, calcul de l'avancement par un poids affect� a chaque type d'activit� (spec/conc/code/tests...)
		- gestion des exigences retenues
	- AggloTestBench
		- gestion du lien entre les exigences et le bug saisi depuis TestConductor
	- AggloCliClient
		- client html


2.5		
	- AggloCliClient
		- portage du fonctionnel python vers cpp/qt (Core, gui, skin)
	- AggloToolKit
		- librairie cpp/python de gestion trace
	- AggloCliServer
        - gestion de l'activation des traces
        - gestion commande systeme
        - gestion de l'affichage de conf (colonne, titre, largeur colonne...)		
		- integration CaccScriptRecorder


2.6		
	- AggloToolKit
		- librairie cpp/python de gestion des E/S g�n�rique
		- libraire FrameTools
    - AggloIO
        - integration de netzob pour l'apprentissage reseau
        - conversion xml description protocole en lua/FrameTools/AggloTestBench
        - snif RS232 sur wireshark
	- AggloTestBench
		- mise � jour du syst�me d'IO cli/scapy en utilisant AggloIO et la lib d'E/S g�n�rique
		- ajout et dump des IO CAN

		
2.7		
	- AggloToolKit
		- librairie c/cpp adresse IP, threads avec guard g�n�r� par xml, boite au lettres, pool de memoire, buffer circulaire
		- xsl de conversion xml->binaire
		- shell de recuperation des param�tres de ligne de commande
	- AggloUnitTest
		- unit test pour scripts shell
		- modification de boost pour generer le xml meme en cas de crash
		- correction du bug boost/ncbi de comptage des tests case skipped/failed
		- tests par mutation
	- Agglo
		- Creation des TU pour l'ensemble des shells du syst�me

		
3.0
	- AggloCliServer
		- portage CLI sous python
	- Agglo
		- Ajout des projets types pour les IDE netbeans/qt/codelite/eclipse/visual
		- mise � jour de l'ensemble de la doc/tracabilite/indicateurs
		- portage sous java
	- AggloProject
		- int�gration d'Agglo � un tableau de bord projet existant (ProjectOrIA, OpenJira...)

