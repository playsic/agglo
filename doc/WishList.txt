AggloProject
    AggloIntegCont
        - configuration par shell
        - configuration de Mantis/Redmine par shell
        - script generiques de compil/config target/demarrage target/testconductor

    - AggloReq
        - analyse d'impact : ascendante (exigences amont), descendantes, cousines, cousines et descendantes. En mode manuel (on specifie une exigence) ou auto � partir d'un diff
        3 Etudier la gestion de plusieurs type de couverture des exigences (profondeur d'arbre 2, largeur 2)
            1 rendre configurable les tags cr��s par les shell d'import exigences/tracabilit�
            2 importer la traca STD_INT
            3 merger tracabilite code et tracabilite STD_INT
            4 update distinct de tracabilite code ou tracabilite STD_INT
            5 mise a jour du xsl de report avancement
            6 mise a jour du xsl de report tracabilite 
            7 mise a jour des scripts mistral
        4 Apporter les evolutions aux scripts et xsl
            1 modifier la gestion des tags xml de fermeture dans les shell d'import exigences/tracabilit�
            2 gerer les valeurs par defauts dans les shell d'import (ex criticit� par d�faut)
            3 gerer l'import d'information complementaire du fichier d'exigence (criticite, complexite, titre, description...)
            4 optimiser Utils_Nodes.xsl (GetNodesRelation...)
            7 Utiliser template Copy pour corriger les problemes d'indentation dans le xsl de merge (remplacer sed dans merge.sh)
            8 ajouter un param�tre d'indentation dans le template Copy
            9 Corriger getColor pour la coloration dans le report avancement
            10 supprimer les variables globales dans les Utils_*.xsl; les remplacer par des param�tres de template
            11inserer dans les reports des liens vers le fichier des exigences
            12 mettre en place le reporting de la tracabilite bidirectionnelle
            13 mettre en place le stockage hebdomadaire des indicateurs globaux (avancement et tracabilite)
            14 mettre en place les indicateurs globaux dans les reports d'avancement et tracabilite : repartition, ponderation...
            15 mettre en place les graphes d'indicateurs globaux (avancement et tracabilite) : hebdomadaire, repartition, ponderation
            16 mettre en place le parametrage dynamique des indicateurs � afficher : titre, display...
            17 etudier la necessite d'un scripts d'inversion de la tracabilite
            18 modifier les reports pour generer des liens hypertextes entre tracabilite et avancement
            19 TODO :creer un template IsReqCovered, � utiliser dans ProgressUpdate et ProgressReport
            20 gerer un fichier de configuration par d�faut de ParamRequirement.xml (poids par d�faut, coloration par d�faut, r�partition de la charge par d�faut...)
            21 ajouter un parametre aux scripts d'extraction des exigences, pour permettre la construction des id d'exigences (ex \1_ID_\2\3, actuellement c'est \1 tout le temps. Ex STD_INT)
            22 d�tecter les erreurs de tracabilit� (doublons d'id, traca vers une exigence inconnue...)
            22 corriger tous les TODO restants
        5 Gerer la tracabilite multi niveau (profondeur d'arbre >2, largeur >2)
            1 creer des fichiers de sous exigences de 2�me niveau (STD_INT, planning, TU...) avec les attributs configurable (complexit� STD_INT, criticit� planning...)
            2 etudier l'articulation des fichiers d'exigences et de sous exigences : tags, merge des fichiers....
            3 mise � jours des xsl de merge de la tracabilite avec une identification dynamique des tags
            4 update distinct de tracabilite avec une identification dynamique des tags
            5 mise a jour du xsl de report avancement avec une identification dynamique des tags
            6 mise a jour du xsl de report tracabilite avec une identification dynamique des tags
            7 mise a jour des scripts mistral
            8 creer des fichiers de sous exigences de n�me niveau (SSS, PIDS STR...)
            9 etudier l'articulation des fichiers de tracabilite et de sous tracabilite : tags, merge des fichiers....
            10 gestion des versions d'exigences, de date de r�f�rence, de date d'extraction
            11 gestion des exigences retenues/non retenues dans le p�rim�tre (d�finition de cette donn�e dans le doc d'exigences ou dans un doc � part)
            10 mettre en place un fichier de configuration des liens entre documents d'exigences (graphe de tracabilite) et definition des tags associes
            11 etudier la generation de scripts shell a partir du fichier de configuration du graphe de tracabilite
            12 modification des report pour generer des liens hypertextes vers les matrices de tracabilite amont/aval
            13 creation de xsl de generation de matrices de tracabilite transverses
            14 modification des report pour generer dynamiquement les matrices de tracabilite transverse
            15 g�n�rer un graphe d'avancement pr�visionnel bas� sur une tracabilit� au niveau du planning
                -> https://google-developers.appspot.com/chart/interactive/docs/quick_start
                -> http://alexis.royer.free.fr/Watch/
                -> http://alexis.royer.free.fr/Watch/sample/abc-deal/2009-04-abc-deal-website-history.html
                -> http://alexis.royer.free.fr/Watch/sample/2009-04-abc-deal-website-history.html.all.txt
                -> http://franklinefrancis.github.io/SvgCharts4Xsl/
            16 gerer pour chaque lien de traca un fichier des exigences amont retenues : des exigences peuvent �tre retenues pour le code, mais pas pour STD_INT
        6 La cerise sur le gateau
            1 modifier les reports avancement/tracabilite pour g�n�rer des liens hypertextes vers les exigences/sous exigences
            2 etudier la necessite d'un script d'extraction de la tracabilite IsCoveredBy
            3 import exigences/tracabilite depuis un pdf
            4 utilisation des type/policy rtf/word dans les regexpr
            4 faire une documentation pour les cas d'usage, notamment les regexpr/shell d'import
            5 donner l'acces par html a l'ensemble des scripts
            6 etudier les possibilites de surcharge par l'utilisateur des reports et des attributs xml
            7 mettre en place les UT
            8 gerer dans le xml decrivant les liens de traca, le poids/pourcentage de chaque lien de traca dans le calcul de l'indicazeur global d'avancement => correspond a la repartition de la charge dans le projet

        
        
AggloDev 
    - AggloToolKit xsl generique de conversion xml->binaire
        - xsl:setEndianness
        - xsl:setPadding
        - xsl:output(value, nbBytes)
        - xsl:output(char)
        - xsl:output(string, bool endchar)
        - xsl:output(value, nbBits)
        - shell convertHexaToBin
        
    - AggloToolkit c/cpp
        - atkThread, avec guard, generation xml, post de message
        - gestion du controle d'acces
        - gestion d'E/S g�n�rique
        - module de trace, gestion des unions de classes de trace


    - AggloToolkit shells
        1 modifier les scripts existants
            1 Mettre en place l'arborescence des scripts : xsl/shell/requirements/svn/openssl/Utils
            2 Mettre en place une fonction shell de stockage de ligne de commande (Flag/Valeur) inspiree de CCommandeLine
            3 Modifier les anciens shells pour utiliser les Utils_*.sh
            4 corriger l'ensemle des shells : if param OK{paramOK=false} if paramOK
            5 ajouter aide dans shell
            6 mettre en place les UT

        
    - AggloVm Vm VirtualBox Ubuntu avec scripts de configuration automatiques
        - configuration ssh
        - configuration reseau
        - configuration rs232
        - copie de fichiers
        - lancement de binaires

    - AggloSnif
        - integration des netzob, apprentissage reseau
        - conversion xml description protocole en lua/FrameTools/AggloTestConductor
        - snif RS232 sur wireshark

    - AggloCli
        - generation de xml pour s'interfacer avec qt
        - utilisation de xsl de alexis pour une IHM de creation du xml des menus
        - gestion de la pagination
        - gestion des droits d'acces par shell
        - gestion de la fermeture des shells, et du forcage des menus des shells (logout...)
        - generation de clients gui html/python/qt simplifi�s et surchargeable � partir du cli xml
        - gestion des acces concurrents au client, file d'attente de commande
        - gestion commande systeme
        - gestion de l'activation des traces
        - gestion de l'affichage de conf (colonne, titre, largeur colonne...)
		- modifier le xml popur specifier les sorties attendues, creer un stub automatiquement pour les tests de scripts,
		  et un client qui connait les sorties attendues (regexpr)
        
AggloTest
    - AggloUnitTest
        - generation de html � partie des r�sultats pythons
        - integration de gcov/gcovr dans les tests unitaires cpp, + generation de rapport xml/html
        - integration de gprof dans les tests unitaires cpp, + generation de rapport xml/html
        - integration de valgrind dans les tests unitaires, + generation de rapport xml/html
        - detecter les crash dans les tests boost, et les integrer dans le rapport : generer le xml au fur et � mesure, pour rendre compte au mieux de la localisation du crash
        - campagne de test, saise de scripts de tests par IHM, selection de fichier par wildcard/recursif/exclusion, systeme de categorie de fiches de tests
        - systeme de selection automatiques de fiches : plus une categorie a �t� ignor�e dans les campagnes pr�c�dentes, plus elle a �t� modifi�e par les commits, plus elle est s�lectionnable; les fiches de tests doivent tourner d'une campagne sur l'autre
        - Mise en place d'un systeme de bouchonnage
            - Description des sorties en fonction des entr�e
            - Bouchonnage synchrone/asynchrone
        - Analyse statique
            - int�gration de codesonar
            - generation de rapport de test html/xml
            - generation des historique
            - generation des scripts de demarrage d'analyse/recuperation des resultats
        - python
            - tester les tests unitaires python sous nose
            - corriger assert pour poursuivre l'execution en cas de assert false
            - generation de rapport XML, avec balise de commentaire, au format boost ou equivalent
        - crer un xsl de conversion xml boost -> junit
        
    - AggloTestConductor
        - Architecture pour la description des target de test : connexion SSH, RS232, telnet; adresses IP; Liens entre target...
        - Framework pour la conversion Objet-> Ligne de commande; Lignes de commandes -> Objet
        - integration de gcov/gcovr dans les tests d'integration cpp, + generation de rapport xml/html
        - integration de gprof dans les tests d'integration cpp, + generation de rapport xml/html
        - creation de scripts de commande dans un fichier texte
		- ajout d'un lien dans les rapports de test vers creation de bugs/bug existant Mantis
        - IOLogs dumper, au format html, xml, pcap, melted...
        
    - AggloContinuous
      - integration de Jenkins, Docker, merge de branche automatiques Octopus(les furets), resolution automatique de conflit git conflict (les furets), system de comparaison de screenshot zeon pixel (les furets)

    
    
Global
    - Gerer des tests unitaires pour chacun des projets
    - Gerer la doc doxygen de chacun des projets
    - Gerer la gestion des requirments de chacun des projets
    - Scripts de mise en place des projets avecs les differentes options (valgrind, efence, coverage, jenkins...)
    - Scripts d'installation d'agglo
    - Gestion des environnements Visual, IronPython, Eclipse
    - Gestion des OS Windows, Cygwin, MinGW, Ubuntu
    