#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_CONF=Release
CND_DISTDIR=dist

# Include project Makefile
include AggloUnitTest_Cpp.mak

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1315775155/ncbi_safe_static.o \
	${OBJECTDIR}/_ext/1315775155/request_control.o \
	${OBJECTDIR}/_ext/1315775155/ncbidiag_p.o \
	${OBJECTDIR}/_ext/1315775155/ncbiargs.o \
	${OBJECTDIR}/_ext/1315775155/ncbitime.o \
	${OBJECTDIR}/_ext/1315775155/expr.o \
	${OBJECTDIR}/_ext/1315775155/ncbiapp.o \
	${OBJECTDIR}/_ext/1315775155/ncbiexpt.o \
	${OBJECTDIR}/_ext/1315775155/ncbienv.o \
	${OBJECTDIR}/_ext/1315775155/version.o \
	${OBJECTDIR}/_ext/1315775155/request_ctx.o \
	${OBJECTDIR}/_ext/1315775155/ncbi_system.o \
	${OBJECTDIR}/_ext/1315775155/ncbimtx.o \
	${OBJECTDIR}/_ext/1315775155/ncbi_stack.o \
	${OBJECTDIR}/_ext/1315775155/metareg.o \
	${OBJECTDIR}/_ext/1315775155/ddumpable.o \
	${OBJECTDIR}/_ext/1315775155/ncbiobj.o \
	${OBJECTDIR}/_ext/1315775155/ncbidiag.o \
	${OBJECTDIR}/_ext/1315775155/stream_utils.o \
	${OBJECTDIR}/_ext/1315775155/ncbifile.o \
	${OBJECTDIR}/_ext/951334299/autUnitTestTemplate.o \
	${OBJECTDIR}/_ext/608762838/ncbicfg.o \
	${OBJECTDIR}/_ext/1315775155/ncbi_param.o \
	${OBJECTDIR}/_ext/1315775155/ncbistre.o \
	${OBJECTDIR}/_ext/1315775155/ncbi_process.o \
	${OBJECTDIR}/_ext/1315775155/ncbistr.o \
	${OBJECTDIR}/_ext/1315775155/test_boost.o \
	${OBJECTDIR}/_ext/1315775155/ncbireg.o \
	${OBJECTDIR}/_ext/1315775155/syslog.o \
	${OBJECTDIR}/_ext/1315775155/ncbithr.o \
	${OBJECTDIR}/_ext/1315775155/ncbimempool.o \
	${OBJECTDIR}/_ext/1315775155/env_reg.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Release.mk dist/Release/Cygwin_4.x-Windows/libagglounittest_cpp.a

dist/Release/Cygwin_4.x-Windows/libagglounittest_cpp.a: ${OBJECTFILES}
	${MKDIR} -p dist/Release/Cygwin_4.x-Windows
	${RM} dist/Release/Cygwin_4.x-Windows/libagglounittest_cpp.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libagglounittest_cpp.a ${OBJECTFILES} 
	$(RANLIB) dist/Release/Cygwin_4.x-Windows/libagglounittest_cpp.a

${OBJECTDIR}/_ext/1315775155/ncbi_safe_static.o: ../../../src/ncbi/corelib/ncbi_safe_static.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbi_safe_static.o ../../../src/ncbi/corelib/ncbi_safe_static.cpp

${OBJECTDIR}/_ext/1315775155/request_control.o: ../../../src/ncbi/corelib/request_control.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/request_control.o ../../../src/ncbi/corelib/request_control.cpp

${OBJECTDIR}/_ext/1315775155/ncbidiag_p.o: ../../../src/ncbi/corelib/ncbidiag_p.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbidiag_p.o ../../../src/ncbi/corelib/ncbidiag_p.cpp

${OBJECTDIR}/_ext/1315775155/ncbiargs.o: ../../../src/ncbi/corelib/ncbiargs.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbiargs.o ../../../src/ncbi/corelib/ncbiargs.cpp

${OBJECTDIR}/_ext/1315775155/ncbitime.o: ../../../src/ncbi/corelib/ncbitime.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbitime.o ../../../src/ncbi/corelib/ncbitime.cpp

${OBJECTDIR}/_ext/1315775155/expr.o: ../../../src/ncbi/corelib/expr.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/expr.o ../../../src/ncbi/corelib/expr.cpp

${OBJECTDIR}/_ext/1315775155/ncbiapp.o: ../../../src/ncbi/corelib/ncbiapp.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbiapp.o ../../../src/ncbi/corelib/ncbiapp.cpp

${OBJECTDIR}/_ext/1315775155/ncbiexpt.o: ../../../src/ncbi/corelib/ncbiexpt.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbiexpt.o ../../../src/ncbi/corelib/ncbiexpt.cpp

${OBJECTDIR}/_ext/1315775155/ncbienv.o: ../../../src/ncbi/corelib/ncbienv.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbienv.o ../../../src/ncbi/corelib/ncbienv.cpp

${OBJECTDIR}/_ext/1315775155/version.o: ../../../src/ncbi/corelib/version.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/version.o ../../../src/ncbi/corelib/version.cpp

${OBJECTDIR}/_ext/1315775155/request_ctx.o: ../../../src/ncbi/corelib/request_ctx.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/request_ctx.o ../../../src/ncbi/corelib/request_ctx.cpp

${OBJECTDIR}/_ext/1315775155/ncbi_system.o: ../../../src/ncbi/corelib/ncbi_system.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbi_system.o ../../../src/ncbi/corelib/ncbi_system.cpp

${OBJECTDIR}/_ext/1315775155/ncbimtx.o: ../../../src/ncbi/corelib/ncbimtx.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbimtx.o ../../../src/ncbi/corelib/ncbimtx.cpp

${OBJECTDIR}/_ext/1315775155/ncbi_stack.o: ../../../src/ncbi/corelib/ncbi_stack.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbi_stack.o ../../../src/ncbi/corelib/ncbi_stack.cpp

${OBJECTDIR}/_ext/1315775155/metareg.o: ../../../src/ncbi/corelib/metareg.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/metareg.o ../../../src/ncbi/corelib/metareg.cpp

${OBJECTDIR}/_ext/1315775155/ddumpable.o: ../../../src/ncbi/corelib/ddumpable.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ddumpable.o ../../../src/ncbi/corelib/ddumpable.cpp

${OBJECTDIR}/_ext/1315775155/ncbiobj.o: ../../../src/ncbi/corelib/ncbiobj.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbiobj.o ../../../src/ncbi/corelib/ncbiobj.cpp

${OBJECTDIR}/_ext/1315775155/ncbidiag.o: ../../../src/ncbi/corelib/ncbidiag.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbidiag.o ../../../src/ncbi/corelib/ncbidiag.cpp

${OBJECTDIR}/_ext/1315775155/stream_utils.o: ../../../src/ncbi/corelib/stream_utils.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/stream_utils.o ../../../src/ncbi/corelib/stream_utils.cpp

${OBJECTDIR}/_ext/1315775155/ncbifile.o: ../../../src/ncbi/corelib/ncbifile.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbifile.o ../../../src/ncbi/corelib/ncbifile.cpp

${OBJECTDIR}/_ext/951334299/autUnitTestTemplate.o: ../../../src/output/autUnitTestTemplate.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/951334299
	${RM} $@.d
	$(COMPILE.c) -O2 -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/951334299/autUnitTestTemplate.o ../../../src/output/autUnitTestTemplate.c

${OBJECTDIR}/_ext/608762838/ncbicfg.o: ../../../src/ncbi/ncbicfg.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/608762838
	${RM} $@.d
	$(COMPILE.c) -O2 -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/608762838/ncbicfg.o ../../../src/ncbi/ncbicfg.c

${OBJECTDIR}/_ext/1315775155/ncbi_param.o: ../../../src/ncbi/corelib/ncbi_param.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbi_param.o ../../../src/ncbi/corelib/ncbi_param.cpp

${OBJECTDIR}/_ext/1315775155/ncbistre.o: ../../../src/ncbi/corelib/ncbistre.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbistre.o ../../../src/ncbi/corelib/ncbistre.cpp

${OBJECTDIR}/_ext/1315775155/ncbi_process.o: ../../../src/ncbi/corelib/ncbi_process.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbi_process.o ../../../src/ncbi/corelib/ncbi_process.cpp

${OBJECTDIR}/_ext/1315775155/ncbistr.o: ../../../src/ncbi/corelib/ncbistr.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbistr.o ../../../src/ncbi/corelib/ncbistr.cpp

${OBJECTDIR}/_ext/1315775155/test_boost.o: ../../../src/ncbi/corelib/test_boost.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/test_boost.o ../../../src/ncbi/corelib/test_boost.cpp

${OBJECTDIR}/_ext/1315775155/ncbireg.o: ../../../src/ncbi/corelib/ncbireg.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbireg.o ../../../src/ncbi/corelib/ncbireg.cpp

${OBJECTDIR}/_ext/1315775155/syslog.o: ../../../src/ncbi/corelib/syslog.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/syslog.o ../../../src/ncbi/corelib/syslog.cpp

${OBJECTDIR}/_ext/1315775155/ncbithr.o: ../../../src/ncbi/corelib/ncbithr.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbithr.o ../../../src/ncbi/corelib/ncbithr.cpp

${OBJECTDIR}/_ext/1315775155/ncbimempool.o: ../../../src/ncbi/corelib/ncbimempool.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/ncbimempool.o ../../../src/ncbi/corelib/ncbimempool.cpp

${OBJECTDIR}/_ext/1315775155/env_reg.o: ../../../src/ncbi/corelib/env_reg.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1315775155
	${RM} $@.d
	$(COMPILE.cc) -O2 -D_AGGLO_BOOST -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1315775155/env_reg.o ../../../src/ncbi/corelib/env_reg.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Release
	${RM} dist/Release/Cygwin_4.x-Windows/libagglounittest_cpp.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
