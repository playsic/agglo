#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_CONF=Debug
CND_DISTDIR=dist

# Include project Makefile
include AggloUnitTest_Cpp_Sample.mak

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/284084632/main.o \
	${OBJECTDIR}/_ext/284084632/Sample_UT.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../AggloUnitTest_Cpp/dist/Debug/Cygwin_4.x-Windows/libagglounittest_cpp.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Debug.mk dist/Debug/Cygwin_4.x-Windows/agglounittest_cpp_sample.exe

dist/Debug/Cygwin_4.x-Windows/agglounittest_cpp_sample.exe: ../AggloUnitTest_Cpp/dist/Debug/Cygwin_4.x-Windows/libagglounittest_cpp.a

dist/Debug/Cygwin_4.x-Windows/agglounittest_cpp_sample.exe: ${OBJECTFILES}
	${MKDIR} -p dist/Debug/Cygwin_4.x-Windows
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/agglounittest_cpp_sample ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/_ext/284084632/main.o: ../../../test/sample/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/284084632
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/284084632/main.o ../../../test/sample/main.cpp

${OBJECTDIR}/_ext/284084632/Sample_UT.o: ../../../test/sample/Sample_UT.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/284084632
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/284084632/Sample_UT.o ../../../test/sample/Sample_UT.cpp

# Subprojects
.build-subprojects:
	cd ../AggloUnitTest_Cpp && ${MAKE}  -f AggloUnitTest_Cpp.mak CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Debug
	${RM} dist/Debug/Cygwin_4.x-Windows/agglounittest_cpp_sample.exe

# Subprojects
.clean-subprojects:
	cd ../AggloUnitTest_Cpp && ${MAKE}  -f AggloUnitTest_Cpp.mak CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
