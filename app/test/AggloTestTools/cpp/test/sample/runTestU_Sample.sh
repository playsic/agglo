
# Global variables
# equivalent $(dirname $0)
TUSampleShellPath=${0%/*}
sAutRunUTShellPath="$TUSampleShellPath/../../src"
TUSampleBinaryPath="$TUSampleShellPath/../../build/netbeans/AggloUnitTest_Cpp_Sample/dist"
TUSampleBinary="agglounittest_cpp_sample.exe"
TUSampleReportPath="$TUSampleShellPath/STR_TU"
TUSampleReport="agglounittest_cpp_sample"

# Check the parameters
ParamOK=true
if [ $# != 1 ] && [ $# != 2 ];
then
    echo "Commmand syntax : runTestU_Sample {DEBUG|RELEASE} [Verbose]"
    ParamOK=false
fi


if $ParamOK ;
then

    # Init parameters
    if [ $1 = "DEBUG" ]
    then
        TUSampleBinaryPath=$TUSampleBinaryPath/Debug/Cygwin_4.x-Windows
    else
        TUSampleBinaryPath=$TUSampleBinaryPath/Release/Cygwin_4.x-Windows
    fi

    if [ $# = 2 ] && [ $2 = "Verbose" ];
    then
        Verbose=$2
    else
        Verbose=""
    fi

    # Run the testU
    $sAutRunUTShellPath/autRunTestU.sh $TUSampleBinaryPath $TUSampleBinary $TUSampleReportPath $TUSampleReport $Verbose
fi

