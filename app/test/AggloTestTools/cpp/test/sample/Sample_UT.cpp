
#include "Sample_UT.h"

#include "aut/autUnitTestTemplate.h"


BOOST_AUTO_TEST_SUITE(Sample_UT)

BOOST_AUTO_TEST_CASE(Sample_UT_FirstTestCase)
{
    UnitTestComment("1.1 First Check");
    BOOST_CHECK(UI_ValidAddressIPv4 == 0x7A0E002D);
    BOOST_CHECK_EQUAL(0, 0);
	    
	UnitTestComment("1.2 Second Check");
    BOOST_CHECK(1==0);
    BOOST_CHECK_EQUAL(1, 0);
}

BOOST_AUTO_TEST_CASE(Sample_UT_SecondTestCase)
{
    UnitTestComment("2.1 First Check");
    BOOST_CHECK(UI_ValidAddressIPv4 == 0x7A0E002D);
    BOOST_CHECK_EQUAL(0, 0);
}

BOOST_AUTO_TEST_SUITE_END()