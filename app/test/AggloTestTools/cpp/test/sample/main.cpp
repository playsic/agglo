

#include "aut/autUnitTestTemplate.h"

USING_NCBI_SCOPE;

NCBITEST_INIT_TREE()
{
    //TODO : comptabiliser les test case disabled dans report
    //NCBITEST_DISABLE(Sample_UT_FirstTestCase);
    //NCBITEST_DISABLE(Sample_UT_SecondTestCase);
}

NCBITEST_AUTO_INIT()
{
    SET_AGGLO_BOOST_TEST_MODULE("AggloTUSample");
    
    // Write init code here
}


NCBITEST_AUTO_FINI()
{
    // Write exit code here
}


