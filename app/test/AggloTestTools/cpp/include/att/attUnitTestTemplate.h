#ifndef _ATT_UNIT_TEST_TEMPLATE_H_
#define _ATT_UNIT_TEST_TEMPLATE_H_

#define BOOST_TEST_NO_MAIN 1

#include "corelib/test_boost.hpp"
#include "ncbi_pch.hpp"

#include <boost/test/unit_test.hpp>

#define SET_AGGLO_BOOST_TEST_MODULE(AGGLO_MASTER_TEST_SUITE_NAME) boost::unit_test::framework::master_test_suite().p_name.value = AGGLO_MASTER_TEST_SUITE_NAME;

void UnitTestHeader(std::string content); // Global Header
void UnitTestFooter(std::string content); // Global Footer
void UnitTestTitle(std::string content); // Title for one test
void UnitTestComment(std::string content); // Comment while processing test

#endif // _ATT_UNIT_TEST_TEMPLATE_H_