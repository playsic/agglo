
#include "att/autUnitTestTemplate.h"

// Global header
void UnitTestHeader(std::string content)
{
    content = "**** DEBUT TEST UNITAIRE DE " + content + " ****";
    int nbChar = content.length();
    std::string StarLine = "";

    for (int i = 0; i < nbChar; i++)
    {
        StarLine += "*";
    }

    BOOST_TEST_MESSAGE(StarLine);
    BOOST_TEST_MESSAGE(content);
    BOOST_TEST_MESSAGE(StarLine);
}

// Title for one test
void UnitTestTitle(std::string content)
{
    content = "**** " + content + " ****";
    BOOST_TEST_MESSAGE(content);
}

// Comment while processing test
void UnitTestComment(std::string content)
{
    BOOST_TEST_MESSAGE(content);
}