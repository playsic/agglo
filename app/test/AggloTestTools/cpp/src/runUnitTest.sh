
# Global variables
# equivalent $(dirname $0)
TUShellPath=${0%/*}
source "$TUShellPath/../../../../development/AggloToolKit/src/shell/utils/atkFileSystem.sh"
sAutOutputPath="../../xsl"

# Check the parameters
ParamOK=true
if [ $# != 4 ] && [ $# != 5 ];
then
    echo "Commmand syntax : autRunTestU PathUTBinary UTBinary sReportPath sReportName [Verbose]"
    echo "example : ../../autRunTestU.sh ../build/Debug testUT.exe testUTReport ../src/test/STR_TU [Verbose]"
    ParamOK=false
fi


if $ParamOK ;
then
    # Init parameters
    PathUTBinary=./$1
    UTBinary=$2
    sReportPath=$3
    sReportName=$4
    BoostReportFile_Log=$sReportPath/$sReportName"_Log.xml"
    BoostReportFile_Result=$sReportPath/$sReportName"_Result.xml"
   
    if [ $# = 5 ] && [ $5 = "Verbose" ];
    then
        Verbose=true
    else
        Verbose=false
    fi

    # Run unit tests
    $PathUTBinary/$UTBinary --log_level=all --log_format=XML --report_level=detailed --report_format=XML --log_sink=$BoostReportFile_Log --report_sink=$BoostReportFile_Result

    # Report unit tests
    $TUShellPath/$sAutOutputPath/autReportUnitTest.sh $asReportPath $asReportName $Verbose
    
    # TODO proposer un affichage du html en option
fi

