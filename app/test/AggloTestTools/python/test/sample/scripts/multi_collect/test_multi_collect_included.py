############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from pytest import fixture
from pytest import mark


@fixture
def test_item(request):
    return request.node


# This test matches the criterias of the first include collect of multi_collect 
# test class (path in ./scripts/multi_collect, mark multi_collect_include_action1).
# Therefore, defined collect level actions are applied to this test (add mark multi_collect_include_mark1)
@mark.multi_collect_include_action1
def test_multi_collect_included_action1(test_item):
    assert test_item.get_marker("multi_collect_include_mark1") is not None
    assert test_item.get_marker("multi_collect_include_mark2") is None

    
# This test matches the criterias of the second include collect of multi_collect 
# test class (path in ./scripts/multi_collect, mark multi_collect_include_action2).
# Therefore, defined collect level actions are applied to this test (add mark multi_collect_include_action2)
@mark.multi_collect_include_action2
def test_multi_collect_included_action2(test_item):
    assert test_item.get_marker("multi_collect_include_mark1") is None
    assert test_item.get_marker("multi_collect_include_mark2") is not None

    
# This test matches the criterias of the first and second include collect of multi_collect 
# test class (path in ./scripts/multi_collect, mark multi_collect_include_action1, 
# mark multi_collect_include_action2). Therefore, defined collect level actions 
# are applied to this test (add mark multi_collect_include_action1, 
# add mark multi_collect_include_action2)
@mark.multi_collect_include_action1
@mark.multi_collect_include_action2
def test_multi_collect_included_all_actions(test_item):
    assert test_item.get_marker("multi_collect_include_mark1") is not None
    assert test_item.get_marker("multi_collect_include_mark2") is not None


# This test matches the criterias of the third include collect of multi_collect 
# test class (path in ./scripts/multi_collect, mark multi_collect_include_no_action), 
# which has no associated actions. Therefore, no action is applied to this test.
@mark.multi_collect_include_no_action
def test_multi_collect_included_no_action(test_item):
    assert test_item.get_marker("multi_collect_include_mark1") is None
    assert test_item.get_marker("multi_collect_include_mark2") is None


