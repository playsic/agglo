############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################



# Global variables
sThisShellPath=${0%/*}

source "$sThisShellPath/../../../../../development/AggloToolKit/shell/src/utils/atkString.sh"
source "$sThisShellPath/../../../../../development/AggloToolKit/shell/src/utils/atkCommandLine.sh"

sAutSource="../../src"

# Parse command line
resetCommand
defineParam "n no-generation" "Disable plugins files generation (use existing ones)"
parseCommandLine $@
sErrorMessage=$(checkCommandLine)

# If there is an error in command line
if [ -n "$sErrorMessage" ]; then
    # Display error message
    echo -e $sErrorMessage
else

    # If help is required
    if [ $(isFlagSet "help") == "true" ]; then
        # Display help
        sHelp=$(getHelp)
        echo -e $sHelp
    else
        # Execute test campaign
        sCampaignFile=$sThisShellPath/campaign.xml
        sReportPath=$sThisShellPath/reports
        # TODO there is an issue on collection through campaign plugin
        # $sThisShellPath/$sAutSource/run_tests.sh --campaign "$sCampaignFile" --tests-name "AutSample" --reports-path $sReportPath "$@"
        $sThisShellPath/$sAutSource/run_tests.sh --tests-path "$sThisShellPath" --tests-name "AutSample" --reports-path $sReportPath "$@"
    fi
fi



# # Global variables
# #equivalent $(dirname $0)
# sRunSampleUnitTestShellPath=${0%/*}
# sCliClientPath="../../../../../../development/AggloCliClient/python/test/sample"
# sAggloUnitTestPath="../../src"
# sAggloUnitTestReportPath="../../../xsl"

# if [ $# = 1 ] && [ $1 = "Verbose" ];
# then
#     Verbose=$1
# else
#     Verbose=""
# fi

# # Update generated cli client source file
# # $sRunSampleUnitTestShellPath/$sCliClientPath/acsGenerateCliClientSample.sh

# # Run test campaign
# # TODO executer python en silent 2>&1 /dev/null
# $sRunSampleUnitTestShellPath/$sAggloUnitTestPath/autRunUnitTest.sh "$sRunSampleUnitTestShellPath/scripts" "$sRunSampleUnitTestShellPath/reports" autSampleUnitTestReport $Verbose
# # python "$sAggloUnitTestPath/autRunPytest.py"

# # Generate report
# # TODO executer en mode Verbose
# # $sRunSampleUnitTestShellPath/$sAggloUnitTestReportPath/autGenerateReport.sh "$sRunSampleUnitTestShellPath/reports" "testReport"
