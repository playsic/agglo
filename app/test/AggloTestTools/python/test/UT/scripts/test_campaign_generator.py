############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from stubs import AutStubPytestFunc

from agglo_tt.fixtures import trace_test_start
from fixtures import test_campaign
from fixtures import campaign_root_path


def test_UT_AttUserCampaign_Constructor(trace_test_start, test_campaign):
    #UnitTestComment("AutUserCampaign: test that test classes and global filter are correctly generated")
    user_campaign = test_campaign.AttCampaignUserCampaign()

    assert user_campaign["collect_mode"] is not None
    assert user_campaign["complex_criterias"] is not None
    assert user_campaign["add_actions"] is not None
    assert user_campaign["multi_collect"] is not None
    assert user_campaign["default_criterias"] is not None
    assert user_campaign["default_include"] is not None
    assert user_campaign["actual_tests"] is not None
    assert user_campaign.global_filter is not None


# def test_UT_AttUserCampaign_CollectModeIgnore(trace_test_start, test_campaign):
#     user_campaign = test_campaign.AttCampaignUserCampaign()
#     campaign_root_path = "/campaign_root"

#     #UnitTestComment("AutUserCampaign: test that bad path/file names are ignored")
#     assert user_campaign.ignore_collect(path="/bad_path")
#     assert user_campaign.ignore_collect(path="/bad_path", file_name="collect_mode_ok.py")
#     assert user_campaign.ignore_collect(path=campaign_root_path + "/collect_mode", 
#                                         file_name="bad_file_name.py")

#     #UnitTestComment("AutUserCampaign: test that correct path/file names are not ignored")
#     assert user_campaign.ignore_collect(path=campaign_root_path + "/collect_mode")
#     assert user_campaign.ignore_collect(path=campaign_root_path + "/collect_mode", 
#                                         file_name="collect_mode_ok.py")
#     assert user_campaign.ignore_collect(path=campaign_root_path + "/collect_mode/child", 
#                                         file_name="collect_mode_ok.py")


def test_UT_AttUserCampaign_CollectModeSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that include criterias are necessary")
    # assert not user_campaign.select(stub_pytest_func, path="/bad_path", file_name="collect_mode_ok.py", 
    #                                 test_name="collect_mode_test_ok", marks="mark_ok")
    # assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode", 
    #                                 file_name="bad_file_name.py", 
    #                                 test_name="collect_mode_test_ok", marks="")
    # assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode", 
    #                                 file_name="collect_mode_ok.py", 
    #                                 test_name="bad_test_name", marks="")

    #UnitTestComment("AutUserCampaign: test that include criterias are sufficient")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode", 
                                file_name="collect_mode_ok.py", 
                                test_name="collect_mode_test_ok")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode/collect_mode_child", 
                                file_name="collect_mode_ok.py", 
                                test_name="collect_mode_test_ok")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode/collect_mode_skipped1", 
                                file_name="collect_mode_ok.py", 
                                test_name="collect_mode_test_ok")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode", 
                                file_name="collect_mode_ok.py", 
                                test_name="collect_mode_xfailed1")

    #UnitTestComment("AutUserCampaign: test that exclude criterias are applied")
    assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode", 
                                    file_name="collect_mode_ok.py", 
                                    test_name="collect_mode_test_ok", 
                                    marks={"collect_mode_excluded"})

    #UnitTestComment("AutUserCampaign: test that skip/xfail criterias are applied")
    stub_pytest_func.set_markers("")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode/collect_mode_skipped_wildcard", 
                                file_name="collect_mode_ok.py", 
                                test_name="collect_mode_test_ok")
    assert "skip" in stub_pytest_func.markers
    stub_pytest_func.set_markers("")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/collect_mode", 
                                file_name="collect_mode_ok.py", 
                                test_name="collect_mode_xfailed1")
    assert "xfail" in stub_pytest_func.markers


def test_UT_AttUserCampaign_ComplexCriteriasSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that criterias modifiers are applied")
    assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                                    file_name="test_file_name_ok.py", test_name="test_name_ok", 
                                    marks={"complex_criterias_not_included"})
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                                    file_name="test_file_name_ok.py", test_name="test_name_ok", 
                                    marks=set())
    assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias/complex_criterias_not_included", 
                                    file_name="test_file_name_ok.py", test_name="test_name_ok", 
                                    marks=set())
    assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias/complex_criterias_not_included/child", 
                                    file_name="test_file_name_ok.py", test_name="test_name_ok", 
                                    marks=set())

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"complex_criterias_xor_xfail1", "complex_criterias_not_xfail"})
    assert "xfail" in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"complex_criterias_xor_xfail1", "complex_criterias_xor_xfail2", "complex_criterias_not_xfail"})
    assert "xfail" not in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"complex_criterias_not_xfail"})
    assert "xfail" not in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"complex_criterias_and"})
    assert "xfail" not in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/complex_criterias", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"complex_criterias_and_not"})
    assert "xfail" in stub_pytest_func.markers


def test_UT_AttUserCampaign_AddMarksSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that include actions are applied to included items")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/add_actions", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks=set())
    assert "add_actions_include_mark_to_add" in stub_pytest_func.markers
    assert "add_actions_xfail_mark_to_add" not in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/add_actions", 
                         file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"add_actions_xfail"})
    assert "add_actions_include_mark_to_add" in stub_pytest_func.markers
    assert "add_actions_xfail_mark_to_add" in stub_pytest_func.markers


def test_UT_AttUserCampaign_MultiCollectSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that multi collect criterias are all applied selects")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/multi_collect", 
                                file_name="test_file_name_ok.py", test_name="test_name_ok", 
                                marks={"multi_collect_include_action1"})
    assert "multi_collect_include_mark1" in stub_pytest_func.markers
    assert "multi_collect_include_mark2" not in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/multi_collect", 
                        file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"multi_collect_include_action2"})
    assert "multi_collect_include_mark1" not in stub_pytest_func.markers
    assert "multi_collect_include_mark2" in stub_pytest_func.markers

    stub_pytest_func.set_markers("")
    user_campaign.select(stub_pytest_func, path=campaign_root_path + "/multi_collect", 
                        file_name="test_file_name_ok.py", test_name="test_name_ok", 
                         marks={"multi_collect_include_action1", "multi_collect_include_action2"})
    assert "multi_collect_include_mark1" in stub_pytest_func.markers
    assert "multi_collect_include_mark2" in stub_pytest_func.markers


def test_UT_AttUserCampaign_DefaultCriteriasSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that default file/test names are correctly set")
    assert not user_campaign.select(stub_pytest_func, path=campaign_root_path + "", 
                                    file_name="bad_file_name.py", 
                                    test_name="test_name_ok", marks={"default_criterias_included"})
    assert not user_campaign.select(stub_pytest_func, path=campaign_root_path, 
                                    file_name="test_file_name_ok.py", 
                                    test_name="bad_test_name", marks={"default_criterias_included"})
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path, file_name="test_file_name_ok.py", 
                                test_name="test_name_ok", marks={"default_criterias_included"})
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path + "/child", file_name="test_file_name_ok.py", 
                                test_name="test_name_ok", marks={"default_criterias_included"})



def test_UT_AttUserCampaign_DefaultIncludeSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that default include collect is correctly set")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path, file_name="test_file_name_ok.py", 
                                test_name="test_name_ok", marks=set())
    assert "default_include" in stub_pytest_func.markers
    
    stub_pytest_func.set_markers("")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path, file_name="test_file_name_ok.py", 
                                test_name="test_name_ok", marks={"default_include_skipped"})
    assert "default_include" in stub_pytest_func.markers
    assert "skip" in stub_pytest_func.markers


def test_UT_AttUserCampaign_GlobalFilterSelect(trace_test_start, test_campaign, campaign_root_path):
    user_campaign = test_campaign.AttCampaignUserCampaign()
    stub_pytest_func = AutStubPytestFunc()

    #UnitTestComment("AutUserCampaign: test that global filter is applied")
    assert user_campaign.select(stub_pytest_func, path=campaign_root_path, file_name="test_file_name_ok.py", 
                                test_name="test_name_ok", marks={"global_filter_skip"})
    assert "default_include" in stub_pytest_func.markers
    assert "GlobalFilter" in stub_pytest_func.markers
    assert "skip" in stub_pytest_func.markers

# TODO test campagne sans root path ou dans empty campaign