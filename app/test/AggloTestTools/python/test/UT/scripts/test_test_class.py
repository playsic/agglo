############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


from collections import Counter

from campaign.test_class import AttTestClass

from agglo_tk.selector import AtkComplexSelector
from agglo_tk.selector import AtkCriteria
from agglo_tk.tree.tree_bool import Or

from agglo_tt.fixtures import trace_test_start



def test_UT_AttTestClass_Ignore(trace_test_start):
    test_class = AttTestClass("TestClass")

    test_class.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include))
    test_class.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude))

    #UnitTestComment("AutTestClass::ignore(): test that exclude selector is not evaluated if include doesn't match")
    result = test_class.ignore(include=False, exclude=True)
    assert not result.include
    assert not result.exclude

    #UnitTestComment("AutTestClass::ignore(): test exclude selector is evaluated if include matches")
    result = test_class.ignore(include=True, exclude=True)
    assert result.include
    assert result.exclude

    #UnitTestComment("AutTestClass::ignore(): test that exclude selector is always evaluated on global match")
    result = test_class.ignore(True, include=False, exclude=True)
    assert not result.include
    assert result.exclude

    #UnitTestComment("AutTestClass::ignore(): test default values in case of insufficient datas")
    result = test_class.ignore()
    assert result.include
    assert not result.exclude
    result = test_class.ignore(include=True)
    assert result.include
    assert not result.exclude


def test_UT_AttTestClass_Select(trace_test_start):
    test_class = AttTestClass("TestClass")

    test_class.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include))
    test_class.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude))
    test_class.selectors["skip"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.skip))
    test_class.selectors["xfail"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.xfail))

    #UnitTestComment("AutTestClass::select(): test that exclude/skip/xfail selectors are not evaluated if include doesn't match")
    result = test_class.select(include=False, exclude=True, skip=True, xfail=True)
    assert not result.include
    assert not result.exclude
    assert not result.skip
    assert not result.xfail

    #UnitTestComment("AutTestClass::select(): test that skip/xfail selectors are not evaluated if exclude matches")
    result = test_class.select(include=True, exclude=True, skip=True, xfail=True)
    assert result.include
    assert result.exclude
    assert not result.skip
    assert not result.xfail

    #UnitTestComment("AutTestClass::select(): test exclude selector is evaluated if include matches")
    result = test_class.select(include=True, exclude=False, skip=True, xfail=True)
    assert result.include
    assert not result.exclude
    assert result.skip
    assert result.xfail

    #UnitTestComment("AutTestClass::select(): test that exclude/skip/xfail selectors are always evaluated on global match")
    result = test_class.select(True, include=False, exclude=True, skip=True, xfail=True)
    assert not result.include
    assert result.exclude
    assert result.skip
    assert result.xfail

    #UnitTestComment("AutTestClass::select(): test default values in case of insufficient datas")
    result = test_class.select()
    assert not result.include
    assert not result.exclude
    assert not result.skip
    assert not result.xfail
    result = test_class.select(include=True)
    assert result.include
    assert not result.exclude
    assert not result.skip
    assert not result.xfail
    result = test_class.select(include=True, exclude=False)
    assert result.include
    assert not result.exclude
    assert not result.skip
    assert not result.xfail
    result = test_class.select(include=True, exclude=False, skip=True)
    assert result.include
    assert not result.exclude
    assert result.skip
    assert not result.xfail


def test_UT_AttTestClass_AddAction(trace_test_start):
    test_class = AttTestClass("TestClass")
    include_selector = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include))
    exclude_selector = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude))
    skip_selector = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.skip))

    test_class.selectors["include"] = include_selector
    test_class.selectors["exclude"] = exclude_selector
    test_class.selectors["skip"] = skip_selector

    #UnitTestComment("AutTestClass::add_action(): test that action is applied on match")
    test_class.add_action(include_selector.criteria, lambda test_class: test_class.action_marks.add("include_mark"))
    test_class.add_action(exclude_selector.criteria, lambda test_class: test_class.action_marks.add("exclude_mark"))
    test_class.select(include=True, exclude=False)
    assert "include_mark" in test_class.action_marks
    assert "exclude_mark" not in test_class.action_marks

    #UnitTestComment("AutTestClass::add_action(): test that several action can applied")
    test_class.add_action(skip_selector.criteria, lambda test_class: test_class.action_marks.add("skip_mark1"))
    test_class.add_action(skip_selector.criteria, lambda test_class: test_class.action_marks.add("skip_mark2"))
    test_class.select(include=True, skip=True)
    assert "skip_mark1" in test_class.action_marks
    assert "skip_mark2" in test_class.action_marks

    #UnitTestComment("AutTestClass::add_action(): test that all actions are evaluated during select")
    xfail_criteria1 = AtkCriteria(match=lambda crit, data: data.xfail1)
    xfail_criteria2 = AtkCriteria(match=lambda crit, data: data.xfail2)
    xfail_criteria = Or(xfail_criteria1, xfail_criteria2)
    test_class.selectors["xfail"] = AtkComplexSelector(xfail_criteria)
    test_class.add_action(xfail_criteria1, lambda test_class: test_class.action_marks.add("xfail_mark1"))
    test_class.add_action(xfail_criteria2, lambda test_class: test_class.action_marks.add("xfail_mark2"))
    test_class.add_action(xfail_criteria, lambda test_class: test_class.action_marks.add("xfail_mark3"))
    test_class.select(include=True, xfail1=True, xfail2=True)
    assert "xfail_mark1" in test_class.action_marks
    assert "xfail_mark2" in test_class.action_marks
    assert "xfail_mark3" in test_class.action_marks
    test_class.select(include=True, xfail1=True, xfail2=False)
    assert "xfail_mark1" in test_class.action_marks
    assert "xfail_mark2" not in test_class.action_marks
    assert "xfail_mark3" in test_class.action_marks


def test_UT_AttTestClass_AddMarkAction(trace_test_start):
    test_class = AttTestClass("TestClass")
    include_selector = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include))

    test_class.selectors["include"] = include_selector

    #UnitTestComment("AutTestClass::add_marks(): test that several marks can be added by one action")
    test_class.add_action(include_selector.criteria, lambda test_class: test_class._add_marks("include_mark1 , include_mark2,include_mark3"))
    test_class.select(include=True)
    assert Counter(["include_mark1", "include_mark2", "include_mark3"]) == Counter(test_class.action_marks)

    #UnitTestComment("AutTestClass::add_marks(): test that action marks are reset after each select")
    test_class.select(include=False)
    assert not test_class.action_marks
