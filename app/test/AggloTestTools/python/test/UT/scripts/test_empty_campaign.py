############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from campaign.campaign import AttCampaign

from agglo_tt.fixtures import trace_test_start


def test_UT_AttEmptyCampaign_Constructor(trace_test_start):
    empty_campaign = AttCampaign("EmptyCampaign")

    assert empty_campaign.name == "EmptyCampaign"
    assert not empty_campaign.test_classes
    assert empty_campaign.global_filter is None


def test_UT_AttEmptyCampaign_IgnoreCollect(trace_test_start):
    empty_campaign = AttCampaign("EmptyCampaign")
    campaign_file_path = os.path(__file__)

    #UnitTestComment("AutEmptyCampaign: test that an empty campaign accepts tests in campaign path")
    assert empty_campaign.ignore_collect(path="not_in_path")
    assert empty_campaign.ignore_collect(path=campaign_file_path + "/../not_in_path")
    assert not empty_campaign.ignore_collect(path=campaign_file_path)
    assert not empty_campaign.ignore_collect(path=campaign_file_path + "/child")


def test_UT_AttEmptyCampaign_Select(trace_test_start):
    empty_campaign = AttCampaign("EmptyCampaign")
    campaign_file_path = os.path(__file__)

    #UnitTestComment("AutEmptyCampaign: test that tests matching default values are accepted")
    assert empty_campaign.select(path=campaign_file_path, file_name="test_file.py", test_name="test_UT_accepted")
    assert empty_campaign.select(path=campaign_file_path + "/child", file_name="test_file.py", test_name="test_UT_accepted")

    #UnitTestComment("AutEmptyCampaign: test that tests with missing datas are refused")
    assert not empty_campaign.select(path=campaign_file_path)

    #UnitTestComment("AutEmptyCampaign: test that tests with wrong datas are refused")
    assert empty_campaign.select(path="not_in_path", file_name="test_file.py", test_name="test_UT_accepted")
    assert empty_campaign.select(path=campaign_file_path, file_name="testfile.py", test_name="test_UT_accepted")
    assert empty_campaign.select(path=campaign_file_path, file_name="test_file.py", test_name="testUT_refused")


def test_UT_AttEmptyCampaign_CampaignPath(trace_test_start, test_campaign):
    empty_campaign = test_campaign.AutEmptyCampaignNoPathDefined
    campaign_file_path = os.path(__file__)

    #UnitTestComment("AutEmptyCampaign: test that when scripts path is not provided, xml campaign's path is used")
    assert not empty_campaign.ignore_collect(path="campaign_path")
    assert empty_campaign.ignore_collect(path=campaign_file_path)
    assert empty_campaign.select(path="campaign_path", file_name="test_file.py", test_name="test_UT_accepted")
    assert not empty_campaign.select(path=campaign_file_path, file_name="test_file.py", test_name="test_UT_accepted")
