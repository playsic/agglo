############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from functools import partial
from collections import Counter

from campaign.campaign import AttCampaign
from campaign.test_class import AttTestClass

from agglo_tk.selector import AtkComplexSelector
from agglo_tk.selector import AtkCriteria
from agglo_tk.tree.tree_bool import Or

from stubs import AttStubPytestFunc

from agglo_tt.fixtures import trace_test_start


def test_UT_AutCampaign_Constructor(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")

    #UnitTestComment("AutCampaign::AutCampaign(): test default properties values")
    assert test_campaign.name == "TestCampaign"
    assert test_campaign.global_filter is None


def test_UT_AutCampaign_BuiltIn(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    test_class = AttTestClass("TestClass")

    #UnitTestComment("AutCampaign: test __getitem__ builtin")
    test_campaign.add(test_class)
    assert test_campaign["TestClass"] is test_class

    #UnitTestComment("AutCampaign: test __contains__ builtin")
    assert "TestClass" in test_campaign
    assert test_class in test_campaign


def test_UT_AutCampaign_Properties(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    global_filter = AttTestClass("GlobaFilter")

    #UnitTestComment("AutCampaign: test global_filter property")
    test_campaign.global_filter = global_filter
    assert test_campaign.global_filter is global_filter

    # TODO risque de doublon de nom si on part la dessus
    #UnitTestComment("AutCampaign: test global filter is accessible through built in")
    # assert "GlobaFilter" in test_campaign
    # assert global_filter in test_campaign


def test_UT_AutCampaign_IgnoreCollect(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    global_filter = AttTestClass("GlobalFilter")
    test_class = AttTestClass("TestClass")

    test_campaign.add(test_class)
    test_class.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include))
    test_class.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude))

    #UnitTestComment("AutCampaign::ignore_collect(): test that exclude selector is not evaluated if include doesn't match")
    assert test_campaign.ignore_collect(include=False, exclude=True)

    #UnitTestComment("AutCampaign::ignore_collect(): test exclude selector is evaluated if include matches")
    assert test_campaign.ignore_collect(include=True, exclude=True)
    
    #UnitTestComment("AutCampaign::ignore_collect(): test default values in case of insufficient datas")
    assert not test_campaign.ignore_collect()
    assert not test_campaign.ignore_collect(include=True)
    assert test_campaign.ignore_collect(include=False)
    assert test_campaign.ignore_collect(exclude=True)

    #UnitTestComment("AutCampaign::ignore_collect(): test that global filter is stronger than test classes")
    test_campaign.global_filter = global_filter
    global_filter.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.global_include))
    global_filter.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.global_exclude))
    assert test_campaign.ignore_collect(include=True, global_exclude=True)
    assert not test_campaign.ignore_collect(include=True, exclude=True, global_include=True)


def test_UT_AutCampaign_Select(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    test_class1 = AttTestClass("TestClass1")
    test_class2 = AttTestClass("TestClass2")
    global_filter = AttTestClass("GlobalFilter")
    stub_pytest_func = AttStubPytestFunc()

    test_campaign.add(test_class1)
    test_class1.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include1))
    test_class1.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude1))
    test_class1.selectors["skip"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.skip1))
    test_campaign.add(test_class2)
    test_class2.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include2))
    test_class2.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude2))
    test_class2.selectors["xfail"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.xfail2))

    #UnitTestComment("AutCampaign::select(): test that an item is included if at least one test class includes it)
    assert test_campaign.select(stub_pytest_func, include1=False, include2=True)
    assert not test_campaign.select(stub_pytest_func, include1=False, include2=False)

    #UnitTestComment("AutCampaign::select(): test that an item is not excluded if at least one test class includes it)
    assert test_campaign.select(stub_pytest_func, include1=False, exclude1=True, include2=True)
    assert not test_campaign.select(stub_pytest_func, include1=True, exclude1=True)

    #UnitTestComment("AutCampaign::select(): test that an item is selected even if skipped by a test class)
    assert test_campaign.select(stub_pytest_func, include1=True, skip1=True)
    assert test_campaign.select(stub_pytest_func, include1=True, skip1=True, include2=True)

    #UnitTestComment("AutCampaign::select(): test that an item is selected even if xfailed by a test class)
    assert test_campaign.select(stub_pytest_func, include2=True, xfail2=True)
    assert test_campaign.select(stub_pytest_func, include1=True, include2=True, xfail2=True)
    
    #UnitTestComment("AutCampaign::select(): test default values in case of insufficient datas")
    assert not test_campaign.select(stub_pytest_func)
    assert test_campaign.select(stub_pytest_func, include1=True)
    assert not test_campaign.select(stub_pytest_func, include1=False)
    assert not test_campaign.select(stub_pytest_func, exclude2=True)
    assert not test_campaign.select(stub_pytest_func, skipped1=True)
    assert not test_campaign.select(stub_pytest_func, xfailed2=True)

    #UnitTestComment("AutCampaign::select(): test that global filter is stronger than test classes")
    test_campaign.global_filter = global_filter
    global_filter.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.global_include))
    global_filter.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.global_exclude))
    assert test_campaign.select(stub_pytest_func, include1=True, global_include=False)
    assert not test_campaign.select(stub_pytest_func, include1=True, global_exclude=True)


def test_UT_AutCampaign_SelectMark(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    test_class1 = AttTestClass("TestClass1")
    test_class2 = AttTestClass("TestClass2")
    global_filter = AttTestClass("GlobalFilter")
    stub_pytest_func = AttStubPytestFunc("mark1")

    test_campaign.add(test_class1)
    test_class1.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include1))
    test_class1.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude1))
    test_class1.selectors["skip"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.skip1))
    test_campaign.add(test_class2)
    test_class2.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.include2))
    test_class2.selectors["exclude"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.exclude2))
    test_class2.selectors["skip"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.skip2))
    test_class2.selectors["xfail"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.xfail2))

    #UnitTestComment("AutCampaign::select(): test that select doesn't modify previous func markers)
    test_campaign.select(stub_pytest_func, include1=False, include2=False)
    assert "mark1" in stub_pytest_func.markers
    assert len(stub_pytest_func.markers) == 1
    test_campaign.select(stub_pytest_func, include1=True)
    assert "mark1" in stub_pytest_func.markers
 
    #UnitTestComment("AutCampaign::select(): test that func item is marked with selecting test class's name and test campaign's name)
    stub_pytest_func.set_markers("mark1")
    test_campaign.select(stub_pytest_func, include1=True, include2=False)
    assert "TestClass1" in stub_pytest_func.markers
    assert "TestCampaign" in stub_pytest_func.markers
    assert len(stub_pytest_func.markers) == 3
    stub_pytest_func.set_markers("mark1")
    test_campaign.select(stub_pytest_func, include1=True, include2=True)
    assert "TestClass1" in stub_pytest_func.markers
    assert "TestClass2" in stub_pytest_func.markers
    assert len(stub_pytest_func.markers) == 4

    #UnitTestComment("AutCampaign::select(): test that skipped item is marked as skip and also with selecting test class's name)
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include1=True, skip1=True)
    assert "TestClass1" in stub_pytest_func.markers
    assert "skip" in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that skip mark is added only once)
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include1=True, skip1=True, include2=True, skip2=True)
    assert "TestClass1" in stub_pytest_func.markers
    assert "TestClass2" in stub_pytest_func.markers
    assert "skip" in stub_pytest_func.markers
    assert len(stub_pytest_func.markers) == 4

    #UnitTestComment("AutCampaign::select(): test that item is not marked as skip if selected by another test class. It is marked by all classes'names)
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include1=True, skip1=True, include2=True)
    assert "TestClass2" in stub_pytest_func.markers
    assert "TestClass1" in stub_pytest_func.markers
    assert "skip" not in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that xfailed item is marked as xfail and also with selecting test class's name)
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include2=True, xfail2=True)
    assert "TestClass2" in stub_pytest_func.markers
    assert "xfail" in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that item is not marked as xfail if selected by another test class. It is marked by all classes'names)
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include1=True, include2=True, xfail2=True)
    assert "TestClass2" in stub_pytest_func.markers
    assert "TestClass1" in stub_pytest_func.markers
    assert "xfail" not in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that skipped/xfailed item is marked as skipped and xfail)
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include2=True, skip2=True, xfail2=True)
    assert "skip" in stub_pytest_func.markers
    assert "xfail" in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that global filter is stronger than test classes")
    test_campaign.global_filter = global_filter
    global_filter.selectors["include"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.global_include))
    global_filter.selectors["skip"] = AtkComplexSelector(AtkCriteria(match=lambda crit, data: data.global_skip))
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include1=False, global_include=True)
    assert "GlobalFilter" in stub_pytest_func.markers
    assert len(stub_pytest_func.markers) == 2
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include1=True, global_include=True, global_skip=True)
    assert "GlobalFilter" in stub_pytest_func.markers
    assert "TestClass1" in stub_pytest_func.markers
    assert "skip" in stub_pytest_func.markers


def test_UT_AutCampaign_ActionMark(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    test_class = AttTestClass("TestClass1")
    stub_pytest_func = AttStubPytestFunc("mark1")
    include_criteria = AtkCriteria(include=None, match=lambda crit, data: data.include)
    skip_criteria = AtkCriteria(skip=None, match=lambda crit, data: data.skip)

    test_campaign.add(test_class)
    test_class.selectors["include"] = AtkComplexSelector(include_criteria)
    test_class.selectors["skip"] = AtkComplexSelector(skip_criteria)
    test_class.add_action(include_criteria, lambda test_class: test_class._add_marks("include_mark"))
    test_class.add_action(include_criteria, lambda test_class: test_class._add_marks("skip_mark"))

    #UnitTestComment("AutCampaign::select(): test that select add action marks to test item
    test_campaign.select(stub_pytest_func, include=True)
    assert "include_mark" in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that select add action marks to test item
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include=True, skip=True)
    assert "include_mark" in stub_pytest_func.markers
    assert "skip_mark" in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that add action is applied even on not evaluated criteria
    sub_criteria1 = AtkCriteria(sub_criteria1=None, match=lambda crit, data: data.sub_criteria1)
    sub_criteria2 = AtkCriteria(sub_criteria2=None, match=lambda crit, data: data.sub_criteria2)
    test_class.selectors["include"].criteria = Or(sub_criteria1, sub_criteria2)
    test_class.add_action(sub_criteria2, lambda test_class: test_class._add_marks("sub_criteria2_mark"))
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, sub_criteria1=True, sub_criteria2=True)
    assert "sub_criteria2_mark" in stub_pytest_func.markers
    test_class.selectors["include"].criteria = Or(sub_criteria2, sub_criteria1)
    # TODO ce serait mieux que l'action soit associe au critere plutot qu'au node
    test_class.add_action(sub_criteria2, lambda test_class: test_class._add_marks("sub_criteria2_mark"))
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, sub_criteria1=True, sub_criteria2=True)
    assert "sub_criteria2_mark" in stub_pytest_func.markers


def test_UT_AutCampaign_SelectedTestClassFilters(trace_test_start):
    test_campaign = AttCampaign("TestCampaign")
    test_class1 = AttTestClass("TestClass1")
    test_class2 = AttTestClass("TestClass2")
    global_filter = AttTestClass("GlobalFilter")
    stub_pytest_func = AttStubPytestFunc()
    include_criteria = AtkCriteria(include=None, match=lambda crit, data: data.include)

    test_campaign.add(test_class1)
    test_class1.selectors["include"] = AtkComplexSelector(AtkCriteria(include1=None, match=lambda crit, data: data.include1))
    test_campaign.add(test_class2)
    test_class2.selectors["include"] = AtkComplexSelector(AtkCriteria(include2=None, match=lambda crit, data: "TestClass1" in data.markers))

    #UnitTestComment("AutCampaign::select(): test that 2nd class can use markers added by previous test class
    test_campaign.select(stub_pytest_func, include1=True)
    assert "TestClass1" in stub_pytest_func.markers
    assert "TestClass2" in stub_pytest_func.markers

    #UnitTestComment("AutCampaign::select(): test that global filter can use markers added by test classes
    test_campaign.global_filter = global_filter
    global_filter.selectors["include"] = AtkComplexSelector(AtkCriteria(global_include=None, match=lambda crit, data: "TestClass2" in data.markers))
    stub_pytest_func.set_markers("")
    test_campaign.select(stub_pytest_func, include2=True)
    assert "GlobalFilter" in stub_pytest_func.markers
    assert "TestClass2" in stub_pytest_func.markers
    assert "TestClass1" not in stub_pytest_func.markers

