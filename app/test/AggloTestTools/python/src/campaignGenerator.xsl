<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:cli="http://alexis.royer.free.fr/CLI"
                              xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
<xsl:output method="text" encoding="iso-8859-1"/>

<xsl:include href="../../../../../doc/licence/aggInsertLicence.xsl"/>
<xsl:include href="../../../../development/AggloToolKit/xsl/src/atkLayout.xsl"/>

<xsl:param name="asHookFile"><!--<xsl:message terminate="yes">Error! Please set asHookFile param</xsl:message>--></xsl:param>
<xsl:param name="asCampaignPath"><!--<xsl:message terminate="yes">Error! Please set asCampaignPath param</xsl:message>--></xsl:param>

<xsl:variable name="gsGlobalFilter"><xsl:text>GlobalFilter</xsl:text></xsl:variable>
<xsl:variable name="gsSelectorInclude"><xsl:text>include</xsl:text></xsl:variable>
<xsl:variable name="gsSelectorExclude"><xsl:text>exclude</xsl:text></xsl:variable>
<xsl:variable name="gsSelectorSkip"><xsl:text>skip</xsl:text></xsl:variable>
<xsl:variable name="gsSelectorXFail"><xsl:text>xfail</xsl:text></xsl:variable>



<xsl:template match="/test_campaign">

    <!-- Insert copyright -->
    <xsl:call-template name="insertCopyRight">
      <xsl:with-param name="aeTypeTarget" select="$gePython"/>
    </xsl:call-template>

    <xsl:choose>
        <xsl:when test="$asHookFile='pytest.ini'">
            <xsl:call-template name="createPytestIni"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="createConftest"/>
        </xsl:otherwise>
    </xsl:choose>

	
</xsl:template>


<xsl:template name="createPytestIni">

    <xsl:text>[pytest]</xsl:text><xsl:value-of select="$gsEndl"/>

    <!-- Init lookup path with root_path -->
    <xsl:text># Look for tests only in campaign root path</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:text>testpaths = </xsl:text><xsl:call-template name="getRootPath"/>
    <xsl:value-of select="$gsEndl"/>
    
    <!-- Init test suites names with included files -->
    <xsl:text>python_files = test_*</xsl:text>
    <xsl:for-each select="//criteria[@type='file_name' and ../@mode='include']">
        <xsl:text> </xsl:text><xsl:value-of select="./@value"/>
    </xsl:for-each>
    <xsl:value-of select="$gsEndl"/>
    
    <!-- Init test cases names with included test cases -->
    <xsl:text>python_functions = test_*</xsl:text>
    <xsl:for-each select="//criteria[@type='test_name' and ../@mode=$gsSelectorInclude]">
        <xsl:text> </xsl:text><xsl:value-of select="./@value"/>
    </xsl:for-each>
    <xsl:value-of select="$gsEndl"/>

</xsl:template>


<xsl:template name="createConftest">

    <xsl:variable name="sCampaignClass">
        <xsl:text>AutCampaign</xsl:text><xsl:value-of select="./@name"/>
    </xsl:variable>
    <xsl:variable name="nsRoot" select="/test_campaign"/>
    <xsl:variable name="nsTestClasses" select="/test_campaign/test_class|/test_campaign[/test_campaign/collect]"/>

    <!-- Insert import -->
    <xsl:value-of select="$gsEndl"/>
    <xsl:if test=".//action">
        <xsl:text>from functools import partial</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:if>
    <xsl:value-of select="$gsEndl"/>
    <xsl:text>import pytest</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>
    <xsl:text>from agglo_tk.selector import AtkComplexSelector</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:if test=".//criteria/@type='path'">
        <xsl:text>from agglo_tk.selector import AtkCriteriaPath</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:if>
    <xsl:if test=".//criteria/@type='mark'">
        <xsl:text>from agglo_tk.selector import AtkCriteria</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:if>
    <xsl:text>from agglo_tk.selector import AtkCriteriaRe</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:text>from agglo_tk.tree.tree_bool import And</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:text>from agglo_tk.tree.tree_bool import Or</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:text>from agglo_tk.tree.tree_bool import Not</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:if test=".//criteria[@type='node' and @value='xor']">
        <xsl:text>from agglo_tk.tree.tree_bool import Xor</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:if>
    <xsl:value-of select="$gsEndl"/>
    <xsl:text>from campaign.campaign import AutCampaign</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:text>from campaign.test_class import AutTestClass</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>

    <!-- Create pytest configuration hook -->
    <xsl:text>def pytest_configure(config):</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$STR_Indent"/><xsl:text>config.campaign = </xsl:text><xsl:value-of select="$sCampaignClass"/><xsl:text>()</xsl:text>
    <xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>

    <!-- Create campaign class -->
    <xsl:value-of select="$gsEndl"/>
    <xsl:text>class </xsl:text><xsl:value-of select="$sCampaignClass"/><xsl:text>(AutCampaign):</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>

    <!-- Create pytest configuration hook -->
    <xsl:value-of select="$STR_Indent"/><xsl:text>def __init__(self):</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:call-template name="Indent">
        <xsl:with-param name="NbIndent" select="2"/>
    </xsl:call-template>
    <xsl:text>AutCampaign.__init__(self, "</xsl:text><xsl:value-of select="./@name"/><xsl:text>")</xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$gsEndl"/>

    <!-- For each test class, including root -->
    <xsl:for-each select="$nsTestClasses">
        <!-- Create test class -->
        <xsl:call-template name="createTestClass"/>
    </xsl:for-each>

</xsl:template>


<xsl:template name="createTestClass">

    <xsl:variable name="sIndent">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bIsGlobalFilter">
        <xsl:call-template name="isGlobalFilter"/>
    </xsl:variable>
    <xsl:variable name="sTestClassName">
        <xsl:call-template name="getTestClassName"/>
    </xsl:variable>
    <xsl:variable name="sTestClassVarName">
        <xsl:call-template name="getTestClassName">
            <xsl:with-param name="abVarName" select="'true'"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="nsTestClasses" select="/test_campaign/test_class|/test_campaign[/test_campaign/collect]"/>

    <!-- Add a comment before starting AutTestClass instantiation -->
    <xsl:if test="count($nsTestClasses) > 1">
        <xsl:value-of select="$sIndent"/>
        <xsl:text># !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</xsl:text>
        <xsl:value-of select="$gsEndl"/>
    </xsl:if>
    <xsl:value-of select="$sIndent"/><xsl:text># Create </xsl:text>
    <xsl:value-of select="$sTestClassName"/><xsl:text> test class </xsl:text><xsl:value-of select="$gsEndl"/>
    <xsl:if test="count($nsTestClasses) > 1">
        <xsl:value-of select="$sIndent"/>
        <xsl:text># !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</xsl:text>
        <xsl:value-of select="$gsEndl"/>
    </xsl:if>

    <!-- Instantiate a new AutTestClass -->
    <xsl:value-of select="$sIndent"/><xsl:value-of select="$sTestClassVarName"/>
    <xsl:text> = AutTestClass("</xsl:text><xsl:value-of select="$sTestClassName"/><xsl:text>")</xsl:text>
    <xsl:value-of select="$gsEndl"/>
    
    <!-- Instantiate a selector for each collect mode, and add it to AutTestClass instance -->
    <!-- Include collect is defined for global filter only if there are user defined include -->
    <!-- For normal test classes, there are restriction criteria for include to definde in any cases -->
    <xsl:if test="($bIsGlobalFilter = 'false') or (./collect[@mode = $gsSelectorInclude])">
        <xsl:call-template name="createIncludeCollectSelector"/>
    </xsl:if>
        <xsl:call-template name="createCollectModeSelector">
        <xsl:with-param name="asCollectMode" select="$gsSelectorExclude"/>
    </xsl:call-template>
    <xsl:call-template name="createCollectModeSelector">
        <xsl:with-param name="asCollectMode" select="$gsSelectorSkip"/>
    </xsl:call-template>
    <xsl:call-template name="createCollectModeSelector">
        <xsl:with-param name="asCollectMode" select="$gsSelectorXFail"/>
    </xsl:call-template>
    <xsl:value-of select="$gsEndl"/>

    <!-- Add AutTestClass instance to campaign -->
    <xsl:choose>
        <xsl:when test="$bIsGlobalFilter = 'false'">
            <xsl:value-of select="$sIndent"/><xsl:text># Add </xsl:text>
            <xsl:value-of select="$sTestClassName"/><xsl:text> test class to campaign</xsl:text><xsl:value-of select="$gsEndl"/>
            <xsl:value-of select="$sIndent"/><xsl:text>self.add(</xsl:text>
            <xsl:value-of select="$sTestClassVarName"/><xsl:text>)</xsl:text><xsl:value-of select="$gsEndl"/>
            <xsl:value-of select="$gsEndl"/>
        </xsl:when>

        <xsl:otherwise>
            <xsl:value-of select="$sIndent"/><xsl:text># Define campaign's global filter</xsl:text><xsl:value-of select="$gsEndl"/>
            <xsl:value-of select="$sIndent"/><xsl:text>self.global_filter = </xsl:text>
            <xsl:value-of select="$sTestClassVarName"/><xsl:value-of select="$gsEndl"/>
            <xsl:value-of select="$gsEndl"/>
        </xsl:otherwise>
    </xsl:choose>
        
</xsl:template>


<xsl:template name="createIncludeCollectSelector">

    <xsl:variable name="sIndent">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bIsGlobalFilter">
        <xsl:call-template name="isGlobalFilter"/>
    </xsl:variable>
    <xsl:variable name="sUserDefinedIncludeMode">
        <xsl:value-of select="$gsSelectorInclude"/><xsl:text>_user</xsl:text>
    </xsl:variable>
    <xsl:variable name="sUserDefinedCriteriaVarName">
        <xsl:call-template name="getCollectName">
            <xsl:with-param name="ansOwner" select="."/>
            <xsl:with-param name="asCollectMode" select="$sUserDefinedIncludeMode"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="sIncludeCriteriaVarName">
        <xsl:call-template name="getCollectName">
            <xsl:with-param name="ansOwner" select="."/>
            <xsl:with-param name="asCollectMode" select="$gsSelectorInclude"/>
        </xsl:call-template>
    </xsl:variable>

    <!-- If test class is not global filter -->
    <xsl:if test="$bIsGlobalFilter = 'false'">
        <!-- Create criterias to restrain include to default values for file_name and test_name, if not provided by user -->
        <xsl:call-template name="createRestrictionCriteria">
            <xsl:with-param name="asCriteriaType" select="'file_name'"/>
        </xsl:call-template>
        <xsl:call-template name="createRestrictionCriteria">
            <xsl:with-param name="asCriteriaType" select="'test_name'"/>
        </xsl:call-template>
    </xsl:if>

    <!-- Create criterias for each include collect -->
    <xsl:call-template name="createCollectCriterias">
        <xsl:with-param name="asCollectMode" select="$gsSelectorInclude"/>
    </xsl:call-template>

    <xsl:if test="$bIsGlobalFilter = 'false'">
        <!-- Add a comment before gathering of all include criterias -->
        <xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$sIndent"/><xsl:text># Gather all </xsl:text><xsl:value-of select="$gsSelectorInclude"/>
        <xsl:text> criterias in a single criteria</xsl:text><xsl:value-of select="$gsEndl"/>

        <!-- If current test class has several include collects -->
        <xsl:if test="count(./collect[@mode=$gsSelectorInclude]) > 1">
            <!-- Gather all include collect in a single criteria -->
            <xsl:value-of select="$sIndent"/>
            <xsl:value-of select="$sUserDefinedCriteriaVarName"/><xsl:text> = </xsl:text>
            <xsl:call-template name="callUserDefinedCriteriaConstructor">
                <xsl:with-param name="asCollectMode" select="$gsSelectorInclude"/>
            </xsl:call-template>
            <xsl:value-of select="$gsEndl"/>
        </xsl:if>

        <!-- Instantiate intermediate criteria for include mode -->
        <xsl:value-of select="$sIndent"/><xsl:value-of select="$sIncludeCriteriaVarName"/><xsl:text> = </xsl:text>
        <xsl:if test="$bIsGlobalFilter = 'false'">
            <xsl:text>And(file_name_restriction_criteria, test_name_restriction_criteria</xsl:text>
        </xsl:if>
        <xsl:if test="./collect[@mode=$gsSelectorInclude]">
            <xsl:if test="$bIsGlobalFilter = 'false'">
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:value-of select="$sUserDefinedCriteriaVarName"/>
        </xsl:if>
        <xsl:if test="$bIsGlobalFilter = 'false'">
            <xsl:text>)</xsl:text>
        </xsl:if>
        <xsl:value-of select="$gsEndl"/>
    </xsl:if>

    <!-- Instantiate include selector and append it to test class selectors map -->
    <xsl:value-of select="$gsEndl"/>
    <xsl:call-template name="instantiateSelector">
        <xsl:with-param name="asCollectMode" select="$gsSelectorInclude"/>
    </xsl:call-template>

</xsl:template>


<xsl:template name="createCollectModeSelector">
    <xsl:param name="asCollectMode"/>

    <!-- If current test class collects test case in current mode-->
    <xsl:if test="./collect[@mode=$asCollectMode]">

        <xsl:variable name="sIndent">
            <xsl:call-template name="Indent">
                <xsl:with-param name="NbIndent" select="2"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="sTestClassName">
            <xsl:call-template name="getTestClassName"/>
        </xsl:variable>
        <xsl:variable name="sTestClassVarName">
            <xsl:call-template name="getTestClassName">
                <xsl:with-param name="abVarName" select="'true'"/>
            </xsl:call-template>
        </xsl:variable>

        <!-- Create criterias for each collect of given mode -->
        <xsl:call-template name="createCollectCriterias">
            <xsl:with-param name="asCollectMode" select="$asCollectMode"/>
        </xsl:call-template>

        <!-- Append newly created selector to test class selectors map -->
        <xsl:value-of select="$gsEndl"/>
        <xsl:call-template name="instantiateSelector">
            <xsl:with-param name="asCollectMode" select="$asCollectMode"/>
        </xsl:call-template>

    </xsl:if>
    
</xsl:template>


<xsl:template name="instantiateSelector">
    <xsl:param name="asCollectMode"/>

    <xsl:variable name="sIndent">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="sIncludeCriteriaVarName">
        <xsl:call-template name="getCollectName">
            <xsl:with-param name="ansOwner" select="."/>
            <xsl:with-param name="asCollectMode" select="$gsSelectorInclude"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="sTestClassName">
        <xsl:call-template name="getTestClassName"/>
    </xsl:variable>
    <xsl:variable name="sTestClassVarName">
        <xsl:call-template name="getTestClassName">
            <xsl:with-param name="abVarName" select="'true'"/>
        </xsl:call-template>
    </xsl:variable>

    <!-- Instantiate selector and append it to test class selectors map -->
    <xsl:value-of select="$sIndent"/><xsl:text># Create selector for </xsl:text><xsl:value-of select="$asCollectMode"/>
    <xsl:text> collect and assign it to </xsl:text><xsl:value-of select="$sTestClassName"/><xsl:text> test class</xsl:text>
    <xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sIndent"/><xsl:value-of select="$sTestClassVarName"/><xsl:text>.selectors</xsl:text>
    <xsl:text>["</xsl:text><xsl:value-of select="$asCollectMode"/><xsl:text>"] = AtkComplexSelector(</xsl:text>
    <xsl:choose>
        <xsl:when test="$asCollectMode = $gsSelectorInclude">
            <xsl:value-of select="$sIncludeCriteriaVarName"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="callUserDefinedCriteriaConstructor">
                <xsl:with-param name="asCollectMode" select="$asCollectMode"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:text>)</xsl:text><xsl:value-of select="$gsEndl"/>

    <!-- Add collect criterias -->
    <xsl:for-each select="./collect[@mode=$asCollectMode]">

        <xsl:variable name="sCollectVarName">
            <xsl:call-template name="getCollectName"/>
        </xsl:variable>

        <!-- Add current collect actions to collect criteria -->
        <!-- TODO gerer l'ajout d'une action au niveau de tous les criteres complexes (collect ET node) -->
        <xsl:call-template name="addActions">
            <xsl:with-param name="asTestClassVarName" select="$sTestClassVarName"/>
            <xsl:with-param name="asCriteriaVarName" select="$sCollectVarName"/>
        </xsl:call-template>
    </xsl:for-each>    

    <!-- If include test class or campaign has actions -->
    <xsl:if test="($asCollectMode = $gsSelectorInclude) and (./action or /test_campaign/action)">

        <!-- Add current test class actions to global include criteria -->
        <xsl:call-template name="addActions">
            <xsl:with-param name="asTestClassVarName" select="$sTestClassVarName"/>
            <xsl:with-param name="asCriteriaVarName" select="$sIncludeCriteriaVarName"/>
        </xsl:call-template>

        <!-- Add campaign actions to global include criteria -->
        <xsl:call-template name="addActions">
            <xsl:with-param name="asTestClassVarName" select="$sTestClassVarName"/>
            <xsl:with-param name="asCriteriaVarName" select="$sIncludeCriteriaVarName"/>
            <xsl:with-param name="ansLevel" select="/test_campaign"/>
        </xsl:call-template>
    </xsl:if>
    
</xsl:template>


<xsl:template name="createRestrictionCriteria">
    <xsl:param name="asCriteriaType"/>

    <!-- nsCriterias is the nodeset containing all criterias with good type, for include collects of current test class -->
    <xsl:variable name="nsCriterias" select="./collect[@mode = $gsSelectorInclude]//criteria[@type = $asCriteriaType]"/>
    <xsl:variable name="sIndent">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="sDefaultCriteriaConstructor">
        <xsl:text>AtkCriteriaRe(</xsl:text><xsl:value-of select="$asCriteriaType"/>
        <xsl:text>="test_*", extract_data=lambda crit, data:data.</xsl:text>
        <xsl:value-of select="$asCriteriaType"/><xsl:text>, wildcard=True)</xsl:text>
    </xsl:variable>

    <!-- Add a comment before restriction criteria instatiation -->
    <xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$sIndent"/><xsl:text># Create criteria to limit </xsl:text><xsl:value-of select="$asCriteriaType"/>
    <xsl:text> accepted for include </xsl:text><xsl:value-of select="./@name"/><xsl:text> test class</xsl:text>
    <xsl:value-of select="$gsEndl"/>

    <!-- If current test class has include criterias of type asCriteriaType -->
    <xsl:if test="$nsCriterias">

        <!-- Instantatiate default criteria -->
        <xsl:value-of select="$sIndent"/><xsl:text>default_</xsl:text><xsl:value-of select="$asCriteriaType"/>
        <xsl:text>_criteria = </xsl:text><xsl:value-of select="$sDefaultCriteriaConstructor"/><xsl:value-of select="$gsEndl"/>

        <!-- For each criteria of good type in current collect -->
        <xsl:for-each select="$nsCriterias">

            <!-- Instantiate a new criteria -->
            <xsl:value-of select="$sIndent"/>
            <xsl:value-of select="$asCriteriaType"/><xsl:text>_criteria</xsl:text><xsl:value-of select="position()"/>
            <xsl:text> = AtkCriteriaRe(</xsl:text><xsl:value-of select="$asCriteriaType"/><xsl:text> = "</xsl:text>
            <xsl:value-of select="./@value"/><xsl:text>", extract_data=lambda crit, data:data.</xsl:text>
            <xsl:value-of select="$asCriteriaType"/><xsl:text>, wildcard=True)</xsl:text><xsl:value-of select="$gsEndl"/>

        </xsl:for-each>
    </xsl:if>

    <!-- Gather all restriction criterias in a new single criteria -->
    <xsl:value-of select="$sIndent"/><xsl:value-of select="$asCriteriaType"/><xsl:text>_restriction_criteria = </xsl:text>
    <xsl:choose>
        <!-- If current test class has include criterias of type asCriteriaType -->
        <xsl:when test="$nsCriterias">
            <!-- Restriction criteria is an Or on default value plus user defined authorized values -->
            <xsl:text>Or(default_</xsl:text><xsl:value-of select="$asCriteriaType"/><xsl:text>_criteria</xsl:text>
            <xsl:for-each select="$nsCriterias">
                <xsl:text>, </xsl:text><xsl:value-of select="$asCriteriaType"/><xsl:text>_criteria</xsl:text><xsl:value-of select="position()"/>
            </xsl:for-each>
            <xsl:text>)</xsl:text>
        </xsl:when>
            
        <xsl:otherwise>
            <!-- Restriction criteria is set on default value -->
            <xsl:value-of select="$sDefaultCriteriaConstructor"/>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$gsEndl"/>

</xsl:template>


<xsl:template name="createCollectCriterias">
    <xsl:param name="asCollectMode"/>

    <xsl:variable name="sTestClassName">
        <xsl:call-template name="getTestClassName"/>
    </xsl:variable>
    <xsl:variable name="sTestClassVarName">
        <xsl:call-template name="getTestClassName">
            <xsl:with-param name="abVarName" select="'true'"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:for-each select="./collect[@mode=$asCollectMode]">

        <xsl:value-of select="$gsEndl"/>
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
        <xsl:text># Create </xsl:text>
        <xsl:if test="position()>1">
            <xsl:text>another </xsl:text>
        </xsl:if>
        <xsl:text>criteria to </xsl:text><xsl:value-of select="$asCollectMode"/>
        <xsl:text> </xsl:text><xsl:value-of select="$sTestClassName"/><xsl:text> test cases </xsl:text><xsl:value-of select="$gsEndl"/>

        <!-- Instantiate collect criteria -->
        <xsl:call-template name="instantiateComplexCriteria"/>
    </xsl:for-each>    
</xsl:template>


<xsl:template name="instantiateComplexCriteria">
    <xsl:variable name="sIndent">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
    </xsl:variable>

    <!-- If complex criteria relies on more than one criteria, or on a complex criteria -->
    <xsl:if test="count(./criteria)>1 or ./criteria/@type='node'">
         
        <!-- Instantiate sub criterias -->
        <xsl:for-each select="./criteria">
        
           <!-- Declare current sub criteria and invoke its constructor -->
            <xsl:call-template name="instantiateCriteria"/>
        </xsl:for-each>
    </xsl:if>
    
    <!-- Declare complex criteria -->
    <xsl:value-of select="$sIndent"/><xsl:call-template name="getComplexName"/><xsl:text> = </xsl:text>

    <!-- Instantiate boolean operator tree node -->
    <xsl:call-template name="getCriteriaClass"/>
        
    <xsl:choose>
        <!-- If complex criteria relies on one simple criteria -->
        <xsl:when test="(count(./criteria)=1) and (./criteria/@type!='node')">
            <xsl:call-template name="callSimpleCriteriaConstructor">
                <xsl:with-param name="ansCriteria" select="./criteria"/>
            </xsl:call-template>
        </xsl:when>
            
        <xsl:otherwise>
            <!-- Append collect's criterias to boolean operator node -->
            <xsl:for-each select="./criteria">

                <xsl:if test="position()>1">
                    <xsl:text>, </xsl:text>
                </xsl:if>
                <xsl:call-template name="getCriteriaName"/>

            </xsl:for-each>
        </xsl:otherwise>
        
    </xsl:choose>

    <!-- Terminate boolean operator tree node -->
    <xsl:call-template name="getCriteriaClass">
        <xsl:with-param name="abClosingBraces" select="true"/>
    </xsl:call-template>
    <xsl:value-of select="$gsEndl"/>
</xsl:template>


<xsl:template name="instantiateCriteria">

    <xsl:choose>
        <xsl:when test="./@type='node'">
            <xsl:call-template name="instantiateComplexCriteria"/>
        </xsl:when>

        <xsl:otherwise>
            <xsl:variable name="sIndent">
                <xsl:call-template name="Indent">
                    <xsl:with-param name="NbIndent" select="2"/>
                </xsl:call-template>
            </xsl:variable>

            <!-- Declare criteria variable and invoke its constructor -->
            <xsl:value-of select="$sIndent"/><xsl:call-template name="getCriteriaName"/><xsl:text> = </xsl:text>
            <xsl:call-template name="callSimpleCriteriaConstructor"/><xsl:value-of select="$gsEndl"/>
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>


<xsl:template name="callSimpleCriteriaConstructor">
    <xsl:param name="ansCriteria" select="."/>
    
    <!-- Instantiate a new criteria, including not option -->
    <xsl:call-template name="getCriteriaClass">
        <xsl:with-param name="ansCriteria" select="$ansCriteria"/>
    </xsl:call-template>

    <!-- Set criteria boundary -->
    <xsl:value-of select="$ansCriteria/@type"/><xsl:text>=</xsl:text>
    <xsl:text>"</xsl:text>
    <xsl:if test="$ansCriteria/@type = 'path'">
        <xsl:call-template name="getRootPath"/><xsl:text>/</xsl:text>
    </xsl:if>
    <xsl:value-of select="$ansCriteria/@value"/><xsl:text>"</xsl:text>

    <!-- Set match function if needed -->
    <!-- TODO possibilite de faire une option sup, eq, inf pour le type mark. Ex:suite_rank >= 5  -->
    <xsl:if test="$ansCriteria/@type = 'mark'">
        <xsl:text>, match=lambda crit, data: crit.mark in data</xsl:text>
    </xsl:if>

    <!-- Set extract function if needed -->
    <xsl:choose>
        <xsl:when test="$ansCriteria/@type = 'mark'">
            <xsl:text>, extract_data=lambda crit, data: data.marks</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>, extract_data=lambda crit, data: data.</xsl:text><xsl:value-of select="$ansCriteria/@type"/>
        </xsl:otherwise>
    </xsl:choose>

    <!-- Set wildcard option for AtkCriteriaRe -->
    <xsl:if test="not(contains($ansCriteria/@option, 'reg_expr')) and (($ansCriteria/@type='file_name') or ($ansCriteria/@type='test_name'))">
        <xsl:text>, wildcard=True</xsl:text>
    </xsl:if>
    <!-- Set recursive option for AtkCriteriaPath -->
    <xsl:if test="($ansCriteria/@type='path') and not(contains($ansCriteria/@option, 'recursive'))">
        <xsl:text>, recursive=False</xsl:text>
    </xsl:if>

    <!-- Terminate criteria constructor call, including not option call -->
    <xsl:call-template name="getCriteriaClass">
        <xsl:with-param name="ansCriteria" select="$ansCriteria"/>
        <xsl:with-param name="abClosingBraces" select="true"/>
    </xsl:call-template>
    
</xsl:template>


<xsl:template name="callUserDefinedCriteriaConstructor">
    <xsl:param name="asCollectMode"/>
    
    <!-- If current test class relies on more than one collect for given mode -->
    <xsl:if test="count(./collect[@mode=$asCollectMode])>1">
        <!-- Instantiate a Or tree node -->
        <xsl:text>Or(</xsl:text>
    </xsl:if>

    <!-- For each collect with required mode in current test class-->
    <xsl:for-each select="./collect[@mode=$asCollectMode]">

        <xsl:if test="position()>1">
            <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:call-template name="getCollectName"/>

    </xsl:for-each>

    <!-- If current test class relies on more than one collect for given mode -->
    <xsl:if test="count(./collect[@mode=$asCollectMode])>1">
        <!-- Terminate Or tree node instanciation-->
        <xsl:text>)</xsl:text>
    </xsl:if>
</xsl:template>


<xsl:template name="addActions">
    <xsl:param name="asTestClassVarName"/>
    <xsl:param name="asCriteriaVarName"/>
    <xsl:param name="ansLevel" select="."/>

    <xsl:variable name="sIndent">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="2"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:if test="$ansLevel/action[@type='add_marks']">
        <xsl:value-of select="$sIndent"/><xsl:value-of select="$asTestClassVarName"/>
        <xsl:text>.add_action(</xsl:text><xsl:value-of select="$asCriteriaVarName"/>

        <xsl:choose>
            <!-- Handle case of add mark actions -->
            <xsl:when test="$ansLevel/action[@type='add_marks']">
                <xsl:text>, partial(AutTestClass._add_marks, marks="</xsl:text>
                <!-- Iterate on current level actions -->
                <xsl:for-each select="$ansLevel/action[@type='add_marks']">
                    <xsl:if test="position()>1">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                    <xsl:value-of select="./@value"/>
                </xsl:for-each>
                <xsl:text>")</xsl:text>
                <xsl:text></xsl:text>
            </xsl:when>
        </xsl:choose>

        <xsl:text>)</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:if>
        
</xsl:template>


<xsl:template name="isGlobalFilter">
    <xsl:param name="ansLevel" select="."/>

    <xsl:choose>
        <xsl:when test="(count(/test_campaign/test_class) > 0) and (name($ansLevel) = 'test_campaign')">
            <xsl:text>true</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:text>false</xsl:text>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name="getRootPath">
    <xsl:variable name="root_path" select="/test_campaign/@root_path"/>

    <xsl:choose>
        <xsl:when test="starts-with($root_path, '/') = 'true' ">
            <xsl:value-of select="$root_path"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$asCampaignPath"/>
            <xsl:if test="$root_path">
                <xsl:text>/</xsl:text><xsl:value-of select="$root_path"/>
            </xsl:if>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name="getTestClassName">
    <xsl:param name="ansLevel" select="."/>
    <xsl:param name="abVarName" select="'false'"/>

    <xsl:variable name="bIsGlobalFilter">
        <xsl:call-template name="isGlobalFilter">
            <xsl:with-param name="ansLevel" select="$ansLevel"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
        <xsl:when test="$bIsGlobalFilter = 'true'">
            <xsl:value-of select="$gsGlobalFilter"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$ansLevel/@name"/>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="$abVarName = 'true'">
        <xsl:text>_test_class</xsl:text>
    </xsl:if>

</xsl:template>


<xsl:template name="getCriteriaClass">
    <xsl:param name="ansCriteria" select="."/>
    <xsl:param name="abClosingBraces" select="'false'"/>

    <xsl:variable name="sCriteriaClass">
        <xsl:choose>
            <xsl:when test="name($ansCriteria)='collect' or $ansCriteria/@type='node'">
                <xsl:choose>
                    <xsl:when test="($ansCriteria/@value='or') or ($ansCriteria/@option='or')">
                        <xsl:text>Or(</xsl:text>
                    </xsl:when>
                    <xsl:when test="($ansCriteria/@value='xor') or ($ansCriteria/@option='xor')">
                        <xsl:text>Xor(</xsl:text>
                    </xsl:when>
                    <xsl:when test="($ansCriteria/@value='not') or ($ansCriteria/@option='not')">
                        <xsl:text>Not(</xsl:text>
                    </xsl:when>
                    <xsl:when test="($ansCriteria/@value='and') or ($ansCriteria/@option='and') or (count($ansCriteria/criteria)>1)">
                        <xsl:text>And(</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text></xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="contains($ansCriteria/@option, 'reg_expr') or ($ansCriteria/@type='file_name') or ($ansCriteria/@type='test_name')">
                        <xsl:text>AtkCriteriaRe(</xsl:text>
                    </xsl:when>

                    <xsl:when test="$ansCriteria/@type='path'">
                        <xsl:text>AtkCriteriaPath(</xsl:text>
                    </xsl:when>

                    <xsl:when test="$ansCriteria/@type='mark'">
                        <xsl:text>AtkCriteria(</xsl:text>
                    </xsl:when>

                    <xsl:otherwise>
                        <xsl:text>AtkCriteriaString(</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="bNotOption">
        <xsl:if test="contains($ansCriteria/@option, 'not')">
            <xsl:text>true</xsl:text>
        </xsl:if>
    </xsl:variable>

    <!-- If criteria is a complex criteria (node or collect) with not value and not option set, return nothing: not(not)<=>identity -->
    <xsl:if test="($sCriteriaClass!='Not(') or ($bNotOption!='true')">
        <xsl:if test="$bNotOption='true'">
            <xsl:choose>
                <xsl:when test="$abClosingBraces='false'">
                    <xsl:text>Not(</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>)</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="$sCriteriaClass!=''">
            <xsl:choose>
                <xsl:when test="$abClosingBraces='false'">
                    <xsl:value-of select="$sCriteriaClass"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>)</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:if>
    
</xsl:template>


<xsl:template name="getComplexName">

    <xsl:choose>
        <xsl:when test="name()='collect'">
            <xsl:call-template name="getCollectName"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="getCriteriaName"/>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>


<xsl:template name="getCollectName">
    <xsl:param name="ansOwner" select=".."/>
    <xsl:param name="asCollectMode" select="./@mode"/>

    <xsl:variable name="sOwnerName">
       <xsl:call-template name="getTestClassName">
            <xsl:with-param name="ansLevel" select="$ansOwner"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="bIsGlobalFilter">
        <xsl:call-template name="isGlobalFilter">
            <xsl:with-param name="ansLevel" select="$ansOwner"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="$sOwnerName"/><xsl:text>_</xsl:text><xsl:value-of select="$asCollectMode"/>
    <!-- For include collects, add _user to generated name -->
    <xsl:if test="(name(.) = 'collect') and ($asCollectMode = $gsSelectorInclude) and ($bIsGlobalFilter = 'false')">
        <xsl:text>_user</xsl:text>
    </xsl:if>
    <xsl:text>_criteria</xsl:text>
    <!-- If parent has more than one collect with same mode -->
    <xsl:if test="count($ansOwner/collect[@mode = $asCollectMode]) > 1">
        <xsl:value-of select="position()"/>
    </xsl:if>
    
</xsl:template>


<xsl:template name="getCriteriaName">
    <xsl:variable name="sType" select="./@type"/>

    <xsl:value-of select="./ancestor::collect/@mode"/><xsl:text>_criteria_</xsl:text><xsl:value-of select="./@type"/>
    <!-- If parent has more than one sub criteria (including criterias contained in following child nodes) with same type -->
    <xsl:if test="count(../criteria[@type=$sType]|following-sibling::criteria[@type='node']//criteria[@type=$sType])>1">
        <xsl:value-of select="count(preceding-sibling::criteria[@type=$sType]) + 1"/>
    </xsl:if>

</xsl:template>

</xsl:stylesheet>