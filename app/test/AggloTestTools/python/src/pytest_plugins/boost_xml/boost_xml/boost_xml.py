############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################

import py
import os
import sys
import time

if sys.version_info[0] < 3:
    from codecs import open
    

# TODO supprimer la variable globale
autPytestListener = None    
    
class CautBoostXmlLog(py.xml.Namespace):
    pass

    
class CautBoostXmlResult(py.xml.Namespace):
    pass

    
def getTagAttributeValue(axmlTag, asAttributeName):
    return getattr(axmlTag.attr, asAttributeName)

    
def setTagAttributeValue(axmlTag, asAttributeName, asNewValue):
    setattr(axmlTag.attr, asAttributeName, asNewValue)
    
    
def pytest_addoption(parser):
    pyGroup = parser.getgroup("reporting")
    
    # Register plugin activation parameter for pytest command line
    pyGroup.addoption('--boostxml', action="store",
                      dest="sBoostXmlPath", metavar="path", default=None,
                      help="create boost xml style report file at given path.")
    pyGroup.addoption('--boostxmlfilename', action="store",
                      dest="sReportFileName", metavar="file", default="testReport",
                      help="file name for generated xml boost report")
    pyGroup.addoption('--boostxmlreportname', action="store",
                      dest="sReportName", metavar="file", default="autUnitTest",
                      help="name for generated xml boost report")

           
def pytest_configure(config):
    # If xml report path is given
    if (config.option.sBoostXmlPath and not hasattr(config, 'slaveinput')):
        global autPytestListener
        autPytestListener =  CautPytestListener(config)
        config._autPytestListener = autPytestListener
        # config._autPytestListener = CautPytestListener(config)
        
        # Register CautPytestListener instance which will handle pytest hooks
        config.pluginmanager.register(config._autPytestListener)
        # config.pluginmanager.register(AllureCollectionListener(reportdir))
        # TODO C'est quoi ca ?
        # pytest.allure._allurelistener = config._allurelistener  # FIXME: maybe we need a different injection mechanism

        
def pytest_unconfigure(config):
    global autPytestListener
    autPytestListener = getattr(config, '_autPytestListener', None)
    
    if (autPytestListener is not None):
        # config.pluginmanager.unregister(autPytestListener)
        del config._autPytestListener
        config.pluginmanager.unregister(autPytestListener)

        
def pytest_assert_passed(config, assertion):
    global autPytestListener

    if (autPytestListener is not None):
        autPytestListener._appendAssertPassed(str(assertion) + " passed")
               
               
class CautPytestListener(object):
    """
    Listens to pytest hooks to generate boost xml reports for unit tests.
    """
    
    def __init__(self, config):
        sBoostXmlPath = config.option.sBoostXmlPath
        sResultFilePath = sBoostXmlPath + "/" + config.option.sReportFileName + "_Result.xml"
        sLogFilePath = sBoostXmlPath + "/" + config.option.sReportFileName + "_Log.xml"
        sFile = ""
        
        sFile = os.path.expanduser(os.path.expandvars(sResultFilePath))
        self.__sResultFilePath = os.path.normpath(os.path.abspath(sFile))
        sFile = os.path.expanduser(os.path.expandvars(sLogFilePath))
        self.__sLogFilePath = os.path.normpath(os.path.abspath(sFile))
        self.__sReportName = config.option.sReportName
        self.__xmlLogTestSuites = []
        self.__xmlResultTestSuites = []
        self.__uiNbPassed = [0, 0]
        self.__uiNbSkipped = [0, 0]
        self.__uiNbFailed = [0, 0]
        self.__uiNbErrors = [0, 0]
        self.__assertList=[]
        # self.__uiStartTime = 0
    
        # self.impl = AllureImpl(asBoostXmlPath)
        # self.__apyConfig = config


    # TODO append test case au demarrage du test case et non a la fin
    def pytest_runtest_logreport(self, report):
        #TODO probablement bugge, risques de report traces plusieurs fois, ou pas du tout trace
        bHandleReport = ((report.passed) and ("call" == report.when)) or \
                        (report.failed) or (report.skipped)
        
        if (bHandleReport):
            sCapturedOutput = ""
            
            # Compute number of failures for current suite and global
            if (report.passed):
                self.__uiNbPassed = [uiNbPassed + 1 for uiNbPassed in self.__uiNbPassed]
            elif (report.failed):
                if (report.when == "call"):
                    self.__uiNbFailed = [uiNbFailed + 1 for uiNbFailed in self.__uiNbFailed]
                else:
                    self.__uiNbErrors = [uiNbErrors + 1 for uiNbErrors in self.__uiNbErrors]
            elif (report.skipped):
                self.__uiNbSkipped = [uiNbSkipped + 1 for uiNbSkipped in self.__uiNbSkipped]
                
            # Instanciate a new test case
            self.__appendTestCase(report)

            
    def pytest_collectreport(self, report):
        # If collection has failed
        if (not report.passed):
            self.__appendTestCase(report)
            
            # TODO : est -ce qu'on a un appel par test case ou par test suite ? il faut peut etre incrementer de plus que 1
            if (report.failed):
                # Failure in the test collection : the test script is wrong but should have been run : increment failure
                self.__uiNbFailed = [uiNbFailed + 1 for uiNbFailed in self.__uiNbFailed]
            else:
                # the test script collection is effective but test case should not run : increment skipped
                self.__uiNbSkipped = [uiNbSkipped + 1 for uiNbSkipped in self.__uiNbSkipped]

                
    def pytest_internalerror(self, excrepr):
        pass
        # pyReport
        
        # self.__appendTestCase(report)
        # self.__uiNbErrors += 1
        
        
        
        # self.errors += 1
        # data = bin_xml_escape(excrepr)
        # self.tests.append(
            # Junit.testcase(
                    # Junit.error(data, message="internal error"),
                    # classname="pytest",
                    # name="internal"))

                    
    def pytest_sessionstart(self):
        # self.__uiStartTime = time.time()
        pass

        
    def pytest_sessionfinish(self):
        pyLogFilePath = open(self.__sLogFilePath, 'w', encoding='utf-8')
        pyResultFilePath = open(self.__sResultFilePath, 'w', encoding='utf-8')
        xmlLogRootTestSuite = CautBoostXmlLog.TestSuite(self.__xmlLogTestSuites, name=self.__sReportName)
        # uiTestExecutionTime = self.__uiStartTime - time.time()
        sTestSuiteResult = "passed" if (0 == self.__uiNbFailed[0]) else "failed"
        # TODO calculer les metriques passed, failed...
        xmlResultRootTestSuite = CautBoostXmlResult.TestSuite(self.__xmlResultTestSuites, name=self.__sReportName, result=sTestSuiteResult, assertions_passed="0", \
                                                                                          assertions_failed="0", expected_failures="0", test_cases_passed=str(self.__uiNbPassed[0]), \
                                                                                          test_cases_failed=str(self.__uiNbFailed[0]), test_cases_skipped=str(self.__uiNbSkipped[0]), \
                                                                                          test_cases_aborted=str(self.__uiNbErrors[0]))
        # suite_stop_time = time.time()
        # suite_time_delta = suite_stop_time - self.suite_start_time
        # numtests = self.passed + self.failed

        # Update status of the previous test suite
        self.__updateLastTestSuiteStatus()

        # Generate boost xml report and boost xml traces files
        pyLogFilePath.write('<?xml version="1.0" encoding="utf-8"?>')
        pyLogFilePath.write(CautBoostXmlLog.TestLog(xmlLogRootTestSuite).unicode(indent=0))
        pyResultFilePath.write('<?xml version="1.0" encoding="utf-8"?>')
        pyResultFilePath.write(CautBoostXmlResult.TestResult(xmlResultRootTestSuite).unicode(indent=0))
        pyLogFilePath.close()
        pyResultFilePath.close()

        
    def pytest_terminal_summary(self, terminalreporter):
        terminalreporter.write_sep("-", "generated xml result file: %s" % (self.__sResultFilePath))
        terminalreporter.write_sep("-", "generated xml log file: %s" % (self.__sLogFilePath))


    def __appendAssertionResult(self, asAssertionMessage, abResult):
        xmlTestCase = self.__xmlLogTestSuites[-1][-1]
        sFormatedResult = "<![CDATA[" + asAssertionMessage + "]]>"
        
        # TODO gestion du fichier et de la ligne
        if (abResult):
            xmlTestCase.append(CautBoostXmlLog.Info(py.xml.raw(sFormatedResult), file="", line=""))
        else:
            xmlTestCase.append(CautBoostXmlLog.Error(py.xml.raw(sFormatedResult), file="", line=""))

    
    def __appendTestCase(self, apyReport):
        sNodeIdNames = apyReport.nodeid.split("::")
        # sFileName = ""
        sSuiteName = ""
        sTestCaseName = ""
        bIsNewTestSuite = False
        sTestCaseResult = "passed"
        xmlLogTestCase = None

        # Compute test suite and test cases' names
        # TODO mieux choper les noms, numeros de ligne...
        sNodeIdNames = [x.replace(".py", "") for x in sNodeIdNames if x != '()']
        # sNodeIdNames[0] = sNodeIdNames[0].replace("/", '.')
        sSuiteName = sNodeIdNames[0].split("/")[-1]
        sTestCaseName = sNodeIdNames[-1]
        bIsNewTestSuite = (0 == len(self.__xmlLogTestSuites)) or (sSuiteName != getTagAttributeValue(self.__xmlLogTestSuites[-1], "name"))

        # If test case belongs to a new test suite
        if (bIsNewTestSuite):
            # Update status of the previous test suite
            self.__updateLastTestSuiteStatus()
               
            # Instanciate a new test suite
            self.__xmlLogTestSuites.append(CautBoostXmlLog.TestSuite(name=sSuiteName))
            self.__xmlResultTestSuites.append(CautBoostXmlResult.TestSuite(name=sSuiteName, result="passed", assertions_passed="0", assertions_failed="0", expected_failures="0", \
                                                                           test_cases_passed="0", test_cases_failed="0", test_cases_skipped="0", test_cases_aborted="0"))
            # Reset test suite counters
            self.__uiNbPassed[1] = 0
            self.__uiNbSkipped[1] = 0
            self.__uiNbFailed[1] = 0
            self.__uiNbErrors[1] = 0

        # Create a new xml test case
        xmlLogTestCase = CautBoostXmlLog.TestCase(name=sTestCaseName)
        self.__xmlLogTestSuites[-1].append(xmlLogTestCase)

        # Append succesfull assertion
        for sAssert in self.__assertList:
            self.__appendAssertionResult(sAssert, True)
            # xmlLogTestCase.append(CautBoostXmlLog.Info(py.xml.raw(sAssert), file="", line=""))
        del(self.__assertList[:])
        
        # In case of tests case failure
        if (apyReport.longrepr is not None):
            # Append failure reason
            # filesystempath, lineno, domaininfo = apyReport.location
            self.__appendAssertionResult(str(apyReport.longrepr), False)

        # Append test case execution time
        xmlLogTestCase.append(CautBoostXmlLog.TestingTime(getattr(apyReport, 'duration', 0)))
        
        # Append test case to the last test suite
        # TODO creer les tests case en meme temps
        sTestCaseResult = "passed" if (apyReport.passed) else "failed" if (apyReport.failed) else "skipped"
        self.__xmlResultTestSuites[-1].append(CautBoostXmlResult.TestCase(name=sTestCaseName, result=sTestCaseResult, assertions_passed="0", assertions_failed="0", expected_failures="0"))

        
    def  _appendAssertPassed(self, asAssert):
        self.__assertList.append(asAssert)

        
    def __updateLastTestSuiteStatus(self):
        if (0 != len(self.__xmlLogTestSuites)):
            sTestSuiteResult = "passed" if (0 == self.__uiNbFailed[1]) else "failed"
            
            setTagAttributeValue(self.__xmlResultTestSuites[-1], "result", sTestSuiteResult)
            setTagAttributeValue(self.__xmlResultTestSuites[-1], "test_cases_passed", self.__uiNbPassed[1])
            setTagAttributeValue(self.__xmlResultTestSuites[-1], "test_cases_skipped", self.__uiNbSkipped[1])
            setTagAttributeValue(self.__xmlResultTestSuites[-1], "test_cases_failed", self.__uiNbFailed[1])
            setTagAttributeValue(self.__xmlResultTestSuites[-1], "test_cases_aborted", self.__uiNbErrors[1])

            
    # def _getCapturedOutput(self, apyReport, abIsErrorCapture):
        # sRes = ""
        # sCaptureType = "err" if (abIsErrorCapture) else "out"
    
        # for sName, sCapture in apyReport.get_sections("Captured std%s" % sCaptureType):
            # sRes += sCapture
                
        # return (sRes)
        
# TODO Est-ce que'on implemente ca ? pour les skipped ?
# class AllureCollectionListener(object):
    # """
    # Listens to pytest collection-related hooks
    # to generate reports for modules that failed to collect.
    # """
    # def __init__(self, logdir):
        # self.impl = AllureImpl(logdir)
        # self.fails = []

        