Suite-Rank
============

Define suit rank.

    @pytest.suite_rank(3)
    def test_method(self):
        # some check-y stuff

