############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################

import inspect

import pytest

def pytest_configure():
    pytest.suite_rank = AttSuiteRank


class AttSuiteRank(object):
    """ A decorator.  Example::

         import py
         @pytest.suite_rank(3)
         def test_function():
            pass

    will make module test cases executed after 2 previous test suites."""
    
    def __init__(self, suite_rank):
        self.__suite_rank = suite_rank
        
    def __call__(self, f):
        module = inspect.getmodule(f)
        module.suite_rank = self.__suite_rank

        return f


    
def pytest_collection_modifyitems(session, config, items):
    # Order suites whose rank is defined
    ordered_suites = {item.getparent(pytest.Module) for item in items \
                                                     if hasattr(item.getparent(pytest.Module).obj, "suite_rank")}
    ordered_suites = list(ordered_suites)
    ordered_suites.sort(key=lambda x : x.obj.suite_rank)

    # Order suites with undefined rank
    # TODO c'est pas bon si items n'est pas ordonne avec tous les case d'une suite les uns apres les autres
    unordered_suites = {item.getparent(pytest.Module):i for i, item in enumerate(items) \
                                                         if not hasattr(item.getparent(pytest.Module).obj, "suite_rank")}
    unordered_suites = [(module_name, rank) for module_name, rank in unordered_suites.items()]
    unordered_suites.sort(key=lambda x : x[1])
    unordered_suites = [val[0] for val in unordered_suites]

    # Merge ordered and unordered test suites
    merged_suites = []
    b_continue = (len(ordered_suites) > 0) or (len(unordered_suites) > 0)
    while b_continue:
        # If all ordered suites have been merged
        if (0 == len(ordered_suites)):
            # Simply append remaining unordered suites, and exit loop
            merged_suites.extend(unordered_suites)
            b_continue = False

        # If there are remaining ordered suites not merged    
        else:
            # If next ordered module must be next one executed
            if ((len(merged_suites) + 1) >= ordered_suites[0].obj.suite_rank):
                # Merge next unordered module
                merged_suites.append(ordered_suites.pop(0))

            # Next ordered module must be scheduled later
            else:
                # If there are no more unordered suites
                if (0 == len(unordered_suites)):
                    # Simply append remaining ordered suites, and exit loop
                    merged_suites.extend(ordered_suites)
                    b_continue = False
                else:
                    # Merge next unordered module
                    merged_suites.append(unordered_suites.pop(0))
    
    # Build a sorted items list
    ls_ordered_items = [item for current_suite in merged_suites \
                             for item in items \
                             if item.getparent(pytest.Module) is current_suite]
    items[:] = ls_ordered_items
