
############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################

from types import MethodType
from collections import namedtuple

from agglo_tk.exceptions import AtkUndecidedError
from agglo_tk.design_patterns.visitor import AtkVisitable
from agglo_tk import trace


# TODO faire une class pour evaluer include et __str__ ?
AttCollectResult = namedtuple("AttCollectResult", ["include", "exclude", "skip", "xfail"])

class AttTestClass(object):

    def __init__(self, name):
        self.__name = name
        self.__selectors = {}
        self.__action_marks = set()
        self.__stop_on_found = True
        self.__has_actions = False


    @property
    def name(self):
        return self.__name


    @property
    def selectors(self):
        return self.__selectors


    @property
    def action_marks(self):
        return self.__action_marks


    def ignore(self, global_match=False, **kwargs):
        include = False
        exclude = False

        # Check if provided datas matches test class inclusion criterias
        # If provided datas are no sufficient to ensure include match,
        # trigger more inspection by pretending include matched
        self.__stop_on_found = True
        include = self.__match("include", True, **kwargs)

        if include or global_match:
            # Check if provided datas matches test class exclusion criterias
            # If provided datas are no sufficient to ensure exclude match,
            # trigger more inspection by pretending exclude didn't match
            exclude = self.__match("exclude", **kwargs)

        return AttCollectResult(include, exclude, False, False)


    def select(self, global_match=False, **kwargs):
        include = False
        exclude = False
        skip = False
        xfail = False
        add_marks = set()
            
        trace(trace_class="AUT", info="AutTestClass:start selection by " + self.name)
        # Reset test_class marks
        self.action_marks.clear()
        self.__stop_on_found = not self.__has_actions

        # Check if provided datas match include and exclude selectors
        include = self.__match("include", **kwargs)
        if include or global_match:
            exclude = self.__match("exclude", **kwargs)

        # If current datas must be included, or global_match requires to match all collects
        if (include and not exclude) or global_match:
            # Check skip ad xfail properties if necessary
            skip = self.__match("skip", **kwargs)
            xfail = self.__match("xfail", **kwargs)

        result = AttCollectResult(include, exclude, skip, xfail)
        trace(trace_class="AUT", info="AutTestClass:match result " + str(result))
        return result
    
    
    # TODO deplacer dans selector
    def add_action(self, criteria, action):
        visitable = None

        for _, selector in self.selectors.items():
            try:
                visitable = selector.criteria.get_node(criteria)
            except ValueError:
                pass
        if visitable is None:
            raise ValueError

        # TODO ce serait mieux que l'action soit associe au critere plutot qu'au node. En effet, vu que les criteres sont
        # crees par l'utilisateur mais les leaf par ATK, des qu'on change les leaf, l'action est perdue.
        # TODO c'est pas bon de monkey patcher accept comme ca il faudrait mieux trouver un moyen de surcharger le visiteur
        def decorated_accept(visitable, visitor, *args, **kwargs):
            result = AtkVisitable.accept(visitable, visitor, *args, **kwargs)
            if result:
                for action in visitable.actions:
                    trace(trace_class="AUT", info="Execute action " + str(id(action)))
                    action(self)
            return result

        # Replace accept methode of visitable
        vars(visitable).setdefault("actions", []).append(action)
        visitable.accept = MethodType(decorated_accept, visitable)

        # Since there are actions registered on criterias, the match methode needs to check all criteria
        self.__has_actions = True

    
    # TODO prendre en parametre un set et non un string avec des virgules    
    def _add_marks(self, marks):
        for mark in marks.replace(" ", "").split(","):
            self.action_marks.add(mark)


    def __match(self, selector_type, default=False, **kwargs):
        ''' default: value return on AtkUndecidedErrorr, when kwargs doesn't
            contain enough datas to properly match''' 
        result = False

        try:
            # Match on selector with stop on found deactivated 
            # so thatall underlying actions will be triggered
            trace(trace_class="AUT", info="AutTestClass:start " + selector_type + " selector")
            result = self.selectors[selector_type].match(stop_on_found=self.__stop_on_found, **kwargs)
        except KeyError:
            # If no selector is provided for given type, it's equivalent to mismatch
            result = False
        except AtkUndecidedError:
            # Provided datas are no sufficient to ensure match
            # Return default value
            trace(trace_class="AUT", info="AtkComplexSelector:match could not decide, " + 
                                          ("include" if default else "exclude") + " by default")
            result = default

        return result
