
############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


import pytest
from _pytest.mark import MarkInfo
from _pytest.mark import MarkDecorator
from path import Path



@pytest.hookimpl(tryfirst=True)
def pytest_ignore_collect(path, config):
    result = False
    match_params = {}
    py_path = Path(str(path))

    # Build dict of match parameters
    if py_path.isfile():
        match_params["path"] = str(py_path.parent)
        match_params["file_name"] = str(py_path.name)
    else:
        match_params["path"] = str(**match_params)

    try:
        result = config.campaign.ignore_collect(**match_params)

    except AttributeError:
        pass

    return result


@pytest.hookimpl(hookwrapper=True)
def pytest_pycollect_makeitem(collector, name, obj):
    selected_items = []
    match_params = {}
    path = Path(collector.fspath)

    # Execute all other hooks to obtain the item object
    outcome = yield
    items = outcome.get_result()

    # Build dict of match parameters
    match_params["path"] = str(path.parent)
    match_params["file_name"] = str(path.name)
            
    try:
        for item in items:
            if isinstance(item, pytest.Function):
                match_params["test_name"] = item.name
                match_params["marks"] = {name for name, value in item.keywords.items() \
                                              if (isinstance(value, MarkInfo) or \
                                                  isinstance(value, MarkDecorator))}
                
                if collector.config.campaign.select(item, **match_params):
                    selected_items.append(item)

        items[:] = selected_items

    # Case where items is empty (no previous selection)
    except TypeError:
        pass
    # Case where campaign is not activated in config    
    except AttributeError :
        pass
