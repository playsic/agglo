
############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################

from collections import OrderedDict
from types import MethodType
import logging

from test_class import AttCollectResult

from agglo_tk import trace


class AttCampaign(object):
    def __init__(self, name):
        self.__global_filter = None
        self.__name = name
        self.__test_classes = OrderedDict()


    def __getitem__(self, test_class_name):
        return self.__test_classes[test_class_name]
        # TODO faut il retourner le global filter ? Risque de doublon de nom
        # result = None

        # try:
        #     self.__test_classes[test_class_name]
        # except KeyError as key_error:
        #     try:
        #         result = (test_class_name == self.global_filter.name)
        #     except AttributeError:
        #         raise key_error

        # return result


    def __contains__(self, name_or_value):
        return (name_or_value in self.__test_classes) or \
               (name_or_value in self.__test_classes.values())


    @property
    def name(self):
        return self.__name


    @property
    def global_filter(self):
        return self.__global_filter


    @global_filter.setter
    def global_filter(self, value):
        self.__global_filter = value


    def add(self, test_class):
        self.__test_classes[test_class.name] = test_class

        
    def ignore_collect(self, **kwargs):
        result = False
        continue_ignore = True

        # First check global test classe
        try:
            include, exclude, _, _ = self.global_filter.ignore(True, **kwargs)
            # If global class excludes current items
            if exclude:
                # Ignore current item and stop matching on remaining classes
                result = True
                continue_ignore = False
            # Else if global class includes current items
            elif include:
                # Keep collecting current item and stop matching on remaining classes
                result = False
                continue_ignore = False
            # Else global has no match (neither including nor excluding) on 
            # current items. Continue inspecting next classes
        except AttributeError:
            # Case of no global filter defined
            pass


        # If global test classe have not determined result
        if continue_ignore:
            found = False

            # Iterate on all the test classes
            for name, test_class in self.__test_classes.items():
                # If current test class may have items in path
                include, exclude, _, _ = test_class.ignore(**kwargs)
                found = include and not exclude
                if found:
                    # Stop matching on classes as soon as at least one test class 
                    # may have item to collect
                    break

            # Return True if no test class matches current items
            result = not found

        return result


    def select(self, func, **kwargs):
        result = False
        final_result = AutCollectResult(False, False, False, False)
        global_filter_result = AutCollectResult(False, False, False, False)
        test_classes_result = AutCollectResult(False, False, False, False)
        continue_select = True
        add_marks = set()
        marks_criteria = kwargs.get("marks", set())

        trace(trace_class="AUT", info="AutTestCampaign:start selection by " + self.name)

        # TODO il faudrait que la mark soit deja presente des qu'on sait que ca match, ainsi les test_classes suivantes peuvent
        # matcher sur la mark de test_class
        # First check global test classe
        try:
            global_filter_result = self.global_filter.select(True, **kwargs)

            # TODO c'est bon ca ? l'objet retourne pourrait avoir une methode nonzero a true si select est OK
            if ((global_filter_result.include or global_filter_result.skip or global_filter_result.xfail) and \
                (not global_filter_result.exclude)):
                new_marks = {self.global_filter.name}
                new_marks |= self.global_filter.action_marks
                add_marks |= new_marks
                trace(trace_class="AUT", info="AutCampaign:global filter appends marks: " + ", ".join(new_marks))

                # Add new marks to kwargs, so that these marks can be applied to other test classes
                marks_criteria |= new_marks

            # If include not set, we need to collect other test classes to check for inclusion
            # If set, we need to collect other test classes to check for other selecting test classes
            # If exlude set, no need to continue
            continue_select = not global_filter_result.exclude
            # TODO on n'ajoute pas les mark?
        except AttributeError:
            # Case of no global filter defined
            pass
        trace(trace_class="AUT", info="AutCampaign:global filter result " + str(global_filter_result))

        if continue_select:
            test_classes = [test_class for name, test_class in self.__test_classes.items()]
            any_include = False
            all_skip = False
            all_xfail = False
            first_included = True

            # Then check normal test classes
            for test_class in test_classes:
                # Check behavior defined by current global test class
                current_result = test_class.select(**kwargs)

                if current_result.include and not current_result.exclude:
                    # Normal test classes result includes if at least one normal 
                    # test class includes (and not excludes)
                    any_include = True

                    # TODO c'est utile ? puisque une mark a deja ete posee ?
                    # vars(func).setdefault("test_class", set()).add(name)

                    # Check if all test classes skip or xfail
                    all_skip = current_result.skip if first_included else all_skip and current_result.skip
                    all_xfail = current_result.xfail if first_included else all_xfail and current_result.xfail
                    first_included = False

                    # test class name and action marks will be appended to func marks
                    # TODO on ajoute toutes les marques, ca depend pas si le collect a effectivement matche ?
                    new_marks = {test_class.name}
                    new_marks |= test_class.action_marks
                    add_marks |= new_marks
                    trace(trace_class="AUT", info="AutCampaign:current test class appends marks: " + ", ".join(new_marks))
                    # trace(trace_class="AUT", info="AutCampaign:current test class appends marks: " + ",".join(new_marks))
                    # add_marks.add(test_class.name)
                    # add_marks |= test_class.action_marks

                    # Add new marks to kwargs, so that these marks can be applied to other test classes
                    marks_criteria |= new_marks


            # Normal test classes result skip (resp.xfail) if all included test 
            # classes skip (resp. xfail)
            test_classes_result = AutCollectResult(any_include, False, all_skip, all_xfail)
        trace(trace_class="AUT", info="AutCampaign:test classes filter result " + str(test_classes_result))

        # Final result is on global and normal tests cases result
        final_result = [global_ or normal for global_, normal in zip(global_filter_result, test_classes_result)]
        final_result = AutCollectResult(*final_result)
        trace(trace_class="AUT", info="AutCampaign:final result " + str(final_result))

        # If item is included
        if final_result.include and not final_result.exclude:
            # Campaign name will be appended to func marks
            add_marks.add(self.name)

            # Add skip and xfail marks
            if final_result.skip:
                trace(trace_class="AUT", info="AutCampaign:append skip mark")
                add_marks.add("skip")
            if final_result.xfail:
                trace(trace_class="AUT", info="AutCampaign:append xfail mark")
                add_marks.add("xfail")

            # Append marks to func
            for mark in add_marks:
                func.add_marker(mark)
        
        return final_result.include and not final_result.exclude
