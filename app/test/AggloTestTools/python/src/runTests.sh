############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 s published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Plese review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "S IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################


# Global variables
# equivalent $(dirname $0)
sThisShellPath=${0%/*}
source "$sThisShellPath/../../../../development/AggloToolKit/shell/src/utils/atkString.sh"
source "$sThisShellPath/../../../../development/AggloToolKit/shell/src/utils/atkCommandLine.sh"
source "$sThisShellPath/../../../../development/AggloToolKit/shell/src/utils/atkFileSystem.sh"
sAutOutputSrcPath="../../xsl"



# Parse command line
# TODO
# param d appel: --session= session="Valid-1.0"
# param d appel: --extend = fichier xml complementaire (xml de skip, ut en rab...)
# param d appel: --skip="type=value options=''" = ajout <criteria type="type" value="value" options="options"> dans collect mode skip
# param d appel: --only="type=value options=''" = ajout <criteria type="type" value="not(value) and not(value)" options="options"> dans collect mode skip = exclude
# TODO
# faire un shell de creation de campagne => permet de faire du skip à la volee:
# buildcamapgn.sh -skip -path... >extend.xml
# puis runcampagn --extend extend.xml
# generate pytest.ini dans quel repertoire ?
# generate plugin campaign.xml + xsl -> campaign.py
# get root path campaign.xml + xsl -> root path
# if root path = null -> root_path = getpath(campaign.xml)
resetCommand
parseCommandLine $@
defineParam "t tests-name" "Report file" "true" 1
defineParam "r reports-path" "Report path" "true" 1
defineParam "p tests-path" "Tests path" "true"
defineParam "c campaign" "Campaign file path" "true"
defineParam "n no-generation" "Disable plugins files generation (use existing ones)"
defineParam "s session" "Name of campaign session" "true"
defineParam "e extend" "Campaign extension file path" "true"
defineParam "o only" "Restrict tests" "true"
defineParam "k skip" "Skip parameters" "true"
defineParam "x xfail" "Xfail parameters" "true"
defineParam "v verbose" "Verbose"
sErrorMessage=$(checkCommandLine)

# If there is an error in command line
if [ -n "$sErrorMessage" ]; then
    # Display error message
    echo -e $sErrorMessage
    
else
        # If help is required
    if [ $(isFlagSet "help") == "true" ]; then
        # Display help
        sHelp=$(getHelp)
        echo -e $sHelp
    else
        sTestsPath=$(getFlagValue "tests-path")
        sReportsPath=$(getFlagValue "reports-path")
        sTestsName=$(getFlagValue "tests-name")
        bNoGeneration=$(isFlagSet "no-generation")
        sSession=$(getFlagValue "session")
        sExtensionFile=$(getFlagValue "extend")
        sOnly=$(getFlagValue "only")
        sSkip=$(getFlagValue "skip")
        sXfail=$(getFlagValue "xfail")
        bVerbose=$(isFlagSet "verbose")
        sCommand="py.test"
        # TODO py.test ne fonctionne ps sous cygwin
        # py.test $sScriptsPath --boostxml=$sReportPath -boostxmlfilename=$sReportName >/dev/null
        # python "$sThisShellPath/autRunPytest.py" $sScriptsPath --boostxml=$sReportPath --boostxmlfilename=$sReportName >/dev/null
        # python "$sThisShellPath/autRunPytest.py" $sScriptsPath --boostxml=$sReportPath --boostxmlfilename=$sReportName -s #-k "test_accIODumper_UT" #-q --tb=no

        # If tests are specified through an xml campaign file
        if [ $(isFlagSet "campaign") == "true" ]; then
            sCampaign=$(getFlagValue "campaign")
            sTestsPath=$(getPath $sCampaign)

            # Update generated hook files if required
            if [ $(isFlagSet "no-generation") == "false" ]; then
                $sThisShellPath/generateCampaign.sh --campaign "$sCampaign"
            fi

            # sCampaignRootPath=$(getCampaignRootPath "$sCampaign")
            # Move to campaign path
            # TODO
            # sCampaignPath=$(getFilePath "$sCampaign")
            # sCampaignRootPath=$(getCampaignRootPath)
            # cd $sCampaignPath/$sCampaignRootPath
        # If tests are specified through a path
        elif [ $(isFlagSet "tests-path") == "true" ]; then
            sCampaignRootPath=$sTestsPath
            # sCommand=$sCommand" "$sTestsPath
        fi

        # Execute tests
        sCommand=$sCommand" "$sTestsPath" --boostxml="$sReportsPath" --boostxmlfilename="$sTestsName
        echo $sCommand
        $sCommand

        # if [ $(isFlagSet "campaign") == "true" ]; then
        #     cd -
        # fi

        # Report unit tests
        $sThisShellPath/$sAutOutputSrcPath/reportUnitTest.sh $sReportsPath $sTestsName $Verbose
 
        # TODO proposer un affichage du html en option
        # sHtmlReportPath=$(getPathAbsolute "$sReportPath/$sReportName.html")
        # gnome-open $sHtmlReportPath
    fi
fi