#!/bin/bash
############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################


# Global variables
# equivalent $(dirname $0)
sThisShellPath=${0%/*}
sApplyPatchShellPath="../../../../development/AggloToolKit/shell/src/patch/atkPatchApply.sh"
sPatchPath="../src/pytestPatch"
sPatchedPath="../src/pytest"
sPluginPath="../src/pytest_plugins/boost_xml"
arPlugins=("boost_xml" "campaign" "suite_rank")

# Check the parameters
ParamOK=true
if [ $# != 0 ] && [ $# != 1 ];
then
    echo "Command format : install.sh [--develop]"
    echo "example : ../install.sh --develop"
    ParamOK=false
fi


if $ParamOK ;
then
	
	sTarget="install"
    if [ $# = 1 ] && [ $1 = "--develop" ];
    then
    	sTarget="develop"
    fi

	# Unzip pytest official version
	tar xopf "$sThisShellPath/$sPatchPath/pytest-2.9.2.tar.gz" -C "$sThisShellPath/$sPatchPath"
    rm -rf  "$sThisShellPath/$sPatchPath/official"
	mv  "$sThisShellPath/$sPatchPath/pytest-2.9.2" "$sThisShellPath/$sPatchPath/official"

	# Apply AUT patch on pytest
	"$sThisShellPath/$sApplyPatchShellPath" "$sThisShellPath/$sPatchPath/patch" "$sThisShellPath/$sPatchedPath" "$sThisShellPath/$sPatchPath/official"

	# Install patched pytest
	sCurrentWorkingDir=$(pwd)
	# echo python $sThisShellPath/$sPatchPath/patched/setup.py $sTarget
	cd "$sThisShellPath/$sPatchedPath"
	python setup.py $sTarget --record "../../install/aut_installed.txt"
	cd "$sCurrentWorkingDir"

	# Install AUT plugins
	cd "$sCurrentWorkingDir"
	cd  "$sThisShellPath/$sPluginPath"
    for sPlugin in "${arPlugins[@]}"; do 
        cd $sPlugin
        python setup.py $sTarget --record "temp.txt"
        cat temp.txt >> "../../../install/aut_installed.txt"
        rm temp.txt
        cd ..
    done
	cd "$sCurrentWorkingDir"

fi

