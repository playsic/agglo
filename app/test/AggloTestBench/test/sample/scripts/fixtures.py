############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from os import path
from os import environ as environment
from os import devnull
import subprocess
from time import sleep
from pytest import fixture

from agglo_tk.io import AtkIOConnectionLogs
from agglo_tk.io import AtkIODeviceTCP

from agglo_tb import AtbTestBench
from agglo_tb import AtbTarget
from agglo_tb.core import AtbEthernetInterface


__all__ = ["generate_cli_client", "build_app", "sample_app", "test_bench", "test_conductor", 
           "io_logs", "sample_cli", "sample_model_cls", "sample_model", "loopback_itf"]

class SampleApp(AtbTarget):
    def __init__(self, path):
        super(SampleApp, self).__init__("sample_app")

        self.__path = path


    def build(self):
        DEVNULL = open(devnull, 'w')
        output = subprocess.check_output(args=["/bin/bash", self.__path + "/build_qt_sample.sh"], stderr=DEVNULL)
        print output


    def start(self):
        # TODO modifier l'install QT https://doc.ubuntu-fr.org/variables_d_environnement
        # my_env = environment.copy()
        # my_env["LD_LIBRARY_PATH"] = "/opt/Qt/5.9.1/gcc_64/lib"
        self.__process = subprocess.Popen(args=[self.__path + "/debug/cliQtSample"])
        # self.__process = subprocess.Popen(args=[self.__path + "/debug/cliQtSample"], env=my_env)


    def terminate(self):
        self.__process.terminate()
        self.__process = None

   
@fixture(scope="session")
def generate_cli_client(request):
    fixture_path = path.realpath(__file__)
    fixture_path = path.dirname(fixture_path)
    # sample_cli_client_path = fixture_path + "/../../Agglo/app/development/AggloCliClient/python/test/sample"
    sample_cli_client_path = fixture_path + "/../../../../../development/AggloCliClient/python/test/sample"

    subprocess.check_output(args=["/bin/bash", sample_cli_client_path + "/generate_cli_client.sh"])
    subprocess.check_output(args=["cp", sample_cli_client_path + "/sample_cli_client.py", fixture_path])

   
@fixture(scope="session")
def build_app(request):
    # TODO deplacer dans les fixtures AUT
    fixture_path = path.realpath(__file__)
    fixture_path = path.dirname(fixture_path)
    # app = SampleApp(fixture_path + "/../../Agglo/app/development/AggloCliServer/cli/samples/clisample_qt/build")
    app = SampleApp(fixture_path + "/../../../../../development/AggloCliServer/cli/samples/clisample_qt/build")

    app.build()

    return app

   
@fixture
def sample_app(build_app):
    # Start the app, listening on TCP port 2002, on 127.0.0.1
    build_app.start()
    sleep(1)
    yield build_app

    # Close the process on fixture exit
    build_app.terminate()


@fixture
def test_bench(generate_cli_client, sample_app):
    from sample_cli_client import AccCliClientacsSampleCliQt
    test_bench = AtbTestBench()

    # Add test target    
    test_bench.add_target(sample_app)

    # Add local interface
    loopback_itf = AtbEthernetInterface("lo", test_bench.io_logs)
    test_bench.conductor.add_interface(loopback_itf)
    test_bench.connect(test_bench.conductor.lo, sample_app)
    
    # Add cli client
    io_device_tcp = AtkIODeviceTCP("127.0.0.1", 2002)
    cli_client = AccCliClientacsSampleCliQt(io_device_tcp, name="sample_cli", io_logs=test_bench.io_logs)
    cli_client.add_parser_reg_expr(async_reg_expr="\[ASYNC\].*\n")
    test_bench.conductor.add_cli_client(cli_client)

    # TODO ca devrait etre fait par test_bench
    loopback_itf.open()
    cli_client.open()
    # test_bench.conductor.open()
    # test_bench.open()
    # TODO workaround car bug sur le timeout d'attente d'ouverture de la connexion
    sleep(1)
    yield test_bench

    # test_bench.close()
    test_bench.conductor.close()


@fixture
def test_conductor(test_bench):
    return test_bench.conductor


@fixture
def io_logs(test_bench):
# def io_logs(request, dump_failed):
    # TODO
    # Register io logs for dump on failure
    # dump_failed._io_logs = sample_cli_client.io_logs
    return test_bench.io_logs


#TODO ajouter un parametre pour ouvrir optionnellement le client au demarrage
@fixture
def sample_cli(test_conductor):
    return test_conductor.sample_cli


@fixture
def sample_model_cls():
    fixture_path = path.realpath(__file__)
    fixture_path = path.dirname(fixture_path)
    sample_cli_client_path = fixture_path + "/../../../../../development/AggloCliClient/python/test/sample"

    subprocess.check_output(args=["cp", sample_cli_client_path + "/sample_model.py", fixture_path])
    from sample_model import AccSampleModel

    return AccSampleModel


@fixture
def sample_model(sample_cli, sample_model_cls):
    return sample_model_cls(sample_cli)


@fixture
def loopback_itf(test_conductor):
    return test_conductor.lo
