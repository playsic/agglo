############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import re

from pytest import mark

from scapy.all import IP
from scapy.all import UDP
from scapy.all import Raw

from agglo_tk.io.scapy import AtkIOScapyCriteria
from agglo_tk.io import AtkIOStringCriteria

# TODO trouver le moyen de n'importer que les fixtures explicitement invoquees ici
from fixtures import *
from agglo_tk.utils.test.fixtures import *


HEARTBEAT_PORT = 56463
HEARTBEAT_DATA = "heartbeat_data"

# TODO passe cli_client en always, ajoute dump_failed a cli_client
@mark.sudo
def test_enableHeartBeatEmission(sample_cli, sample_model, loopback_itf, dump_failed):
    udp_criteria = AtkIOScapyCriteria(io_type=loopback_itf.io_type_input, dport=HEARTBEAT_PORT, 
                                      match_io_data=lambda crit, io_data: crit.match_field(io_data, UDP, "dport"))

    dump_failed.io_logs = sample_cli.io_logs

    #UnitTestComment("Disable talker mode and check that no UDP is emitted")
    sample_model.local_port = HEARTBEAT_PORT - 1
    sample_model.remote_port = HEARTBEAT_PORT
    sample_model.talker_enabled = False
    sample_model.talker_mode_interval = 2
    sample_model.heartbeat_data = HEARTBEAT_DATA
    udp_criteria.from_now(10000)
    sample_model.serialize()
    assert not sample_cli.io_logs.check(udp_criteria, nb_occurrences=0)

    #UnitTestComment("Enable talker mode and check that one UDP frame is emitted")
    sample_model.talker_enabled = True
    udp_criteria.from_now(4000)
    sample_model.serialize()
    # TODO bizarre que ca passe, on devrait attendre sur loopback_itf ?...
    received_udp = sample_cli.wait_io(udp_criteria).data
    assert received_udp[UDP].payload == Raw(HEARTBEAT_DATA)

    #UnitTestComment("Change talker mode port and check that one UDP frame is emitted on new port")
    sample_model.remote_port = HEARTBEAT_PORT + 1
    udp_criteria.dport = sample_model.remote_port
    udp_criteria.from_now(4000)
    sample_model.serialize()
    received_udp = sample_cli.wait_io(udp_criteria).data
    assert received_udp[UDP].payload == Raw(HEARTBEAT_DATA)


@mark.sudo
def test_enableHeartBeatReception(sample_cli, sample_model, loopback_itf, dump_failed):
    hearbeat = IP(dst="127.0.0.1")/UDP(sport=HEARTBEAT_PORT, dport=HEARTBEAT_PORT-1)/HEARTBEAT_DATA
    async_cli_criteria = AtkIOStringCriteria(io_type=sample_cli.io_type_async)
    heartbeat_regexpr = "\[ASYNC\]heartbeat ([0-9]*) received: " + HEARTBEAT_DATA + "\n"

    dump_failed.io_logs = sample_cli.io_logs

    #UnitTestComment("Disable listener mode and check that no UDP is received")
    sample_model.local_port = HEARTBEAT_PORT - 1
    sample_model.remote_port = HEARTBEAT_PORT
    sample_model.listener_enabled = False
    sample_model.serialize()
    loopback_itf.send(hearbeat)
    assert not sample_cli.io_logs.check(async_cli_criteria, nb_occurrences=0)

    #UnitTestComment("Enable listener mode and check that UDP frames are received")
    sample_model.listener_enabled = True
    sample_model.serialize()
    async_cli_criteria.from_now(1000)
    loopback_itf.send(hearbeat)
    received_async_cli = sample_cli.wait_io(async_cli_criteria).data
    match = re.search(heartbeat_regexpr, received_async_cli)
    assert match.group(1) == "1"
    async_cli_criteria.from_now(1000)
    loopback_itf.send(hearbeat)
    received_async_cli = sample_cli.wait_io(async_cli_criteria).data
    match = re.search(heartbeat_regexpr, received_async_cli)
    assert match.group(1) == "2"

    #UnitTestComment("Check that reseting listener mode reset frames counter")
    sample_model.listener_enabled = False
    sample_model.serialize()
    loopback_itf.send(hearbeat)
    sample_model.listener_enabled = True
    sample_model.serialize()
    async_cli_criteria.from_now(1000)
    loopback_itf.send(hearbeat)
    received_async_cli = sample_cli.wait_io(async_cli_criteria).data
    match = re.search(heartbeat_regexpr, received_async_cli)
    assert match.group(1) == "1"


@mark.sudo
def test_setHeartBeatPeriod(sample_cli, sample_model, loopback_itf, dump_failed):
    udp_criteria = AtkIOScapyCriteria(io_type=loopback_itf.io_type_input, dport=HEARTBEAT_PORT, 
                                      match_io_data=lambda crit, io_data: crit.match_field(io_data, UDP, "dport"))

    dump_failed.io_logs = sample_cli.io_logs

    #UnitTestComment("Set talker mode period and check UDP are received corresponding to configured period")
    sample_model.local_port = HEARTBEAT_PORT - 1
    sample_model.remote_port = HEARTBEAT_PORT
    sample_model.talker_enabled = True
    sample_model.talker_mode_interval = 1
    sample_model.heartbeat_data = HEARTBEAT_DATA
    udp_criteria.from_now(10000)
    sample_model.serialize()
    received_udp = sample_cli.wait_io(udp_criteria, 8)
    assert received_udp[-1].data[UDP].payload == Raw(HEARTBEAT_DATA)
    
    #UnitTestComment("Modify talker mode period and check UDP are applied period is modified")
    sample_model.talker_mode_interval = 2
    udp_criteria.from_now(10000)
    sample_model.serialize()
    received_udp = sample_cli.wait_io(udp_criteria, 4)
    assert received_udp[-1].data[UDP].payload == Raw(HEARTBEAT_DATA)
