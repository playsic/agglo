############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from time import sleep

from scapy.all import IP, UDP, Raw

# TODO trouver le moyen de n'importer que les fixtures explicitement invoquees ici
from fixtures import *
from agglo_tk.io import AtkIOCriteria
from agglo_tk.io import AtkIOStringCriteria
from agglo_tk.utils.test.fixtures import *


HEARTBEAT_PORT = 56463
HEARTBEAT_DATA = "heartbeat_data"

def test_testBenchAbstraction_cliSerializableTestTarget(test_bench, sample_cli, sample_model_cls, dump_failed):
    sent_command_criteria = AtkIOCriteria(io_type=sample_cli.io_type_command)

    dump_failed.io_logs = test_bench.io_logs

    #UnitTestComment("Configure test target remote address")
    # Serialization can be used to send cli commands
    sample_model = sample_model_cls(sample_cli)
    sample_model.local_port = 25
    sample_model.serialize()
    sleep(0.5)
    sent_commands = test_bench.io_logs[sent_command_criteria]
    assert sent_commands
    assert "local port 25\n" in [io.data for io in sent_commands] 

    #UnitTestComment("Check configured remote address")
    # Unserialization can be used to check target configuration
    sample_model_server_side = sample_model_cls(sample_cli)
    sample_model_server_side.unserialize()
    assert sample_model_server_side.local_port == 25


def test_testBenchAbstraction_testConductorInterfaces(test_bench, test_conductor, sample_model, sample_app, dump_failed):
    heartbeat = IP(dst="127.0.0.1")/UDP(sport=HEARTBEAT_PORT, dport=HEARTBEAT_PORT-1)/HEARTBEAT_DATA
    heartbeat_regexpr = "\[ASYNC\]heartbeat ([0-9]*) received: " + HEARTBEAT_DATA + ".*\n"
    heartbeat_received_criteria = AtkIOStringCriteria(io_type=test_conductor.sample_cli.io_type_async, reg_expr=heartbeat_regexpr)

    dump_failed.io_logs = test_bench.io_logs

    #UnitTestComment("Disable listener mode and check that no UDP is received by target")
    # Test bench send method can specify test conductor interface to use for emission
    sample_model.local_port = HEARTBEAT_PORT - 1
    sample_model.remote_port = HEARTBEAT_PORT
    sample_model.listener_enabled = False
    heartbeat_received_criteria.from_now(2000)
    sample_model.serialize()
    test_conductor.send(heartbeat, local_interface=test_conductor.lo)
    assert len(test_bench.io_logs.check(heartbeat_received_criteria, nb_occurrences=0)) == 0

    #UnitTestComment("Enable listener mode and check that UDP is received by target")
    sample_model.listener_enabled = True
    heartbeat_received_criteria.from_now(2000)
    sample_model.serialize()
    test_conductor.send(heartbeat, local_interface=test_conductor.lo)
    received_cli_async = test_conductor.sample_cli.wait_io(heartbeat_received_criteria).data
    assert received_cli_async == "[ASYNC]heartbeat 1 received: heartbeat_data\n"

    #UnitTestComment("Modify sent data and check that cli notification changes")
    # As a single interface connects test conductor and test target, test bench send method 
    # can also rely on test target argument to retrieve interface to use for UDP emission
    heartbeat[UDP].payload = Raw(HEARTBEAT_DATA + "1")
    heartbeat_received_criteria.from_now(2000)
    test_conductor.send(heartbeat, test_target=sample_app)
    received_cli_async = test_conductor.sample_cli.wait_io(heartbeat_received_criteria).data
    assert received_cli_async == "[ASYNC]heartbeat 2 received: heartbeat_data1\n"
