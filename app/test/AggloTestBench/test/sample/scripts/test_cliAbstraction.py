############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import re

from fixtures import *
from agglo_tk.utils.test.fixtures import *

from agglo_tk.io import AtkIOCriteria
from agglo_tk.io import AtkIOStringCriteria
from agglo_cc.core.cli_io_types import *
from agglo_tk.io.io_dumper_txt import AtkIODumperTxt


def compare_dump(as_file1, as_file2):
    b_res = True
    py_file1 = open(as_file1, 'r')
    py_file2 = open(as_file2, 'r')

    lines1 = py_file1.readlines()
    lines2 = py_file2.readlines()

    if len(lines1) == len(lines2):
        for i in range(len(lines1)):
            if lines1[i] != lines1[i]:
                b_res = False
                break

    return b_res
    

def test_AtbSampleConfig_AbstractionLevel0_NoAbstraction(sample_cli, io_logs, dump_failed):
    dump_failed.io_logs = io_logs

    # One can use basic AccCliClient send and executeCommand methods, 
    # to either navigate through menus or execute cli commands. In a no abstraction 
    # mode, menu navigation and cli commands are hard coded by tester
    sample_cli.send("configure service heartbeat")
    sample_cli.send("\n")
    assert sample_cli.executeCommand("remote port 456")
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="remote port 456\n").rank(-1))

    # IOLogs check method can be used to verify io
    assert sample_cli.executeCommand("mode listener off")
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command).rank(-1)).data == "mode listener off\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data == "listener mode: disabled\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_prompt).rank(-1)).data == "config_heartbeat>"
    
    assert sample_cli.executeCommand("local port 123")
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="local port 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command_result, reg_expr="local port: 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr="config_heartbeat>").rank(-1))

    # Command format check can also be done through regular expression parameters, 
    # to check final command result and prompt
    assert sample_cli.executeCommand("mode talker on", "talker mode: enabled\n", "config_heartbeat>")
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="mode talker on\n").rank(-1))

    # Execute show methods to check that configuration has been actually modified
    assert sample_cli.executeCommand("exit", expected_prompt="acsSampleCliQt>")
    assert sample_cli.executeCommand("show heartbeat config")
    retrieved_config = io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data
    config_regexpr = "------------------ HeartBeat Config ------------------\n" + \
                     "Remote listener: ([0-9]*)\nLocal listener port: ([0-9]*)\n" + \
                     "Mode listener: (.*)\nMode talker: (.*)\nMode talker interval: ([0-9]*)\n"
    match = re.search(config_regexpr, retrieved_config)
    assert match is not None
    assert match.group(1) == "456"
    assert match.group(2) == "123"
    assert match.group(3) == "off"
    assert match.group(4) == "on"
    assert match.group(5) == "1"

    # Dump io logs to compare with further tests
    acc_io_dumper = AtkIODumperTxt("test_AtbSampleConfig_AbstractionLevel0_NoAbstraction.txt", \
                                io_logs, AtkIOCriteria(io_type=sample_cli.io_type_input))
    acc_io_dumper.start(False)

    
def test_AtbSampleConfig_AbstractionLevel1_CommandFormats(sample_cli, io_logs, dump_failed):
    dump_failed.io_logs = io_logs

    from sample_cli_client import *

    # One can use basic AccCliClient send and executeCommand methods, 
    # to either navigate through menus or execute cli commands. In a no abstraction 
    # mode, menu navigation and cli commands are hard coded by tester
    sample_cli.send(AccMenuConfig_heartbeat.get_enter_command())
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandRemotePort(456))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="remote port 456\n").rank(-1))

    # IOLogs check method can be used to verify io
    # Command name method can be forced by cli xml specification
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandDisable_listener_mode())
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command).rank(-1)).data == "mode listener off\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data == "listener mode: disabled\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_prompt).rank(-1)).data == MENUPROMPT_CONFIG_HEARTBEAT
    
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandLocalPort(123))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="local port 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command_result, reg_expr="local port: 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr=MENUPROMPT_CONFIG_HEARTBEAT).rank(-1))

    # Command format check can also be done through regular expression parameters, 
    # to check final command result and prompt
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandModeTalkerOn(), "talker mode: enabled\n", MENUPROMPT_CONFIG_HEARTBEAT)
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="mode talker on\n").rank(-1))

    # Execute show methods to check that configuration has been actually modified
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat.get_exit_command(), expected_prompt=MENUPROMPT_ROOT)
    assert sample_cli.executeCommand(AccMenuAcsSampleCliQt().getCommandShowHeartbeatConfig())
    retrieved_config = io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data
    config_regexpr = "------------------ HeartBeat Config ------------------\n" + \
                     "Remote listener: ([0-9]*)\nLocal listener port: ([0-9]*)\n" + \
                     "Mode listener: (.*)\nMode talker: (.*)\nMode talker interval: ([0-9]*)\n"
    match = re.search(config_regexpr, retrieved_config)
    assert match is not None
    assert match.group(1) == "456"
    assert match.group(2) == "123"
    assert match.group(3) == "off"
    assert match.group(4) == "on"
    assert match.group(5) == "1"
        
    # Current test abstraction is higher: commands and prompts are not hardcoded 
    # anymore, but are instead generated by custom cli client method, automatically 
    # generated by AggloCliClient.
    # Script is more stable to cli command format modification, and easier to write  
    # and read; yet it generates the same ios, and is equivalent in term of tested behaviour
    # Compare io logs generated by this test to io_logs of previous test. 
    acc_io_dumper = AtkIODumperTxt("test_AtbSampleConfig_AbstractionLevel1_CommandFormats.txt", \
                                io_logs, AtkIOCriteria(io_type=sample_cli.io_type_input))
    acc_io_dumper.start(False)
    assert compare_dump("test_AtbSampleConfig_AbstractionLevel1_CommandFormats.txt", \
                        "test_AtbSampleConfig_AbstractionLevel0_NoAbstraction.txt")

    
def test_AtbSampleConfig_AbstractionLevel2_MenuNavigation(sample_cli, io_logs, dump_failed):
    dump_failed.io_logs = io_logs

    from sample_cli_client import *

    # One can use basic AccCliClient send and executeCommand methods, 
    # to either navigate through menus or execute cli commands. In a no abstraction 
    # mode, menu navigation and cli commands are hard coded by tester
    assert sample_cli.navigate_to_menu(MENUPATH_CONFIG_HEARTBEAT, MENUPROMPT_CONFIG_HEARTBEAT)
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandRemotePort(456))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="remote port 456\n").rank(-1))

    # IOLogs check method can be used to verify io
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandDisable_listener_mode())
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command).rank(-1)).data == "mode listener off\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data == "listener mode: disabled\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_prompt).rank(-1)).data == MENUPROMPT_CONFIG_HEARTBEAT
    
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandLocalPort(123))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="local port 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command_result, reg_expr="local port: 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr=MENUPROMPT_CONFIG_HEARTBEAT).rank(-1))

    # Command format check can also be done through regular expression parameters, 
    # to check final command result and prompt
    assert sample_cli.executeCommand(AccMenuConfig_heartbeat(None).getCommandModeTalkerOn(), "talker mode: enabled\n", MENUPROMPT_CONFIG_HEARTBEAT)
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="mode talker on\n").rank(-1))

    # Execute show methods to check that configuration has been actually modified
    assert sample_cli.navigate_to_menu(MENUPATH_ROOT, MENUPROMPT_ROOT)
    assert sample_cli.executeCommand(AccMenuAcsSampleCliQt().getCommandShowHeartbeatConfig())
    retrieved_config = io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data
    config_regexpr = "------------------ HeartBeat Config ------------------\n" + \
                     "Remote listener: ([0-9]*)\nLocal listener port: ([0-9]*)\n" + \
                     "Mode listener: (.*)\nMode talker: (.*)\nMode talker interval: ([0-9]*)\n"
    match = re.search(config_regexpr, retrieved_config)
    assert match is not None
    assert match.group(1) == "456"
    assert match.group(2) == "123"
    assert match.group(3) == "off"
    assert match.group(4) == "on"
    assert match.group(5) == "1"
        
    # Current test abstraction is higher: navigation between menu is automatically  
    # handled by AggloCliClient.
    # Script is more stable to modification in menu arborescence; yet it generates 
    # the same ios, and is equivalent in term of tested behaviour
    # Compare io logs generated by this test to io_logs of previous test. 
    acc_io_dumper = AtkIODumperTxt("test_AtbSampleConfig_AbstractionLevel2_MenuNavigation.txt", \
                                io_logs, AtkIOCriteria(io_type=sample_cli.io_type_input))
    acc_io_dumper.start(False)
    assert compare_dump("test_AtbSampleConfig_AbstractionLevel2_MenuNavigation.txt", \
                        "test_AtbSampleConfig_AbstractionLevel1_CommandFormats.txt")

                        
def test_AtbSampleConfig_AbstractionLevel3_CommandMethods(sample_cli, io_logs, dump_failed):
    dump_failed.io_logs = io_logs

    from sample_cli_client import *

    # Execute CaccCliClientSample generated method to send navigation and configuration commands
    assert sample_cli.config_heartbeat_RemotePort(456).succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="remote port 456\n").rank(-1))
    
    # Cli client method names can be auto-generated, or specified by user in cli xml.
    # Here, "disable_listener_mode" is a method name specified in sample cli xml, 
    # therefore its syntax doesn't follow the same rule than the other methods, 
    # "config_heartbeat_LocalPort" for instance. This allows this part of the script 
    # to be independant of cli xml modification (menus names, menus and commands 
    # arborescence, commands keywords...)
    assert sample_cli.disable_listener_mode("listener mode: disabled\n").succeeded
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command).rank(-1)).data == "mode listener off\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data == "listener mode: disabled\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_prompt).rank(-1)).data == MENUPROMPT_CONFIG_HEARTBEAT
    
    # IOLogs check method can be used to verify io
    assert sample_cli.config_heartbeat_LocalPort(123).succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="local port 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command_result, reg_expr="local port: 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr=MENUPROMPT_CONFIG_HEARTBEAT).rank(-1))

    # Command format check can also be done through regular expression parameters, 
    # to check final command result and prompt
    assert sample_cli.config_heartbeat_ModeTalkerOn("talker mode: enabled\n", MENUPROMPT_CONFIG_HEARTBEAT).succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="mode talker on\n").rank(-1))

    # Execute show methods to check that configuration has been actually modified
    assert sample_cli.acsSampleCliQt_ShowHeartbeatConfig(expected_prompt=MENUPROMPT_ROOT).succeeded
    retrieved_config = io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data
    config_regexpr = "------------------ HeartBeat Config ------------------\n" + \
                     "Remote listener: ([0-9]*)\nLocal listener port: ([0-9]*)\n" + \
                     "Mode listener: (.*)\nMode talker: (.*)\nMode talker interval: ([0-9]*)\n"
    match = re.search(config_regexpr, retrieved_config)
    assert match is not None
    assert match.group(1) == "456"
    assert match.group(2) == "123"
    assert match.group(3) == "off"
    assert match.group(4) == "on"
    assert match.group(5) == "1"
        
    # Current test abstraction is higher: a single custom cli client method, automatically 
    # generated by AggloCliClient, handles the menu navigation, the command generation and 
    # execution, and the check of final result and prompt. Script coder only has to enter 
    # the parameters of this command (ip address, port...).
    # This method name can be automatically generated by AggloCliClient, from the command 
    # keywords. 
    # But a better solution is to specify in the cli xml the name of the generated method. 
    # This will make the script more stable to both menu arborescence modification and 
    # command format modification
    # Script is more stable to cli menus and command format modification, and easier to write  
    # and read; yet it generates the same ios, and is equivalent in term of tested behaviour
    # Compare io logs generated by this test to io_logs of previous test. 
    acc_io_dumper = AtkIODumperTxt("dump_test_AtbSampleConfig_AbstractionLevel3_CommandMethods.txt", \
                                io_logs, AtkIOCriteria(io_type=sample_cli.io_type_input))
    acc_io_dumper.start(False)
    # assert compare_dump("dump_test_AtbSampleConfig_AbstractionLevel3_CommandMethods.txt", \
                        # "dump_test_AtbSampleConfig_AbstractionLevel2_MenuNavigation.txt")

    
def test_AtbSampleConfig_AbstractionLevel4_Serialization(sample_cli, sample_model_cls, io_logs, dump_failed):
    dump_failed.io_logs = io_logs

    # Instantiate a model object, which inheritates from CaccCliSerializable. 
    # This object defines which command generation methods of Csample_cli 
    # are associated with model attributes
    from sample_cli_client import *
    from sample_model import AccSampleModel
    sample_model = AccSampleModel(sample_cli)
    
    # Force serialization needed property. This is done for tests reasons only, to 
    # ensure that generated suite of commands will be the same than in previous tests
    sample_model.set_serialization_needed("remote_address", "local_port", \
                                          "listener_enabled", "talker_enabled", \
                                          "talker_mode_interval", "heartbeat_data", \
                                          needed=False)
        
    # Modify one or several attributes, and call serialize method to send commands to cli server
    sample_model.remote_port = 456
    assert sample_model.serialize().succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="remote port 456\n").rank(-1))
    
    # One can select in io_logs last entry to check after command execution that command return and prompt are fine
    sample_model.listener_enabled = False
    sample_model.serialize()
    # TODO decaler les check d'IO dans les methodes de serialization AccModel, en modifiant les tests precedents pour
    # que toutes les etapes soient pareillement verifiees
    # TODO utiliser les selecteurs de command_result, plutot qu'une nouvelle instanciation
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command).rank(-1)).data == "mode listener off\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_command_result).rank(-1)).data == "listener mode: disabled\n"
    assert io_logs.check(AtkIOCriteria(io_type=sample_cli.io_type_prompt).rank(-1)).data == MENUPROMPT_CONFIG_HEARTBEAT
   
    # One can also use check method
    # sample_model.local_port = 123
    sample_model.local_port = 123
    assert sample_model.serialize().succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="local port 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command_result, reg_expr="local port: 123\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr=MENUPROMPT_CONFIG_HEARTBEAT).rank(-1))

    # Command format check can also be done through regular expression parameters, 
    # to check final command result and prompt
    # sample_model.talker_enabled = True
    sample_model.talker_enabled = True
    assert sample_model.serialize().succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr=MENUPROMPT_CONFIG_HEARTBEAT).rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command, reg_expr="mode talker o.{1,2}\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_command_result, reg_expr="talker mode: .*\n").rank(-1))

    # Retrieve configuration on server side to check that configuration has been actually modified
    sample_model_server_side = AccSampleModel(sample_cli)
    assert sample_model_server_side.unserialize().succeeded
    assert io_logs.check(AtkIOStringCriteria(io_type=sample_cli.io_type_prompt, reg_expr=MENUPROMPT_ROOT).rank(-1))
    assert sample_model == sample_model_server_side
        
    # Current test abstraction is higher: a custom serializable data model, based on 
    # AggloCliClient serialization classes, handles command execution after one or 
    # several attributes modification.
    # The server side configuration can also be unserialized in such an object, and compared 
    # to local configuration data, to make sure that server configuration is fine.
    # Script is more stable to cli modification, since every command generation methods can be 
    # gathered in one or several model classes.  
    # Script is also easier to write and read; yet it generates the same ios, and is 
    # equivalent in term of tested behaviour
    # Compare io logs generated by this test to io_logs of previous test. 
    acc_io_dumper = AtkIODumperTxt("test_AtbSampleConfig_AbstractionLevel4_Serialization.txt", \
                                io_logs, AtkIOCriteria(io_type=sample_cli.io_type_input))
    acc_io_dumper.start(False)
    assert compare_dump("test_AtbSampleConfig_AbstractionLevel4_Serialization.txt", \
                        "dump_test_AtbSampleConfig_AbstractionLevel3_CommandMethods.txt")

