############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


import pytest
from collections import Counter

from agglo_tb.core.test_bench import AtbTestBench
from agglo_tb.core.host import AtbHost
from agglo_tb.core.target import AtbTarget
from agglo_tb.core.host_interface import AtbHostInterface
from agglo_tb.core.node import AtbNode

from agglo_tb.exceptions import AtbError
from agglo_tb.exceptions import AtbTestBenchError
from agglo_tk.exceptions import AtkLimitedAccessError

from stubs import AtbStubInterfaceConnection

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(5)
def test_UT_AtbTestBench_Constructor(trace_test_start):
    #UnitTestComment("AtbTestBench::constructor")
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    assert not test_bench.targets
    assert conductor is not None
    assert conductor in test_bench
    assert 1 == len(test_bench.hosts)
    assert test_bench.hosts[0] is conductor


def test_UT_AtbTestBench_Properties(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor

    #UnitTestComment("AtbTestBench(): check hosts belonging to test_bench are correctly retrieved")
    for i in range(4):
        target = AtbTarget("Target" + str(i + 1))
        test_bench.add_target(target)
        assert test_bench.targets[i] is target
        assert test_bench.hosts[0] is conductor
        assert test_bench.hosts[i + 1] is target

    #UnitTestComment("AtbTestBench(): check nodes belonging to test_bench are correctly retrieved")
    conductor.add_interface(AtbStubInterfaceConnection("Itf1"))
    for i in range(4):
        node = test_bench.connect(conductor, node_name="Node" + str(i + 1))
        assert node in test_bench.nodes


def test_UT_AtbTestBench_BuiltIn(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor

    #UnitTestComment("AtbTestBench(): check that in builtin checks that an target belongs to the test_bench")
    for i in range(4):
        target = AtbTarget("Target" + str(i + 1))
        test_bench.add_target(target)
        assert conductor in test_bench
        assert target in test_bench


def test_UT_AtbTestBench_AddTargetErrorCases(trace_test_start):
    test_bench = AtbTestBench()
    target1 = AtbTarget("Target1")
    target1_bis = AtbTarget("Target1")
    host_conductor = AtbHost("conductor")

    #UnitTestComment("AtbTestBench::add_target(): check that a test_bench cannot have 2 target with same name")
    test_bench.add_target(target1)
    with pytest.raises(AtkLimitedAccessError) as exception:
        test_bench.add_target(target1_bis)
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert 1 == len(test_bench.targets)
    
    #UnitTestComment("AtbTestBench::add_target(): check that a test_bench cannot have a target with conductor's name")
    with pytest.raises(AtkLimitedAccessError) as exception:
        test_bench.add_target(host_conductor)
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert 1 == len(test_bench.targets)

    
def test_UT_AtbTestBench_AddTarget(trace_test_start):
    test_bench = AtbTestBench()

    #UnitTestComment("AtbTestBench::add_target(): check that new target is aggregated to former target list")
    for i in range(3):
        test_bench.add_target(AtbTarget("Target" + str(i + 1)))
        assert (i + 1) == len(test_bench.targets)
        for j in range(i + 1):
            assert ("Target" + str(j + 1)) == test_bench.targets[j].name
            
    #UnitTestComment("AtbTestBench::add_target(): check that new target is referenced by test test_bench by its name")
    assert "Target1" == test_bench.Target1.name
    assert "Target3" == test_bench.Target3.name

    #UnitTestComment("AtbTestBench.add_target(): check that adding a target twice has no effect")
    test_bench.add_target(test_bench.Target1)
    assert 3 == len(test_bench.targets)


def test_UT_AtbTestBench_ConnectErrorCases(trace_test_start):
    test_bench = AtbTestBench()
    target_interface1 = AtbHostInterface("Itf1")
    target_interface2 = AtbHostInterface("Itf2")
    target = AtbTarget("Target")
    node = AtbNode("Node")

    #UnitTestComment("AtbTestBench::connect(): check that interface to connect must belong to an host")
    with pytest.raises(AtbError) as exception:
        test_bench.connect(target_interface1, node_name="TestNode")
    assert str(exception.value) == AtbError.UNKNOWN
    assert not test_bench.nodes

    #UnitTestComment("AtbTestBench::connect(): check that the node must belong to the test_bench")
    target.add_interface(target_interface1)
    with pytest.raises(AtbError) as exception:
        test_bench.connect(target_interface1, node=node)
    assert str(exception.value) == AtbError.UNKNOWN
    assert not test_bench.nodes

    #UnitTestComment("AtbNode::connect(): check that test item to connect are needed")
    with pytest.raises(AtbError) as exception:
        test_bench.connect(node_name="TestNode2")
    assert str(exception.value) == AtbError.EMPTY
    assert not test_bench.nodes

    # UnitTestComment("AtbTestBench::connect(): check that first error is raised")
    with pytest.raises(AtbError) as exception:
        test_bench.connect(target_interface2, node=node)
    assert str(exception.value) == AtbError.UNKNOWN


def test_UT_AtbTestBench_ConnectInterfaces(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface1 = AtbStubInterfaceConnection("Itf1")
    conductor_interface2 = AtbStubInterfaceConnection("Itf2")
    target_interface1 = AtbHostInterface("Itf1")
    target_interface2 = AtbHostInterface("Itf2")
    target_interface3 = AtbHostInterface("Itf3")
    unknown_interface1 = AtbHostInterface("Itf1")
    target = AtbTarget("Target")
    unknown_target = AtbTarget("UnknownTarget")

    # Create test test_bench
    conductor.add_interface(conductor_interface1)
    conductor.add_interface(conductor_interface2)
    target.add_interface(target_interface1)
    target.add_interface(target_interface2)
    test_bench.add_target(target)

    #UnitTestComment("AtbTestBench::connect(): check connection of interfaces on a new node")
    node = test_bench.connect(conductor_interface2, target_interface1, node_name="TestNode1")
    assert node is test_bench.TestNode1
    assert conductor_interface2 in test_bench.TestNode1
    assert target_interface1 in test_bench.TestNode1
    assert not conductor_interface1 in test_bench.TestNode1

    #UnitTestComment("AtbTestBench.add_target(): check that connecting an interface twice has no effect")
    node = test_bench.connect(conductor_interface2, node_name="TestNode1")
    assert node is test_bench.TestNode1
    assert len(node.interfaces) == 2
    
    #UnitTestComment("AtbTestBench::connect(): check that an interface can be connected to several node")
    node = test_bench.connect(target_interface1, node_name="TestNode2")
    assert node is test_bench.TestNode2
    assert test_bench.Target.Itf1 in test_bench.TestNode1
    assert test_bench.Target.Itf1 in test_bench.TestNode2
    
    #UnitTestComment("AtbTestBench::connect(): check that an interface can be connected on an existing node")
    node = test_bench.connect(target_interface2, node_name="TestNode2")
    assert node is test_bench.TestNode2
    assert test_bench.Target.Itf2 in test_bench.TestNode2
    
    #UnitTestComment("AtbTestBench::connect(): check that node instance can be used to connect an interface on an existing node")
    target.add_interface(target_interface3)
    node = test_bench.connect(target_interface3, node=node)
    assert node is test_bench.TestNode2
    assert test_bench.Target.Itf3 in test_bench.TestNode2
    
    #UnitTestComment("AtbTestBench::connect(): check that a new node is created when neither node nor node name are specified")
    node = test_bench.connect(target_interface3)
    assert node is not None
    assert node is not test_bench.TestNode1
    assert node is not test_bench.TestNode2
    assert node in test_bench
    assert test_bench.Target.Itf3 in node

    #UnitTestComment("AtbTestBench::connect(): check that interface's host is added to test_bench when unknown")
    unknown_target.add_interface(unknown_interface1)
    node = test_bench.connect(unknown_interface1, node_name="TestNode3")
    assert unknown_target in test_bench.targets
    assert node is test_bench.TestNode3


def test_UT_AtbTestBench_ConnectTargets(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface1 = AtbStubInterfaceConnection("Itf1")
    conductor_interface2 = AtbStubInterfaceConnection("Itf2")
    target_interface1 = AtbHostInterface("Itf1")
    target_interface2 = AtbHostInterface("Itf2")
    unknown_interface1 = AtbHostInterface("Itf1")
    target1 = AtbTarget("Target1")
    unknown_target = AtbTarget("UnknownTarget")
    empty_target = AtbTarget("EmptyTarget")

    # Create test test_bench
    conductor.add_interface(conductor_interface1)
    conductor.add_interface(conductor_interface2)
    target1.add_interface(target_interface1)
    target1.add_interface(target_interface2)
    test_bench.add_target(target1)

    #UnitTestComment("AtbTestBench::connect(): check connection of targets on a new node")
    node = test_bench.connect(conductor, target1, node_name="TestNode1")
    assert node is test_bench.TestNode1
    assert target_interface1 in test_bench.TestNode1
    assert target_interface2 in test_bench.TestNode1
    assert conductor_interface1 in test_bench.TestNode1
    assert conductor_interface2 in test_bench.TestNode1

    #UnitTestComment("AtbTestBench.connect(): check that connecting a target twice has no effect")
    node = test_bench.connect(target1, node_name="TestNode1")
    assert node is test_bench.TestNode1
    assert len(node.hosts) == 2
    assert len(node.interfaces) == 4

    #UnitTestComment("AtbTestBench::connect(): check that target is added to test_bench when unknown")
    unknown_target.add_interface(unknown_interface1)
    node = test_bench.connect(unknown_target, node_name="TestNode2")
    assert unknown_target in test_bench.targets
    assert node is test_bench.TestNode2

    #UnitTestComment("AtbTestBench::connect(): check that a node with empty target can be created")
    test_bench.connect(empty_target, node_name="EmptyNode")
    assert len(test_bench.EmptyNode.hosts) == 1
    assert not test_bench.EmptyNode.interfaces


def test_UT_AtbTestBench_IsConnectedErrorCases(trace_test_start):
    test_bench = AtbTestBench()
    interface = AtbHostInterface("Itf1")
    target = AtbTarget("Target")

    #UnitTestComment("AtbTestBench::is_connected(): check that test_item1 and 2 must be different")
    with pytest.raises(AtbTestBenchError) as exception:
        test_bench.is_connected(interface, interface)
    assert str(exception.value) == AtbTestBenchError.SAME_ITEM

    #UnitTestComment("AtbTestBench::is_connected(): check that test_item1 and 2 cannot be an interface and its target")
    target.add_interface(interface)
    test_bench.add_target(target)
    with pytest.raises(AtbError) as exception:
        test_bench.is_connected(target, interface)
    assert str(exception.value) == AtbTestBenchError.HOST_INTERFACE
    with pytest.raises(AtbError) as exception:
        test_bench.is_connected(interface, target)
    assert str(exception.value) == AtbTestBenchError.HOST_INTERFACE

#     #UnitTestComment("AtbTestBench::is_connected(): check that the node must belong to the test_bench")
#     with pytest.raises(AtbError) as exception:
#         test_bench.is_connected(node)
#     assert str(exception.value) == AtbError.UNKNOWN

    # TODO si on fait is_connected(target, target.itf), on return True ou exception?


def test_UT_AtbTestBench_IsConnected(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface1 = AtbStubInterfaceConnection("Itf1")
    conductor_interface2 = AtbStubInterfaceConnection("Itf2")
    target_interface1_1 = AtbHostInterface("Itf1")
    target_interface1_2 = AtbHostInterface("Itf2")
    target_interface2_1 = AtbHostInterface("Itf1")
    target1 = AtbTarget("Target1")
    target2 = AtbTarget("Target2")
   
   # TODO deplacer dans builtin ?
   #  #UnitTestComment("AtbTestBench::is_connected(): check that conductor is by default connected to test_bench")
   #  assert test_bench.is_connected(conductor)

   #  #UnitTestComment("AtbTestBench::is_connected(): check that unregistered items are not connected to test_bench")
   #  target1.add_interface(target_interface1_1)
   #  assert not test_bench.is_connected(target_interface1_1)
   #  assert not test_bench.is_connected(target1)

   #  #UnitTestComment("AtbTestBench::is_connected(): check that registered items are connected to test_bench")
   #  test_bench.add_target(target1)
   #  assert test_bench.is_connected(target_interface1_1)
   #  assert test_bench.is_connected(target1)
    
   #  #UnitTestComment("AtbTestBench::is_connected(): check that a target modification is detected")
   #  # TODO c'est ca qu'on veut?. L'ajout des proxy risque de compliquer ca
   #  target1.add_interface(target_interface1_2)
   #  assert test_bench.is_connected(target_interface1_2)

    target1.add_interface(target_interface1_1)
    test_bench.add_target(target1)
    target1.add_interface(target_interface1_2)
    #UnitTestComment("AtbTestBench::is_connected(): check that node connects targets and interfaces")
    conductor.add_interface(conductor_interface1)
    target2.add_interface(target_interface2_1)
    test_bench.add_target(target2)
    assert not test_bench.is_connected(conductor, target_interface1_1)
    test_bench.connect(target_interface1_1, conductor, target_interface2_1, node_name="TestNode1")
    assert test_bench.is_connected(conductor, target_interface1_1)
    assert test_bench.is_connected(target_interface1_1, target_interface2_1)
    assert test_bench.is_connected(conductor, target_interface2_1)
    assert test_bench.is_connected(test_bench.TestNode1, conductor)
    assert test_bench.is_connected(target_interface1_1, test_bench.TestNode1)
    assert not test_bench.is_connected(target_interface2_1, target_interface1_2)
    
    #UnitTestComment("AtbTestBench::is_connected(): check that interface connection implies target connection")
    assert test_bench.is_connected(conductor, target1)
    assert test_bench.is_connected(target2, conductor)
    assert test_bench.is_connected(target1, target2)
    assert test_bench.is_connected(target1, test_bench.TestNode1)

    #UnitTestComment("AtbTestBench::is_connected(): check that a node modification is detected")
    # TODO c'est ca qu'on veut?. L'ajout des proxy risque de compliquer ca
    conductor.add_interface(conductor_interface2)
    assert not test_bench.is_connected(test_bench.TestNode1, conductor_interface2)
    test_bench.TestNode1.connect(conductor_interface2)
    assert test_bench.is_connected(test_bench.TestNode1, conductor_interface2)

    #UnitTestComment("AtbTestBench::is_connected(): check that 2 interfaces of same target are not connected by default")
    assert not test_bench.is_connected(target_interface1_1, target_interface1_2)
    test_bench.TestNode1.connect(target_interface1_2)
    assert test_bench.is_connected(target_interface1_1, target_interface1_2)
    
    #UnitTestComment("AtbTestBench::is_connected(): check that 2 nodes connected with same interface are not connected")
    # TODO c'est ca qu'on veut?
    test_bench.connect(conductor_interface2, node_name="TestNode2")
    assert test_bench.is_connected(test_bench.TestNode2, conductor_interface2)
    assert not test_bench.is_connected(test_bench.TestNode2, test_bench.TestNode1)


def test_UT_AtbTestBench_GetConductorInterfacesErrorCases(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface = AtbStubInterfaceConnection("Itf1")

    #UnitTestComment("AtbTestBench::get_conductor_interfaces(): check that the item cannot be a conductor item")
    conductor.add_interface(conductor_interface)
    with pytest.raises(AtbTestBenchError) as exception:
        test_bench.get_conductor_interfaces(conductor_interface)
    assert str(exception.value) == AtbTestBenchError.HOST_INTERFACE
    with pytest.raises(AtbTestBenchError) as exception:
        test_bench.get_conductor_interfaces(conductor)
    assert str(exception.value) == AtbTestBenchError.SAME_ITEM


def test_UT_AtbTestBench_GetConductorInterfaces(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface1 = AtbStubInterfaceConnection("CdtItf1")
    conductor_interface2 = AtbStubInterfaceConnection("CdtItf2")
    conductor_interface3 = AtbStubInterfaceConnection("CdtItf3")
    target_interface1_1 = AtbHostInterface("Itf1_1")
    target_interface1_2 = AtbHostInterface("Itf1_2")
    target_interface2_1 = AtbHostInterface("Itf2_1")
    target1 = AtbTarget("Target1")
    target2 = AtbTarget("Target2")

    #UnitTestComment("AtbTestBench::get_conductor_interfaces(): check that item not belonging to the test_bench is not connected")
    target1.add_interface(target_interface1_1)
    assert not test_bench.get_conductor_interfaces(target_interface1_1)
    assert not test_bench.get_conductor_interfaces(target1)
    assert not test_bench.get_conductor_interfaces(AtbNode("Node"))

    #UnitTestComment("AtbTestBench::get_conductor_interfaces(): check that no interface is connected to conductor by default")
    test_bench.add_target(target1)
    assert not test_bench.get_conductor_interfaces(target_interface1_1)
    assert not test_bench.get_conductor_interfaces(target1)

    #UnitTestComment("AtbTestBench::get_conductor_interfaces(): check that interfaces connected to conductor are retrieved")
    target1.add_interface(target_interface1_2)
    target2.add_interface(target_interface2_1)
    conductor.add_interface(conductor_interface1)
    test_bench.connect(target_interface1_2, conductor_interface1, node_name="TestNode1")
    assert not test_bench.get_conductor_interfaces(target_interface1_1)
    assert test_bench.get_conductor_interfaces(target_interface1_2) == [conductor_interface1]
    assert test_bench.get_conductor_interfaces(target1) == [conductor_interface1]
    assert test_bench.get_conductor_interfaces(test_bench.TestNode1) == [conductor_interface1]

    #UnitTestComment("AtbTestBench::get_conductor_interfaces(): check that several conductor interfaces can be retrieved")
    test_bench.add_target(target2)
    conductor.add_interface(conductor_interface2)
    test_bench.connect(target_interface1_2, target_interface2_1, conductor_interface2, node_name="TestNode2")
    assert test_bench.get_conductor_interfaces(target_interface1_2) == [conductor_interface1, conductor_interface2]
    assert test_bench.get_conductor_interfaces(target_interface2_1) == [conductor_interface2]
    assert test_bench.get_conductor_interfaces(target1) == [conductor_interface1, conductor_interface2]
    assert test_bench.get_conductor_interfaces(target2) == [conductor_interface2]
    assert test_bench.get_conductor_interfaces(test_bench.TestNode2) == [conductor_interface2]

    #UnitTestComment("AtbTestBench::get_conductor_interfaces(): check that node modification can modify interfaces retrieved")
    conductor.add_interface(conductor_interface3)
    test_bench.connect(conductor_interface3, node_name="TestNode2")
    assert test_bench.get_conductor_interfaces(target_interface2_1) == [conductor_interface2, conductor_interface3]
    assert test_bench.get_conductor_interfaces(target2) == [conductor_interface2, conductor_interface3]
    assert test_bench.get_conductor_interfaces(test_bench.TestNode2) == [conductor_interface2, conductor_interface3]


def test_UT_AtbTestBench_GetConnectedInterfaces(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface1 = AtbStubInterfaceConnection("CdtItf1")
    conductor_interface2 = AtbStubInterfaceConnection("CdtItf2")
    conductor_interface3 = AtbStubInterfaceConnection("CdtItf3")
    target_interface1_1 = AtbHostInterface("Itf1_1")
    target_interface1_2 = AtbHostInterface("Itf1_2")
    target_interface2_1 = AtbHostInterface("Itf2_1")
    target_interface2_2 = AtbHostInterface("Itf2_2")
    target1 = AtbTarget("Target1")
    target2 = AtbTarget("Target2")

    #UnitTestComment("AtbTestBench::get_connected_interfaces(): check that item not belonging to the test_bench is not connected")
    target1.add_interface(target_interface1_1)
    assert not test_bench.get_conductor_interfaces(target_interface1_1)
    assert not test_bench.get_conductor_interfaces(target1)
    assert not test_bench.get_conductor_interfaces(AtbNode("Node"))

    #UnitTestComment("AtbTestBench::get_connected_interfaces(): check that no interface is connected to conductor by default")
    conductor.add_interface(conductor_interface1)
    test_bench.add_target(target1)
    assert not test_bench.get_connected_interfaces(conductor)

    #UnitTestComment("AtbTestBench::get_connected_interfaces(): check that interfaces connected are retrieved")
    target1.add_interface(target_interface1_2)
    target2.add_interface(target_interface2_1)
    test_bench.connect(conductor_interface1, target_interface1_2, node_name="TestNode1")
    assert not test_bench.get_connected_interfaces(target_interface1_1)
    assert test_bench.get_connected_interfaces(target_interface1_2) == [conductor_interface1]
    assert test_bench.get_connected_interfaces(target1) == [conductor_interface1]
    assert test_bench.get_connected_interfaces(conductor_interface1) == [target_interface1_2]
    assert test_bench.get_connected_interfaces(conductor) == [target_interface1_2]
    assert test_bench.get_connected_interfaces(test_bench.TestNode1) == [conductor_interface1, target_interface1_2]

    #UnitTestComment("AtbTestBench::get_connected_interfaces(): check that several interfaces can be retrieved")
    test_bench.add_target(target2)
    conductor.add_interface(conductor_interface2)
    test_bench.connect(conductor_interface2, target_interface1_2, target_interface2_1, node_name="TestNode2")
    assert test_bench.get_connected_interfaces(target_interface1_2) == [conductor_interface1, conductor_interface2, \
                                                                      target_interface2_1]
    assert test_bench.get_connected_interfaces(target_interface2_1) == [conductor_interface2, target_interface1_2]
    assert test_bench.get_connected_interfaces(target1) == [conductor_interface1, conductor_interface2, target_interface2_1]
    assert test_bench.get_connected_interfaces(target2) == [conductor_interface2, target_interface1_2]
    assert test_bench.get_connected_interfaces(test_bench.TestNode2) == [conductor_interface2, target_interface1_2, \
                                                                     target_interface2_1]
    assert Counter(test_bench.get_connected_interfaces(test_bench.TestNode2)) == Counter(test_bench.TestNode2.interfaces)

    #UnitTestComment("AtbTestBench::get_connected_interfaces(): check that node modification can modify interfaces retrieved")
    conductor.add_interface(conductor_interface3)
    target2.add_interface(target_interface2_2)
    test_bench.connect(conductor_interface3, target_interface2_2, node_name="TestNode2")
    assert test_bench.get_connected_interfaces(target_interface2_1) == [conductor_interface2, conductor_interface3, \
                                                                      target_interface1_2, target_interface2_2]
    assert test_bench.get_connected_interfaces(target2) == [conductor_interface2, conductor_interface3, target_interface1_2]
    assert test_bench.get_connected_interfaces(test_bench.TestNode2) == [conductor_interface2, conductor_interface3, \
                                                                     target_interface1_2, target_interface2_1, \
                                                                     target_interface2_2]
    assert Counter(test_bench.get_connected_interfaces(test_bench.TestNode2)) == Counter(test_bench.TestNode2.interfaces)


def test_UT_AtbTestBench_GetConnectedHosts(trace_test_start):
    test_bench = AtbTestBench()
    conductor = test_bench.conductor
    conductor_interface1 = AtbStubInterfaceConnection("CdtItf1")
    conductor_interface2 = AtbStubInterfaceConnection("CdtItf2")
    conductor_interface3 = AtbStubInterfaceConnection("CdtItf3")
    target_interface1_1 = AtbHostInterface("Itf1_1")
    target_interface1_2 = AtbHostInterface("Itf1_2")
    target_interface2_1 = AtbHostInterface("Itf2_1")
    target_interface2_2 = AtbHostInterface("Itf2_2")
    target_interface3_1 = AtbHostInterface("Itf3_1")
    target1 = AtbTarget("Target1")
    target2 = AtbTarget("Target2")
    target3 = AtbTarget("Target3")

    #UnitTestComment("AtbTestBench::get_connected_hosts(): check that item not belonging to the test_bench is not connected")
    target1.add_interface(target_interface1_1)
    assert not test_bench.get_connected_hosts(target_interface1_1)
    assert not test_bench.get_connected_hosts(target1)
    assert not test_bench.get_connected_hosts(AtbNode("Node"))

    #UnitTestComment("AtbTestBench::get_connected_hosts(): check that no target is connected to conductor by default")
    conductor.add_interface(conductor_interface1)
    test_bench.add_target(target1)
    assert not test_bench.get_connected_hosts(conductor)

    #UnitTestComment("AtbTestBench::get_connected_hosts(): check that hosts connected are retrieved")
    target1.add_interface(target_interface1_2)
    target2.add_interface(target_interface2_1)
    test_bench.connect(conductor_interface1, target_interface1_2, node_name="TestNode1")
    assert not test_bench.get_connected_hosts(target_interface1_1)
    assert test_bench.get_connected_hosts(target_interface1_2) == [conductor]
    assert test_bench.get_connected_hosts(target1) == [conductor]
    assert test_bench.get_connected_hosts(conductor_interface1) == [target1]
    assert test_bench.get_connected_hosts(conductor) == [target1]
    assert test_bench.get_connected_hosts(test_bench.TestNode1) == [conductor, target1]

    #UnitTestComment("AtbTestBench::get_connected_hosts(): check that several hosts can be retrieved")
    test_bench.add_target(target2)
    conductor.add_interface(conductor_interface2)
    test_bench.connect(conductor_interface2, target_interface1_2, target_interface2_1, node_name="TestNode2")
    assert test_bench.get_connected_hosts(target_interface1_2) == [conductor, target2]
    assert test_bench.get_connected_hosts(target_interface2_1) == [conductor, target1]
    assert test_bench.get_connected_hosts(target1) == [conductor, target2]
    assert test_bench.get_connected_hosts(target2) == [conductor, target1]
    assert test_bench.get_connected_hosts(test_bench.TestNode2) == [conductor, target1, target2]
    assert Counter(test_bench.get_connected_hosts(test_bench.TestNode2)) == Counter(test_bench.TestNode2.hosts)

    #UnitTestComment("AtbTestBench::get_connected_hosts(): check that node modification can modify hosts retrieved")
    target3.add_interface(target_interface3_1)
    test_bench.add_target(target3)
    test_bench.connect(target_interface3_1, node_name="TestNode2")
    assert test_bench.get_connected_hosts(target_interface2_1) == [conductor, target1, target3]
    assert test_bench.get_connected_hosts(target2) == [conductor, target1, target3]
    assert test_bench.get_connected_hosts(test_bench.TestNode2) == [conductor, target1, target2, target3]
    assert Counter(test_bench.get_connected_hosts(test_bench.TestNode2)) == Counter(test_bench.TestNode2.hosts)
