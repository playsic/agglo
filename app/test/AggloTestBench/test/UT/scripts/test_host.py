############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


import pytest

from agglo_tb.core.host import AtbHost
from agglo_tb.core.host_interface import AtbHostInterface

from agglo_tk.exceptions import AtkLimitedAccessError
from agglo_tb.exceptions import AtbError
from agglo_tb.exceptions import AtbHostError

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(2)
def test_UT_AtbHost_Constructor(trace_test_start):
    #UnitTestComment("AtbHost.AtbHost(InterfaceName)")
    host = AtbHost("Target1")
    assert "Target1" == host.name
    assert not host.interfaces


def test_UT_AtbHost_BuiltIn(trace_test_start):
    host = AtbHost("Target1")

    #UnitTestComment("AtbHost(): check that in builtin checks that an interface belongs to an host")
    for i in range(4):
        interface = AtbHostInterface("Itf" + str(i + 1))
        host.add_interface(interface)
        assert interface in host

    #UnitTestComment("AtbHost(): check that next builtin iterates on host's interfaces")
    for i, interface in enumerate(host):
        assert interface in host.interfaces
    assert (i + 1) == len(host.interfaces)            


def test_UT_AtbHost_AddInterfaceErrorCases(trace_test_start):
    host1 = AtbHost("Target1")
    host2 = AtbHost("Target2")
    interface1 = AtbHostInterface("Itf1")
    interface1_bis = AtbHostInterface("Itf1")
    
    #UnitTestComment("AtbHost.add_interface(): check that an host cannot have 2 interfaces with same name")
    host1.add_interface(interface1)
    with pytest.raises(AtkLimitedAccessError) as exception:
        host1.add_interface(interface1_bis)
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert 1 == len(host1.interfaces)
            
    #UnitTestComment("AtbHost.add_interface(): check that an interface cannot be added to 2 different hosts")
    with pytest.raises(AtbHostError) as exception:
        host2.add_interface(interface1)
    assert str(exception.value) == AtbHostError.CONFLICT
    assert not host2.interfaces


def test_UT_AtbHost_AddInterface(trace_test_start):
    host = AtbHost("Target")

    #UnitTestComment("AtbHost.add_interface(): check that new interface is aggregated to former interface list")
    for i in range(3):
        host.add_interface(AtbHostInterface("Itf" + str(i + 1)))
        assert (i + 1) == len(host.interfaces)
        for j in range(i + 1):
            assert ("Itf" + str(j + 1)) == host.interfaces[j].name
        # TODO tester itf in host 

    #UnitTestComment("AtbHost.add_interface(): check that new interface is referenced by host by its name")
    assert "Itf1" == host.Itf1.name
    assert "Itf3" == host.Itf3.name

    #UnitTestComment("AtbHost.add_interface(): check that adding an interface twice has no effect")
    host.add_interface(host.Itf1)
    assert 3 == len(host.interfaces)
   


