############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


import pytest

from agglo_tb.core.node import AtbNode
from agglo_tb.core.host import AtbHost
from agglo_tb.core.host_interface import AtbHostInterface

from agglo_tb.exceptions import AtbError
from agglo_tk.exceptions import AtkLimitedAccessError

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(3)
def test_UT_AtbNode_Constructor(trace_test_start):
    #UnitTestComment("AtbNode::constructor")
    node = AtbNode("Node1")
    assert "Node1" == node.name
    assert not node.interfaces
    assert not node.hosts


def test_UT_AtbNode_BuiltIn(trace_test_start):
    node = AtbNode("Node")

    #UnitTestComment("AtbNode(): check that in builtin checks that an interface belongs to a node")
    for i in range(3):
        host = AtbHost("Target" + str(i))
        interface = AtbHostInterface("Itf" + str(i))
        host.add_interface(interface)
        assert interface not in node
        assert host not in node
        node.connect(interface)
        assert interface in node
        assert host in node

    #UnitTestComment("AtbNode(): check that next builtin iterates on node's interfaces")
    for i, interface in enumerate(node):
        assert interface in node.interfaces
    assert (i + 1) == len(node.interfaces)            


def test_UT_AtbNode_Properties(trace_test_start):
    node = AtbNode("Node1")
    interface1_1 = AtbHostInterface("Itf1")
    interface1_2 = AtbHostInterface("Itf2")
    interface2_1 = AtbHostInterface("Itf1")
    interface3_1 = AtbHostInterface("Itf1")
    target1 = AtbHost("Target1")
    target2 = AtbHost("Target2")
    target3 = AtbHost("Target3")

    #UnitTestComment("AtbNode::properties: check name property
    assert node.name == "Node1"

    #UnitTestComment("AtbNode::properties: check interfaces belonging to node are correctly retrieved
    target1.add_interface(interface1_1)
    target1.add_interface(interface1_2)
    target2.add_interface(interface2_1)
    assert interface1_1 not in node.interfaces
    node.connect(interface1_1)
    assert interface1_1 in node.interfaces
    assert interface1_2 not in node.interfaces
    node.connect(interface1_2)
    assert interface1_2 in node.interfaces
    assert interface2_1 not in node.interfaces
    node.connect(interface2_1)
    assert interface2_1 in node.interfaces

    #UnitTestComment("AtbNode::properties: check hosts belonging to node are correctly retrieved
    assert target1 in node.hosts
    assert target2 in node.hosts
    assert len(node.hosts) == 2
    target3.add_interface(interface3_1)
    node.connect(interface3_1)
    assert target3 in node.hosts


def test_UT_AtbNode_ConnectErrorCases(trace_test_start):
    node = AtbNode("Node1")
    host1 = AtbHost("Target1")
    host1_bis = AtbHost("Target1")
    empty_host = AtbHost("TargetEmpty")
    interface1 = AtbHostInterface("Itf1")
    interface1_bis = AtbHostInterface("Itf1_bis")

    #UnitTestComment("AtbNode::connect(): check that test item to connect are needed")
    with pytest.raises(AtbError) as exception:
        node.connect()
    assert str(exception.value) == AtbError.EMPTY
    assert not node.interfaces
    assert not node.hosts

    #UnitTestComment("AtbNode::connect(): check that an interface cannot be connected if it doesn't belong to an host")
    with pytest.raises(AtbError) as exception:
        node.connect(interface1)
    assert str(exception.value) == AtbError.UNKNOWN
    assert not node.interfaces
    assert not node.hosts

    #UnitTestComment("AtbNode::connect(): check that different hosts with same name cannot be connected to node")
    host1.add_interface(interface1)
    host1_bis.add_interface(interface1_bis)
    node.connect(interface1)
    assert 1 == len(node.hosts)
    with pytest.raises(AtkLimitedAccessError) as exception:
        node.connect(interface1_bis)
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert 1 == len(node.hosts)


def test_UT_AtbNode_Connect(trace_test_start):
    node1 = AtbNode("Node1")
    node2 = AtbNode("Node2")
    node3 = AtbNode("Node3")
    host1 = AtbHost("Target1")
    host2 = AtbHost("Target2")
    host1_bis = AtbHost("Target1")
    interface1_1 = AtbHostInterface("Itf1")
    interface2_1 = AtbHostInterface("Itf1")
    interface2_2 = AtbHostInterface("Itf2")
    interface2_3 = AtbHostInterface("Itf3")
    interface2_4 = AtbHostInterface("Itf4")

    #UnitTestComment("AtbNode::connect(): check connection of interfaces")
    host1.add_interface(interface1_1)
    host2.add_interface(interface2_2)
    node1.connect(interface1_1, interface2_2)
    assert interface1_1 in node1
    assert interface2_2 in node1
    assert interface2_1 not in node1
    assert host1 in node1
    assert host2 in node1
            
    #UnitTestComment("AtbNode::connect(): check that connecting an interface twice has no effect")
    node1.connect(interface1_1, interface2_2)
    assert 2 == len(node1.interfaces)

    #UnitTestComment("AtbNode::connect(): check hosts and interfaces referencement by new node")
    assert host1 in [node1.Target1]
    assert host2 in [node1.Target2]
    assert interface1_1 in [node1.Target1.Itf1]
    assert interface2_2 in [node1.Target2.Itf2]
            
    #UnitTestComment("AtbNode::connect(): check that an interface can be connected to several nodes")
    node2.connect(interface1_1)
    assert interface1_1 in node1
    assert interface1_1 in node2

    #UnitTestComment("AtbNode::connect(): check connection of hosts")
    host2.add_interface(interface2_3)
    node3.connect(host1, host2)
    assert interface1_1 in node3
    assert interface2_2 in node3
    assert interface2_3 in node3
    assert interface2_1 not in node3
    assert host1 in node3
    assert host2 in node3
    assert interface2_3 in [node3.Target2.Itf3]
            
    #UnitTestComment("AtbNode::connect(): check that connection an host twice does'nt fails but instead completes the node")
    host2.add_interface(interface2_4)
    assert 3 == len(node3.interfaces)
    node3.connect(host2)
    assert interface2_4 in node3
    assert 4 == len(node3.interfaces)
       
    #UnitTestComment("AtbNode::connect(): check that node can contain 2 interfaces with same name if they belong to different hosts")
    host2.add_interface(interface2_1)
    node3.connect(interface2_1)
    assert interface2_1 in [node3.Target2.Itf1]
    assert interface1_1 in [node3.Target1.Itf1]
    assert node3.Target2.Itf1.name == node3.Target1.Itf1.name
    assert node3.Target2.Itf1 not in [node3.Target1.Itf1]
