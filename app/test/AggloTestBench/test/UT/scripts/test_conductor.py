############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from time import sleep

import pytest

from agglo_tk.exceptions import AtkIODeviceError

from agglo_tb.core.conductor import AtbConductor
from agglo_tb.core.host_interface import AtbHostInterface
from agglo_tb.core.target import AtbTarget
from agglo_tb.exceptions import AtbError
from agglo_tb.exceptions import AtbConductorError
from agglo_tk.exceptions import AtkIODeviceError
from agglo_tk.exceptions import AtkLimitedAccessError

from stubs import AtbStubPlatform
from stubs import AtbStubCliClient
from stubs import AtbStubInterfaceConnection

from fixtures import test_cleaner
from agglo_tt.fixtures import trace_test_start

@pytest.suite_rank(4)
def test_UT_AtbConductor_Constructor(trace_test_start):
    #UnitTestComment("AtbConductor::constructor")
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    assert "conductor" == conductor.name
    assert not conductor.interfaces
    assert not conductor.cli_clients
    assert not conductor.io_logs


def test_UT_AtbConductor_AddInterface(trace_test_start):
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    interface = AtbStubInterfaceConnection("Itf1")

    #UnitTestComment("AtbConductor::add_host_interface()")
    conductor.add_interface(interface)
    assert interface in conductor
    assert interface in [conductor.Itf1]


def test_UT_AtbConductor_OpenInterfaceErrorCases(trace_test_start, test_cleaner):
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    interface1 = AtbStubInterfaceConnection("Itf1")
    interface2 = AtbStubInterfaceConnection("Itf2")

    test_cleaner.conductor = conductor
    
    #UnitTestComment("AtbConductor::open(): check that interface opening requires a registered interface")
    with pytest.raises(AtbError) as exception:
        conductor.open(interface1)
    assert str(exception.value) == AtbError.UNKNOWN

    #UnitTestComment("AtbConductor::open(): check that an interface cannot be reopened")
    conductor.add_interface(interface1)
    conductor.open(interface1)
    with pytest.raises(AtkIODeviceError) as exception:
        conductor.open(interface1)
    assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED


def test_UT_AtbConductor_OpenInterface(trace_test_start, test_cleaner):
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    interface1 = AtbStubInterfaceConnection("Itf1")
    interface2 = AtbStubInterfaceConnection("Itf2")

    test_cleaner.conductor = conductor
    
    #UnitTestComment("AtbConductor::open(): test interface opening")
    conductor.add_interface(interface1)
    conductor.add_interface(interface2)
    conductor.open(interface1)
    assert interface1.opened
    assert not interface2.opened

    #UnitTestComment("AtbConductor::open(): check that io_logs is available once at least one interface is opened")
    assert conductor.io_logs is not None


# TODO
def test_UT_AtbConductor_CloseInterface(trace_test_start, test_cleaner):
    pass


def test_UT_AtbConductor_SendOnInterfaceErrorCases(trace_test_start, test_cleaner):
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    interface1 = AtbStubInterfaceConnection("Itf1")
    interface2 = AtbStubInterfaceConnection("Itf2")

    test_cleaner.conductor = conductor
    
    #UnitTestComment("AtbConductor::send(): check that send requires a known interface")
    with pytest.raises(AtbError) as exception:
        conductor.send("test_payload1", interface1)
    assert str(exception.value) == AtbError.UNKNOWN

    #UnitTestComment("AtbConductor::send(): check that send requires an opened interface")
    conductor.add_interface(interface1)
    with pytest.raises(AtkIODeviceError) as exception:
        conductor.send("test_payload1", interface1)
    assert str(exception.value) == AtkIODeviceError.NOT_OPENED

    #UnitTestComment("AtbConductor::send(): check that an error is raised if local interface cannot be determined")
    conductor.open(interface1)
    conductor.add_interface(interface2)
    conductor.open(interface2)
    platform.default_conductor_itf = [interface2, interface1]
    with pytest.raises(AtbConductorError) as exception:
        conductor.send("test_payload3")
    assert str(exception.value) == AtbConductorError.UNDECIDED_INTERFACE


def test_UT_AtbConductor_SendOnInterface(trace_test_start, test_cleaner):
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    interface1 = AtbStubInterfaceConnection("Itf1")
    interface2 = AtbStubInterfaceConnection("Itf2")

    test_cleaner.conductor = conductor
    
    # UnitTestComment("AtbConductor::send(): test send on various interfaces")
    conductor.add_interface(interface1)
    conductor.add_interface(interface2)
    conductor.open(interface1)
    conductor.open(interface2)
    conductor.send("test_payload1", interface1)
    assert "test_payload1" == interface1.sent
    assert "" == interface2.sent
    conductor.send("test_payload2", interface1)
    assert "test_payload2" == interface1.sent
    assert "" == interface2.sent

    #UnitTestComment("AtbConductor::send(): check that platform provides local conductor interface if necessary")
    platform.default_conductor_itf = [interface2]
    conductor.send("test_payload3")
    assert "test_payload2" == interface1.sent
    assert "test_payload3" == interface2.sent


def test_UT_AtbConductor_ReceiveOnInterface(trace_test_start, test_cleaner):
    platform = AtbStubPlatform()
    conductor = AtbConductor(platform)
    interface1 = AtbStubInterfaceConnection("Itf1")
    interface2 = AtbStubInterfaceConnection("Itf2")
    interface3 = AtbStubInterfaceConnection("Itf3", interface2.io_hub)
    io_handler1 = interface1.io_handler
    io_handler2 = interface2.io_handler
    io_handler3 = interface3.io_handler

    test_cleaner.conductor = conductor
    
    # Add 3 physical interfaces handled by 2 different io hubs (and reading threads)
    conductor.add_interface(interface1)
    conductor.add_interface(interface2)
    conductor.add_interface(interface3)
    conductor.open(interface1)
    conductor.open(interface2)
    conductor.open(interface3)
    
    #UnitTestComment("AtbConductor: test read on conductor interfaces")
    conductor.send("data1", interface1)
    conductor.send("data2", interface2)
    conductor.send("data3", interface3)
    sleep(0.5)
    assert io_handler1.received == "data1"
    assert io_handler2.received == "data2"
    assert io_handler3.received == "data3"
    
    #UnitTestComment("AtbConductor: test that datas is received on different threads according to each interface's io hub")
    assert io_handler1.thread_id != io_handler2.thread_id
    assert io_handler2.thread_id == io_handler3.thread_id


def test_UT_AtbConductor_AddCliClientErrorCases(trace_test_start):
    conductor = AtbConductor()
    target = AtbTarget("Target1")
    cli_client1 = AtbStubCliClient("CliClientStub1")
    cli_client2 = AtbStubCliClient("CliClientStub1")

    #UnitTestComment("AtbConductor::add_cli_client(): test that a cli client cannot be added twice with different names
    conductor.add_cli_client(cli_client1)
    with pytest.raises(AtbConductorError) as exception:
        conductor.add_cli_client(cli_client1, client_name="CliClientStub1_bis")
    assert str(exception.value) == AtbConductorError.CONFLICT
    assert len(conductor.cli_clients) == 1

    #UnitTestComment("AtbConductor::add_cli_client(): test that 2 clients with same name cannot be added
    with pytest.raises(AtkLimitedAccessError) as exception:
        conductor.add_cli_client(cli_client2)
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert cli_client2 not in conductor.cli_clients

    #UnitTestComment("AtbConductor::add_cli_client(): test that test target cannot reference cli if it already has an attribute with same name
    target.CliClientStub2 = None
    with pytest.raises(AtkLimitedAccessError) as exception:
        conductor.add_cli_client(cli_client2, client_name="CliClientStub2", test_target=target)
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert cli_client2 in conductor.cli_clients


def test_UT_AtbConductor_AddCliClient(trace_test_start):
    conductor = AtbConductor()
    target = AtbTarget("Target1")

    #UnitTestComment("AtbConductor::add_cli_client(): check that cli client is registered in conductor using cli client's name")
    cli_client1 = AtbStubCliClient("CliClientStub1")
    conductor.add_cli_client(cli_client1)
    assert cli_client1 in conductor.cli_clients
    assert cli_client1 is conductor.CliClientStub1
            
    #UnitTestComment("AtbNode.connect(): check that new cli client is by default referenced by test conductor and test target by its name")
    cli_client2 = AtbStubCliClient("CliClientStub2")
    conductor.add_cli_client(cli_client2, test_target=target)
    assert conductor.CliClientStub2 is cli_client2
    assert target.CliClientStub2 is cli_client2
            
    #UnitTestComment("AtbNode.connect(): check that new cli client reference name can be different from cli client name")
    cli_client3 = AtbStubCliClient("CliClientStub3")
    conductor.add_cli_client(cli_client3, client_name="TestCliClientStub", test_target=target)
    assert conductor.TestCliClientStub is cli_client3
    assert target.TestCliClientStub is cli_client3
    with pytest.raises(AttributeError) as exception:
        conductor.CliClientStub3


# TODO
def test_UT_AtbConductor_OpenCliClient(trace_test_start):
    pass


def test_UT_AtbConductor_CloseCliClient(trace_test_start):
    pass


def test_UT_AtbConductor_SendOnCliClient(trace_test_start):
    pass


def test_UT_AtbConductor_ReceiveOnCliClient(trace_test_start):
    pass


def test_UT_AtbConductor_MixInterfaceAndCliClient(trace_test_start):
    pass
