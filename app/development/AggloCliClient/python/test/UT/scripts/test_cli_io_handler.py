############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

import pytest

from agglo_tk.io import AtkIOConnectionLogs

from agglo_cc.core.cli_io_types import *
from agglo_cc.core.cli_io_handler import AccCliInputHandler
from agglo_cc.core.cli_io_handler import DEFAULT_PROMPT_REG_EXPR

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(7)
def test_UT_AccCliInputHandler_Constructor(trace_test_start):
    #UnitTestComment("AccCliInputHandler::constructor: test default prompt reg expr")
    cli_io_logs = AtkIOConnectionLogs(*IO_TYPES_CLI)
    input_handler = AccCliInputHandler(cli_io_logs)
    with pytest.raises(ValueError) as exception:
        input_handler.remove_parser_reg_expr(prompt_reg_expr=DEFAULT_PROMPT_REG_EXPR)

    #UnitTestComment("AccCliInputHandler::constructor: test another prompt reg expr")
    input_handler = AccCliInputHandler(cli_io_logs, "Test.*>")
    with pytest.raises(ValueError) as exception:
        input_handler.remove_parser_reg_expr(prompt_reg_expr="Test.*>")

    #UnitTestComment("AccCliInputHandler::constructor: test access to cli io logs")
    assert not cli_io_logs[IO_TYPE_CLI_PROMPT]
    assert not cli_io_logs[IO_TYPE_CLI_COMMAND]
    assert not cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT]
    assert not cli_io_logs[IO_TYPE_CLI_ASYNC]


def test_UT_AccCliInputHandler_EditRegExpr(trace_test_start):
    input_handler = AccCliInputHandler(AtkIOConnectionLogs(*IO_TYPES_CLI))
      
    #UnitTestComment("AccCliInputHandler::add_parser_reg_expr(): test nominal case")
    input_handler.add_parser_reg_expr(prompt_reg_expr=".*")
    input_handler.add_parser_reg_expr(async_reg_expr=".*")

    #UnitTestComment("AccCliInputHandler::remove_parser_reg_expr(): test nominal case")
    input_handler.remove_parser_reg_expr(prompt_reg_expr=".*")
    with pytest.raises(KeyError) as exception:
        assert input_handler.remove_parser_reg_expr(prompt_reg_expr=".*")
    input_handler.remove_parser_reg_expr(async_reg_expr=".*")
    with pytest.raises(KeyError) as exception:
        assert input_handler.remove_parser_reg_expr(async_reg_expr=".*")


def test_UT_AccCliInputHandler_HandleInput(trace_test_start):
    cli_io_logs = AtkIOConnectionLogs(*IO_TYPES_CLI)
    input_handler = AccCliInputHandler(cli_io_logs)

    #UnitTestComment("AccCliInputHandler::handle_input(): handle cli welcome and first prompt")
    input_handler.add_parser_reg_expr(async_reg_expr="Asynchronous.*\n")
    input_handler.handle_input("Welcome 1\nPrompt1>")
    assert cli_io_logs[IO_TYPE_CLI_PROMPT][-1].data == "Prompt1>"

    #UnitTestComment("AccCliInputHandler::handle_input(): handle cli first command")
    input_handler.handle_input("Command 1\n")
    assert cli_io_logs[IO_TYPE_CLI_COMMAND][-1].data == "Command 1\n"
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].data == ""

    #UnitTestComment("AccCliInputHandler::handle_input(): handle cli result beginning")
    input_handler.handle_input("Result 1\n")
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].data == "Result 1\n"

    #UnitTestComment("AccCliInputHandler::handle_input(): handle cli asynchronous input")
    input_handler.handle_input("Asynchronous 1\n")
    assert cli_io_logs[IO_TYPE_CLI_ASYNC][-1].data == "Asynchronous 1\n"

    #UnitTestComment("AccCliInputHandler::handle_input(): handle cli result finalization and next prompt")
    input_handler.handle_input("Result 2\nPrompt2>")
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].data == "Result 1\nResult 2\n"
    assert cli_io_logs[IO_TYPE_CLI_PROMPT][-1].data == "Prompt2>"

    #UnitTestComment("AccCliInputHandler::handle_input(): test that new command/command result doesn't remove previous io logs entries")
    input_handler.handle_input("Command 2\nPrompt3>Asynchronous 2\n")
    assert cli_io_logs[IO_TYPE_CLI_PROMPT][-2].data == "Prompt2>"
    assert cli_io_logs[IO_TYPE_CLI_PROMPT][-3].data == "Prompt1>"
    assert cli_io_logs[IO_TYPE_CLI_COMMAND][-2].data == "Command 1\n"
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-2].data == "Result 1\nResult 2\n"
    assert cli_io_logs[IO_TYPE_CLI_ASYNC][-2].data == "Asynchronous 1\n"


def test_UT_AccCliInputHandler_InputTime(trace_test_start):
    cli_io_logs = AtkIOConnectionLogs(*IO_TYPES_CLI)
    input_handler = AccCliInputHandler(cli_io_logs)
   
    #UnitTestComment("AccIOParser::parse():check that entry time is correctly registered
    start = time.time()
    input_handler.handle_input("Prompt1>")
    assert cli_io_logs[IO_TYPE_CLI_PROMPT][-1].time >= start
    assert cli_io_logs[IO_TYPE_CLI_PROMPT][-1].time <= time.time()
    time.sleep(0.01)
    input_handler.handle_input("Command Name 1\n")
    assert cli_io_logs[IO_TYPE_CLI_COMMAND][-1].time > cli_io_logs[IO_TYPE_CLI_PROMPT][-1].time
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].time >= cli_io_logs[IO_TYPE_CLI_COMMAND][-1].time

    #UnitTestComment("AccIOParser::parse():check that command result time is the time of last received entry
    initial_result_time = cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].time
    time.sleep(0.01)
    input_handler.handle_input("Partial Result1\n")
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].time > initial_result_time
    first_result_time = cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].time
    time.sleep(0.01)
    input_handler.handle_input("Partial Result2\n")
    assert cli_io_logs[IO_TYPE_CLI_COMMAND_RESULT][-1].time > first_result_time
