############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from __future__ import with_statement
import time
import os

import pytest

from agglo_cc.core.cli_client import AccCliClient
from agglo_cc.core.cli_io_types import *
from agglo_tk.io import AtkIOLogs
from agglo_tk.io import AtkIOStringCriteria
from agglo_tk.io import AtkIODeviceTCP
from agglo_cc.exceptions import AccExecutionError
from agglo_cc.core.command_results import *
from agglo_tk.exceptions import AtkIODeviceError

from stubs import *
from fixtures import stub_cli_server
from fixtures import test_cleaner
from agglo_tt.fixtures import trace_test_start


def send(cli_client, data, timeout_sec=0):
    cli_client.send(data)
    
    if (0 != timeout_sec):
        time.sleep(timeout_sec)
    
    
@pytest.suite_rank(9)
def test_UT_AccCliClient_Constructor(trace_test_start):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)

    #UnitTestComment("AccCliClient::AccCliClient():check that constructor doesn't open connection")
    assert not cli_client.opened


def test_UT_AccCliClient_OpenClose(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    
    test_cleaner.cli_client = cli_client
    cli_client.open_timeout_ms = 0

    for i in range(0, 2):
        #TODO TU de l'attente du 1er prompt
        #TODO TU de la regexpr par defaut

        #UnitTestComment("CaccCliConnection::open():test Cli connection re-opening")
        cli_client.open()
        assert cli_client.opened
        with pytest.raises(AtkIODeviceError) as exception:
            cli_client.open()
        assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED

        #UnitTestComment("CaccCliConnection::close()")
        cli_client.close()
        assert not cli_client.opened
        cli_client.close()
        
    
    
def test_UT_AccCliClient_send(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    io_logs = cli_client.io_logs
    
    test_cleaner.cli_client = cli_client
    cli_client.open_timeout_ms = 0
    cli_client.open()

    #UnitTestComment("AccCliClient:test writing of a command in several parts")
    time.sleep(0.1)
    send(cli_client, "Cmd ", 0.5)
    assert ''.join([entry.data for entry in io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_all)]]) == "Prompt0>Cmd "
    send(cli_client, "Name 2\n", 0.2)
    assert ''.join([entry.data for entry in io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_all)]]) == "Prompt0>Cmd Name 2\nPrompt1>"

    cli_client.close()

# TODO TU sur IOLogs


def test_UT_AccCliClient_ParseCommand(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    io_logs = cli_client.io_logs
    
    test_cleaner.cli_client = cli_client
    cli_client.open_timeout_ms = 0
    cli_client.open()

    #UnitTestComment("AccCliClient:parse a command with a result in several lines")
    send(cli_client, "Display Result 0\n", 0.2)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][0].data == "Display Result 0\n"
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][0].data == ""
    send(cli_client, "Display Result 3\n", 0.2)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][1].data == "Display Result 3\n"
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][1].data == "Partial Result 1\nPartial Result 2\nPartial Result 3\n"

    #UnitTestComment("AccCliClient:parse a command in several parts")
    send(cli_client, "Cmd ", 0.2)
    assert len(io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)]) == 2
    send(cli_client, "Name 2\n", 0.2)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][2].data == "Cmd Name 2\n"

    #UnitTestComment("AccCliClient:parse a command with result received in several parts")
    send(cli_client, "Display Result 3 span 1\n", 0.2)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][3].data == "Partial Result 1\n"
    time.sleep(1)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][3].data == "Partial Result 1\nPartial Result 2\n"
    time.sleep(1)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][3].data == "Partial Result 1\nPartial Result 2\nPartial Result 3\n"

    cli_client.close()


def test_UT_AccCliClient_ParsePrompt(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    io_logs = cli_client.io_logs
    
    test_cleaner.cli_client = cli_client
    cli_client.open_timeout_ms = 0
    cli_client.open()

    #UnitTestComment("AccCliClient:parse prompt with default regexpr")
    time.sleep(0.5)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_prompt)][0].data == "Prompt0>"
    send(cli_client, "Login\n******\n", 0.5)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_prompt)][1].data == "Prompt1>"
    assert len(io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)]) == 1
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][0].data == "login:******\nsuccessfull login\n"
    assert len(io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)]) == 1
    
    #UnitTestComment("AccCliClient:parse a login prompt with a new login regexpr")
    cli_client.add_parser_reg_expr(prompt_reg_expr="[^>]*:")
    send(cli_client, "Login\n******\n", 0.5)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_prompt)][2].data == "login:"
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][2].data == "******\n"
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][1].data == ""
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][2].data == "successfull login\n"
    
    cli_client.close()


def test_UT_AccCliClient_ParseAsynchronous(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    io_logs = cli_client.io_logs
    
    test_cleaner.cli_client = cli_client
    cli_client.open_timeout_ms = 0
    cli_client.open()

    #UnitTestComment("AccCliClient:parse asynchronous input without defined regexpr")
    send(cli_client, "Send Asynchronous 1\n", 2)
    assert len(io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_async)]) == 0
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][1].data == "[000]Asynchronous Message 0\n"
    send(cli_client, "Display Result 2 Send Asynchronous 0\n", 2)
    assert len(io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_async)]) == 0
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][1].data == \
           "Display Result 2 Send Asynchronous 0\nPartial Result 1\n[001]Asynchronous Message 1\nPartial Result 2\n"

    #UnitTestComment("AccCliClient:parse an asynchronous input with an asynchronous regexpr")
    cli_client.add_parser_reg_expr(async_reg_expr="\[00[0-9]\].*\n")
    send(cli_client, "Send Asynchronous 1\n", 2)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_async)][0].data == "[002]Asynchronous Message 2\n"
    send(cli_client, "Display Result 2 Send Asynchronous 0\n", 0.2)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_async)][1].data == "[003]Asynchronous Message 3\n"
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][3].data == "Partial Result 1\nPartial Result 2\n"

    cli_client.close()


# TODO tester contenu exception
def test_UT_AccCliClient_ExecuteCommandErrorCases(trace_test_start, stub_cli_server, test_cleaner):
    pass
      
from agglo_tk import trace
def test_UT_AccCliClient_ExecuteCommand(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    io_logs = cli_client.io_logs
    test_cleaner.cli_client = cli_client
    
    cli_client.open()
    
    # TODO workaround car l'attente du prompt dans open ne marche pas
    time.sleep(1)

    #UnitTestComment("CaccCliConnection::executeCommand():test command timeout")
    ui_start_time = time.time()
    assert cli_client.executeCommand("Wait 3\n", timeout_ms=4000).succeeded
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_prompt)][1].data == "Prompt1>"
    pyEndTime = time.time()
    assert 3 <= (pyEndTime - ui_start_time) < 3.5
    ui_start_time = time.time()
    with pytest.raises(AccExecutionError) as exception_info:
        cli_client.executeCommand("Wait 3\n", timeout_ms=1000)
    assert exception_info.value.command_results.result == COMMAND_ERROR_TIMEOUT
    assert len(io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_prompt)]) == 2
    pyEndTime = time.time()
    assert 1 <= (pyEndTime - ui_start_time) < 1.5
    time.sleep(3)

    #UnitTestComment("CaccCliConnection::executeCommand():test command with default timeout")
    
    #UnitTestComment("AccCliClient::executeCommand():test send and read of a command with a result in several lines")
    assert cli_client.executeCommand("Display Result 0\n").succeeded
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][2].data == ""
    cli_client.executeCommand("Display Result 3\n")
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][3].data == "Display Result 3\n"
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][3].data == "Partial Result 1\nPartial Result 2\nPartial Result 3\n"

    #UnitTestComment("AccCliClient:test execution of a command with result received in several parts")
    with pytest.raises(AccExecutionError):
        cli_client.executeCommand("Display Result 3 span 1\n")
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][4].data == "Partial Result 1\n"
    time.sleep(1)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][4].data == "Partial Result 1\nPartial Result 2\n"
    time.sleep(1)
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command_result)][4].data == "Partial Result 1\nPartial Result 2\nPartial Result 3\n"

    #UnitTestComment("CaccCliConnection::executeCommand():test \n insertion")
    assert cli_client.executeCommand("Display Result 0").succeeded
    assert io_logs[AtkIOStringCriteria(io_type=cli_client.io_type_command)][5].data == "Display Result 0\n"

    # TODO essayer de bousculer, par exemple 2 commandes a false successives, puis attente des 2 resultats; inserer des asynchrones au milieu de la commande

    #TODO executecommand avec Login
    cli_client.close()

#TODO UT executeScriptFile
#TODO UT navigate


# TODO
def test_UT_AccCliClient_MixsendExecuteCommand(trace_test_start, stub_cli_server, test_cleaner):
    pass


def test_UT_AccCliClient_Record(trace_test_start, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    root_menu = AccStubMenuRoot()
    cli_client = AccCliClient("CliClient", root_menu, io_device_tcp)
    io_logs = cli_client.io_logs
    read_datas = ""
    
    test_cleaner.cli_client = cli_client
    cli_client.open_timeout_ms = 0
    cli_client.open()
    if (os.path.exists("script_file.txt")):
       os.remove("script_file.txt")

    #UnitTestComment("CaccScriptRecorder:failure case")
    # assert not(cli_client.start_script_recording(""))
    # assert not(cli_client.stop_script_recording())
    # assert cli_client.start_script_recording()
    # assert not(cli_client.start_script_recording())
    # assert cli_client.stop_script_recording()
    
    #UnitTestComment("CaccCliConnection:simple case recording")
    assert cli_client.start_script_recording("script_file.txt")
    assert os.path.exists("script_file.txt")
    cli_client.executeCommand("enter ChildMenu1\n")
    cli_client.send("Cmd1\n")
    with open("script_file.txt") as script_file:
       read_datas = script_file.read()
    assert read_datas ==  "enter ChildMenu1\nCmd1\n"
    cli_client.stop_script_recording()
    assert os.path.exists("script_file.txt")

    #UnitTestComment("CaccCliConnection:append recording")
    cli_client.start_script_recording("script_file.txt", True)
    assert cli_client.executeCommand("exit")
    cli_client.send("Cmd2\n")
    with open("script_file.txt") as script_file:
       read_datas = script_file.read()
    assert read_datas ==  "enter ChildMenu1\nCmd1\nexit\nCmd2\n"
    cli_client.stop_script_recording()
    cli_client.start_script_recording("script_file.txt")
    cli_client.executeCommand("Cmd0")
    with open("script_file.txt") as script_file:
       read_datas = script_file.read()
    assert read_datas == "Cmd0\n"

    #UnitTestComment("CaccCliConnection:pause recording")
    cli_client.pause_script_recording(True)
    cli_client.executeCommand("enter ChildMenu1")
    cli_client.send("Cmd1\n")
    cli_client.pause_script_recording(False)
    cli_client.send("enter ChildMenu2")
    cli_client.send("Cmd2\n")
    with open("script_file.txt") as script_file:
       read_datas = script_file.read()
    assert read_datas ==  "Cmd0\nenter ChildMenu2Cmd2\n"
    cli_client.stop_script_recording()

    os.remove("script_file.txt")
    cli_client.close()
