############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import threading
from time import sleep
from select import select
import re
from socketserver import TCPServer
from socketserver import BaseRequestHandler

from agglo_cc.core.menu import AccMenu
#import CaccIODevice
from agglo_cc.core.command_results import AccCommandResults

from agglo_tk import trace

ROOT_MENU = "RootMenu"
CHILD_MENU1 = "ChildMenu1"
CHILD_MENU2 = "ChildMenu2"
CHILD_MENU2_3 = "ChildMenu2_3"
CHILD_MENU2_4  = "ChildMenu2_4"


DEFAULT_STUB_CLI_TCP_PORT = 22222


class AccStubMenuRoot(AccMenu):
    
    def __init__(self):
        super(AccStubMenuRoot, self).__init__(ROOT_MENU)
        self._accMenuNavigatorUT_Child1 = self.AccStubMenuChild1(CHILD_MENU1, self)
        self._accMenuNavigatorUT_Child2 = self.AccStubMenuChild2(CHILD_MENU2, self)

    def Wait(self, auiNbSec):
        return("Wait " + str(auiNbSec) + "\n")


    class AccStubMenuChild1(AccMenu):

        def __init__(self, menu_name, parent_menu):
            super(AccStubMenuRoot.AccStubMenuChild1, self).__init__(menu_name, parent_menu)

        # TODO rendre get_enter_command statique (methode de classe)
        @staticmethod
        def get_enter_command():
            return ("configure " + CHILD_MENU1 + "\n")

        def DisplayResult(self, nb_lines, nb_sec_span = None):
            sRes = "Display Result " + str(nb_lines)

            if (nb_sec_span is not None):
                sRes += " span " + str(nb_sec_span)
            sRes += "\n"

            return (sRes)


    class AccStubMenuChild2(AccMenu):

        def __init__(self, menu_name, parent_menu):
            super(AccStubMenuRoot.AccStubMenuChild2, self).__init__(menu_name, parent_menu)
            self._accMenuNavigatorUT_Child2_3 = self.AccStubMenuChild2_3(CHILD_MENU2_3, self)
            self._accMenuNavigatorUT_Child2_4 = self.AccStubMenuChild2_4(CHILD_MENU2_4, self)

        # TODO rendre get_enter_command statique (methode de classe)
        @staticmethod
        def get_enter_command():
            return ("configure " + CHILD_MENU2 + "\n")

        def Login(self):
            return ("Login\n")


        class AccStubMenuChild2_3(AccMenu):

            def __init__(self, menu_name, parent_menu):
                super(AccStubMenuRoot.AccStubMenuChild2.AccStubMenuChild2_3, self).__init__(menu_name, parent_menu)

            # TODO rendre get_enter_command statique (methode de classe)
            @staticmethod
            def get_enter_command():
                return ("configure " + CHILD_MENU2_3 + "\n")

            def SendAsynchronous(self, nb_sec_span):
                return ("Send Asynchronous " + str(nb_sec_span) + "\n")


        class AccStubMenuChild2_4(AccMenu):

            def __init__(self, menu_name, parent_menu):
                super(AccStubMenuRoot.AccStubMenuChild2.AccStubMenuChild2_4, self).__init__(menu_name, parent_menu)

            # TODO rendre get_enter_command statique (methode de classe)
            @staticmethod
            def get_enter_command():
                return ("configure " + CHILD_MENU2_4 + "\n")


            def DisplayResultAsynch(self, nb_lines):
                return ("Display Result " + str(nb_lines) + " Send Asynchronous 0\n")


class AccStubCliClient(object):

    def __init__(self):
        pass
        self.__s_io_buffer = ""

        
    @property    
    def io_buffer(self):
        return self.__s_io_buffer

        
    @io_buffer.setter    
    def io_buffer(self, data):
        self.__s_io_buffer = data


    def executeCommand(self, command):
        self.__s_io_buffer += command
        return AccCommandResults()



class AccStubCliServer(TCPServer):

    def __init__(self, prompt_history=False):
        TCPServer.__init__(self, ('localhost', DEFAULT_STUB_CLI_TCP_PORT), AccStubCliServer.CliClientHandler)
        self.run = False
        self._prompt_history = []
        if (prompt_history):
            self._prompt_history.append("RootMenu>")


    def server_bind(self):
        # self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.allow_reuse_address = True
        TCPServer.server_bind(self)


    def start(self):
        server_thread = threading.Thread(target=self.serve_forever)

        # server_thread.dameon = True
        self.run = True
        server_thread.start()
        sleep(0.2)


    def shutdown(self):
        self.run = False
        TCPServer.shutdown(self)


    class CliClientHandler(BaseRequestHandler):

        def __init__(self, *args, **kwargs):
            self.__remaining_data = ""
            AccStubCliServer.CliClientHandler._sendAsynchronous._nb_async_cmd = 0
            AccStubCliServer.CliClientHandler._executeCommand._nb_cmd = 0

            BaseRequestHandler.__init__(self, *args, **kwargs)


        def handle(self):
            self.__remaining_data = ""

            try:
                # Send a first prompt
                self.__printPrompt()

                while self.server.run:
                    # TODO ca ne bloque pas sur le select, apres recv trouve 0 bytes
                    readable, writable, exceptional = select([self.request.fileno()], [], [], 0.5)
                    if readable:
                        data = self.request.recv(4096).decode()
                        trace(trace_class="ACC_UT", info="AccStubCliServer::received " + \
                                                         str(len(data)) + " bytes: " + repr(data))
                        if not data:
                            break

                        first_char_idx = len(self.__remaining_data)
                        self.__remaining_data += data
                        while (-1 != self.__remaining_data.find("\n")):
                            # Retrieve first command
                            command, self.__remaining_data = self.__remaining_data.split("\n", 1)
                            command += "\n"

                            # Echo received command
                            trace(trace_class="ACC_UT", info="AccStubCliServer::echo command " + \
                                                             repr(command[first_char_idx:]))
                            self.__send(command[first_char_idx:])
                            first_char_idx = 0
                            
                            # Execute received command
                            self._executeCommand(command)
                            
                            # Display new prompt
                            self.__printPrompt()
                        
                        # If there is a partially received command
                        if ("" != self.__remaining_data):
                            # Echo partially received command
                            trace(trace_class="ACC_UT", info="AccStubCliServer::echo partial command " + \
                                                             repr(self.__remaining_data))
                            self.__send(self.__remaining_data)

                trace(trace_class="ACC_UT", info="AccStubCliServer::handler loop exited")
            except Exception as exception:
                trace(trace_class="ACC_UT", info="AccStubCliServer::error " + str(exception))
                raise


        def __printPrompt(self):
            prompt = ""
            if self.server._prompt_history:
                prompt = self.server._prompt_history[-1]
            else:
                nb_cmd = AccStubCliServer.CliClientHandler._executeCommand._nb_cmd
                prompt = "Prompt" + str(nb_cmd) + ">"

            trace(trace_class="ACC_UT", info="AccStubCliServer::send prompt " + \
                                              repr(prompt))
            self.__send(prompt)


        def _executeCommand(self, command):
            trace(trace_class="ACC_UT", info="AccStubCliServer::execute command " + \
                                             repr(command))
            match = re.search("Wait ([\d]+)\n", command)
            
            if match:
                nb_sec_wait = int(match.group(1))
                sleep(nb_sec_wait)
                 
            if not match:
                match = re.search("^Display Result ([\d]+)\n", command)
                
                if match:
                    nb_display = int(match.group(1))
                    for i in range(0, nb_display):
                        self.__send("Partial Result " + str(i + 1) + "\n")
                
            if not match:
                match = re.search("^Display Result ([\d]+) span ([\d]+)\n", command)
                
                if match:
                    nb_display = int(match.group(1))
                    nb_sec_span = int(match.group(2))
                    for i in range(0, nb_display):
                        self.__send("Partial Result " + str(i + 1) + "\n")
                        if (i < (nb_display - 1)):
                            sleep(nb_sec_span)
                 
            if not match:
                match = re.search("^Login\n", command)

                if match:
                    data = ""

                    # Send the login prompt
                    self.__send("login:")

                    # Read login received from client
                    while self.server.run and (-1 == self.__remaining_data.find("\n")):
                        try:
                            readable, writable, exceptional = select([self.request.fileno()], [], [])
                            if readable:
                                data = self.request.recv(4096).decode()
                        except:
                            data = ""

                        self.__remaining_data += data
                    login, self.__remaining_data = self.__remaining_data.split('\n', 1)
                    login += "\n"

                    # Send the login sequence
                    self.__send(login + "successfull login\n")
                 
            if not match:
                match = re.search("^Send Asynchronous ([\d]+)\n", command)
                
                if match:
                    nb_sec_span = int(match.group(1))
                    
                    send_async_timer = threading.Timer(nb_sec_span, \
                                            AccStubCliServer.CliClientHandler._sendAsynchronous, [self])
                    send_async_timer.start()
                 
            if not match:
                match = re.search("^Display Result ([\d]+) Send Asynchronous 0\n", command)
                
                if match:
                    nb_display = int(match.group(1))
                    for i in range(0, nb_display):
                        self.__send("Partial Result " + str(i + 1) + "\n")
                        if (i == 0):
                            self._sendAsynchronous()

            if not match:
                match = re.search("^configure (.*)\n", command)

                if (match and self.server._prompt_history):
                    self.server._prompt_history.append(match.group(1) + ">")

            if not match:
                match = re.search("^exit\n", command)

                if match and self.server._prompt_history:
                    self.server._prompt_history.pop()

            AccStubCliServer.CliClientHandler._executeCommand._nb_cmd += 1


        def _sendAsynchronous(self):
            nb_async_cmd = AccStubCliServer.CliClientHandler._sendAsynchronous._nb_async_cmd
            self.__send("[00" + str(nb_async_cmd) + "]Asynchronous Message " +  str(nb_async_cmd) + "\n")
            AccStubCliServer.CliClientHandler._sendAsynchronous._nb_async_cmd += 1


        def __send(self, data):
            self.request.sendall(data.encode())

       
# TODO utiliser la lib mock cf sam max        
class AccStubIOHandler(object):

    def __init__(self):
        self.last_logs = ""
        self.last_prompt = ""
        self.last_command = ""
        self.last_command_result = ""
        self.last_asynchronous = ""

    def handle_new_prompt(self, prompt):
        self.last_logs = ""
        self.last_prompt = prompt

    def handle_new_command(self, command):
        self.last_command = command

    def handle_new_command_result(self, command_result):
        self.last_command_result = command_result

    def handle_new_asynchronous(self, async_input):
        self.last_asynchronous = async_input
        