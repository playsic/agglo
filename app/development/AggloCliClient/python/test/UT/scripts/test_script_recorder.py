############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from __future__ import with_statement

import os

import pytest

from agglo_cc.utils.script_recorder import AccScriptRecorder

from agglo_tt.fixtures import trace_test_start

@pytest.suite_rank(8)
def test_UT_AccScriptRecorder_Record(trace_test_start):
    sReadData = ""
    if (os.path.exists("test.txt")):
        os.remove("test.txt")

    #UnitTestComment("AccScriptRecorder:file creation")
    accScriptRecorder = AccScriptRecorder("test.txt")
    assert not(os.path.exists("test.txt"))
    accScriptRecorder.start()
    assert os.path.exists("test.txt")
    accScriptRecorder.stop()
    assert os.path.exists("test.txt")

    #UnitTestComment("AccScriptRecorder:failure case")
    accScriptRecorder = AccScriptRecorder("")
    assert not(accScriptRecorder.start())
    accScriptRecorder = AccScriptRecorder("test.txt")
    assert not(accScriptRecorder.stop())
    assert accScriptRecorder.start()
    assert not(accScriptRecorder.start())
    assert accScriptRecorder.stop()
        
    #UnitTestComment("AccScriptRecorder:simple case ")
    assert not(accScriptRecorder.record("enter ChildMenu1"))
    assert accScriptRecorder.start()
    assert accScriptRecorder.record("enter ChildMenu1")
    accScriptRecorder.record("Cmd1\n")
    with open("test.txt") as pyFile:
        sReadData = pyFile.read()
    assert sReadData ==  "enter ChildMenu1Cmd1\n"
    
    #UnitTestComment("AccScriptRecorder:append ")
    accScriptRecorder.start(True)
    accScriptRecorder.record("exit\n")
    accScriptRecorder.record("Cmd0\n")
    with open("test.txt") as pyFile:
        sReadData = pyFile.read()
    assert sReadData ==  "enter ChildMenu1Cmd1\nexit\nCmd0\n"
    accScriptRecorder.stop()
    accScriptRecorder.start()
    accScriptRecorder.record("Cmd0\n")
    with open("test.txt") as pyFile:
        sReadData = pyFile.read()
    assert sReadData ==  "Cmd0\n"
    
    #UnitTestComment("AccScriptRecorder:pause ")
    accScriptRecorder.pause(True)
    accScriptRecorder.record("enter ChildMenu1\n")
    accScriptRecorder.record("Cmd1\n")
    accScriptRecorder.pause(False)
    accScriptRecorder.record("enter ChildMenu2\n")
    accScriptRecorder.record("Cmd2\n")
    with open("test.txt") as pyFile:
        sReadData = pyFile.read()
    assert sReadData ==  "Cmd0\nenter ChildMenu2\nCmd2\n"
    accScriptRecorder.stop()
    
    os.remove("test.txt")
