############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_cc.core.menu_path import AccMenuPath

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(1)
def test_UT_AccMenuPath_Constructor(trace_test_start):
    #UnitTestComment("AccMenuPath(MenuName)")
    parent_path = AccMenuPath("ParentPath1")
    assert str(parent_path) == "\ParentPath1"

    #UnitTestComment("AccMenuPath(ChildMenu)")
    child_path = AccMenuPath("ChildMenu", parent_path)
    assert str(child_path) == "\ParentPath1\ChildMenu"

    #TODO tester initWithPath

        
def test_UT_AccMenuPath_BuiltIns(trace_test_start):
    #UnitTestComment("AccMenuPath.operator ==")
    parent_path1 = AccMenuPath("ParentPath2", "\ParentPath1")
    parent_path2 = AccMenuPath("ParentPath2", "\ParentPath1")
    assert parent_path1 == parent_path2

    #UnitTestComment("AccMenuPath.operator !=")
    parent_path3 = AccMenuPath("ParentPath4", "\ParentPath1")
    assert parent_path1 != parent_path3


def test_UT_AccMenuPath_MenuName(trace_test_start):
    #UnitTestComment("AccMenuPath.menu_name : Check nominal case")
    parent_path1 = AccMenuPath("Root1")
    child_path1 = AccMenuPath("Child1", parent_path1)
    assert parent_path1.menu_name == "Root1"
    assert child_path1.menu_name == "Child1"

    #UnitTestComment("AccMenuPath.menu_name : Check menu path no separator")
    parent_path2 = AccMenuPath("Root2")
    child_path2 = AccMenuPath("Child2", parent_path2)
    assert parent_path2.menu_name == "Root2"
    assert child_path2.menu_name == "Child2"


def test_UT_AccMenuPath_CommonAncestor(trace_test_start):
    #UnitTestComment("AccMenuPath.get_common_ancestor() : Check common ancestor direct descendant")
    parent_path1 = AccMenuPath("ParentPath1")
    parent_path2 = AccMenuPath("ParentPath2", parent_path1)
    common_ancestor1_1 = parent_path1.get_common_ancestor(parent_path1)
    common_ancestor1_2 = parent_path2.get_common_ancestor(parent_path1)
    common_ancestor2_1 = parent_path1.get_common_ancestor(parent_path2)
    assert str(common_ancestor1_1) == "\ParentPath1"
    assert str(common_ancestor1_2) == "\ParentPath1"
    assert str(common_ancestor1_2) == str(common_ancestor2_1)

    #UnitTestComment("AccMenuPath.get_common_ancestor() : Check common ancestor sibblings")
    parent_path3 = AccMenuPath("ParentPath3", parent_path1)
    common_ancestor1_3 = parent_path2.get_common_ancestor(parent_path3)
    common_ancestor3_1 = parent_path3.get_common_ancestor(parent_path2)
    assert str(common_ancestor1_3) == "\ParentPath1"
    assert str(common_ancestor1_3) == str(common_ancestor3_1)

    #UnitTestComment("AccMenuPath.get_common_ancestor() : Check no common ancestor")
    parent_path4 = AccMenuPath("ParentPath5", "\ParentPath4")
    common_ancestor1_4 = parent_path2.get_common_ancestor(parent_path4)
    common_ancestor4_1 = parent_path4.get_common_ancestor(parent_path2)
    assert str(common_ancestor1_4) == "\\"
    assert str(common_ancestor1_4) == str(common_ancestor4_1)

    #UnitTestComment("AccMenuPath.get_common_ancestor() : Check common ancestor complex case")
    parent_path5 = AccMenuPath("ParentPath6", "\ParentPath1\ParentPath2 Extra")
    common_ancestor1_5 = parent_path2.get_common_ancestor(parent_path5)
    common_ancestor5_1 = parent_path5.get_common_ancestor(parent_path2)
    assert str(common_ancestor1_5) == "\ParentPath1"
    assert str(common_ancestor1_5) == str(common_ancestor5_1)


def test_UT_AccMenuPath_Heredity(trace_test_start):
    #UnitTestComment("AccMenuPath.is_ancestor_of() : simple case")
    parent_path1 = AccMenuPath("ParentPath1")
    parent_path2 = AccMenuPath("ParentPath3", "\ParentPath1\ParentPath2")
    parent_path3 = AccMenuPath("ParentPath4")
    assert parent_path1.is_ancestor_of(parent_path2)
    assert parent_path2.is_ancestor_of(parent_path2)
    assert not(parent_path1.is_ancestor_of(parent_path3))

    #UnitTestComment("AccMenuPath.is_descendant_of() : simple case")
    assert parent_path2.is_descendant_of(parent_path2)
    assert parent_path2.is_descendant_of(parent_path1)
    assert not(parent_path3.is_descendant_of(parent_path1))

    #UnitTestComment("AccMenuPath.is_ancestor_of() : complex case")
    parent_path4 = AccMenuPath("ParentPath6 Extra", parent_path1)
    parent_path5 = AccMenuPath("ParentPath5", "\ParentPath1\ParentPath4 Extra")
    parent_path6 = AccMenuPath("ParentPath7", "\ParentPath1 Extra")
    assert parent_path1.is_ancestor_of(parent_path4)
    assert parent_path1.is_ancestor_of(parent_path5)
    assert not(parent_path1.is_ancestor_of(parent_path6))

    #UnitTestComment("AccMenuPath.is_descendant_of() : complex case")
    assert parent_path4.is_descendant_of(parent_path1)
    assert parent_path5.is_descendant_of(parent_path1)
    assert not(parent_path6.is_descendant_of(parent_path1))


