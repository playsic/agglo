############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import os
import subprocess
from time import sleep

import pytest

from stubs import AccStubCliServer


enable_prompt_history = False

@pytest.fixture
def prompt_history(request):
    global enable_prompt_history

    enable_prompt_history = True;
    
    def disable_prompt_history():
        enable_prompt_history = False

    request.addfinalizer(disable_prompt_history)
    
    return None

    
@pytest.fixture
def stub_cli_server(request):
    global enable_prompt_history
    server = AccStubCliServer(enable_prompt_history)

    # Start cli server stub
    server.start()
    
    def close():
        server.shutdown()
        server.server_close()
        
    request.addfinalizer(close)
    
    return server

    
@pytest.fixture(scope="module")
def user_cli_client(request):
    fixture_path = os.path.realpath(__file__)
    fixture_path = os.path.dirname(fixture_path)
    generate_shell_path = "sh " + fixture_path + "/../../../src/xsl/generate_cli_client.sh "
    subprocess.check_output(args=["/bin/bash", fixture_path + "/../../../agglo_cc/xsl/generate_cli_client.sh", 
                                  fixture_path + "/user_cli_client.xml", fixture_path + "/user_cli_client.py"])
    sleep(0.5)

    import user_cli_client

    return user_cli_client


@pytest.fixture
def test_cleaner(request):
    class Cleaner(object):
        def __init__(self):
            self.cli_client = None

    def clean():
        cleaner = request.cleaner
        if cleaner.cli_client is not None:
            cleaner.cli_client.close()
            cleaner.cli_client = None

    request.cleaner = Cleaner()
    request.addfinalizer(clean)

    return request.cleaner
