#############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use self file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of self file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http:#www.gnu.org/copyleft/gpl.html.
##
## In addition, following conditions apply:
##     # Redistributions in binary form must reproduce the above copyright
##       notice, list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     # Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from self software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, NOT
## LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, CONSEQUENTIAL DAMAGES (INCLUDING, NOT LIMITED
## TO, OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, IN CONTRACT, LIABILITY, TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
#############################################################################

import pytest

from agglo_cc.core.menu import AccMenu
from agglo_cc.core.menu_path import AccMenuPath

from agglo_tt.fixtures import trace_test_start

@pytest.suite_rank(2)
def test_UT_AccMenu_Constructor(trace_test_start):
    #UnitTestComment("AccMenu(RootMenu)")
    root_menu = AccMenu("RootMenu")
    assert str(root_menu.path) ==  "\RootMenu"
    assert root_menu.parent_menu ==  None
    
    #UnitTestComment("AccMenu(ChildMenu)")
    child_menu1 = AccMenu("ChildMenu1", root_menu)
    child_menu2 = AccMenu("ChildMenu2", child_menu1)
    assert str(child_menu2.path) ==  "\RootMenu\ChildMenu1\ChildMenu2"
    assert child_menu2.parent_menu.parent_menu ==  root_menu


def test_UT_AccMenu_MenuConstruction(trace_test_start):
    #UnitTestComment("AccMenu.get_child_menu() : simple case")
    root_menu = AccMenu("RootMenu")
    child_menu1 = AccMenu("ChildMenu1", root_menu)
    child_menu1Path = AccMenuPath("ChildMenu1", "\RootMenu")
    assert root_menu.get_child_menu(child_menu1Path) ==  child_menu1
    assert child_menu1.get_child_menu(child_menu1Path) is None
    
    #UnitTestComment("AccMenu.get_child_menu() : complex case")
    child_menu2 = AccMenu("ChildMenu2", root_menu)
    child_menu1_3 = AccMenu("ChildMenu1_3", child_menu1)
    child_menu2_4 = AccMenu("ChildMenu2_4", child_menu2)
    child_menu4_5 = AccMenu("ChildMenu4_5", child_menu2_4)
    child_menu1_3Path = AccMenuPath("ChildMenu1_3", "\RootMenu\ChildMenu2")
    child_menu2_4Path = AccMenuPath("ChildMenu2_4", "\RootMenu\ChildMenu2")
    child_menu4_5Path = AccMenuPath("ChildMenu4_5", "\RootMenu\ChildMenu2\ChildMenu2_4")
    assert child_menu1.get_child_menu(child_menu1_3Path) is None
    assert child_menu2.get_child_menu(child_menu2_4Path) ==  child_menu2_4
    assert root_menu.get_child_menu(child_menu4_5Path) ==  child_menu2

