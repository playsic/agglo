############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_cc._private.cli_io_parser import AccCliIOParser

from stubs import AccStubIOHandler

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(5)
def test_UT_AccCliIOParser_EditRegExprErrorCases(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*:")

    #UnitTestComment("AccCliIOParser::remove_reg_expr(): check that unknown reg expr cannot be removed")
    with pytest.raises(KeyError) as exception:
        cli_io_parser.remove_reg_expr(prompt_reg_expr=".*")

    #UnitTestComment("AccCliIOParser::remove_reg_expr(): check that last prompt reg expr cannot be removed")
    with pytest.raises(ValueError) as exception:
        cli_io_parser.remove_reg_expr(prompt_reg_expr=".*:")


def test_UT_AccCliIOParser_EditRegExpr(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*:")
      
    #UnitTestComment("AccCliIOParser::add_reg_expr(): test nominal case")
    cli_io_parser.add_reg_expr(prompt_reg_expr=".*")
    cli_io_parser.add_reg_expr(async_reg_expr=".*")

    #UnitTestComment("AccCliIOParser::add_reg_expr(): test that a reg expr can be added twice without error")
    cli_io_parser.add_reg_expr(prompt_reg_expr=".*")
    cli_io_parser.add_reg_expr(async_reg_expr=".*")

    #UnitTestComment("AccCliIOParser::remove_reg_expr(): test nominal case")
    cli_io_parser.remove_reg_expr(prompt_reg_expr=".*")
    with pytest.raises(KeyError) as exception:
        assert cli_io_parser.remove_reg_expr(prompt_reg_expr=".*")
    cli_io_parser.remove_reg_expr(async_reg_expr=".*")
    with pytest.raises(KeyError) as exception:
        assert cli_io_parser.remove_reg_expr(async_reg_expr=".*")
    

def test_UT_AccCliIOParser_ParseFullLines(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*>")
   
    #UnitTestComment("AccCliIOParser::parse():check that prompt parsing is correct
    cli_io_parser.parse("Prompt1>")
    assert cli_io_handler.last_prompt == "Prompt1>"

    #UnitTestComment("AccCliIOParser::parse():check that command parsing is correct
    cli_io_parser.parse("Command Name 1\n")
    assert cli_io_handler.last_command == "Command Name 1\n"

    #UnitTestComment("AccCliIOParser::parse():check that command result parsing is correct
    cli_io_parser.parse("Partial Result 1\n")
    assert cli_io_handler.last_command_result == "Partial Result 1\n"
    cli_io_parser.parse("Partial Result 2\n")
    assert cli_io_handler.last_command_result == "Partial Result 2\n"
    assert cli_io_handler.last_prompt == "Prompt1>"
    assert cli_io_handler.last_command == "Command Name 1\n"

                
def test_UT_AccCliIOParser_ParsePartialLines(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*>")
   
    #UnitTestComment("AccCliIOParser::parse(): check reception of partial prompt
    cli_io_parser.parse("Prom")
    assert cli_io_handler.last_prompt == ""
    
    #UnitTestComment("AccCliIOParser::parse(): check reception of partial command
    cli_io_parser.parse("pt1>Command ")
    assert cli_io_handler.last_prompt == "Prompt1>"
    assert cli_io_handler.last_command == ""
    
    #UnitTestComment("AccCliIOParser::parse(): check reception of partial result
    cli_io_parser.parse("Name 1\nPartial ")
    assert cli_io_handler.last_command == "Command Name 1\n"
    assert cli_io_handler.last_command_result == ""
    cli_io_parser.parse("Result 1\n")
    cli_io_parser.parse("Parti")
    assert cli_io_handler.last_command == "Command Name 1\n"
    assert cli_io_handler.last_command_result == "Partial Result 1\n"
    
    #UnitTestComment("AccCliIOParser::parse(): check reception of another partial prompt
    cli_io_parser.parse("al Result 2\nProm")
    assert cli_io_handler.last_command == "Command Name 1\n"
    assert cli_io_handler.last_command_result == "Partial Result 2\n"
    assert cli_io_handler.last_prompt == "Prompt1>"
    
    #UnitTestComment("AccCliIOParser::parse(): check reception of another partial command
    cli_io_parser.parse("pt2>C")
    assert cli_io_handler.last_prompt == "Prompt2>"
    assert cli_io_handler.last_command == "Command Name 1\n"
    assert cli_io_handler.last_command_result == "Partial Result 2\n"


def test_UT_AccCliIOParser_ParsePrompt(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*>")
   
    # TODO ajouter la case de test d'echec parse("Prompt>Prompt>\n")
    #UnitTestComment("AccCliIOParser::parse():parse prompt with default regexpr")
    cli_io_parser.parse("Prompt1>Command Name 1\n")
    assert cli_io_handler.last_prompt == "Prompt1>"
    assert cli_io_handler.last_command == "Command Name 1\n"
    cli_io_parser.parse("login:******\n")
    assert cli_io_handler.last_prompt == "Prompt1>"
    assert cli_io_handler.last_command_result == "login:******\n"
    
    #UnitTestComment("AccCliIOParser::parse():parse a login prompt with a new login regexpr")
    cli_io_parser.add_reg_expr(prompt_reg_expr="[^>]*:")
    cli_io_parser.parse("Prompt2>Command Name 2\n")
    assert cli_io_handler.last_prompt == "Prompt2>"
    assert cli_io_handler.last_command == "Command Name 2\n"
    cli_io_parser.parse("login:******\n")
    assert cli_io_handler.last_prompt == "login:"
    assert cli_io_handler.last_command == "******\n"

    #UnitTestComment("AccCliIOParser::parse():parse with extra regexpr removed
    cli_io_parser.remove_reg_expr(prompt_reg_expr="[^>]*:")
    cli_io_parser.parse("Prompt3>Command Name 3\n")
    cli_io_parser.parse("login:******\n")
    assert cli_io_handler.last_prompt == "Prompt3>"
    assert cli_io_handler.last_command == "Command Name 3\n"
    assert cli_io_handler.last_command_result == "login:******\n"


def test_UT_AccCliIOParser_ParseAsynchronous(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*>")
   
    #UnitTestComment("AccCliIOParser::parse():parse asynchronous input without defined regexpr")
    cli_io_parser.parse("Prompt1>[001]Asynchronous Message 1\n")
    assert cli_io_handler.last_command == "[001]Asynchronous Message 1\n"
    assert cli_io_handler.last_asynchronous == ""
    cli_io_parser.parse("Command Name 1\n")
    assert cli_io_handler.last_command_result == "Command Name 1\n"
    cli_io_parser.parse("[002]Asynchronous Message 2\n")
    assert cli_io_handler.last_command_result == "[002]Asynchronous Message 2\n"
    assert cli_io_handler.last_asynchronous == ""
    
    #UnitTestComment("AccCliIOParser::parse():parse an asynchronous input with an asynchronous regexpr")
    cli_io_parser.parse("Prompt2>Command Name 2\nResult 2\n")
    cli_io_parser.add_reg_expr(async_reg_expr="\[00[0-9]\].*\n")
    cli_io_parser.parse("Prompt3>[003]Asynchronous Message 3\n")
    assert cli_io_handler.last_command == "Command Name 2\n"
    assert cli_io_handler.last_asynchronous == "[003]Asynchronous Message 3\n"
    cli_io_parser.parse("Command Name 3\nResult 3\n")
    assert cli_io_handler.last_command == "Command Name 3\n"
    cli_io_parser.parse("[004]Asynchronous Message 4\n")
    assert cli_io_handler.last_asynchronous == "[004]Asynchronous Message 4\n"
    assert cli_io_handler.last_command_result == "Result 3\n"

    #UnitTestComment("AccCliIOParser::parse():parse an asynchronous input with a 2nd asynchronous regexpr")
    cli_io_parser.parse("Prompt4>[00A]Asynchronous Message A\n")
    assert cli_io_handler.last_command == "[00A]Asynchronous Message A\n"
    cli_io_parser.add_reg_expr(async_reg_expr="\[00[A-Z]\].*\n")
    cli_io_parser.parse("Prompt5>[00B]Asynchronous Message B\n")
    assert cli_io_handler.last_asynchronous == "[00B]Asynchronous Message B\n"

    #UnitTestComment("AccCliIOParser::parse():parse an asynchronous input with extra regexpr removed
    cli_io_parser.remove_reg_expr(async_reg_expr="\[00[0-9]\].*\n")
    cli_io_parser.parse("[005]Asynchronous Message 5\n")
    assert cli_io_handler.last_command == "[005]Asynchronous Message 5\n"
    cli_io_parser.parse("Prompt7>[00C]Asynchronous Message C\n")
    assert cli_io_handler.last_asynchronous == "[00C]Asynchronous Message C\n"
    

def test_UT_AccCliIOParser_ParseWelcome(trace_test_start):
    cli_io_handler = AccStubIOHandler()
    cli_io_parser = AccCliIOParser(cli_io_handler, ".*>")
   
    #UnitTestComment("AccCliIOParser::parse():parse welcome message")
    cli_io_parser.parse("Welcome Line 1\n")
    assert cli_io_handler.last_prompt == ""
    assert cli_io_handler.last_command == ""
    assert cli_io_handler.last_command_result == ""
    assert cli_io_handler.last_asynchronous == ""

    #UnitTestComment("AccCliIOParser::parse():test asnchronous inputs during welcome parsing")
    cli_io_parser.add_reg_expr(async_reg_expr="\[00[0-9]\].*\n")
    cli_io_parser.parse("Welcome Line 2\n")
    cli_io_parser.parse("[001]Asynchronous Message 1\n")
    cli_io_parser.parse("Welcome Line 3\n")
    assert cli_io_handler.last_prompt == ""
    assert cli_io_handler.last_command == ""
    assert cli_io_handler.last_command_result == ""
    assert cli_io_handler.last_asynchronous == "[001]Asynchronous Message 1\n"

    #UnitTestComment("AccCliIOParser::parse():parse first prompt/command/command return after Welcome message")
    cli_io_parser.parse("Prompt1>Command Name 1\n")
    assert cli_io_handler.last_prompt == "Prompt1>"
    assert cli_io_handler.last_command == "Command Name 1\n"

    
# TODO gerer le cas du welcome dans UserCliClient ?        
# def test_UT_AccCliIOParser_ParseMeltedIO():
    # cli_io_handler = AccStubIOHandler()
    # cli_io_parser = AccCliIOParser(cli_io_handler)
   
    # #UnitTestComment("AccCliIOParser::parse():parse IO melted")
    # cli_io_parser.parse("ProDEB_ASYNCHAsynchFIN_ASYNCHmpt0>DEB_CMDCommandDEB_ASYNCHAsynchFIN_ASYNCH NameFIN_CMD")
        