############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from time import time
import inspect

import pytest

from agglo_cc.core.cli_io_types import *
from agglo_tk.io import AtkIODeviceTCP
from agglo_tk.io import AtkIOStringCriteria

from agglo_cc.exceptions import AccExecutionError

from stubs import DEFAULT_STUB_CLI_TCP_PORT
from agglo_tk.utils.test.stubs.io_device import AtkStubIODevice

from fixtures import user_cli_client
from fixtures import stub_cli_server
from fixtures import prompt_history
from fixtures import test_cleaner
from agglo_tt.fixtures import trace_test_start


def index_safe(_list, value):
    result = -1
    
    try:
        result = _list.index(value)
    except:
        pass
    
    return result
   
   
def get_local_fields(cls, field_type):
    all_fields = [elem[0] for elem in inspect.getmembers(cls, field_type)]
    result = list(set(cls.__dict__) & set(all_fields))
    
    return result

    
@pytest.suite_rank(10)
def test_UT_AccUserCliClient_Constructor(trace_test_start, user_cli_client):
    cli_client = user_cli_client.AccCliClientUserCli(AtkStubIODevice())

    #UnitTestComment("AccUserCliClient::AccUserCliClient():check that constructor doesn't open connection")
    assert not cli_client.opened


def test_UT_AccUserCliClient_MenuGeneration(trace_test_start, user_cli_client):
    #UnitTestComment("AccUserCliClient:test a class is generated for every menu and for the cli client")
    # TODO filtrer les imports
    nb_imported_classes = 4
    cli_client_classes = inspect.getmembers(user_cli_client, inspect.isclass)
    assert len(cli_client_classes) == 6 + nb_imported_classes
    # assert cli_client_classes[AccMenuUserCli] is not None
    # assert cli_client_classes[AccMenuChildMenu1] is not None
    # assert cli_client_classes[AccMenuChildMenu2] is not None
    # assert cli_client_classes[AccMenuChildMenu2_3] is not None
    # assert cli_client_classes[AccMenuChildMenu2_4] is not None
    # assert cli_client_classes[AccCliClientUserCli] is not None
    
    # #UnitTestComment("AccUserCliClient:test constants menu name and menu path are defined for each menu")
    # lsDefinedConstants = inspect.getmembers(user_cli_client, inspect.isattribute)
    # assert macro


def test_UT_AccUserCliClient_CommandGeneration(trace_test_start, user_cli_client):
    root_menu = user_cli_client.AccMenuUserCli()
    root_menu_methods = get_local_fields(user_cli_client.AccMenuUserCli, inspect.isfunction)

    # Check that cli client generation has generate expected methods in AccMenuUserCli, and only these ones
    assert len(root_menu_methods) == 19
    assert index_safe(root_menu_methods, "__init__") != -1
    assert index_safe(root_menu_methods, "getCommandKw1") != -1
    assert index_safe(root_menu_methods, "getCommandKw2") != -1
    assert index_safe(root_menu_methods, "getCommandKw2Kw3") != -1
    assert index_safe(root_menu_methods, "getCommandKw4") != -1
    assert index_safe(root_menu_methods, "getCommandKw4Kw5") != -1
    assert index_safe(root_menu_methods, "getCommandKw4Kw6") != -1
    assert index_safe(root_menu_methods, "getCommandKw7Kw8") != -1
    assert index_safe(root_menu_methods, "getCommandKw9") != -1
    assert index_safe(root_menu_methods, "getCommandKw11") != -1
    assert index_safe(root_menu_methods, "getCommandKw11Prm1") != -1
    assert index_safe(root_menu_methods, "getCommandKw11Kw12Kw13") != -1
    assert index_safe(root_menu_methods, "getCommandKw14") != -1
    assert index_safe(root_menu_methods, "getCommandKw14Kw15") != -1
    assert index_safe(root_menu_methods, "getCommandKw14Kw16") != -1
    assert index_safe(root_menu_methods, "getCommandKw18Kw19Kw20") != -1
    assert index_safe(root_menu_methods, "getCommandKw18Kw19Kw21") != -1
    assert index_safe(root_menu_methods, "getCommandKw18Kw19Kw22") != -1
    assert index_safe(root_menu_methods, "getCommandWait") != -1
    
    # Check that generated methods in AccMenuUserCli returns correct cli command syntax
    assert root_menu.getCommandKw1() == "kw1\n"
    assert root_menu.getCommandKw2() == "kw2\n"
    assert root_menu.getCommandKw2Kw3() == "kw2 kw3\n"
    assert root_menu.getCommandKw4() == "kw4\n"
    assert root_menu.getCommandKw4Kw5() == "kw4 kw5\n"
    assert root_menu.getCommandKw4Kw6() == "kw4 kw6\n"
    assert root_menu.getCommandKw7Kw8() == "kw7 kw8\n"
    assert root_menu.getCommandKw4() == "kw4\n"
    assert root_menu.getCommandKw14(1111) == "kw14 1111\n"
    assert root_menu.getCommandKw14Kw15(1111) == "kw14 1111 kw15\n"
    assert root_menu.getCommandKw9(1111, 1112, 1113) == "kw9 1111 kw10 1112 1113\n"
    assert root_menu.getCommandKw11() == "kw11\n"
    assert root_menu.getCommandKw11Prm1(1111) == "kw11 1111\n"
    assert root_menu.getCommandKw11Kw12Kw13() == "kw11 kw12 kw13\n"
    assert root_menu.getCommandKw14Kw16(1111, 1112) == "kw14 1111 kw16 1112 kw17\n"
    assert root_menu.getCommandKw18Kw19Kw20() == "kw18 kw19 kw20\n"
    assert root_menu.getCommandKw18Kw19Kw21() == "kw18 kw19 kw21\n"
    assert root_menu.getCommandKw18Kw19Kw22(1111) == "kw18 kw19 kw22 1111\n"
    assert root_menu.getCommandWait(1111) == "Wait 1111\n"
    
    # assert kw1.defaultparam=vide
    # assert kw1kw2.defaultparam[0]=false,[1]=true...
    # faire des getcommand avec les parametres par defaut


def test_UT_AccUserCliClient_ExecuteUserCommand(trace_test_start, user_cli_client, 
                                                prompt_history, stub_cli_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_STUB_CLI_TCP_PORT)
    cli_client = user_cli_client.AccCliClientUserCli(io_device_tcp)
    io_logs = cli_client.io_logs
    
    test_cleaner.cli_client = cli_client
    cli_client.open()
    cli_client.add_parser_reg_expr(prompt_reg_expr="[^>]*:")
    cli_client.add_parser_reg_expr(async_reg_expr="\[00[0-9]\].*\n")
    
    #TODO ajouter les assert pour la navigation confiugure/exit
    #UnitTestComment("AccUserCliClient:test generated command:login")
    assert cli_client.Login()
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="Login\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, reg_expr="").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="login:").rank(-1))
    assert cli_client.executeCommand("******")
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="\*\*\*\*\*\*\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, reg_expr="successfull login\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="ChildMenu2>").rank(-1))
    
    #UnitTestComment("AccUserCliClient:test generated command:send asynch")
    start_time = time()
    assert cli_client.SendAsynchronous(3)
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="Send Asynchronous 3\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, reg_expr="").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="ChildMenu2_.>").rank(-1))
    cli_client.wait_io(AtkIOStringCriteria(io_type=cli_client.io_type_async, reg_expr="\[00[0-9]\].*\n").from_now(10000))
    assert start_time + 3 <= time() <= start_time + 3 + 1
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_async, \
                                             reg_expr="\[000\]Asynchronous Message 0\n").rank(-1))
                           
    
    #UnitTestComment("AccUserCliClient:test generated command:display result lines")
    start_time = time()
    assert cli_client.DisplayResult(3)
    assert start_time <= time() <= start_time + 1
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="Display Result \d\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, reg_expr="Partial Result 1\nPartial Result 2\nPartial Result 3\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="ChildMenu.>").rank(-1))
    assert start_time <= time() <= start_time + 2
    
    #UnitTestComment("AccUserCliClient:test generated command:display result lines with time span")
    start_time = time()
    with pytest.raises(AccExecutionError) as exception:
        cli_client.DisplayResult(4, 1)
    assert str(exception.value) == AccExecutionError.TIMEOUT
    cli_client.wait_io(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="ChildMenu\d>").from_now(10000))
    assert start_time + 3 <= time() <= start_time + 3 + 1
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="Display Result 4 span 1\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, \
                                             reg_expr="Partial Result 1\nPartial Result 2\nPartial Result 3\nPartial Result 4\n").rank(-1))
                           
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="ChildMenu1>").rank(-1))
    
    #UnitTestComment("AccUserCliClient:test generated command:display result lines mixed with asynchronous")
    assert cli_client.DisplayResultAsynch(5)
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="^D.* Result 5 Send Asynchronous 0\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, \
                                             reg_expr="Partial Result 1\nPartial Result 2\nPartial Result 3\nPartial Result 4\nPartial Result 5\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_async, \
                                             reg_expr="\[00" + str((0) + 1) + "\]Asynchronous Message 1\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="ChildMenu2_4>").rank(-1))
    
    #UnitTestComment("AccUserCliClient:test generated command:wait")
    start_time = time()
    with pytest.raises(AccExecutionError) as exception:
        cli_client.Wait(2 * (1))
    assert str(exception.value) == AccExecutionError.TIMEOUT
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command, reg_expr="Wait 2\n").rank(-1))
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_command_result, reg_expr="").rank(-1))
    cli_client.wait_io(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="RootMenu>").from_now(10000))
    assert start_time + 2 * (1) <= time() <= start_time + 2 * (1) + 1
    assert io_logs.check(AtkIOStringCriteria(io_type=cli_client.io_type_prompt, reg_expr="RootMenu>").rank(-1))

    # TODO essayer de bousculer, par exemple 2 commandes a false successives, puis attente des 2 resultats; inserer des asynchrones au milieu de la commande

    cli_client.close()
