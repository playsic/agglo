############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_cc.core.menu_path import AccMenuPath
from agglo_cc.core.menu_navigator import AccMenuNavigator
  
from stubs import AccStubCliClient
from stubs import AccStubMenuRoot

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(3)
def test_UT_AccMenuNavigator_Navigation(trace_test_start):
    #UnitTestComment("CaccMenu.navigate_to_menu() : beginning")
    #TODO revoir ces TU : checker resultat de navigate_to_menu, usage de Cli client avec commande qui echoue..., navigate vers le menu ou on est deja
    cli_client = AccStubCliClient()
    menu_navigator = AccMenuNavigator(AccStubMenuRoot(), cli_client)
    menu_navigator.navigate_to_menu(AccMenuPath("RootMenu"))
    assert cli_client.io_buffer ==  ""
   
    #UnitTestComment("CaccMenu.get_child_menu() : go down to Child2_3")
    menu_navigator.navigate_to_menu(AccMenuPath("ChildMenu2_3", "\RootMenu\ChildMenu2"))
    assert cli_client.io_buffer ==  "configure ChildMenu2\nconfigure ChildMenu2_3\n"
    cli_client.io_buffer = ""
   
    #UnitTestComment("CaccMenu.get_child_menu() : go up and down to Child2_4")
    menu_navigator.navigate_to_menu(AccMenuPath("ChildMenu2_4", "\RootMenu\ChildMenu2"))
    assert cli_client.io_buffer ==  "exit\nconfigure ChildMenu2_4\n"
    cli_client.io_buffer = ""
    
    #UnitTestComment("CaccMenu.get_child_menu() : go up and down to Child1")
    menu_navigator.navigate_to_menu(AccMenuPath("ChildMenu1", "\RootMenu"))
    assert cli_client.io_buffer ==  "exit\nexit\nconfigure ChildMenu1\n"
    cli_client.io_buffer = ""

    #UnitTestComment("CaccMenu.get_child_menu() : go up and down to Child2_3")
    menu_navigator.navigate_to_menu(AccMenuPath("ChildMenu2_3", "\RootMenu\ChildMenu2"))
    assert cli_client.io_buffer ==  "exit\nconfigure ChildMenu2\nconfigure ChildMenu2_3\n"
    cli_client.io_buffer = ""

    #UnitTestComment("CaccMenu.get_child_menu() : go up root")
    menu_navigator.navigate_to_menu(AccMenuPath("RootMenu"))
    assert cli_client.io_buffer ==  "exit\nexit\n"
    cli_client.io_buffer = ""
