#############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use self file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of self file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http:#www.gnu.org/copyleft/gpl.html.
##
## In addition, following conditions apply:
##     # Redistributions in binary form must reproduce the above copyright
##       notice, list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     # Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from self software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, NOT
## LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, CONSEQUENTIAL DAMAGES (INCLUDING, NOT LIMITED
## TO, OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, IN CONTRACT, LIABILITY, TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
#############################################################################


from agglo_cc.core.command_results import *

from agglo_tt.fixtures import trace_test_start


def test_UT_AccCommandResults_Constructor(trace_test_start):
    #UnitTestComment("AccCommandResults()")
    command_results = AccCommandResults()
    assert "" == command_results.name
    assert not command_results.selectors
    assert not command_results.children
    
    #UnitTestComment("AccCommandResults()")
    command_results = AccCommandResults("test1")
    assert "test1" == command_results.name


def test_UT_AccCommandResults_AppendChild(trace_test_start):
    root_result = AccCommandResults("root")
    child1_result = AccCommandResults("child1")
    child2_result = AccCommandResults("child2")

    #UnitTestComment("AccCommandResults::append_child() with successful child")
    root_result.append_child(child1_result)
    root_result.append_child(child2_result)
    assert 2 == len(root_result.children)
    assert child1_result.parent is root_result
    assert root_result.children[0] is child1_result
    assert child2_result.parent is root_result
    assert root_result.children[1] is child2_result
    
    #UnitTestComment("AccCommandResults::append_child() with failed child: check that failed result goes up the tree")
    child11_result = AccCommandResults("child1_1", COMMAND_ERROR_MATCH)
    child1_result.append_child(child11_result)
    assert 2 == len(root_result.children)
    assert 1 == len(child1_result.children)
    assert child11_result.parent is child1_result
    assert COMMAND_ERROR_MATCH == child1_result.result
    assert COMMAND_ERROR_MATCH == root_result.result
    assert COMMAND_OK == child2_result.result
    
    #UnitTestComment("AccCommandResults::append_child() with failed child : check that a new failure does not override previous error")
    child21_result = AccCommandResults("child2_1", COMMAND_ERROR_CONNECTION)
    child2_result.append_child(child21_result)
    assert COMMAND_ERROR_CONNECTION == child2_result.result
    assert COMMAND_ERROR_MATCH == child1_result.result
    assert COMMAND_ERROR_MATCH == root_result.result


def test_UT_AccCommandResults_Properties(trace_test_start):
    root_result = AccCommandResults()
    
    #UnitTestComment("AccCommandResults::name")
    root_result.name = "test"
    assert "test" == root_result.name
    
    #UnitTestComment("AccCommandResults::selectors")
    root_result.selectors[0] = 5
    root_result.selectors[2] = 7
    assert 2 == len(root_result.selectors)
    assert 5 == root_result.selectors[0]
    assert 7 == root_result.selectors[2]
    
    #UnitTestComment("AccCommandResults::result: check automatic update of parent in case of child failure")
    child1_result = AccCommandResults()
    child2_result = AccCommandResults()
    root_result.append_child(child1_result)
    root_result.append_child(child2_result)
    child1_result.result = COMMAND_ERROR_TIMEOUT
    assert COMMAND_ERROR_TIMEOUT == child1_result.result
    assert COMMAND_ERROR_TIMEOUT == root_result.result
    assert COMMAND_OK == child2_result.result
    child1_result.result = COMMAND_OK
    assert COMMAND_OK == child1_result.result
    assert COMMAND_ERROR_TIMEOUT == root_result.result
    
    #UnitTestComment("AccCommandResults::succeeded")
    assert child1_result.succeeded
    assert child2_result.succeeded
    assert not root_result.succeeded
    
    #UnitTestComment("AccCommandResults::boolean")
    assert child1_result
    assert child2_result
    assert not root_result
    
    #UnitTestComment("AccCommandResults::leaves")
    assert child1_result.leaves[0] is child1_result
    leaves = root_result.leaves
    assert 2 == len(leaves)
    assert leaves[0] is child1_result
    assert leaves[1] is child2_result
    child11_result = AccCommandResults()
    child1_result.append_child(child11_result)
    leaves = root_result.leaves
    assert 2 == len(leaves)
    assert leaves[0] is child11_result
    assert leaves[1] is child2_result
    child12_result = AccCommandResults()
    child1_result.append_child(child12_result)
    leaves = root_result.leaves
    assert 3 == len(leaves)
    assert leaves[0] is child11_result
    assert leaves[1] is child12_result
    assert leaves[2] is child2_result

    #UnitTestComment("AccCommandResults::children")
    assert 2 == len(root_result.children)
    assert root_result.children[0] is child1_result
    assert root_result.children[1] is child2_result
    assert 2 == len(child1_result.children)
    assert child1_result.children[0] is child11_result
    assert child1_result.children[1] is child12_result
    assert 0 == len(child2_result.children)

    #UnitTestComment("AccCommandResults::parent")
    assert root_result.parent is None
    assert child1_result.parent is root_result
    assert child2_result.parent is root_result
    assert child11_result.parent is child1_result
    assert child12_result.parent is child1_result

    #UnitTestComment("AccCommandResults::ancestors")
    assert 0 == len(root_result.ancestors)
    assert 1 == len(child1_result.ancestors)
    assert child1_result.ancestors[0] is root_result
    assert 2 == len(child11_result.ancestors)
    assert child11_result.ancestors[0] is child1_result
    assert child11_result.ancestors[1] is root_result


def test_UT_AccCommandResults_GetLeaf(trace_test_start):
    root_result = AccCommandResults()
    child1_result = AccCommandResults()
    child2_result = AccCommandResults()
    root_result.append_child(child1_result)
    root_result.append_child(child2_result)
    child11_result = AccCommandResults()
    child12_result = AccCommandResults()
    child1_result.append_child(child11_result)
    child1_result.append_child(child12_result)
    
    #UnitTestComment("AccCommandResults::GetLeafSelector with unset selectors")
    assert root_result.get_leaf_selector(1, 0) is None
    child12_result.selectors[1] = 2
    assert root_result.get_leaf_selector(2, 1) is None
    
    #UnitTestComment("AccCommandResults::GetLeafSelector with index out of bound")
    assert root_result.get_leaf_selector(1, 3) is None
    assert root_result.get_leaf_selector(1, -4) is None
    
    #UnitTestComment("AccCommandResults::GetLeafSelector with correct type and index")
    assert 2 == root_result.get_leaf_selector(1, -2)
    child2_result.selectors[3] = 5
    child2_result.selectors[4] = 6
    assert 5 == root_result.get_leaf_selector(3, 2)
    assert 6 == root_result.get_leaf_selector(4, 2)
    
    #UnitTestComment("AccCommandResults::GetLeafSelector with correct type and default index")
    assert 5 == root_result.get_leaf_selector(3)
    assert 6 == root_result.get_leaf_selector(4)
    
    #UnitTestComment("AccCommandResults::GetLeafResult with index out of bound")
    assert root_result.get_leaf_result(3) is None
    assert root_result.get_leaf_result(-4) is None
    
    #UnitTestComment("AccCommandResults::GetLeafResult with correct index")
    child11_result.result = COMMAND_ERROR_TIMEOUT
    child2_result.result = COMMAND_ERROR_CONNECTION
    assert COMMAND_ERROR_TIMEOUT == root_result.get_leaf_result(0)
    assert COMMAND_OK == root_result.get_leaf_result(-2)
    assert COMMAND_ERROR_CONNECTION == root_result.get_leaf_result(2)
    
    #UnitTestComment("AccCommandResults::GetLeafResult with default index")
    assert COMMAND_ERROR_CONNECTION == root_result.get_leaf_result()
