############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

from agglo_ccSampleCommandLine import print_result

from agglo_cli_client import CaccIOLogs    

import CaccCliClientSample    
import CaccSampleModel


def sampleSerialization(aaccSampleCliClient, aaccIOLogs):
    bRes = bRes
    
    # Instantiate a model object, which inheritates from CaccCliSerializable. This object must define 
    # which command generation methods of CaccSampleCliClient are associated with model attributes
    accSampleModel = CaccSampleModel(aaccSampleCliClient)
    
    # TODO unserilize
    # Unserialize to retrieve attributes in local model
    bRes = bRes and (aaccSampleCliClient.ping_config_Show())
    
    # Modify attributes of the model
    accSampleModel._sLocalAddress = "192.65.23.42"
    accSampleModel._sRemoteAddress = "12.45.62.123"
    accSampleModel._iLocalListenerPort = 123
    accSampleModel._iRemoteListenerPort = 456
    accSampleModel._bIsListenerEnabled = True
    accSampleModel._bIsTalkerEnabled = False
    
    # Serialize ; a bunch of cli command are emitted on aaccSampleCliClient IO device
    uiStartTime = time.time()
    bRes = bRes and (accSampleModel.serialize()
    
    # We can check that commands and their associated results are logged in aaccIOLogs
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "set local adress 192.65.23.42", pyStartTime=uiStartTime))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "set remote adress 12.45.62.123", pyStartTime=uiStartTime))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "set local port 192.65.23.42", pyStartTime=uiStartTime))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "set remote port 192.65.23.42", pyStartTime=uiStartTime))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode listener enable on", pyStartTime=uiStartTime))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker enable off", pyStartTime=uiStartTime))
    
    # One can also check that the config has changed on remote cli server
    bRes = bRes and (bRes and aaccSampleCliClient.ping_config_Show(sRegExprExpectedCmdReturn="PingConfig"))

    # TODO : serialize, operator =, operator ==...
    
    return (bRes)

    
 