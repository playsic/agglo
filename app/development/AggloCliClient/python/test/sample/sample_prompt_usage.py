############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

from agglo_cli_client import CaccIOLogs    

import CaccSampleCliClient    

from sample_framework import print_result



@print_result    
def samplePromptUsage(aaccSampleCliClient, aaccIOLogs):
    bRes = True

    # The default regular expression to parse regexpr allows cli client to retrieve last prompt.
    # It can be done by CaccIOLogs checkEntry method on IOTYPE_PROMPT
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_TEST_GUI_MAIN))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_TEST_GUI_MAIN + ">"))
    
    # It can also be done as an expected prompt rgular expression passed to the CaccSampleCliClient cli command method
    bRes = bRes and (bRes and aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_PING, 
                                                                 sRegExprExpectedPrompt=CaccSampleCliClient.MENUNAME_CONFIG_PING + ">"))

    # During login for instance, a second prompt is sent by cli server to enter a password
    # TODO enrichier ce test avec une commande login sans passage de timeout et un passage d'expression reguliere pour le prompt
    # bRes = bRes and (not(aaccSampleCliClient.acsCliQtSample_Login(sRegExprExpectedPrompt=CaccSampleCliClient.MENUNAME_ROOT + ">")))
    # aaccSampleCliClient.executeCommand("111111\n")
    # time.sleep(0.5)
    
    # # So far "Enter your password:" prompt is recognized as a command entered by the user, and "*******Wrong Password\n" the result
    # # of this command sent by cli server
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "Enter your password:"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "\*\*\*\*\*\*Wrong Password\n"))
    
    # # But one can define extra parser regular expression to recognize new prompt type, so that "Enter your password:" is 
    # # now recognized as a prompt sent by cli server, "********" as a command sent by the user, and "Wrong Password\n" is the result
    # aaccSampleCliClient.addParserRegExpr(CaccIOLogs.IOTYPE_PROMPT, "[^>]*:")
    # aaccSampleCliClient.acsCliQtSample_Login()
    # aaccSampleCliClient.executeCommand("111111")
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "Enter your password:"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "\*\*\*\*\*\*"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "Wrong Password\n"))

    # # One can use the checkEntry method to check modification on prompt, for instance on a login timeout of 10000 ms
    # aaccSampleCliClient.acsCliQtSample_Login(sRegExprExpectedPrompt=CaccSampleCliClient.MENUNAME_ADMIN + ">")
    # time.sleep(11)
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_ROOT + ">"))
    
    # # There is a simpler way to wait for a modification on an IO : use the CaccIOLogs waitIO method
    # aaccSampleCliClient.acsCliQtSample_Login()
    # bRes = bRes and (not(aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_ROOT + ">")))
    # uiStartTime = time.time()
    # bRes = bRes and (aaccSampleCliClient.waitIO(CaccIOLogs.IOTYPE_PROMPT, 10000, CaccSampleCliClient.MENUNAME_ROOT + ">"))
    # uiEndTime = time.time()
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_ROOT + ">"))
    # bRes = bRes and (9 <= uiStartTime - uiEndTime <= 11)
    
    # TODO Listner d'un changement de prompt necessaire dans le cas du cli client, pas dans le cas du test conductor
    
    aaccSampleCliClient.removeParserRegExpr(CaccIOLogs.IOTYPE_PROMPT, "[^>]*:")

    return (bRes)



    
 