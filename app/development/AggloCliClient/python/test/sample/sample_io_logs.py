############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

from agglo_cli_client import CaccIOLogs

import CaccSampleCliClient

from sample_framework import print_result

   
    
@print_result
def sampleBasicIOLogs(aaccSampleCliClient, aaccIOLogs):
    bRes = True
    uiStartTime = 0
    lsMatchingEntries = []
    
    # First clean CaccIOLogs instance;this is not mandatory, we do it for test reasons only
    aaccIOLogs.reset()
    
    # Then navigate to a menu in which we can execute commands. This can be done with hard coded 
    # commands (see "sampleHardCodedMenuNavigation"), or with generated method of CaccSampleCliClient (see "sampleAutomatedMenuNavigation").
    # In this sample we will use generated methods for conveniency reasons
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_MONITOR_PING))

    # The CaccIOLogs instance associated with aaccSampleCliClient registers all cli IO. IOs are ordered by type and logged 
    # into an IO buffer dedicated to each type. The 4 existing types are CaccIOLogs.IOTYPE_PROMPT, CaccIOLogs.IOTYPE_COMMAND, 
    # CaccIOLogs.IOTYPE_COMMAND_RESULT, CaccIOLogs.IOTYPE_ASYNCHRONOUS. There is an extra type to register every IO, CaccIOLogs.IOTYPE_LOGS.
    # One can use checkEntry method to check various IO. We have just navigate to MENUPATH_MONITOR_PING, so let's check we've 
    # received expected prompt on the IO device
    # TODO ce bout de code est sensible, ca ne marche pas toujours; ca ressemble au bug de cliConnection.open() : la reception du resultat
    # semble aleatoire, le timeout est peut etre trop court ? ou alors c'est autre chose
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "monitor_ping>"))
    
    # checkEntry can also be used to check command sent, result received, asynchronous input received
    # The result can be empty, one line long or several lines long
    bRes = bRes and (aaccSampleCliClient.executeCommand("ping receive timeout 5 frames 4", 5000))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "ping receive timeout 5 frames 4\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "Ping 1 received\r\nPing 2 received\r\nPing 3 received\r\nPing 4 received\r\n"))
    
    # checkEntry can also use regular expression
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "monitor_.*>"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "ping receive timeout [0-9]{1,2} frames [0-9]{1,2}\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-9] received\r\n){4}"))
    # TODO
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_ASYNCHRONOUS, "EVENT [0-9]{3]:Ping [0-9]* received\r\n"))
    
    # checkEntry match a regular expression against last entry by default, but it can be used to match another entry if needed
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_PING))
    bRes = bRes and (aaccSampleCliClient.executeCommand("show"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "show\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "PingConfig\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure service ping\r\n", -2))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "", -2))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "ping receive timeout [0-9]{1,2} .*\r\n", -4))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-9] received\r\n){4}", -4))
    # TODO
    # bRes = bRes and (not(aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "", -10000)))
    
    # We can also retrieve all the entries corresponding to a given regular expression
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_PING))
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker off"))
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker on"))
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker off"))
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker on"))
    # bRes = bRes and (3 == len(aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, "mode talker on\r\n")))
    # bRes = bRes and (2 == len(aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, "mode talker off\r\n")))
    # bRes = bRes and (5 == len(aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, "mode talker o.{1,2}\r\n")))

    # # Matching entries research can also use time parameters
    # uiStartTime = time.time()
    # bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker on"))
    # time.sleep(0.5)
    # uiEndTime = time.time()
    # bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker enable on"))
    # bRes = bRes and ((2 == len((aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, "mode talker on", uiStartTime))))
    # bRes = bRes and ((1 == len((aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, "mode talker on", uiStartTime, uiEndTime))))
    # bRes = bRes and ((5 == len((aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, "mode talker on", pyEndTime=uiEndTime))))

    # # One can search for every entries matching a time period, whatever its format
    # lsMatchingEntries = aaccIOLogs.getMatchingEntries(CaccIOLogs.IOTYPE_COMMAND, pyStartTime=uiStartTime, pyEndTime=uiEndTime)
    # bRes = bRes and (1 == len((lsMatchingEntries))
    # bRes = bRes and ("mode talker enable on" == lsMatchingEntries[0])

    # # IOLogs can be reset
    aaccIOLogs.reset()
    bRes = bRes and (not(aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode on")))
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker on"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker on"))
    
    # There are richer usages of aaccIOLogs which allow to wait for or to be notified of a specified IO. These 
    # advanced functionnalities are detailed in further samples "sampleAsynchronousInputs" and "samplePromptUsage"
    
    # Return to root menu; this is not mandatory, we do it for test reasons only
    bRes = (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_ROOT)) and bRes
    
    return (bRes)
    

    
 