############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


from agglo_ccSampleCommandLine import print_result


from agglo_cli_client import CaccIOLogs    

import CaccCliClientSample    

@print_result    
def sampleCommandDelay(aaccSampleCliClient, aaccIOLogs):
    bRes = True
    
    # The cli client command method returns true when the command is executed (i.e. the next prompt is received)
    bRes = aaccSampleCliClient.monitor_ping_PingEmit(10, 500, 10000) and bRes
    
    # The cli client command method returns false when the timeout has triggered
    bRes = not(aaccSampleCliClient.monitor_ping_PingEmit(10, 500, 5000)) and bRes
    
    # In that case, one can monitor the partially received command return
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-4] emitted\n){4}") and bRes
    bRes = aaccSampleCliClient.waitIO(CaccIOLogs.IOTYPE_COMMAND_RESULT, "Ping 10 emitted\n", 5000) and bRes

    # One can send 2 unsuccesfull command, result of each of this command will be succesfully parsed and linked 
    # to its respective command at reception
    bRes = not(aaccSampleCliClient.monitor_ping_PingEmit(10, 500, 5000)) and bRes
    bRes = not(aaccSampleCliClient.monitor_ping_PingEmit(10, 500, 5000)) and bRes
    bRes = aaccSampleCliClient.waitIO(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping 10 emitted\n){2}", 10000) and bRes
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-4]{1,2} emitted\n){10}", -1) and bRes
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-4]{1,2} emitted\n){10}", -2) and bRes
    
    return (bRes)
 