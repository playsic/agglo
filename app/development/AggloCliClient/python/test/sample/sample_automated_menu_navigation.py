############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


from agglo_cli_client import CaccIOLogs    

import CaccSampleCliClient    

from sample_framework import print_result

@print_result
def sampleAutomatedMenuNavigation(aaccSampleCliClient, aaccIOLogs):
    bRes = True
    
    # As shown in sample "sampleHardCodedMenuNavigation", hard coded command becomes 
    # obsolete if commands to navigate inside a menu is changed in cli xml definition
    # One partial solution is to get navigation commands from CaccSampleCliClient 
    # classes. It does'nt resolve a menu name or a arborescence modification though
    # TODO est-ce que les getEnterCommand devraient etre statique?
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuMonitor_ping.getEnterCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "monitor_ping>"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "monitor ping\n"))
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuMonitor_ping.getExitCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "acsCliQtSample>"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\n"))
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuTest_gui_main.getEnterCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "test_gui_main>"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "test gui main\n"))
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuTest_gui_main.getExitCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, "acsCliQtSample>"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\n"))

    # # One can also use constant defined in CaccSampleCliClient
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuConfig_ping.getEnterCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_CONFIG_PING + ">"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure ping\n"))
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuConfig_ping.getExitCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_ROOT + ">"))
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuConfig_interfaces.getEnterCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_CONFIG_INTERFACES + ">"))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure interfaces\n"))
    # bRes = bRes and (aaccSampleCliClient.executeCommand(CaccSampleCliClient.CaccMenuConfig_interfaces.getExitCommand()))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_ROOT + ">"))
    
    # # As we've been navigating through menus without using automated menu navigation system, 
    # # it is compulsory to update CaccSampleCliClient current menu.
    # bRes = bRes and (aaccSampleCliClient.updateCurrentMenu())
    
    # The easiest way to navigate through menus is to use methods defined in CaccSampleCliClient
    # These methods generate all enter and exit commands, and keeps CaccSampleCliClient current menu up to date
    # Therefore CaccSampleCliClient.updateCurrentMenu() is not needed anymore. The only case where it is useful 
    # is the case of an unexpected menu change, for instance on timeout (see "samplePromptUsage")
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_MONITOR_PING))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_MONITOR_PING + ">"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "monitor service ping\r\n"))
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_TEST_GUI_MAIN))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_TEST_GUI_MAIN + ">"))
    # TODO tester toute la chaine de commande, pas seulement la derniere
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\r\ntest gui main\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "test gui main\r\n"))
    
    # One can directly add the expected prompt in the navigateToMenuCommand. In that case there is no use in executing 
    # an extra checkEntry on IOTYPE_PROMPT after navigateToMenu method call
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_PING, CaccSampleCliClient.MENUNAME_CONFIG_PING + ">"))
    # TODO tester toute la chaine de commande, pas seulement la derniere
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\r\nconfigure service ping\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure service ping\r\n"))
    bRes = bRes and (not(aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_INTERFACES, "bad_prompt>")))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_CONFIG_INTERFACES + ">"))
    # TODO tester toute la chaine de commande, pas seulement la derniere
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\r\nconfigure interfaces\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure interfaces\r\n"))
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_ROOT, CaccSampleCliClient.MENUNAME_ROOT + ">"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\r\n"))
    
    return (bRes)
    
 