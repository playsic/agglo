############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import re

from agglo_cc import AccCliSerializable
from agglo_cc import COMMAND_ERROR_MATCH
from agglo_cc.exceptions import AccExecutionError


__all__ = ["AccSampleModel"]

class AccSampleModel(AccCliSerializable):

    def __init__(self, cli_client):
        super(AccSampleModel, self).__init__(cli_client)
        
        # Model data
        self.remote_address = "0.0.0.0"
        self.local_port = 0
        self.remote_port = 0
        self.listener_enabled = False
        self.talker_enabled = False
        self.talker_mode_interval = 1
        self.heartbeat_data = ""

        # Connect data with serialization methods
        # self.add_serialize_method(AccSampleModel.serialize_remote_address, "remote_address")
        self.add_serialize_method(AccSampleModel.serialize_local_port, "local_port")
        self.add_serialize_method(AccSampleModel.serialize_remote_port, "remote_port")
        self.add_serialize_method(AccSampleModel.serialize_listener_mode, "listener_enabled")
        self.add_serialize_method(AccSampleModel.serialize_talker_mode, "talker_enabled")
        self.add_serialize_method(AccSampleModel.serialize_talker_mode_interval, "talker_mode_interval")
        self.add_serialize_method(AccSampleModel.serialize_heartbeat_data, "heartbeat_data")

        # Connect data with unserialization methods
        self.add_unserialize_method(AccSampleModel.unserialize_model, \
                                    "remote_address", \
                                    "local_port", "remote_port", \
                                    "listener_enabled", "talker_enabled", 
                                    "talker_mode_interval", "heartbeat_data")
        
        
    def __eq__(self, other):
        return ((self.remote_address == other.remote_address) and \
                (self.local_port == other.local_port) and (self.remote_port == other.remote_port) and \
                (self.listener_enabled == other.listener_enabled) and (self.talker_enabled == other.talker_enabled) and \
                (self.talker_mode_interval == other.talker_mode_interval) and (self.heartbeat_data == other.heartbeat_data))

                
    def __str__(self):
        result = "Remote " + self.remote_address + ":" + str(self.remote_port) + \
                 ", local port:" + str(self.local_port) + \
                 "\nlistener(" +  str(self.listener_enabled) + \
                 "), talker(" + str(self.talker_enabled) + \
                 ")\ntalker interval " + str(self.talker_mode_interval) + \
                 ", data: " + self.heartbeat_data
        
        return result

        
    def serialize_remote_address(self):
        return self._cli_client.config_heartbeat_RemoteIpAddress(self.remote_address)

        
    def serialize_local_port(self):
        return self._cli_client.config_heartbeat_LocalPort(self.local_port)

        
    def serialize_remote_port(self):
        return self._cli_client.config_heartbeat_RemotePort(self.remote_port)

        
    def serialize_listener_mode(self):
        command_result = None
        
        if self.listener_enabled:
            command_result = self._cli_client.config_heartbeat_ModeListenerOn()
        else:
            command_result = self._cli_client.disable_listener_mode()
        
        return command_result

        
    def serialize_talker_mode(self):
        command_result = None
        
        if self.talker_enabled:
            command_result = self._cli_client.config_heartbeat_ModeTalkerOn()
        else:
            command_result = self._cli_client.config_heartbeat_ModeTalkerOff()
        
        return command_result

        
    def serialize_talker_mode_interval(self):
        return self._cli_client.config_heartbeat_ModeTalkerInterval(self.talker_mode_interval)

        
    def serialize_heartbeat_data(self):
        return self._cli_client.config_heartbeat_ModeTalkerData(self.heartbeat_data)

        
    def unserialize_model(self):
        command_result = self._cli_client.acsSampleCliQt_ShowHeartbeatConfig()
        
        if command_result.succeeded:
            # Retrieve show command return
            retrieved_config = self._cli_client.io_logs[command_result.get_leaf_selector(self._cli_client.io_type_command_result)][0].data
            
            # Extract configuration values from command return
            config_regexpr = "------------------ HeartBeat Config ------------------\n" + \
                             "Remote listener: ([0-9]*)\nLocal listener port: ([0-9]*)\n" + \
                             "Mode listener: (.*)\nMode talker: (.*)\nMode talker interval: ([0-9]*)\n"
            match = re.search(config_regexpr, retrieved_config)
            
            # If regular expression has matched
            if match is not None:
                # Set attributes with server values, without setting serialization needed property
                # self.set_attr_furtive("remote_address", match.group(1))
                self.set_attr_furtive("remote_port", int(match.group(1)))
                self.set_attr_furtive("local_port", int(match.group(2)))
                self.set_attr_furtive("listener_enabled",  match.group(3) == "on")
                self.set_attr_furtive("talker_enabled", match.group(4) == "on")
                self.set_attr_furtive("talker_mode_interval", int(match.group(5)))
            else:
                command_result.result = COMMAND_ERROR_MATCH
                raise AccExecutionError(command_result)
                
        return command_result
