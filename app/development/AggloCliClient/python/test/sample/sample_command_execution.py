############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


import time

from agglo_cli_client import CaccIOLogs

import CaccSampleCliClient

from sample_framework import print_result

    
@print_result
def sampleBasicCommandExecution(aaccSampleCliClient, aaccIOLogs):
    bRes = True
    
    # To execute a cli command manually, one has to navigate to proper menu. This can be done with hard coded 
    # commands (see "sampleHardCodedMenuNavigation"), or with generated method of CaccSampleCliClient (see "sampleAutomatedMenuNavigation").
    # In this sample we will use generated methods for conveniency reasons.
    # It is a blocking call, hence it returns only when underlying commands have been successfully executed, 
    # or on timeout. Default timeout is set to CaccCliClient.WAIT_TIMEOUT_MS = 500ms
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_PING))

    # Then we can send the cli command. 
    # The first way to do so is to simply send the buffer. In that case, we have to include the \n
    # to make cli server execute the command
    # We also have to wait a little after sendRawData method call, until command echo and result is
    # received, since sendRawData is not blocking
    # Note:check of received IO (commands, results...) is explained in following sample "sampleBasicIOLogs"
    bRes = bRes and (aaccSampleCliClient.sendRawData("mode talker on\n"))
    bRes = bRes and (not aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker on\r\n"))
    time.sleep(0.5)
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker on\r\n"))
    
    # An incomplete command (without terminating \n) is not stored in CaccIOLogs.IOTYPE_COMMAND
    # It will be stored only once terminating \n is received
    bRes = bRes and (aaccSampleCliClient.sendRawData("mode talker off"))
    time.sleep(0.2)
    bRes = bRes and (not aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker off\n"))
    bRes = bRes and (aaccSampleCliClient.sendRawData("\n"))
    time.sleep(0.2)
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker off\r\n"))
    
    # A plain \n is also seen as a command
    bRes = bRes and (aaccSampleCliClient.sendRawData("\n"))
    time.sleep(0.2)
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "\r\n"))
    
    # The result of a command is stored in CaccIOLogs.IOTYPE_COMMAND_RESULT buffer
    bRes = bRes and (aaccSampleCliClient.sendRawData("mode talker on\n"))
    time.sleep(0.2)
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker on\r\n"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "talker mode: enabled"))
    
    # Another way to execute command is to use executeCommand method. In that case, the \n is automatically 
    # added to the command if needed.
    # It is a blocking call, hence it returns only when command has been successfully executed, 
    # or on timeout. Default timeout is set to CaccCliClient.WAIT_TIMEOUT_MS = 500ms
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode listener on"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode listener on\r\n"))
    
    # When using executeCommand method, the method will return at next prompt reception, 
    # or on timeout. Default timeout is set to CaccCliClient.WAIT_TIMEOUT_MS = 500ms
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_MONITOR_PING))
    uiStartTime = time.time()
    bRes = bRes and (aaccSampleCliClient.executeCommand("ping receive timeout 5 frames 4", 10000))
    uiEndTime = time.time()
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-9] received\r\n){4}"))
    bRes = bRes and ((3 <= (uiEndTime - uiStartTime) <= 6))
    uiStartTime = time.time()
    bRes = bRes and (not(aaccSampleCliClient.executeCommand("ping receive timeout 10 frames 5", 3000)))
    uiEndTime = time.time()
    bRes = bRes and (not(aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-9] received\r\n){4}")))
    bRes = bRes and ((3 < (uiEndTime - uiStartTime) <= 3.5))
    
    # executeCommand method also includes result and prompt check params. The check parameters are regular expressions which 
    # have to match result and/or prompt if provided. Check is done only if executeCommand is successful, i.e. the command has 
    # ended and next prompt has been received
    # These parameters avoid invocation of CaccIOLogs.checkEntry with IOTYPE_COMMAND_RESULT or IOTYPE_PROMPT after a executeCommand call
    time.sleep(3)
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_CONFIG_PING))
    bRes = bRes and (aaccSampleCliClient.executeCommand("mode talker on", 
                                                        sRegExprExpectedCmdReturn="talker mode.*\r\n", 
                                                        sRegExprExpectedPrompt="config_ping>"))
    bRes = bRes and (aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_MONITOR_PING))
    bRes = bRes and (aaccSampleCliClient.executeCommand("ping receive timeout 5 frames 4", 10000, "(Ping [0-9] received\r\n){4}"))
    bRes = bRes and (aaccSampleCliClient.executeCommand("ping receive timeout 5", 10000, sRegExprExpectedPrompt="monitor_.*>"))
    
    # Return to root menu; this is not mandatory, we do it for test reasons only
    bRes = aaccSampleCliClient.navigateToMenu(CaccSampleCliClient.MENUPATH_ROOT) and bRes
    
    return (bRes)
    
 