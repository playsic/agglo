############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import os
import subprocess
import time

from agglo_cc import AccCliIOLogs
from agglo_tk.io import AtkIODeviceTCP

from agglo_ccSampleCliClient import AaccSampleCliClient
from sample_command_execution import sampleBasicCommandExecution
from sample_io_logs import sampleBasicIOLogs
from sample_hard_coded_menu_navigation import sampleHardCodedMenuNavigation
from sample_automated_menu_navigation import sampleAutomatedMenuNavigation
from sample_automated_cli_commands import sampleAutomatedCliCommands
from sample_prompt_usage import samplePromptUsage
# from agglo_ccSampleSerialization import sampleSerialization

    
def runCliClientSequence(accIOLogs, accSampleCliClient1, accSampleCliClient2 = None):
    result = True
    
    result = sampleBasicCommandExecution(accSampleCliClient1, accIOLogs) and result
    if (accSampleCliClient2 is not None):
        result = sampleBasicCommandExecution(accSampleCliClient2, accIOLogs) and result
        
    result = sampleBasicIOLogs(accSampleCliClient1, accIOLogs) and result
    if (accSampleCliClient2 is not None):
        result = sampleBasicIOLogs(accSampleCliClient2, accIOLogs) and result
        
    result = sampleHardCodedMenuNavigation(accSampleCliClient1, accIOLogs) and result
    if (accSampleCliClient2 is not None):
        result = accSampleCliClient2, accIOLogs) and result
        
    result = sampleAutomatedMenuNavigation(accSampleCliClient1, accIOLogs) and result
    if (accSampleCliClient2 is not None):
        result = sampleAutomatedMenuNavigation(accSampleCliClient2, accIOLogs) and result
        
    result = sampleAutomatedCliCommands(accSampleCliClient1, accIOLogs) and result
    if (accSampleCliClient2 is not None):
        result = sampleAutomatedCliCommands(accSampleCliClient2, accIOLogs) and result
        
    result = samplePromptUsage(accSampleCliClient1, accIOLogs) and result
    if (accSampleCliClient2 is not None):
        result = samplePromptUsage(accSampleCliClient2, accIOLogs) and result
        
    # result = accSampleSerialization(accSampleCliClient1, accIOLogs) and result
    # if (accSampleCliClient2 is not None):
        # result = accSampleSerialization(accSampleCliClient2, accIOLogs) and result
        
    # result = accSampleAsynchronousInputs(accSampleCliClient1, accIOLogs) and result
    # if (accSampleCliClient2 is not None):
        # result = accSampleAsynchronousInputs(accSampleCliClient2, accIOLogs) and result
        
    # TODO instanciation du client par runCliClientSequence ?
    # TODO script recording
    # TODO print IOLogs
        
    return (result)
    

if __name__ == '__main__':
    # Start the cli server, listening on TCP port 2002, on 127.0.0.1
    s_cli_server_path = os.path.join(os.path.dirname(__file__), '../../../../AggloCliServer/cli/samples/clisample_qt/build/debug/cliQtSample')
    pyCliServerProcess = subprocess.Popen(args=[s_cli_server_path])
    time.sleep(5)

    # One can open and close several cli client sucessively
    # for i in range(3):
    for i in range(1):
        # Create objects for cli communication : iodevice, log, cli client
        # TODO script recorder
        accIODeviceTelnet = AtkIODeviceTelnet.AtkIODeviceTelnet("127.0.0.1", CP
        accIOLogs = AccCliIOLogs.AccCliIOLogs()
        accSampleCliClient = CaccSampleCliClient.CaccCliClientacsSampleCliQt(accIODeviceTelnet)
        
        # Start the cli client: cli logs will be registered in accIOLogs
        # Timeout (ie time until cli client receives the first prompt) for cli communication start is 0.5s
        accSampleCliClient.open(accIOLogs, 0.5)
        # time.sleep(2)
        
        # Execute sample cli client/server communication
        result = runCliClientSequence(accIOLogs, accSampleCliClient)

        # Stop the cli client
        accSampleCliClient.close()
        accSampleCliClient = None

    # One can also open several cli client at the same time
    # accIODeviceTelnet1 = AtkIODeviceTelnet.AtkIODeviceTelnet("127.0.0.1", CP
    # accIOLogs1 = AccCliIOLogs.AccCliIOLogs()
    # accSampleCliClient1 = CaccSampleCliClient.CaccCliClientacsSampleCliQt(accIODeviceTelnet1)
    # accIODeviceTelnet2 = AtkIODeviceTelnet.AtkIODeviceTelnet("127.0.0.1", CP
    # accIOLogs2 = AccCliIOLogs.AccCliIOLogs()
    # accSampleCliClient2 = CaccSampleCliClient.CaccCliClientacsSampleCliQt(accIODeviceTelnet2)
    # result = runCliClientSequence(accSampleCliClient1, accSampleCliClient2)

    # Stop the cli client
    # accSampleCliClient.close()
    # accSampleCliClient = None
        
    # Stop the cli server
    pyCliServerProcess.terminate()
 