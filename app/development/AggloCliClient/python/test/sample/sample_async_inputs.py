############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

from agglo_ccSampleCommandLine import print_result


from agglo_cli_client import CaccIOLogs    

import CaccCliClientSample    
    


@print_result    
def sampleAsynchronousInputs(aaccSampleCliClient, aaccIOLogs):
    bRes = True

    Define regular expression to parse asynchronous input
    bRes = aaccSampleCliClient.addParserRegExpr(CaccIOLogs.IOTYPE_ASYNCHRONOUS, "\[00[0-9]\].*\n")
    
    # Start listening to emit a ping every 500ms
    bRes = aaccSampleCliClient2.config_ping_ModeTalkerPeriod(500) and bRes
    
    # Start listening to ping emitted by remote host
    bRes = aaccSampleCliClient.config_ping_RemoteIpAddress("127.0.0.1") and bRes
    bRes = aaccSampleCliClient.setRemotePort(9212) and bRes
    bRes = aaccSampleCliClient.config_ping_ModeListenerEnable(True) and bRes

    # Wait until one asynchronous event is received. Timeout is set to 10000ms
    bRes = aaccSampleCliClient.waitIO(CaccIOLogs.IOTYPE_ASYNCHRONOUS, 10000, "\[0[0-9]{2}\]Ping [0-9] received\n") and bRes

    # One can also wait until 10 asynchronous event is received. Timeout is set to 10000ms
    bRes = aaccSampleCliClient.waitIO(CaccIOLogs.IOTYPE_ASYNCHRONOUS, 10000, "\[0[0-9]{2}\]Ping [0-9]{1,2} received\n", 10) and bRes

    # The asynchronous entry is stored in IOBuffer Asynchronous and in IOBuffer All
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_ASYNCHRONOUS, "\[0[0-9]{2}\]Ping [0-9]{1,2} received\n")) and bRes
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_ALL, "\[0[0-9]{2}\]Ping [0-9]{1,2} received\n")) and bRes
    
    # The asynchronous can be received while a command is being executed and cli client is waiting for result
    # For instance, this method emits 10 ping, one every 500 ms, then returns
    bRes = aaccSampleCliClient.monitor_ping_PingEmit(10, 500) and bRes
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_ASYNCHRONOUS, "\[0[0-9]{2}\]Ping [0-9]{1,2} received\n")) and bRes
     
    # TODO inserer des asynchrones au milieu de la commande
    # TODO Listner d'une entree asynchrone necessaire dans le cas du cli client, pas dans le cas du test conductor
            
    # Undefine regular expression to parse asynchronous input. Now received asynchronous input will be interpreted as command or 
    # result, depending at which time it arrives
    aaccSampleCliClient.removeParserRegExpr(CaccIOLogs.IOTYPE_ASYNCHRONOUS, "\[00[0-9]\].*\n")
    time.sleep(500)
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "\[0[0-9]{2}\]Ping [0-9]{1,2} received\n")) and bRes
    bRes = aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_RESULT, "\[0[0-9]{2}\]Ping [0-9]{1,2} received\n")) and bRes

    return (bRes)


    
 