############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time
    
from agglo_cli_client import CaccIOLogs    

import CaccSampleCliClient

from sample_framework import print_result

        
@print_result
def sampleAutomatedCliCommands(aaccSampleCliClient, aaccIOLogs):
    bRes = True
    
    # Cli command execution can be done by navigating through menus (manually or automatically), then execute a hard coded 
    # cli command, as seen in previous sample "sampleBasicCommandExecution". This suffers of several backwards:
    # - menu name can change,
    # - menu arborescence can change,
    # - command keywords can change,
    # To workaround these problems, AggloCliClient offers an automated code generation for cli command invocation, which 
    # generate codes for automatically navigate to proper menu then send up to date cli command, wait until next prompt
    # is available, then check if result and final prompt match expected values
    # Generated method generation respect following rules
    # - Agglo generates as few methods as possible
    # - generated method are named by menu name, and command keywords;
    # - the shortest possible name is generated, 
    # - if the command contains optional argument, a single method is generated, with default values set to None for optional arguments; 
    # - if a command can end in several ways, i.e. with different keywords, several methods are generated.
    
    # We can see that both user command and menu navigation commands are automatically generated and properly executed
    bRes = bRes and (aaccSampleCliClient.config_ping_ModeTalkerOn())
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker on\r\n", -1))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure service ping\r\n", -2))
    bRes = bRes and (aaccSampleCliClient.config_interfaces_LocalIpAddress("123.45.65.12"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "local ip address 123.45.65.12\r\n", -1))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "configure interfaces\r\n", -2))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "exit\r\n", -3))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "mode talker on\r\n", -4))
        
    # If a command name is specified in cli xml and if the method needs to be generated(regards to rules enounced above), the name 
    # specified in cli xml is used. This allows the cli client script to always invoke the same method, regardless of cli xml
    # modification (command arborescence, menu name, command keywords...)
    bRes = bRes and (aaccSampleCliClient.setRemotePort(85))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "remote port 85\r\n"))
    
    # One can also use CaccSampleCliClient menus method to retrieve format of generated commands
    bRes = bRes and (aaccSampleCliClient.config_ping_ModeListenerOn())
    # TODO utilisation de commandes statiques
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, CaccSampleCliClient.CaccMenuConfig_ping.getCommandModeListenerOn(), -1))
    # bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, CaccSampleCliClient.CaccMenuConfig_ping.getCommandSetRemotePort(85), -2))

    # When possible, AggloCliClient generates only one method if xml cli command format expects optional params
    bRes = bRes and (aaccSampleCliClient.emitPing())
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "ping emit\r\n"))
    bRes = bRes and (aaccSampleCliClient.emitPing(3))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "ping emit 3\r\n"))
    bRes = bRes and (aaccSampleCliClient.emitPing(3, 4))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND, "ping emit 3 interval 4\r\n"))
    
    # Generated method for cli command execution return at next prompt reception, or on timeout. Default timeout is set to 
    # CaccCliClient.WAIT_TIMEOUT_MS = 500ms
    uiStartTime = time.time()
    bRes = bRes and (aaccSampleCliClient.receivePing(5, 4, auiNbMsTimeout=10000))
    uiEndTime = time.time()
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-9] received\r\n){4}"))
    bRes = bRes and (3 <= (uiEndTime - uiStartTime) <= 6)
    uiStartTime = time.time()
    bRes = bRes and (not(aaccSampleCliClient.receivePing(5, 5, auiNbMsTimeout=3000)))
    uiEndTime = time.time()
    bRes = bRes and (not(aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_COMMAND_RESULT, "(Ping [0-9] received\r\n){4}")))
    bRes = bRes and (3 < (uiEndTime - uiStartTime) <= 3.5)
    time.sleep(3)
    
    # Generated method for cli command execution also includes result and prompt check params. The check parameters are regular 
    # expressions which have to match result and/or prompt if provided. Check is done only if executeCommand is successful, 
    # i.e. the command has ended and next prompt has been received
    # These params avoid invocation of CaccIOLogs.checkEntry with IOTYPE_COMMAND_RESULT or IOTYPE_PROMPT after a executeCommand call
    bRes = bRes and (aaccSampleCliClient.config_ping_RemoteIpAddress("101.23.124.12", sRegExprExpectedCmdReturn="remote address: 101\.23\.124\..*"))
    bRes = bRes and (aaccIOLogs.checkEntry(CaccIOLogs.IOTYPE_PROMPT, CaccSampleCliClient.MENUNAME_CONFIG_PING))
    bRes = bRes and (aaccSampleCliClient.config_interfaces_LocalIpAddress("10.12.123.45", sRegExprExpectedPrompt="config_.*"))

    return (bRes)

    
    
 