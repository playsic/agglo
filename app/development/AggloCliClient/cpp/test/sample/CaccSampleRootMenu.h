/***************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
***************************************************************************/

#include "atc/CatcMenu.h"


#define ROOTMENU "TestConductorSample"
#define FONCTIONMENU "Fonction"
#define IHMMENU "ihm"
#define DLG1MENU "Dlg1"
#define DLG2MENU "Dlg2"


class CatcSampleRootMenu : public CatcMenu
{
public :
    CatcSampleRootMenu():CatcMenu(ROOTMENU)
    {
        const CatcSampleMenuFonc* const atcp_SampleMenuFonc = new CatcSampleMenuFonc(this, std::string(ROOTMENU) + MENUSEPARATOR + FONCTIONMENU);
        const CatcSampleMenuIhm* const atcp_SampleMenuIhm = new CatcSampleMenuIhm(this, std::string(ROOTMENU) + MENUSEPARATOR + IHMMENU);
        
        addChildMenu(*atcp_SampleMenuFonc);
        addChildMenu(*atcp_SampleMenuIhm);
    };
    virtual ~CatcSampleRootMenu(){};

    virtual void enterMenu() const {printf("start CLI\n");};

    class CatcSampleMenuFonc : public CatcMenu
    {
    private:
        friend class CatcSampleRootMenu;
        CatcSampleMenuFonc(const CatcSampleRootMenu* const aatcp_ParentMenu, const std::string& s_Path):CatcMenu(aatcp_ParentMenu, s_Path){};
        virtual ~CatcSampleMenuFonc(){};
        
    public:    
        virtual void enterMenu() const {printf("configure fonction\n");};
        const bool Cmd3() const {printf("Cmd3\n");};
    };
    
    class CatcSampleMenuIhm : public CatcMenu
    {
    private:
        friend class CatcSampleRootMenu;
        CatcSampleMenuIhm(const CatcSampleRootMenu* const aatcp_ParentMenu, const std::string& s_Path):CatcMenu(aatcp_ParentMenu, s_Path)
        {
            CatcSampleMenuDlg1* const atcp_SampleMenuDlg1 = new CatcSampleMenuDlg1(this, matc_Path.getPath() + MENUSEPARATOR + DLG1MENU);
            CatcSampleMenuDlg2* const atcp_SampleMenuDlg2 = new CatcSampleMenuDlg2(this, matc_Path.getPath() + MENUSEPARATOR + DLG2MENU);

            addChildMenu(*atcp_SampleMenuDlg1);
            addChildMenu(*atcp_SampleMenuDlg2);
        };
        
        virtual ~CatcSampleMenuIhm(){};
        
        virtual void enterMenu() const {printf("configure ihm\n");};
    
    public:    
        class CatcSampleMenuDlg1 : public CatcMenu
        {
        private:
            friend class CatcSampleMenuIhm;
            CatcSampleMenuDlg1(const CatcSampleMenuIhm* const aatcp_ParentMenu, const std::string& s_Path):CatcMenu(aatcp_ParentMenu, s_Path){};
            virtual ~CatcSampleMenuDlg1(){};
            
        public:    
            virtual void enterMenu() const {printf("configure Dlg1\n");};
            const bool Cmd1() const {printf("Cmd1\n");};
        };
    
        class CatcSampleMenuDlg2 : public CatcMenu
        {
        private:
            friend class CatcSampleMenuIhm;
            CatcSampleMenuDlg2(const CatcSampleMenuIhm* const aatcp_ParentMenu, const std::string& s_Path):CatcMenu(aatcp_ParentMenu, s_Path){};
            virtual ~CatcSampleMenuDlg2(){};
            
        public:    
            virtual void enterMenu() const {printf("configure Dlg2\n");};
            const bool Cmd2() const {printf("Cmd2\n");};
        };
    };
};

