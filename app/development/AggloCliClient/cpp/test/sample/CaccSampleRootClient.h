/***************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
***************************************************************************/

#include "atc/CatcTestConductor.h"

#include "CatcSampleRootMenu.h"

class CatcSampleTestConductor : public CatcTestConductor
{
public:
    CatcSampleTestConductor(const CatcSampleRootMenu& aatc_SampleRootMenu):CatcTestConductor(aatc_SampleRootMenu){};
    ~CatcSampleTestConductor(){};
    
    const bool Cmd1()
    {
        bool b_Res = true;        
        const CatcMenu* patc_SampleMenuDlg1_Temp = NULL;
        const CatcSampleRootMenu::CatcSampleMenuIhm::CatcSampleMenuDlg1* patc_SampleMenuDlg1 = NULL;

        patc_SampleMenuDlg1_Temp = navigateToMenu(CatcMenuPath(std::string(ROOTMENU) + MENUSEPARATOR + IHMMENU + MENUSEPARATOR + DLG1MENU));
        patc_SampleMenuDlg1 = dynamic_cast<const CatcSampleRootMenu::CatcSampleMenuIhm::CatcSampleMenuDlg1*>(patc_SampleMenuDlg1_Temp);
        b_Res = (NULL != patc_SampleMenuDlg1);
        
        if (b_Res)
        {
            b_Res = patc_SampleMenuDlg1->Cmd1();
        }
        
        return b_Res;
    }
    
    const bool Cmd2()
    {
        bool b_Res = true;        
        const CatcMenu* patc_SampleMenuDlg2_Temp = NULL;
        const CatcSampleRootMenu::CatcSampleMenuIhm::CatcSampleMenuDlg2* patc_SampleMenuDlg2 = NULL;
        
        patc_SampleMenuDlg2_Temp = navigateToMenu(CatcMenuPath(std::string(ROOTMENU) + MENUSEPARATOR + IHMMENU + MENUSEPARATOR + DLG2MENU));
        patc_SampleMenuDlg2 = dynamic_cast<const CatcSampleRootMenu::CatcSampleMenuIhm::CatcSampleMenuDlg2*>(patc_SampleMenuDlg2_Temp);
        b_Res = (NULL != patc_SampleMenuDlg2);
        
        if (b_Res)
        {
            b_Res = patc_SampleMenuDlg2->Cmd2();
        }
        
        return b_Res;
    }
    
    const bool Cmd3Name()
    {
        bool b_Res = true;        
        const CatcMenu* patc_SampleMenuFonction_Temp = NULL;
        const CatcSampleRootMenu::CatcSampleMenuFonc* patc_SampleMenuFonction = NULL;
        
        patc_SampleMenuFonction_Temp = navigateToMenu(CatcMenuPath(std::string(ROOTMENU) + MENUSEPARATOR + FONCTIONMENU));
        patc_SampleMenuFonction = dynamic_cast<const CatcSampleRootMenu::CatcSampleMenuFonc*>(patc_SampleMenuFonction_Temp);
        b_Res = (NULL != patc_SampleMenuFonction);
        
        if (b_Res)
        {
            b_Res = patc_SampleMenuFonction->Cmd3();
        }
        
        return b_Res;
    }

};

