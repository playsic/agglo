/***************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
** PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
***************************************************************************/

#ifndef _ATC_MENU_NAVIGATOR_UT_H_
#define _ATC_MENU_NAVIGATOR_UT_H_

#include "CatcMenu_UT.h"
#include "atc/CatcMenuNavigator.h"

#define ROOTMENU "RootMenu"
#define CHILDMENU1 "ChildMenu1"
#define CHILDMENU2 "ChildMenu2"
#define CHILDMENU2_3 "ChildMenu2_3"
#define CHILDMENU2_4 "ChildMenu2_4"


class CatcMenuNavigatorUT_Root : public CatcMenuUTSample
{
public :
    CatcMenuNavigatorUT_Root():CatcMenuUTSample(ROOTMENU)
    {
        const CatcMenuNavigatorUT_Child1* const atcp_MenuNavigatorUT_Child1 = new CatcMenuNavigatorUT_Child1(this, CHILDMENU1);
        const CatcMenuNavigatorUT_Child2* const atcp_MenuNavigatorUT_Child2 = new CatcMenuNavigatorUT_Child2(this, CHILDMENU2);
    };
    virtual ~CatcMenuNavigatorUT_Root(){};

    class CatcMenuNavigatorUT_Child1 : public CatcMenuUTSample
    {
    private:
        friend class CatcMenuNavigatorUT_Root;
        CatcMenuNavigatorUT_Child1(CatcMenuNavigatorUT_Root* const aatcp_ParentMenu, const std::string& s_MenuName):CatcMenuUTSample(aatcp_ParentMenu, s_MenuName){};
        virtual ~CatcMenuNavigatorUT_Child1(){};
    };
    
    class CatcMenuNavigatorUT_Child2 : public CatcMenuUTSample
    {
    private:
        friend class CatcMenuNavigatorUT_Root;
        CatcMenuNavigatorUT_Child2(CatcMenuNavigatorUT_Root* const aatcp_ParentMenu, const std::string& s_MenuName):CatcMenuUTSample(aatcp_ParentMenu, s_MenuName)
        {
            CatcMenuNavigatorUT_Child2_3* const atcp_MenuNavigatorUT_Child2_3 = new CatcMenuNavigatorUT_Child2_3(this, CHILDMENU2_3);
            CatcMenuNavigatorUT_Child2_4* const atcp_MenuNavigatorUT_Child2_4 = new CatcMenuNavigatorUT_Child2_4(this, CHILDMENU2_4);
        };
        virtual ~CatcMenuNavigatorUT_Child2(){};
    
    public:    
        class CatcMenuNavigatorUT_Child2_3 : public CatcMenuUTSample
        {
        private:
            friend class CatcMenuNavigatorUT_Child2;
            CatcMenuNavigatorUT_Child2_3(CatcMenuNavigatorUT_Child2* const aatcp_ParentMenu, const std::string& s_MenuName):CatcMenuUTSample(aatcp_ParentMenu, s_MenuName){};
            virtual ~CatcMenuNavigatorUT_Child2_3(){};
        };
    
        class CatcMenuNavigatorUT_Child2_4 : public CatcMenuUTSample
        {
        private:
            friend class CatcMenuNavigatorUT_Child2;
            CatcMenuNavigatorUT_Child2_4(CatcMenuNavigatorUT_Child2* const aatcp_ParentMenu, const std::string& s_MenuName):CatcMenuUTSample(aatcp_ParentMenu, s_MenuName){};
            virtual ~CatcMenuNavigatorUT_Child2_4(){};
        };
    };
};


class CatcMenuNavigatorUT : public CatcMenuNavigator
{
public:
    CatcMenuNavigatorUT(const CatcMenuNavigatorUT_Root& aatc_MenuNavigatorUT_Root):CatcMenuNavigator(aatc_MenuNavigatorUT_Root){};
    ~CatcMenuNavigatorUT(){};
};


#endif //_ATC_MENU_NAVIGATOR_UT_H_