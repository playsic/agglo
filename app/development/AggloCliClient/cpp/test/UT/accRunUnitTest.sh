
# Global variables
# equivalent $(dirname $0)
TUTestConductorShellPath=${0%/*}
BOOSTShellPath=$TUTestConductorShellPath/../../../../../AggloUnitTest/cpp/src/output
TUTestConductorBinaryPath=$TUTestConductorShellPath/../../build/netbeans/AggloTestConductor_Cpp_UT/dist
TUTestConductorBinary=agglotestconductor_cpp_ut.exe
TUTestConductorReportResPath=$TUTestConductorShellPath/STR_TU

# Check the parameters
ParamOK=true
if [ $# != 1 ] && [ $# != 2 ];
then
    echo "Commmand syntax : runTestU_Sample {DEBUG|RELEASE} [Verbose]"
    ParamOK=false
fi


if $ParamOK ;
then

    # Init parameters
    if [ $1 = "DEBUG" ]
    then
        TUTestConductorBinaryPath=$TUTestConductorBinaryPath/Debug/Cygwin_4.x-Windows
    else
        TUTestConductorBinaryPath=$TUTestConductorBinaryPath/Release/Cygwin_4.x-Windows
    fi

    if [ $# = 2 ] && [ $2 = "Verbose" ];
    then
        Verbose="Verbose"
    else
        Verbose=""
    fi

    # Run the testU
    $BOOSTShellPath/autRunTestU.sh $TUTestConductorBinaryPath $TUTestConductorBinary $BOOSTShellPath $TUTestConductorReportResPath $Verbose
fi

