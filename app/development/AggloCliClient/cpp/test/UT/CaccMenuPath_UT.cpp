/***************************************************************************
**
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
**
** This file is part of the Agglo project.
**
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Plaisic.  For licensing terms and
** conditions contact eti.laurent@gmail.com.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** In addition, the following conditions apply:
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in
**       the documentation and/or other materials provided with the
**       distribution.
**     * Neither the name of the Agglo project nor the names of its
**       contributors may be used to endorse or promote products derived
**       from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
** PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** AGGLO_END_LICENSE
**
***************************************************************************/

#include "aut/autUnitTestTemplate.h"

#include "atc/CatcMenuPath.h"


BOOST_AUTO_TEST_SUITE(CatcMenuPath_UT)

BOOST_AUTO_TEST_CASE(CatcMenuPath_UT_Constructor)
{
    UnitTestComment("CatcMenuPath::CatcMenuPath(Path)");
    CatcMenuPath atc_ParentPath("\\ParentPath1\\ParentPath2");
    BOOST_CHECK_EQUAL(atc_ParentPath.getPath(), "\\ParentPath1\\ParentPath2");
    
    UnitTestComment("CatcMenuPath::CatcMenuPath(ChildMenu)");
    CatcMenuPath atc_ChildPath(atc_ParentPath, "ChildMenu");
    BOOST_CHECK_EQUAL(atc_ChildPath.getPath(), "\\ParentPath1\\ParentPath2\\ChildMenu");

    UnitTestComment("CatcMenuPath::CatcMenuPath() : Check menu path no separator");
    CatcMenuPath atc_ParentPath2("ParentPath2");
    CatcMenuPath atc_ChildPath2("ParentPath2\\ChildMenu2");
    BOOST_CHECK_EQUAL(atc_ParentPath2.getPath(), "\\ParentPath2");
    BOOST_CHECK_EQUAL(atc_ChildPath2.getPath(), "\\ParentPath2\\ChildMenu2");
}

BOOST_AUTO_TEST_CASE(CatcMenuPath_UT_Operator)
{
    UnitTestComment("CatcMenuPath::operator ==");
    CatcMenuPath atc_ParentPath1("\\ParentPath1\\ParentPath2");
    CatcMenuPath atc_ParentPath2("\\ParentPath1\\ParentPath2");
    BOOST_CHECK_EQUAL(atc_ParentPath1, atc_ParentPath2);

    UnitTestComment("CatcMenuPath::operator !=");
    CatcMenuPath atc_ParentPath3("\\ParentPath1\\ParentPath4");
    BOOST_CHECK(atc_ParentPath1 != atc_ParentPath3);
}


BOOST_AUTO_TEST_CASE(CatcMenuPath_UT_MenuName)
{
    UnitTestComment("CatcMenuPath::getMenuName() : Check nominal case");
    CatcMenuPath atc_ParentPath1("\\Root1");
    CatcMenuPath atc_ChildPath1("\\Root1\\Child1");
    BOOST_CHECK_EQUAL(atc_ParentPath1.getMenuName(), "Root1");
    BOOST_CHECK_EQUAL(atc_ChildPath1.getMenuName(), "Child1");
    
    UnitTestComment("CatcMenuPath::getMenuName() : Check menu path no separator");
    CatcMenuPath atc_ParentPath2("Root2");
    CatcMenuPath atc_ChildPath2("Root2\\Child2");
    BOOST_CHECK_EQUAL(atc_ParentPath2.getMenuName(), "Root2");
    BOOST_CHECK_EQUAL(atc_ChildPath2.getMenuName(), "Child2");
}


BOOST_AUTO_TEST_CASE(CatcMenuPath_UT_CommonAncestor)
{
    UnitTestComment("CatcMenuPath::getCommonAncestor() : Check common ancestor direct descendant");
    CatcMenuPath atc_ParentPath0("\\ParentPath1");
    CatcMenuPath atc_ParentPath1("\\ParentPath1\\ParentPath2");
    CatcMenuPath atc_CommonAncestor0_0 = atc_ParentPath0.getCommonAncestor(atc_ParentPath0);
    CatcMenuPath atc_CommonAncestor0_1 = atc_ParentPath1.getCommonAncestor(atc_ParentPath0);
    CatcMenuPath atc_CommonAncestor1_0 = atc_ParentPath0.getCommonAncestor(atc_ParentPath1);
    BOOST_CHECK_EQUAL(atc_CommonAncestor0_0.getPath(), "\\ParentPath1");
    BOOST_CHECK_EQUAL(atc_CommonAncestor0_1.getPath(), "\\ParentPath1");
    BOOST_CHECK_EQUAL(atc_CommonAncestor0_1.getPath(), atc_CommonAncestor1_0.getPath());
    
    UnitTestComment("CatcMenuPath::getCommonAncestor() : Check common ancestor sibblings");
    CatcMenuPath atc_ParentPath2("\\ParentPath1\\ParentPath3");
    CatcMenuPath atc_CommonAncestor1_2 = atc_ParentPath1.getCommonAncestor(atc_ParentPath2);
    CatcMenuPath atc_CommonAncestor2_1 = atc_ParentPath2.getCommonAncestor(atc_ParentPath1);
    BOOST_CHECK_EQUAL(atc_CommonAncestor1_2.getPath(), "\\ParentPath1");
    BOOST_CHECK_EQUAL(atc_CommonAncestor1_2.getPath(), atc_CommonAncestor2_1.getPath());

    UnitTestComment("CatcMenuPath::getCommonAncestor() : Check no common ancestor");
    CatcMenuPath atc_ParentPath3("\\ParentPath4\\ParentPath5");
    CatcMenuPath atc_CommonAncestor1_3 = atc_ParentPath1.getCommonAncestor(atc_ParentPath3);
    CatcMenuPath atc_CommonAncestor3_1 = atc_ParentPath3.getCommonAncestor(atc_ParentPath1);
    BOOST_CHECK_EQUAL(atc_CommonAncestor1_3.getPath(), "\\");
    BOOST_CHECK_EQUAL(atc_CommonAncestor1_3.getPath(), atc_CommonAncestor3_1.getPath());

    UnitTestComment("CatcMenuPath::getCommonAncestor() : Check common ancestor complex case");
    CatcMenuPath atc_ParentPath4("\\ParentPath1\\ParentPath2 Extra\\ParentPath6");
    CatcMenuPath atc_CommonAncestor1_4 = atc_ParentPath1.getCommonAncestor(atc_ParentPath4);
    CatcMenuPath atc_CommonAncestor4_1 = atc_ParentPath4.getCommonAncestor(atc_ParentPath1);
    BOOST_CHECK_EQUAL(atc_CommonAncestor1_4.getPath(), "\\ParentPath1");
    BOOST_CHECK_EQUAL(atc_CommonAncestor1_4.getPath(), atc_CommonAncestor4_1.getPath());
}

BOOST_AUTO_TEST_CASE(CatcMenuPath_UT_Heredity)
{
    UnitTestComment("CatcMenuPath::isAncestorOf() : simple case");
    CatcMenuPath atc_ParentPath1("\\ParentPath1");
    CatcMenuPath atc_ParentPath2("\\ParentPath1\\ParentPath2\\ParentPath3");
    CatcMenuPath atc_ParentPath3("\\ParentPath4");
    BOOST_CHECK(atc_ParentPath1.isAncestorOf(atc_ParentPath2));
    BOOST_CHECK(atc_ParentPath2.isAncestorOf(atc_ParentPath2));
    BOOST_CHECK(!atc_ParentPath1.isAncestorOf(atc_ParentPath3));
    
    UnitTestComment("CatcMenuPath::isDescendantOf() : simple case");
    BOOST_CHECK(atc_ParentPath2.isDescendantOf(atc_ParentPath2));
    BOOST_CHECK(atc_ParentPath2.isDescendantOf(atc_ParentPath1));
    BOOST_CHECK(!atc_ParentPath3.isDescendantOf(atc_ParentPath1));
    
    UnitTestComment("CatcMenuPath::isAncestorOf() : complex case");
    CatcMenuPath atc_ParentPath4("\\ParentPath1\\ParentPath6 Extra");
    CatcMenuPath atc_ParentPath5("\\ParentPath1\\ParentPath4 Extra\\ParentPath5");
    CatcMenuPath atc_ParentPath6("\\ParentPath1 Extra\\ParentPath7");
    BOOST_CHECK(atc_ParentPath1.isAncestorOf(atc_ParentPath4));
    BOOST_CHECK(atc_ParentPath1.isAncestorOf(atc_ParentPath5));
    BOOST_CHECK(!atc_ParentPath1.isAncestorOf(atc_ParentPath6));
    
    UnitTestComment("CatcMenuPath::isDescendantOf() : complex case");
    BOOST_CHECK(atc_ParentPath4.isDescendantOf(atc_ParentPath1));
    BOOST_CHECK(atc_ParentPath5.isDescendantOf(atc_ParentPath1));
    BOOST_CHECK(!atc_ParentPath6.isDescendantOf(atc_ParentPath1));
}

BOOST_AUTO_TEST_SUITE_END()