/***************************************************************************
**
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
**
** This file is part of the Agglo project.
**
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Plaisic.  For licensing terms and
** conditions contact eti.laurent@gmail.com.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** In addition, the following conditions apply:
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in
**       the documentation and/or other materials provided with the
**       distribution.
**     * Neither the name of the Agglo project nor the names of its
**       contributors may be used to endorse or promote products derived
**       from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
** PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** AGGLO_END_LICENSE
**
***************************************************************************/

#include "aut/autUnitTestTemplate.h"

#include "CatcMenu_UT.h"

void CatcMenuUTSample::enterMenu() const
{
    CatcMenuUTSample* const_this = const_cast<CatcMenuUTSample*>(this);
    
    const_this->addOutput("configure " + matc_Path.getMenuName() + "\n");
}


void CatcMenuUTSample::exitMenu() const
{
    CatcMenuUTSample* const_this = const_cast<CatcMenuUTSample*>(this);
    
    const_this->addOutput("exit\n");
}


void CatcMenuUTSample::addOutput(const std::string& as_Output)
{
    CatcMenuUTSample* const atcp_ParentMenu = const_cast<CatcMenuUTSample*>(dynamic_cast<const CatcMenuUTSample*>(getParentMenu()));
    
    if (NULL == atcp_ParentMenu)
    {
        ms_OutputBuffer += as_Output;
    }
    else
    {
        atcp_ParentMenu->addOutput(as_Output);
    }
}


BOOST_AUTO_TEST_SUITE(CatcMenu_UT)

BOOST_AUTO_TEST_CASE(CatcMenu_UT_Constructor)
{
    UnitTestComment("CatcMenu::CatcMenu(RootMenu)");
    CatcMenuUTSample atc_RootMenu("RootMenu");
    BOOST_CHECK_EQUAL(atc_RootMenu.getPath().getPath(), "\\RootMenu");
    BOOST_CHECK_EQUAL(atc_RootMenu.getParentMenu(), (CatcMenuUTSample*)NULL);
    
    UnitTestComment("CatcMenu::CatcMenu(ChildMenu)");
    CatcMenuUTSample atc_ChildMenu1(&atc_RootMenu, "ChildMenu1");
    CatcMenuUTSample atc_ChildMenu2(&atc_ChildMenu1, "ChildMenu2");
    BOOST_CHECK_EQUAL(atc_ChildMenu2.getPath().getPath(), "\\RootMenu\\ChildMenu1\\ChildMenu2");
    BOOST_CHECK_EQUAL(atc_ChildMenu2.getParentMenu()->getParentMenu(), &atc_RootMenu);
}

BOOST_AUTO_TEST_CASE(CatcMenu_UT_MenuConstruction)
{
    UnitTestComment("CatcMenu::getChildMenu() : simple case");
    CatcMenuUTSample atc_RootMenu("RootMenu");
    CatcMenuUTSample atc_ChildMenu1(&atc_RootMenu, "ChildMenu1");
    BOOST_CHECK_EQUAL(atc_RootMenu.getChildMenu(CatcMenuPath("\\RootMenu\\ChildMenu1")), &atc_ChildMenu1);
    BOOST_CHECK_EQUAL(atc_ChildMenu1.getChildMenu(CatcMenuPath("\\RootMenu\\ChildMenu1")), (CatcMenuUTSample*)NULL);
    
    UnitTestComment("CatcMenu::getChildMenu() : complex case");
    CatcMenuUTSample atc_ChildMenu2(&atc_RootMenu, "ChildMenu2");
    CatcMenuUTSample atc_ChildMenu1_3(&atc_ChildMenu1, "ChildMenu1_3");
    CatcMenuUTSample atc_ChildMenu2_4(&atc_ChildMenu2, "ChildMenu2_4");
    CatcMenuUTSample atc_ChildMenu4_5(&atc_ChildMenu2_4, "ChildMenu4_5");
    BOOST_CHECK_EQUAL(atc_ChildMenu1.getChildMenu(CatcMenuPath("\\RootMenu\\ChildMenu2\\ChildMenu1_3")), (CatcMenuUTSample*)NULL);
    BOOST_CHECK_EQUAL(atc_ChildMenu2.getChildMenu(CatcMenuPath("\\RootMenu\\ChildMenu2\\ChildMenu2_4")), &atc_ChildMenu2_4);
    BOOST_CHECK_EQUAL(atc_RootMenu.getChildMenu(CatcMenuPath("\\RootMenu\\ChildMenu2\\ChildMenu2_4\\ChildMenu4_5")), &atc_ChildMenu2);
}

BOOST_AUTO_TEST_SUITE_END()