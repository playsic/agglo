/***************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
***************************************************************************/

#include "atc/CatcMenuNavigator.h"


CatcMenuNavigator::CatcMenuNavigator(const CatcMenu& aatc_RootMenu):matc_RootMenu(aatc_RootMenu)
{
    matcp_CLICurrentMenu = &matc_RootMenu;
}


/**
 * @function : getMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Retrieves the instance of any CatcMenu in the menu tree, given the path of this menu
 * @param : aatc_DestPath : path of the menu to retrieve
 * @return: pointer to the menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::getMenu(const CatcMenuPath& aatc_DestPath)
{
    return getDescendantMenu(matc_RootMenu, aatc_DestPath, false);
}


/**
 * @function : getDescendantMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Retrieves the CatcMenu instance of a descendant of a given menu
 * @param : aatc_SourceMenu : source menu from which menu lookup start
 * @param : aatc_DescendantMenuPath : path of the aimed menu. It must be included in the menu tree of aatc_SourceMenu
 * @param : ab_GenerateCommands : true if the commands to navigate to aatc_DescendantMenuPath are generated during menu lookup
 * @return: pointer to the descendant menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::getDescendantMenu(const CatcMenu& aatc_SourceMenu, 
                                                           const CatcMenuPath& aatc_DescendantMenuPath, 
                                                           const bool ab_GenerateCommands)
{
    const CatcMenu* atcp_Res = NULL;
    const CatcMenu* atcp_CurrentMenu = &aatc_SourceMenu;
    const CatcMenu* atcp_NextChildMenu = aatc_SourceMenu.getChildMenu(aatc_DescendantMenuPath);
    bool b_Continue = true;

    //TODO : rendre la commande safe : pas de enter si descendant path inexistant
    
    // Keep looking to descendant menu if current menu is not descendant menu, and if aatc_SourceMenu and aatc_DescendantMenuPath have the same common root
    atcp_Res = (aatc_SourceMenu.getPath() == aatc_DescendantMenuPath) ? &aatc_SourceMenu : NULL;
    b_Continue = (NULL == atcp_Res) &&  aatc_SourceMenu.getPath().isAncestorOf(aatc_DescendantMenuPath);
    
    // Iterate on descendant menus
    while (b_Continue)
    {
        // Go one level down : generate enter command for current menu, and get next child menu
        if (ab_GenerateCommands)
        {
            atcp_NextChildMenu->enterMenu();
            matcp_CLICurrentMenu = atcp_NextChildMenu;
        }
        atcp_CurrentMenu = atcp_NextChildMenu;
        atcp_NextChildMenu = atcp_NextChildMenu->getChildMenu(aatc_DescendantMenuPath);
        
        // Check if current menu is lookedup menu
        if (atcp_CurrentMenu->getPath() == aatc_DescendantMenuPath)
        {
            atcp_Res = atcp_CurrentMenu;
        }
        
        // Stop if there is no more child, or if lookedup menu has been found
        b_Continue = (NULL != atcp_NextChildMenu) && (NULL == atcp_Res);
    }
    
    return atcp_Res;
}


/**
 * @function : getAncestorMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Retrieves the CatcMenu instance of an ancestor of a given menu
 * @param : aatc_SourceMenu : source menu from which menu lookup start
 * @param : aatc_AncestorMenuPath : path of the aimed menu. aatc_SourceMenu must be included in the menu tree of aatc_AncestorMenuPath
 * @param : ab_GenerateCommands : true if the commands to navigate to aatc_AncestorMenuPath are generated during menu lookup
 * @return: pointer to the ancestor menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::getAncestorMenu(const CatcMenu& aatc_SourceMenu, const CatcMenuPath& aatc_AncestorMenuPath, 
                                                         const bool ab_GenerateCommands)
{
    const CatcMenu* atcp_Res = NULL;
    const CatcMenu* atcp_CurrentMenu = &aatc_SourceMenu;
    const CatcMenu* atcp_NextParentMenu = aatc_SourceMenu.getParentMenu();
    bool b_Continue = true;
    
    //TODO : rendre la commande safe : pas de exit si ancestor path inexistant
    
    // Keep looking to ancestor menu if current menu is not ancestor menu, and if aatc_SourceMenu and aatc_AncestorMenuPath have the same common root
    atcp_Res = (aatc_SourceMenu.getPath() == aatc_AncestorMenuPath) ? &aatc_SourceMenu : NULL;
    b_Continue = (NULL == atcp_Res) &&  aatc_AncestorMenuPath.isAncestorOf(aatc_SourceMenu.getPath());
    
    // Iterate on ancestor menus
    while (b_Continue)
    {
        // Go one level up : generate exit command for current menu, and get next parent menu
        if (ab_GenerateCommands)
        {
            atcp_CurrentMenu->exitMenu();
            matcp_CLICurrentMenu = atcp_NextParentMenu;
        }
        atcp_CurrentMenu = atcp_NextParentMenu;
        atcp_NextParentMenu = atcp_CurrentMenu->getParentMenu();
        
        // Check if current menu is lookedup menu
        if (atcp_CurrentMenu->getPath() == aatc_AncestorMenuPath)
        {
            atcp_Res = atcp_CurrentMenu;
        }
        
        // Stop if there is no more parent, or if lookedup menu has been found
        b_Continue = (NULL != atcp_NextParentMenu) && (NULL == atcp_Res);
    }
    
    return atcp_Res;
}


/**
 * @function : getCurrentMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Retrieves the instance of the current menu during CLI navigation
 * @param : ab_GeneratePwdCommand : true if the pwd command can be used to get current menu. If set to false, 
 * the registered current menu will be used, but it can be wrong (for instance, due to an automatic 
 * menu change due to a timeout)
 * @return: pointer to the menu itself if found, otherwise NULL (should not happen)
 */
const CatcMenu* const CatcMenuNavigator::getCLICurrentMenu(const bool ab_GeneratePwdCommand)
{
    if (ab_GeneratePwdCommand)
    {
        /*std::string s_CurrentPath;

        output("pwd\n");
        s_CurrentPath = getSynchronousInput(timeout);
        matcp_CLICurrentMenu = getMenu(CatcMenuPath(s_CurrentPath));*/
    }

    return matcp_CLICurrentMenu;
}


/**
 * @function : navigateToMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Generate the commands to navigate from current menu to another menu, given the path of destination menu
 * @param : aatc_DestPath : path of the aimed menu
 * @return: pointer to the menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::navigateToMenu(const CatcMenuPath& aatc_DestPath)
{
    const CatcMenu* atcp_CurrentMenu = getCLICurrentMenu(DEFAULT_PWDCOMMAND_BEHAVIOUR);
    const CatcMenu* atcp_Res = NULL;
    
    if (NULL != atcp_CurrentMenu)
    {
        atcp_Res = navigateToMenu(*atcp_CurrentMenu, aatc_DestPath);
    }
    
    return atcp_Res;
}


/**
 * @function : navigateToMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Generate the commands to navigate from a source menu to another menu, given the path of destination menu
 * @param : aatc_SourceMenu : source menu from which navigation start
 * @param : aatc_DestPath : path of the aimed menu
 * @return: pointer to the dest menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::navigateToMenu(const CatcMenu& aatc_SourceMenu, const CatcMenuPath& aatc_DestPath)
{
    const CatcMenu* atcp_Res = NULL;
    CatcMenuPath atc_SourcePath = aatc_SourceMenu.getPath();
    CatcMenuPath atc_CommonParentPath = atc_SourcePath.getCommonAncestor(aatc_DestPath);
    const CatcMenu* atcp_CommonParentMenu = NULL;
    
    // If we already set on common ancestor menu
    if (atc_CommonParentPath == atc_SourcePath)
    {
        // No need to navigate
        atcp_CommonParentMenu = &aatc_SourceMenu;
    }
    else
    {
        // Otherwise go up to common ancestor menu
        atcp_CommonParentMenu = navigateToAncestorMenu(aatc_SourceMenu, atc_CommonParentPath);
        
    }
    
    // If we already set on destination menu, there is no need to go down to descendant menu
    if ((NULL != atcp_CommonParentMenu) && (atcp_CommonParentMenu->getPath() != aatc_DestPath))
    {
        atcp_Res = navigateToDescendantMenu(*atcp_CommonParentMenu, aatc_DestPath);
    }
    
    return atcp_Res;
}


/**
 * @function : navigateToDescendantMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Generate the commands to navigate from a source menu to an ancestor menu, given the path of destination menu
 * @param : aatc_SourceMenu : source menu from which navigation start
 * @param : aatc_DescendantMenuPath : path of the aimed menu. It must be included in the menu tree of aatc_SourceMenu
 * @return: pointer to the dest menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::navigateToDescendantMenu(const CatcMenu& aatc_SourceMenu, 
                                                                  const CatcMenuPath& aatc_DescendantMenuPath)
{
    return getDescendantMenu(aatc_SourceMenu, aatc_DescendantMenuPath, true);
}


/**
 * @function : navigateToAncestorMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Generate the commands to navigate from a source menu to an ancestor menu, given the path of destination menu
 * @param : aatc_SourceMenu : source menu from which navigation start
 * @param : aatc_AncestorMenuPath : path of the aimed menu. aatc_SourceMenu must be included in the menu tree of aatc_AncestorMenuPath
 * @return: pointer to the dest menu itself if found, otherwise NULL
 */
const CatcMenu* const CatcMenuNavigator::navigateToAncestorMenu(const CatcMenu& aatc_SourceMenu, 
                                                                const CatcMenuPath& aatc_AncestorMenuPath)
{
    return getAncestorMenu(aatc_SourceMenu, aatc_AncestorMenuPath, true);
}



