/***************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
** PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
***************************************************************************/

#include "atc/CatcMenu.h"


/**
 * @function : CatcMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Build the menu corresponding to root menu
 * @param : s_RootMenuName : root menu name, without MENUSEPARATOR
 */
CatcMenu::CatcMenu(const std::string& s_RootMenuName)
                            : matcp_ParentMenu(NULL), 
                              matc_Path(std::string(MENUSEPARATOR) + s_RootMenuName)
{
    
}


/**
 * @function : CatcMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Build a new menu and attaches it to its parent (both child and parent are modified).
 *			This constructor is not dedicated to root menu
 * @param : aatcp_ParentMenu : parent menu
 * @param : s_MenuName : menu name, without MENUSEPARATOR
 */
CatcMenu::CatcMenu(CatcMenu* const aatcp_ParentMenu, const std::string& s_MenuName) : CatcMenu(s_MenuName)
{
    // TODO : c'est bizarre de modifier aatcp_ParentMenu dans le constructeur du child
    // Il vaudrait mieux construire le child, puis lier les 2 dans addChildMenu (renommer addLink)
    if (NULL != aatcp_ParentMenu)
    {
        matc_Path = CatcMenuPath(aatcp_ParentMenu->getPath(), s_MenuName);
        aatcp_ParentMenu->addChildMenu(*this);
    }
}


/**
 * @function : addChildMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Creates the pointer from the parent to the child
 * @param : aatc_NewChildMenu : child menu
 * @return: void
 */
void CatcMenu::addChildMenu(const CatcMenu& aatc_NewChildMenu)
{
    // TODO assert &aatc_NewChildMenu absent
    // TODO cette fonction est bancale puisque on modifie le parent, sans modifier le child
    mstl_VectorChildMenus.push_back(&aatc_NewChildMenu);
}


/**
 * @function : getChildMenu
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Gets the next child menu step on the way to a given path
 * @param : aatc_DescendantMenuPath : path of a descendant menu. It can be a child menu, 
 *          or a descendant of a child menu
 * @return: Child menu of this, or NULL if there is no way to aatc_DescendantMenuPath
 */
const CatcMenu* const CatcMenu::getChildMenu(const CatcMenuPath& aatc_DescendantMenuPath) const
{
    const CatcMenu* atcp_Res = NULL;
    
    // this must be an ancestor of aatc_DescendantMenuPath
    if ((aatc_DescendantMenuPath != matc_Path) && matc_Path.isAncestorOf(aatc_DescendantMenuPath))
    {
        const CatcMenu* atcp_CurrentMenu = NULL;
        std::vector<const CatcMenu*>::const_iterator iter = mstl_VectorChildMenus.begin();

        while ((NULL == atcp_Res) && (mstl_VectorChildMenus.end() != iter))
        {
            atcp_CurrentMenu = *iter;
            if (atcp_CurrentMenu->getPath().isAncestorOf(aatc_DescendantMenuPath))
            {
                atcp_Res = atcp_CurrentMenu;
            }
            else
            {
                iter++;
            }
        }
    }
    
    return atcp_Res;
}

