/***************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
***************************************************************************/

#include "atc/CatcMenuPath.h"


/**
 * @function : CatcMenuPath::CatcMenuPath()
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Build a path. It ensures path start with a MENUSEPARATOR
 * @param : aatc_ParentPath : path of the menu
 */
CatcMenuPath::CatcMenuPath(const std::string& as_Path)
{
    ms_Path = as_Path;
    
	// TODO MENUSEPARATOR peut etre sur plusieurs char, remplacer par un find
    if (ms_Path[0] != MENUSEPARATOR[0])
    {
        ms_Path = MENUSEPARATOR + ms_Path;
    }
}


/**
 * @function : CatcMenuPath::CatcMenuPath()
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Build a path of a child menu. It ensures path start with a MENUSEPARATOR
 * @param : aatc_ParentPath : path of the parent
 * @param : as_ChildMenuName : name of the child menu, without separator
 */
CatcMenuPath::CatcMenuPath(const CatcMenuPath& aatc_ParentPath, const std::string& as_ChildMenuName)
{
    *this = aatc_ParentPath;
    if ("" != as_ChildMenuName)
    {
        ms_Path += MENUSEPARATOR + as_ChildMenuName;
    }
    
    if (ms_Path[0] != MENUSEPARATOR[0])
    {
        ms_Path = MENUSEPARATOR + ms_Path;
    }
}


 /**
 * @function : CatcMenuPath::getMenuName()
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Gets the name of the menu, without separator or parent path
 * @return: name of the menu
 */
const std::string CatcMenuPath::getMenuName() const
{
    int i_Path1LastSeparator = 0;

    i_Path1LastSeparator = ms_Path.rfind(MENUSEPARATOR);
    i_Path1LastSeparator = (i_Path1LastSeparator == std::string::npos) ? 0 : i_Path1LastSeparator + 1;

    return (ms_Path.substr(i_Path1LastSeparator, ms_Path.length()));
}


/**
 * @function : CatcMenuPath::getCommonAncestor
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Gets the common ancestor of 2 paths. A path is considered common ancestor of itself
 * @param : aatc_Path : path to compare with this
 * @return: Common ancestor, or menu separator if no ancestor has been found
 */
const CatcMenuPath CatcMenuPath::getCommonAncestor(const CatcMenuPath& aatc_Path) const
{
    CatcMenuPath atc_CommonAncestor;
    std::string s_Path2 = aatc_Path.getPath();
    
    // this is common ancestor of itself
    if (ms_Path == s_Path2)
    {
        atc_CommonAncestor = *this;
    }
    else
    {
        int i_FirstDifferentChar = 0;
        int i_Path1NextSeparator = 0, i_Path2NextSeparator = 0;
        bool bContinue = true;

        // Iterate on path, starting from root
        atc_CommonAncestor = CatcMenuPath(MENUSEPARATOR);
        while (bContinue)
        {
            // Find position of next separator. If there is no separator left, it means that we 
            // are on the last menu ; consider there is an extra separator is a the end of the path
            i_Path1NextSeparator = ms_Path.find(MENUSEPARATOR, i_Path1NextSeparator);
            i_Path1NextSeparator = (i_Path1NextSeparator == std::string::npos) ? ms_Path.length() : i_Path1NextSeparator;
            i_Path2NextSeparator = s_Path2.find(MENUSEPARATOR, i_Path2NextSeparator);
            i_Path2NextSeparator = (i_Path2NextSeparator == std::string::npos) ? s_Path2.length() : i_Path2NextSeparator ;
            
            // Check that next separator is on the same position on both path
            bContinue = (i_Path1NextSeparator == i_Path2NextSeparator);
            if (bContinue)
            {
                // Check that path until next separator is the same on both path
                bContinue = (ms_Path.compare(0, i_Path1NextSeparator, s_Path2, 0, i_Path2NextSeparator) == 0);
            }
            
            if (bContinue)
            {
                if (i_Path1NextSeparator > 0)
                {
                    // Extract path from root until next separator
                    atc_CommonAncestor = ms_Path.substr(0, i_Path1NextSeparator);
                }
                
                // Now we gonna look next menu level
                i_Path1NextSeparator++;
                i_Path2NextSeparator++;
            }
        }
    }
    
    return atc_CommonAncestor;
}


/**
 * @function : CatcMenuPath::isDescendantOf
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Check wether a path is descendant of this. As a path is common ancester
 *          with itself, this is considered descendant of itself
 * @param : aatc_Path : path to compare with this
 * @return: true if aatc_path is a descendant of this
 */
const bool CatcMenuPath::isDescendantOf(const CatcMenuPath& aatc_Path) const
{
    std::string s_Path = aatc_Path.getPath() + MENUSEPARATOR;
    
    return ((*this == aatc_Path) || (ms_Path.find(s_Path) == 0));
}


/**
 * @function : CatcMenuPath::isAncestorOf()
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Check wether a path is ancestor of this. As a path is common ancester
 *          with itself, this is considered ancestor of itself
 * @param : aatc_Path : path to compare with this
 * @return: true if aatc_path is a ancestor of this
 */
const bool CatcMenuPath::isAncestorOf(const CatcMenuPath& aatc_Path) const
{
    
    return (aatc_Path.isDescendantOf(*this));
}


/**
 * @function : CatcMenuPath::operator ==
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Compares 2 path
 * @param : a_Path1 : path to compare
 * @param : a_Path2 : path to compare
 * @return: true if the 2 CatcMenuPath have the same path
 */
const bool operator == (const CatcMenuPath& a_Path1, const CatcMenuPath& a_Path2)
{
    return (a_Path1.getPath() == a_Path2.getPath());
}


/**
 * @function : CatcMenuPath::operator !=
 * @requirement :
 * @file  : CatcMenuNavigator.cpp
 * @brief : Compares 2 path
 * @param : a_Path1 : path to compare
 * @param : a_Path2 : path to compare
 * @return: true if the 2 CatcMenuPath have different path
 */
const bool operator != (const CatcMenuPath& a_Path1, const CatcMenuPath& a_Path2)
{
    return !(a_Path1 == a_Path2);
}
