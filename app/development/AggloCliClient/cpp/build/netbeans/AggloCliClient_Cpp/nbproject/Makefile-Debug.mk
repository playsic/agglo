#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_CONF=Debug
CND_DISTDIR=dist

# Include project Makefile
include AggloCliClient_Cpp.mak

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1386528437/CatcMenu.o \
	${OBJECTDIR}/_ext/1386528437/CatcMenuPath.o \
	${OBJECTDIR}/_ext/1386528437/CatcMenuNavigator.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Debug.mk dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a

dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a: ${OBJECTFILES}
	${MKDIR} -p dist/Debug/Cygwin_4.x-Windows
	${RM} dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libagglocliclient_cpp.a ${OBJECTFILES} 
	$(RANLIB) dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a

${OBJECTDIR}/_ext/1386528437/CatcMenu.o: ../../../src/CatcMenu.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1386528437/CatcMenu.o ../../../src/CatcMenu.cpp

${OBJECTDIR}/_ext/1386528437/CatcMenuPath.o: ../../../src/CatcMenuPath.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1386528437/CatcMenuPath.o ../../../src/CatcMenuPath.cpp

${OBJECTDIR}/_ext/1386528437/CatcMenuNavigator.o: ../../../src/CatcMenuNavigator.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1386528437
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1386528437/CatcMenuNavigator.o ../../../src/CatcMenuNavigator.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Debug
	${RM} dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
