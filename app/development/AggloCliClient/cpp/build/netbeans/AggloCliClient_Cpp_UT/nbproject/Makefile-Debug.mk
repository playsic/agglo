#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_CONF=Debug
CND_DISTDIR=dist

# Include project Makefile
include AggloCliClient_Cpp_UT.mak

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/2456179/CatcMenuPath_UT.o \
	${OBJECTDIR}/_ext/2456179/main.o \
	${OBJECTDIR}/_ext/2456179/CatcMenuNavigator_UT.o \
	${OBJECTDIR}/_ext/2456179/CatcMenu_UT.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../AggloCliClient_Cpp/dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a ../../../../../../AggloUnitTest/cpp/build/netbeans/AggloUnitTest_Cpp/dist/Debug/Cygwin_4.x-Windows/libagglounittest_cpp.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Debug.mk dist/Debug/Cygwin_4.x-Windows/agglocliclient_cpp_ut.exe

dist/Debug/Cygwin_4.x-Windows/agglocliclient_cpp_ut.exe: ../AggloCliClient_Cpp/dist/Debug/Cygwin_4.x-Windows/libagglocliclient_cpp.a

dist/Debug/Cygwin_4.x-Windows/agglocliclient_cpp_ut.exe: ../../../../../../AggloUnitTest/cpp/build/netbeans/AggloUnitTest_Cpp/dist/Debug/Cygwin_4.x-Windows/libagglounittest_cpp.a

dist/Debug/Cygwin_4.x-Windows/agglocliclient_cpp_ut.exe: ${OBJECTFILES}
	${MKDIR} -p dist/Debug/Cygwin_4.x-Windows
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/agglocliclient_cpp_ut ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/_ext/2456179/CatcMenuPath_UT.o: ../../../test/UT/CatcMenuPath_UT.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2456179
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -I../../../../../../AggloUnitTest/cpp/include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/2456179/CatcMenuPath_UT.o ../../../test/UT/CatcMenuPath_UT.cpp

${OBJECTDIR}/_ext/2456179/main.o: ../../../test/UT/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2456179
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -I../../../../../../AggloUnitTest/cpp/include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/2456179/main.o ../../../test/UT/main.cpp

${OBJECTDIR}/_ext/2456179/CatcMenuNavigator_UT.o: ../../../test/UT/CatcMenuNavigator_UT.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2456179
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -I../../../../../../AggloUnitTest/cpp/include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/2456179/CatcMenuNavigator_UT.o ../../../test/UT/CatcMenuNavigator_UT.cpp

${OBJECTDIR}/_ext/2456179/CatcMenu_UT.o: ../../../test/UT/CatcMenu_UT.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2456179
	${RM} $@.d
	$(COMPILE.cc) -g -I../../../include -I../../../../../../AggloUnitTest/cpp/include -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/2456179/CatcMenu_UT.o ../../../test/UT/CatcMenu_UT.cpp

# Subprojects
.build-subprojects:
	cd ../AggloCliClient_Cpp && ${MAKE}  -f AggloCliClient_Cpp.mak CONF=Debug
	cd ../../../../../../AggloUnitTest/cpp/build/netbeans/AggloUnitTest_Cpp && ${MAKE}  -f AggloUnitTest_Cpp.mak CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Debug
	${RM} dist/Debug/Cygwin_4.x-Windows/agglocliclient_cpp_ut.exe

# Subprojects
.clean-subprojects:
	cd ../AggloCliClient_Cpp && ${MAKE}  -f AggloCliClient_Cpp.mak CONF=Debug clean
	cd ../../../../../../AggloUnitTest/cpp/build/netbeans/AggloUnitTest_Cpp && ${MAKE}  -f AggloUnitTest_Cpp.mak CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
