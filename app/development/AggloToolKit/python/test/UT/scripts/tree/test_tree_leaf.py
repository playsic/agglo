﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.tree.leaf import AtkTreeLeaf
from agglo_tk.tree.node import AtkTreeNode
from agglo_tk._private.tree_element import set_tree_element_attr


class Data(object):
    def __init__(self, attr):
        self.attr = attr

    def method(self):
        return True

    
def test_UT_AtkTreeLeaf_Properties():
    parent = AtkTreeNode()
    child1 = AtkTreeNode()
    child1_1 = AtkTreeLeaf(Data(5))

    #UnitTestComment("AtkTreeLeaf: test parent property)
    parent.append(child1)
    child1.append(child1_1)
    assert child1_1.parent is child1

    #UnitTestComment("AtkTreeLeaf: test ancestors property)
    assert len(child1_1.ancestors) == 2
    assert child1_1.ancestors[0] is child1
    assert child1_1.ancestors[1] is parent


def test_UT_AtkTreeLeaf_Leaves():
    data = Data(5)
    leaf = AtkTreeLeaf(data)
    
    #UnitTestComment("AtkTreeLeaf::leaves(): retrieve leave without filtering)
    leaves = leaf.leaves()
    assert len(leaves) == 1
    assert leaves[0] is data
    
    #UnitTestComment("AtkTreeLeaf::leaves(): retrieve leave with filtering)
    leaves = leaf.leaves(lambda x: x.attr < 7)
    assert len(leaves) == 1
    assert leaves[0] is data
    leaves = leaf.leaves(lambda x: x.attr > 7)
    assert not leaves
    
    #UnitTestComment("AtkTreeLeaf::leaves(): retrieve AtkLeave instance instead of leaves' values)
    leaves = leaf.leaves(value=False)
    assert len(leaves) == 1
    assert leaves[0] is leaf

    #UnitTestComment("AtkTreeLeaf::leaves(): retrieve leave with filtering on AtkTreeLeaf property)
    class TreeLeafSample(AtkTreeLeaf):
        def __init__(self, value, attr2):
            AtkTreeLeaf.__init__(self, value)
            set_tree_element_attr(self, "attr2", attr2)
    leaf = TreeLeafSample(data, 10)
    assert hasattr(leaf, "attr2")
    assert not hasattr(data, "attr2")
    leaves = leaf.leaves(lambda x: x.attr2 < 13)
    assert len(leaves) == 1
    assert leaves[0] is data
    leaves = leaf.leaves(lambda x: x.attr2 > 13)
    assert not leaves
