﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk._private.tree_element import *


class Data(object):
    def __init__(self, attr):
        self.attr = attr

    def method(self):
        return self.attr

        
class OverridenTreeElement(AtkTreeElement):
    tag = "Overriden"
    
    def __init__(self, value=None):
        AtkTreeElement.__init__(self, value)
        set_tree_element_attr(self, "elt_attr", 10)
        self.value_attr = 20
        

# TODO tests tres incomplet, en particulier le parent et tout ce qui concern le proxy (cf limited_access_proxy)
def test_UT_AtkTreeElement_Constructor():
    #UnitTestComment("AtkTreeElement::AtkTreeElement(): test default constructor)
    tree_element = AtkTreeElement()
    assert tree_element.tag == ""
    assert tree_element.value is not None
    assert tree_element.parent is None

    #UnitTestComment("AtkTreeElement::AtkTreeElement(): test constructor with attached data)
    data = Data(5)
    tree_element = AtkTreeElement(data)
    assert tree_element.value is data
        
    #UnitTestComment("AtkTreeElement: test construction of an overriden element)
    overriden = OverridenTreeElement(data)
    assert overriden.tag == "Overriden"
    assert overriden.elt_attr == 10
    assert not hasattr(overriden.value, "elt_attr")
    assert overriden.value is data
    assert overriden.value_attr == 20
    assert overriden.value.value_attr == 20
    

def test_UT_AtkTreeElement_Proxy():
    #UnitTestComment("AtkTreeElement: test proxy behavior of an element with an attached value)
    data = Data(5)
    tree_element = AtkTreeElement(data)
    assert tree_element.attr == 5
    assert tree_element.value.attr == 5
    tree_element.attr = 6
    assert tree_element.attr == 6
    assert tree_element.method()
    tree_element.attr2 = 7
    assert data.attr2 == 7
    assert tree_element.value.attr2 == 7

    #UnitTestComment("AtkTreeElement: test proxy behavior of an element with an empty value)
    tree_element = AtkTreeElement()
    tree_element.attr = 8
    assert tree_element.value.attr == 8
            
    #UnitTestComment("AtkTreeElement: test proxy behavior of an overriden element)
    overriden = OverridenTreeElement()
    set_tree_element_attr(overriden, "elt_attr", 11)
    assert overriden.elt_attr == 11
    assert not hasattr(overriden.value, "elt_attr")
    set_tree_element_attr(overriden, "elt_attr2", 12)
    assert overriden.elt_attr2 == 12
    assert not hasattr(overriden.value, "elt_attr")
    overriden.attr3 = 9
    assert overriden.attr3 == 9
    assert overriden.value.attr3 == 9

    
def test_UT_AtkTreeElement_Accept():
    class GetAttrVisitor(object):
        def visit_AtkTreeElement(visitor, visitable, *args, **kwargs):
            return visitable.method()
      
    #UnitTestComment("AtkTreeElement::accept(): test visit of tree element)
    tree_element = AtkTreeElement(Data(5))
    visitor = GetAttrVisitor()
    assert tree_element.accept(visitor) == 5
    