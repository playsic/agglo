﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.tree.node import AtkTreeNode
from agglo_tk.tree.leaf import AtkTreeLeaf
from agglo_tk.tree.tree import AtkTree


class Data(object):
    def __init__(self, attr):
        self.attr = attr

    def method(self):
        return True

# class OverridenNode1(AtkTreeNode):
    # tag="OverridenNode1"

    # def __init__(self, *args):
        # AtkTreeNode.__init__(self)

      
# class OverridenNode2(AtkTreeNode):
    # tag="OverridenNode2"

    # def __init__(self, *args):
        # AtkTreeNode.__init__(self)

      
# class OverridenNode3(AtkTreeNode):
    # tag="OverridenNode3"

    # def __init__(self, *args):
        # AtkTreeNode.__init__(self)


# class OverridenTree(AtkTree):

    # OverridenTree._add_type(OverridenNode1)
    # OverridenTree._add_type(OverridenNode2)

    # def __init__(self, root):
        # AtkTree.__init__(self, root)
        # # self._add_type(OverridenNode1)
        # # self._add_type(OverridenNode2)
        

def test_UT_AtkTree_Constructor():
    #UnitTestComment("AtkTree::AtkTree(): test constructor with empty value)
    tree = AtkTree()
    assert isinstance(tree.root, AtkTreeLeaf)
    tree.root.value = 1
    assert tree.root.value == 1
    
    #UnitTestComment("AtkTree::AtkTree(): test constructor with data value)
    data = Data(5)
    tree = AtkTree(data)
    assert isinstance(tree.root, AtkTreeLeaf)
    assert tree.root.value is data
    
    #UnitTestComment("AtkTree::AtkTree(): test constructor with AtkTreeLeaf value)
    leaf = AtkTreeLeaf(data)
    tree = AtkTree(leaf)
    assert tree.root is leaf
    
    #UnitTestComment("AtkTree::AtkTree(): test constructor with AtkTreeNode value)
    node = AtkTreeLeaf(leaf)
    tree = AtkTree(node)
    assert tree.root is node
    

def test_UT_AtkTree_Properties():
    #UnitTestComment("AtkTree:root: test set root with a data)
    root1 = AtkTreeLeaf(5)
    tree = AtkTree(root1)
    assert tree.root.value == 5
    tree.root = 6
    assert tree.root.value == 6

    #UnitTestComment("AtkTree:root: test set root with a tree element)
    root2 = AtkTreeLeaf(7)
    tree.root = root2
    assert tree.root.value == 7


def test_UT_AtkTree_Leaves():
    root = AtkTreeNode()
    child1 = AtkTreeNode()
    data = Data(5)
    child2 = AtkTreeLeaf(data)
    child1_1 = AtkTreeLeaf(Data(7))
    child1_2 = AtkTreeLeaf(Data(6))
    tree = AtkTree(root)
    
    #UnitTestComment("AtkTree::leaves(): retrieve leave without filtering)
    root.append(child1)
    child1.append(child1_1)
    root.append(child2)
    child1.append(child1_2)
    leaves = tree.leaves()
    assert len(leaves) == 3
    assert data in leaves
    assert child1_1.value in leaves
    assert child1_2.value in leaves
    
    #UnitTestComment("AtkTree::leaves(): retrieve leave with filtering)
    leaves = tree.leaves(filtre=lambda x: x.attr < 7)
    assert len(leaves) == 2
    assert child2 in leaves
    assert child1_2 in leaves
    
    #UnitTestComment("AtkTree::leaves(): retrieve AtkLeave instance instead of leaves' values)
    leaves = tree.leaves(value=False)
    assert len(leaves) == 3
    assert child2 in leaves


# TODO    
def test_UT_AtkTree_Accept():
    pass

# def test_UT_AtkTree_Heritage():
    # TODO deplacer is_node dans tree_builder
    # #UnitTestComment("AtkTree::is_node_allowed(): test definition of a node)

    