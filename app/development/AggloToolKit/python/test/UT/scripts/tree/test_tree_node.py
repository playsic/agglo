﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.tree.node import AtkTreeNode
from agglo_tk.tree.leaf import AtkTreeLeaf


class Data(object):
    def __init__(self, attr):
        self.attr = attr

    def method(self):
        return True
        

def test_UT_AtkTreeNode_Constructor():
    #UnitTestComment("AtkTreeNode::AtkTreeNode(): test constructor)
    tree_node = AtkTreeNode()
    assert len(tree_node.children) == 0

    
def test_UT_AtkTreeNode_Properties():
    parent = AtkTreeNode()
    child1 = AtkTreeNode()
    child2 = AtkTreeNode()
    child2_1 = AtkTreeNode()

    #UnitTestComment("AtkTreeNode: test children property)
    assert len(parent.children) == 0
    parent.append(child1)
    assert len(parent.children) == 1
    assert child1 is parent.children[0]
    parent.append(child2)
    assert len(parent.children) == 2
    assert child1 is parent.children[0]
    assert child2 is parent.children[1]
    child2.append(child2_1)
    assert len(parent.children) == 2
    assert len(child2.children) == 1
    assert child2_1 is child2.children[0]

    #UnitTestComment("AtkTreeNode: test parent property)
    assert child1.parent is parent
    assert child2.parent is parent
    assert child2_1.parent is child2

    #UnitTestComment("AtkTreeNode: test ancestors property)
    assert len(child2.ancestors) == 1
    assert child2.ancestors[0] is parent
    assert len(child2_1.ancestors) == 2
    assert child2_1.ancestors[0] is child2
    assert child2_1.ancestors[1] is parent
    

def test_UT_AtkTreeNode_BuiltIn():
    parent = AtkTreeNode()
    child1 = AtkTreeNode()
    child2 = AtkTreeNode()
    child2_1 = AtkTreeNode()

    #UnitTestComment("AtkTreeNode: test iteration on children)
    parent.append(child1)
    parent.append(child2)
    child2.append(child2_1)
    for i, child in enumerate(parent):
        if (i == 0):
            assert child is child1
        elif (i == 1):
            assert child is child2
        else:
            assert False
    

def test_UT_AtkTreeNode_Append():
    parent = AtkTreeNode()
    child1 = AtkTreeNode()
    child2 = AtkTreeLeaf(Data(5))
    child1_1 = AtkTreeNode()
    child1_2 = Data(6)

    #UnitTestComment("AtkTreeNode::append(): test append of nodes)
    parent.append(child1)
    assert child1 in parent.children
    assert child1.parent is parent
    child1.append(child1_1)
    assert child1_1 not in parent.children
    assert child1_1 in child1.children
    assert child1_1.parent is child1
    assert child1.parent is parent

    #UnitTestComment("AtkTreeNode::append(): test append of leaves)
    parent.append(child2)
    assert child2 in parent.children
    assert child2.parent is parent
    assert len(parent.children) == 2

    #UnitTestComment("AtkTreeNode::append(): test append of values)
    child1.append(child1_2)
    assert len(child1.children) == 2
    assert child1_2 in child1.children
    assert isinstance(child1.children[1], AtkTreeLeaf)
    assert child1_2 is child1.children[1].value
    assert child1.children[1].parent is child1


def test_UT_AtkTreeNode_Leaves():
    parent = AtkTreeNode()
    child1 = AtkTreeNode()
    data = Data(5)
    child2 = AtkTreeLeaf(data)
    child1_1 = AtkTreeLeaf(Data(7))
    child1_2 = AtkTreeLeaf(Data(6))
    
    #UnitTestComment("AtkTreeNode::leaves(): retrieve leave without filtering)
    parent.append(child1)
    child1.append(child1_1)
    parent.append(child2)
    child1.append(child1_2)
    leaves = parent.leaves()
    assert len(leaves) == 3
    assert data in leaves
    assert child1_1.value in leaves
    assert child1_2.value in leaves
    leaves = child1.leaves()
    assert len(leaves) == 2
    assert data not in leaves
    
    #UnitTestComment("AtkTreeNode::leaves(): retrieve leave with filtering)
    leaves = parent.leaves(filtre=lambda x: x.attr < 7)
    assert len(leaves) == 2
    assert child2 in leaves
    assert child1_2 in leaves
    
    #UnitTestComment("AtkTreeNode::leaves(): retrieve AtkLeave instance instead of leaves' values)
    leaves = parent.leaves(value=False)
    assert len(leaves) == 3
    assert child2 in leaves
