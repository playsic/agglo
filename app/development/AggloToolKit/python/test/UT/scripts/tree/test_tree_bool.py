﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.tree.tree_bool import *
from agglo_tk.tree.node import AtkTreeNode


class Condition(object)  :
    def __init__(self, value):
        self.value=value
        
    def __bool__(self):
        return self.value

# TODO fusionner les test_UT_s        
def test_UT_AndOrXor_Constructor():
    true = Condition(True)
    false = Condition(False)
    conditions = [true, true, false, true, false]
    nodes = [And, Or, Xor]

    for node_class in nodes:
        #UnitTestComment("AtkBoolNode::AtkBoolNode(): test_UT_ constructor with several conditions)
        node = node_class(*conditions)
        for i, condition in enumerate(conditions):
            assert node.conditions[i].value is condition
    
    
def test_UT_Not_Constructor():
    true = Condition(True)
    
    #UnitTestComment("Not::Not(): test_UT_ constructor)
    not1 = Not(true)
    assert len(not1.conditions) == 1
    assert not1.condition.value is true
    
    
def test_UT_Not_Properties():
    true = Condition(True)
    false = Condition(False)
    
    #UnitTestComment("Not::condition: test_UT_ condition setter)
    not1 = Not(true)
    not1.condition = false
    assert len(not1.conditions) == 1
    assert not1.condition.value is false
        
    
def test_UT_Not_Append():
    true = Condition(True)
    false = Condition(False)
    
    #UnitTestComment("Not::append(): test_UT_ condition append)
    not1 = Not(true)
    not1.append(false)
    assert len(not1.conditions) == 1
    assert not1.condition.value is false
    
    #UnitTestComment("Not::append(): test_UT_ condition append through parent append method)
    AtkTreeNode.append(not1, true)
    assert len(not1.conditions) == 2
    assert not1.condition.value is false
    not1.append(true)
    assert len(not1.conditions) == 1
    assert not1.condition.value is true
    
        
def test_UT_AtkBoolTree_Constructor():
    #UnitTestComment("AtkBoolTree::AtkBoolTree(): test_UT_ class AtkBoolTree)
    assert AtkBoolTree.is_node(And)
    assert AtkBoolTree.is_node(Or)
    assert AtkBoolTree.is_node(Xor)
    assert AtkBoolTree.is_node(Not)
    assert not AtkBoolTree.is_node(AtkTreeNode)
    
    #UnitTestComment("AtkBoolTree::AtkBoolTree(): test_UT_ default constructor)
    root = Not(Condition(True))
    bool_tree = AtkBoolTree(root)
    bool_tree.root is root
        

def test_UT_AtkBoolTree_Eval():
    true = Condition(True)
    false = Condition(False)
    bool_tree = AtkBoolTree(true)
    
    #UnitTestComment("AtkBoolTree::eval(): test_UT_ And node eval)
    bool_tree.root = And(true, true, true)
    assert bool_tree.eval()
    bool_tree.root = And(true, true, false)
    assert not bool_tree.eval()
    
    #UnitTestComment("AtkBoolTree::eval(): test_UT_ Or node eval)
    bool_tree.root = Or(false, false, false)
    assert not bool_tree.eval()
    bool_tree.root = Or(false, false, true)
    assert bool_tree.eval()
    
    #UnitTestComment("AtkBoolTree::eval(): test_UT_ Xor node eval)
    bool_tree.root = Or(false, false, false)
    assert not bool_tree.eval()
    bool_tree.root = Xor(false, true, true)
    assert not bool_tree.eval()
    bool_tree.root = Xor(false, false, true)
    assert bool_tree.eval()
    
    #UnitTestComment("AtkBoolTree::eval(): test_UT_ Not node eval)
    bool_tree.root = Not(false)
    assert bool_tree.eval()
    bool_tree.root = Not(true)
    assert not bool_tree.eval()

    #UnitTestComment("AtkBoolTree::eval(): test_UT_ complex expression eval)
    xor = Xor(true, false)
    not1 = Not(false)
    and1 = And(xor, not1)
    bool_tree.root = and1
    assert bool_tree.eval()
    # TODO ce serait plus sympa de faire xor.conditions[1] = true
    xor.conditions[1].value = true
    assert not bool_tree.eval()
    xor.conditions[1].value = false
    not1.condition = true
    assert not bool_tree.eval()
