############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic. For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file. Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
## * Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in
## the documentation and/or other materials provided with the
## distribution.
## * Neither the name of the Agglo project nor the names of its
## contributors may be used to endorse or promote products derived
## from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time
from threading import Thread

import pytest

from agglo_tk.io import AtkIOConnection
from agglo_tk.io import AtkIOConnectionLogs
from agglo_tk.io import AtkIOHub
from agglo_tk.io import IO_TYPE_INPUT
from agglo_tk.io import IO_TYPE_OUTPUT
from agglo_tk.io import AtkIOCriteria

from agglo_tk.exceptions import AtkIODeviceError
from agglo_tk.exceptions import AtkSelectorError

from agglo_tk.utils.test.stubs.io_device import AtkStubIODevice
from agglo_tk.utils.test.fixtures import *
from agglo_tt.fixtures import trace_test_start
from fixtures import test_cleaner



def async_io(io_connection, nb_ios, elapsed_ms=0.5, blocking=False):
    if blocking:
        for i in range(0, nb_ios):
            time.sleep(elapsed_ms)
            io_connection.handle_input(io_connection._io_device, "data" + str(i+1))
    else:
        async_io_thread = Thread(name="", target=async_io, args=(io_connection, nb_ios, elapsed_ms, True))
        async_io_thread.start()

    
def test_UT_AtkIOConnection_Constructor(trace_test_start):
    io_device = AtkStubIODevice()
    io_logs = AtkIOConnectionLogs()
    io_hub = AtkIOHub()

    #UnitTestComment("AtkIOConnection::AtkIOConnection():check that constructor set defaults parameters")
    io_connection = AtkIOConnection("test", io_device)
    assert io_connection.name == "test"
    assert io_connection.io_logs is not None
    assert io_connection.io_hub is not None
    assert not io_connection.io_hub.is_opened(io_device)

    #UnitTestComment("AtkIOConnection::AtkIOConnection():check that input/output logs are created")
    assert io_connection.io_logs[io_connection.io_logs.get_io_type(IO_TYPE_INPUT, io_connection)] is not None
    assert io_connection.io_logs[io_connection.io_logs.get_io_type(IO_TYPE_OUTPUT, io_connection)] is not None

    #UnitTestComment("AtkIOConnection::AtkIOConnection():check that constructor doesn't open connection")
    assert not io_connection.opened
    assert not io_device.opened

    #UnitTestComment("AtkIOConnection::AtkIOConnection():check constructor with other parameters")
    io_connection = AtkIOConnection("test", io_device, io_logs, io_hub)
    assert io_connection.io_logs is io_logs
    assert io_logs[io_logs.get_io_type(IO_TYPE_INPUT, io_connection)] is not None
    assert io_logs[io_logs.get_io_type(IO_TYPE_OUTPUT, io_connection)] is not None
    assert io_connection.io_hub is io_hub


def test_UT_AtkIOConnection_BuiltIn(trace_test_start):
    io_device = AtkStubIODevice()
    io_logs = AtkIOConnectionLogs()
    io_connection = AtkIOConnection("test", io_device, io_logs)

    #UnitTestComment("AtkIOConnection::AtkIOConnection():check that io_type are accessible")
    assert io_connection.io_type_input == io_logs.get_io_type(IO_TYPE_INPUT, io_connection)
    assert io_connection.io_type_output == io_logs.get_io_type(IO_TYPE_OUTPUT, io_connection)


def test_UT_AtkIOConnection_OpenClose(trace_test_start, test_cleaner):
    io_device = AtkStubIODevice()
    io_connection = AtkIOConnection("test", io_device)
    
    for i in range (0, 2):
        #UnitTestComment("AtkIOConnection::open():test io connection opening")
        test_cleaner.io_connection = io_connection
        io_connection.open()
        assert io_connection.opened
        assert io_device.opened
        assert io_connection.io_hub.is_opened(io_device)

        #UnitTestComment("AtkIOConnection::open():test io connection reopening")
        with pytest.raises(AtkIODeviceError) as exception:
            io_connection.open()
        assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED
        assert io_connection.opened
        assert io_device.opened
        assert io_connection.io_hub.is_opened(io_device)

        #UnitTestComment("AtkIOConnection::close()")
        io_connection.close()
        test_cleaner.io_connection = None
        assert not io_connection.opened
        assert not io_device.opened
        assert not io_connection.io_hub.is_opened(io_device)

        #UnitTestComment("AtkIOConnection::open():test io connection closing while not opened")
        io_connection.close()
        
        
def test_UT_AtkIOConnection_ReadWrite(trace_test_start, test_cleaner):
    io_device = AtkStubIODevice()
    io_logs = AtkIOConnectionLogs()
    io_connection = AtkIOConnection("test", io_device, io_logs)
    
    test_cleaner.io_connection = io_connection
    io_connection.open()

    for i in range(0, 3):
        #UnitTestComment("AtkIOConnection::test read/write")
        io_connection.send("data" + str(i))
        assert io_logs[io_connection.io_type_output][-1].data == "data" + str(i)
        assert len(io_logs[io_connection.io_type_output]) == i + 1
        time.sleep(0.1)
        assert io_logs[io_connection.io_type_input][-1].data == "data" + str(i)
        assert len(io_logs[io_connection.io_type_input]) == i + 1


def test_UT_AtkIOConnection_WaitIO(trace_test_start, dump_failed):
    io_device = AtkStubIODevice()
    io_connection = AtkIOConnection("test", io_device)

    dump_failed.io_logs = io_connection.io_logs

    #UnitTestComment("AtkIOConnection::wait_io():test that wait_io expects one occurence by default")
    start_time = time.time()
    async_io(io_connection, 1)
    found_io = io_connection.wait_io(AtkIOCriteria(io_type=io_connection.io_type_input).from_now(2000))
    assert found_io.data == "data1"
    assert start_time + 0.5 <= time.time() <= start_time + 1

    #UnitTestComment("AtkIOConnection::wait_io(): test that number of expected occurences can be set")
    start_time = time.time()
    async_io(io_connection, 2)
    assert len(io_connection.wait_io(AtkIOCriteria(io_type=io_connection.io_type_input).from_now(2000), 2)) == 2
    assert start_time + 1 <= time.time() <= start_time + 1.5
    
    #UnitTestComment("AtkIOConnection::wait_io(): test that selector attributes are correctly used")
    start_time = time.time()
    async_io(io_connection, 2, 1.5)
    with pytest.raises(AtkSelectorError) as exception:
        io_connection.wait_io(AtkIOCriteria(io_type=io_connection.io_type_input).from_now(2000), 2)
    assert start_time + 2 <= time.time() <= start_time + 2.5


    # TODO deplacer dans test_cli_client ?
    # io_device_tcp = AtkIODeviceTcp("localhost", CaccMock_UT.MOCK_CLI_TELNET_PORT)
    # accRootMenu_UTMock = CaccMock_UT.CaccRootMenu_UTMock()
    # io_logs_multi = AtkIOLogsMulti()
    # accCliClient = CaccCliClient.CaccCliCieent(io_deviec_tcp, accRootMenu_UTMock)
    # accMatchLogs = None
    
    # startStopCliServerMock._accCliClient = accCliClient
    # accCliClient.open(io_logs_multi, 0.5)

    # #UnitTestComment("Client::waitIO():wait for first prompt")
    # #UnitTestComment("Client::waitIO():wait for any IO")

    # #UnitTestComment("Client::waitIO():wait for a command result")
    # ui_start_time = time.time()
    # sendRawData(accCliClient, "Display Result 5 span 1\n")
    # assert accCliClient.waitIO(cli_AtkIOSelector(Cio_logs_multi.IOTYPE_COMMAND_RESULT, "Partial Result [4-5]").from_now(10000))
    # assert ui_start_time + 3 <= time.time() <= ui_start_time + 4
    # assert accCliClient.select(cli_AtkIOSelector(Cio_logs_multi.IOTYPE_COMMAND_RESULT))[-1].data ==  "Partial Result 1\nPartial Result 2\nPartial Result 3\nPartial Result 4\n"
    # time.sleep(1)

    # #UnitTestComment("Client::waitIO():wait a new prompt")
    # ui_start_time = time.time()
    # sendRawData(accCliClient, "Display Result 5 span 1\n")
    # assert accCliClient.waitIO(cli_AtkIOSelector(Cio_logs_multi.IOTYPE_PROMPT, ".*").from_now(10000))
    # assert ui_start_time + 4 <= time.time() <= ui_start_time + 5
    # assert accCliClient.select(cli_AtkIOSelector(Cio_logs_multi.IOTYPE_PROMPT))[-1].data ==  "Prompt2>"

    # #UnitTestComment("Client::waitIO():wait for an asynchronous IO")
    # ui_start_time = time.time()
    # accCliClient.addParserRegExpr(Cio_logs_multi.IOTYPE_ASYNCHRONOUS, "\[00[0-9]\].*\n")
    # sendRawData(accCliClient, "Send Asynchronous 5\n")
    # accMatchLogs = accCliClient.waitIO(Cio_logs_multi.IOTYPE_ASYNCHRONOUS, 10000)
    # assert accCliClient.waitIO(cli_AtkIOSelector(Cio_logs_multi.IOTYPE_ASYNCHRONOUS).from_now(10000))
    # assert ui_start_time + 4.5 <= time.time() <= ui_start_time + 6
    # assert accCliClient.select(cli_AtkIOSelector(Cio_logs_multi.IOTYPE_ASYNCHRONOUS))[-1].data ==  "[000]Asynchronous Message 0\n"

    # #UnitTestComment("Client::waitIO():include already received IO in the wait")

    # #UnitTestComment("Client::waitIO():ignore received IO during a while")

    # #UnitTestComment("Client::waitIO():check that waitIO cannot be used to match only against already received IOs")

    # # TODO TU : nb de match, timeout

    # accCliClient.close()
    # startStopCliServerMock._accCliClient = None
    

# TODO
def test_UT_AtkIOConnection_PeriodicIO(trace_test_start, test_cleaner):
    pass
