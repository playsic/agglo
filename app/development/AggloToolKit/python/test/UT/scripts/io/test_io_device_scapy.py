############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic. For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file. Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
## * Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in
## the documentation and/or other materials provided with the
## distribution.
## * Neither the name of the Agglo project nor the names of its
## contributors may be used to endorse or promote products derived
## from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest
from pytest import mark

from scapy.all import IP
from scapy.all import ICMP

from agglo_tk.io.scapy import AtkIODeviceScapy
from agglo_tk.exceptions import AtkIODeviceError

from agglo_tt.fixtures import trace_test_start
from fixtures import test_cleaner

    
def test_UT_AtkIODeviceScapy_Constructor(trace_test_start):
    io_device_scapy = AtkIODeviceScapy("lo")

    #UnitTestComment("AtkIODeviceScapy::AtkIODeviceScapy():check that constructor doesn't open connection")
    assert not io_device_scapy.opened
    assert io_device_scapy.fd_in == -1
    assert io_device_scapy.fd_out == -1
    

@mark.sudo
def test_UT_AtkIODeviceScapy_OpenClose(trace_test_start, test_cleaner):
    io_device_scapy = AtkIODeviceScapy("lo")
    
    for i in range (0, 2):
        #UnitTestComment("AtkIODeviceScapy::open():test io device tcp opening")
        io_device_scapy.open()
        test_cleaner.io_device_scapy = io_device_scapy
        assert io_device_scapy.opened
        assert io_device_scapy.fd_in >= 0
        assert io_device_scapy.fd_out >= 0

        #UnitTestComment("AtkIODeviceScapy::open():test io device tcp reopening")
        with pytest.raises(AtkIODeviceError) as exception:
            io_device_scapy.open()
        assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED

        #UnitTestComment("AtkIODeviceScapy::close()")
        io_device_scapy.close()
        test_cleaner.io_device_scapy = None
        assert not io_device_scapy.opened
        assert io_device_scapy.fd_in == -1
        assert io_device_scapy.fd_out == -1

        #UnitTestComment("AtkIODeviceScapy::open():test io device tcp closing while not opened")
        io_device_scapy.close()
        
        
@mark.sudo
def test_UT_AtkIODeviceScapy_ReadWrite(trace_test_start, test_cleaner):
    io_device_scapy = AtkIODeviceScapy("lo")
    ip_local = IP(dst="127.0.0.1")
    ping = ICMP()

    test_cleaner.io_device_scapy = io_device_scapy
    io_device_scapy.open()

    for i in range(0, 3):
        #UnitTestComment("AtkIODeviceScapy::write():test write and read at OSI layer 3")
        io_device_scapy.write(ip_local/ping/("data " + (i + 1) * str(i)))
        # Since we're operating on local loop, there is an echo on packet we send
        io_device_scapy.read(1)
        ping_reply = io_device_scapy.read(1)
        assert str(ping_reply[ICMP].payload) ==  "data " + (i + 1) * str(i)

    # TODO test read write at OSI layer 2