############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import socket
from time import sleep

import pytest

from agglo_tk.io.io_hub import AtkIOHub
from agglo_tk.exceptions import AtkIOHubError
from agglo_tk.exceptions import AtkIODeviceError

from agglo_tk.utils.test.stubs.io_handler import AtkStubIOHandler
from agglo_tk.utils.test.stubs.io_device import AtkStubIODevice
from fixtures import test_cleaner
from agglo_tt.fixtures import trace_test_start
    

def test_UT_AtkIOHub_Constructor(trace_test_start):
    io_hub = AtkIOHub()

    #UnitTestComment("AtkIOHub::AtkIOHub():check that constructor doesn't open connection")
    assert not(io_hub.is_opened())


def test_UT_AtkIOHub_OpenCloseErrorCases(trace_test_start, test_cleaner):
    io_hub = AtkIOHub()
    io_handler = AtkStubIOHandler()
    io_device = AtkStubIODevice()

    test_cleaner.io_hub = io_hub

    #UnitTestComment("AtkIOHub::open(): test opening a non attached socket")
    with pytest.raises(AtkIOHubError) as exception:
        io_hub.open(io_device)
    assert str(exception.value) == AtkIOHubError.UNKNOWN_DEVICE

    #UnitTestComment("AtkIOHub::open(): test opening a device outside of io hub")
    io_hub.attach(io_device, io_handler)
    io_device.open()
    with pytest.raises(AtkIODeviceError) as exception:
        io_hub.open(io_device)
    assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED

    #UnitTestComment("AtkIOHub::open(): test closing a device opened outside of io hub")
    with pytest.raises(AtkIOHubError) as exception:
        io_hub.close(io_device)
    assert str(exception.value) == AtkIOHubError.CONFLICT
    assert io_device.opened

    #UnitTestComment("AtkIOHub::open(): test opening a device twice")
    io_device.close()
    io_hub.open(io_device)
    with pytest.raises(AtkIODeviceError) as exception:
        io_hub.open(io_device)
    assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED


# TODO test attach
# TODO test openclose pour tous les device
def test_UT_AtkIOHub_OpenClose(trace_test_start, test_cleaner):
    io_hub = AtkIOHub()
    io_handler = AtkStubIOHandler()
    io_device = AtkStubIODevice()
    test_cleaner.io_hub = io_hub

    io_hub.attach(io_device, io_handler)
    for i in range (0, 2):
        #UnitTestComment("AtkIOHub::open():test io hub opening")
        io_hub.open(io_device)
        assert io_hub.is_opened(io_device)

        #UnitTestComment("AtkIOHub::close()")
        io_hub.close(io_device)
        assert not io_hub.is_opened(io_device)

        #UnitTestComment("AtkIOHub::close(): close a device twice")
        io_hub.close(io_device)
    

def test_UT_AtkIOHub_Read(trace_test_start, test_cleaner):
    io_hub = AtkIOHub()
    io_handler1 = AtkStubIOHandler()
    io_handler2 = AtkStubIOHandler()
    io_device1 = AtkStubIODevice()
    io_device2 = AtkStubIODevice()
    test_cleaner.io_hub = io_hub

    io_hub.attach(io_device1, io_handler1)
    io_hub.attach(io_device2, io_handler2)
    io_hub.open()

    #UnitTestComment("AtkIOHub::read:test read")
    io_device1.write("data1\n")
    sleep(0.2)
    assert io_handler1.received == "data1\n"

    #UnitTestComment("AtkIOHub::read:test reception thread id")
    io_device2.write("data2\n")
    sleep(0.2)
    assert io_handler1.thread_id != -1
    assert io_handler1.thread_id == io_handler2.thread_id
