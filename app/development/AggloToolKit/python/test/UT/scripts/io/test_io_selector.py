############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time
from collections import Counter

import pytest

from agglo_tk.io.io_selector import AtkIOSelector
from agglo_tk.io import AtkIOCriteria
from agglo_tk.io import AtkIO
from agglo_tk.io import AtkIOLogs
from agglo_tk.io import AtkIOConnectionLogs

from agglo_tk.exceptions import AtkSelectorError
from agglo_tk.exceptions import AtkIOSelectorError

from agglo_tt.fixtures import trace_test_start


def test_UT_AtkIOSelector_Constructor(trace_test_start):
    IO_TYPE1 = 1

    #UnitTestComment("AtkIOSelector: construct instance with default io criteria")
    io_selector = AtkIOSelector(AtkIOCriteria())
    assert io_selector.io_type is None
    assert io_selector.first_rank is None
    assert io_selector.last_rank is None
    assert io_selector.start_time is None
    assert io_selector.end_time is None

    #UnitTestComment("AtkIOSelector: construct instance with io criteria set")
    io_selector = AtkIOSelector(AtkIOCriteria(io_type=IO_TYPE1, first_rank=1 , last_rank=5, \
                                              start_time=102.5, end_time=107.891))
    assert io_selector.io_type == IO_TYPE1
    assert io_selector.first_rank == 1
    assert io_selector.last_rank == 5
    assert io_selector.start_time == 102.5
    assert io_selector.end_time == 107.891


def test_UT_AtkIOSelector_Properties(trace_test_start):
    IO_TYPE1 = 1
    IO_TYPE2 = 2
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1)
    io_selector = AtkIOSelector(io_criteria)

    #UnitTestComment("AtkIOSelector: set parameters")
    io_selector.io_type = IO_TYPE2
    assert io_selector.io_type == IO_TYPE2
    assert io_criteria.io_type == IO_TYPE2
    io_selector.first_rank = 4
    assert io_selector.first_rank == 4
    assert io_criteria.first_rank == 4
    io_selector.last_rank = 5
    assert io_selector.last_rank == 5
    assert io_criteria.last_rank == 5
    io_selector.start_time = 2
    assert io_selector.start_time == 2
    assert io_criteria.start_time == 2
    io_selector.end_time = 12
    assert io_selector.end_time == 12
    assert io_criteria.end_time == 12

 
def test_UT_AtkIOSelector_SelectIOLogs(trace_test_start):
    IO_TYPE1 = 1
    IO_TYPE2 = 2
    io_logs = AtkIOLogs(IO_TYPE1)
    start_time = time.time()

    time.sleep(0.001)
    io1 = io_logs.add_io("123 test")
    time.sleep(0.001)
    end_time1 = time.time()
    time.sleep(0.001)
    io2 = io_logs.add_io("12 test")
    time.sleep(0.001)
    end_time2 = time.time()
    
    #UnitTestComment("AtkIOSelector::select() without specifiers")
    matching_ios = AtkIOSelector(AtkIOCriteria()).select(io_logs)
    assert matching_ios[0] is io1
    assert matching_ios[1] is io2
    assert len(matching_ios) == 2
    
    #UnitTestComment("AtkIOSelector::select() with io_type specifier")
    matching_ios = AtkIOSelector(AtkIOCriteria(io_type=IO_TYPE1)).select(io_logs)
    assert matching_ios[0] is io1
    assert matching_ios[1] is io2
    assert len(matching_ios) == 2
    assert not AtkIOSelector(AtkIOCriteria(io_type=IO_TYPE2)).select(io_logs)
    
    #UnitTestComment("AtkIOSelector::select() with rank specifier")
    matching_ios = AtkIOSelector(AtkIOCriteria(first_rank=0)).select(io_logs)
    assert matching_ios[0] is io1
    assert matching_ios[1] is io2
    assert len(matching_ios) == 2
    matching_ios = AtkIOSelector(AtkIOCriteria(first_rank=1)).select(io_logs)
    assert matching_ios[0] is io2
    assert len(matching_ios) == 1
    matching_ios = AtkIOSelector(AtkIOCriteria(last_rank=0)).select(io_logs)
    assert matching_ios[0] is io1
    assert len(matching_ios) == 1
    assert not AtkIOSelector(AtkIOCriteria(first_rank=2)).select(io_logs)
    matching_ios = AtkIOSelector(AtkIOCriteria(first_rank=-1)).select(io_logs)
    assert matching_ios[0] is io2
    assert len(matching_ios) == 1
    matching_ios = AtkIOSelector(AtkIOCriteria(last_rank=-2)).select(io_logs)
    assert matching_ios[0] is io1
    assert len(matching_ios) == 1
    
    #UnitTestComment("AtkIOSelector::select() with time specifiers")
    matching_ios = AtkIOSelector(AtkIOCriteria(start_time=start_time)).select(io_logs)
    assert matching_ios[0] is io1
    assert matching_ios[1] is io2
    assert len(matching_ios) == 2
    matching_ios = AtkIOSelector(AtkIOCriteria(start_time=end_time1)).select(io_logs)
    assert matching_ios[0] is io2
    assert len(matching_ios) == 1
    matching_ios = AtkIOSelector(AtkIOCriteria(end_time=end_time1)).select(io_logs)
    assert matching_ios[0] is io1
    assert len(matching_ios) == 1
    assert not AtkIOSelector(AtkIOCriteria(start_time=end_time2)).select(io_logs)
    
    #UnitTestComment("AtkIOSelector::select() with several specifiers")
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=0, last_rank=2, 
                                start_time=start_time, end_time=end_time2)
    matching_ios = AtkIOSelector(io_criteria).select(io_logs)
    assert matching_ios[0] is io1
    assert matching_ios[1] is io2
    assert len(matching_ios) == 2
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=0, last_rank=2, 
                                start_time=end_time1, end_time=end_time2)
    matching_ios = AtkIOSelector(io_criteria).select(io_logs)
    assert matching_ios[0] is io2
    assert len(matching_ios) == 1


def test_UT_AtkIOSelector_SelectIOConnectionLogs(trace_test_start):
    io_connection_logs = AtkIOConnectionLogs("input", "output")

    io_connection_logs["input"].add_io("input1")
    io_connection_logs["input"].add_io("input2")
    io_connection_logs["output"].add_io("output1")

    #UnitTestComment("AtkIOSelector::select(): test that selecting with unknown io type selects no io")
    matching_ios = AtkIOSelector(AtkIOCriteria(io_type="unknown")).select(io_connection_logs)
    assert not matching_ios
    
    #UnitTestComment("AtkIOSelector::select(): test that select operates in the good io logs")
    matching_ios = AtkIOSelector(AtkIOCriteria(io_type="input")).select(io_connection_logs)
    assert len(matching_ios) == 2
    matching_ios = AtkIOSelector(AtkIOCriteria(io_type="output")).select(io_connection_logs)
    assert len(matching_ios) == 1

    #UnitTestComment("AtkIOSelector::select(): test that select can operates on several ios")
    matching_ios = AtkIOSelector(AtkIOCriteria()).select(io_connection_logs)
    assert len(matching_ios) == 3
    
    #UnitTestComment("AtkIOSelector::select(): test that selector attributes are correctly used")
    matching_ios = AtkIOSelector(AtkIOCriteria(io_type="input", first_rank=1)).select(io_connection_logs)
    assert len(matching_ios) == 1

# TODO tester avec des criterias string et scapy


# def test_UT_AtkIOScapyCriteria_SelectIOLogs(trace_test_start):
    # match_protocol=lambda crit, data: crit.match_protocol(data, "protocol")

    # ip_criteria = AtkIOScapyCriteria(IP)
    # tcp_criteria = AtkIOScapyCriteria(TCP)
    # icmp_criteria = AtkIOScapyCriteria(ICMP)
    # udp_criteria = AtkIOScapyCriteria(UDP)

    # IO_TYPE1 = 1
    # IO_TYPE2 = 2
    # io_logs = AtkIOLogs(IO_TYPE1)
    # start_time = time.time()

    # time.sleep(0.001)
    # io_tcp = io_logs.add_io(IP()/TCP())
    # time.sleep(0.001)
    # end_time1 = time.time()
    # time.sleep(0.001)
    # io_icmp = io_logs.add_io(IP()/ICMP())
    # time.sleep(0.001)
    # end_time2 = time.time()
    
    # #UnitTestComment("AtkIOScapyCriteria::select() without specifiers")
    # matching_ios = AtkIOScapyCriteria().select(io_logs)
    # assert matching_ios[0] is io_tcp
    # assert matching_ios[1] is io_icmp
    # assert len(matching_ios) == 2
    
    # #UnitTestComment("AtkIOScapyCriteria::select() with tcp protocol specifier")
    # matching_ios = AtkIOScapyCriteria(protocol=IP, match_io_data=match_protocol).select(io_logs)
    # assert matching_ios[0] is io_tcp
    # assert matching_ios[1] is io_icmp
    # assert len(matching_ios) == 2
    # matching_ios = AtkIOScapyCriteria(protocol=ICMP, match_io_data=match_protocol).select(io_logs)
    # assert matching_ios[0] is io_icmp
    # assert len(matching_ios) == 1
    # matching_ios = AtkIOScapyCriteria(protocol=UDP, match_io_data=match_protocol).select(io_logs)
    # assert not matching_ios
    
    # #UnitTestComment("AtkIOScapyCriteria::select() with several specifiers")
    # matching_ios = AtkIOScapyCriteria(first_rank=0, last_rank=2, 
    #                                   start_time=start_time, end_time=end_time2, 
    #                                   io_type=IO_TYPE1, protocol=ICMP, 
    #                                   match_io_data=match_protocol).select(io_logs)
    # assert matching_ios[0] is io_icmp
    # assert len(matching_ios) == 1
    # matching_ios = AtkIOScapyCriteria(first_rank=0, last_rank=2, 
    #                                   start_time=end_time1, end_time=end_time2, 
    #                                   io_type=IO_TYPE1, protocol=ICMP, 
    #                                   match_io_data=match_protocol).select(io_logs)
    # assert matching_ios[0] is io_icmp
    # assert len(matching_ios) == 1

 
# def test_UT_AtkIOStringCriteria_SelectIOLogs(trace_test_start):
#     IO_TYPE1 = 1
#     IO_TYPE2 = 2
#     io_logs = AtkIOLogs(IO_TYPE1)
#     start_time = time.time()

#     time.sleep(0.001)
#     io1 = io_logs.add_io("123 test")
#     time.sleep(0.001)
#     end_time1 = time.time()
#     time.sleep(0.001)
#     io2 = io_logs.add_io("12 test")
#     time.sleep(0.001)
#     end_time2 = time.time()
    
#     #UnitTestComment("AtkIOStringCriteria::select() without specifiers")
#     matching_ios = AtkIOStringCriteria().select(io_logs)
#     assert matching_ios[0] is io1
#     assert matching_ios[1] is io2
#     assert len(matching_ios) == 2
    
#     #UnitTestComment("AtkIOStringCriteria::select() with reg expr specifier")
#     matching_ios = AtkIOStringCriteria(reg_expr="[1-9]* [a-z]*").select(io_logs)
#     assert matching_ios[0] is io1
#     assert matching_ios[1] is io2
#     assert len(matching_ios) == 2
#     matching_ios = AtkIOStringCriteria(reg_expr="^[1-9]{2} [a-z]*").select(io_logs)
#     assert matching_ios[0] is io2
#     assert len(matching_ios) == 1
#     matching_ios = AtkIOStringCriteria(reg_expr="^[1-9] [a-z]*").select(io_logs)
#     assert not matching_ios
    
#     #UnitTestComment("AtkIOStringCriteria::select() with several specifiers")
#     matching_ios = AtkIOStringCriteria(io_type=IO_TYPE1, reg_expr="^[1-9]{2} [a-z]*", 
#                                    first_rank=0, last_rank=2, 
#                                    start_time=start_time, end_time=end_time2).select(io_logs)
#     assert matching_ios[0] is io2
#     assert len(matching_ios) == 1
#     matching_ios = AtkIOStringCriteria(io_type=IO_TYPE1, reg_expr="[1-9]{2} [a-z]*", 
#                                    first_rank=0, last_rank=2, 
#                                    start_time=end_time1, end_time=end_time2).select(io_logs)
#     assert matching_ios[0] is io2
#     assert len(matching_ios) == 1
