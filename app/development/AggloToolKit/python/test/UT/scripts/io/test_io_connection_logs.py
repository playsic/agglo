############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.io import AtkIOConnectionLogs

from agglo_tt.fixtures import trace_test_start


class Owner:
    def __init__(self, name):
        self.name = name


def testUT_AtkIOConnectionLogs_BuiltInErrorCases(trace_test_start):
    io_connection_logs = AtkIOConnectionLogs()

    #UnitTestComment("AtkIOConnectionLogs::__getitem__(): test that unknown io type raises an error")
    with pytest.raises(KeyError):
        io_connection_logs["unknown"]


def testUT_AtkIOConnectionLogs_GetIOType(trace_test_start):
    io_connection_logs = AtkIOConnectionLogs()
    owner1 = Owner("owner1")
    owner2 = Owner("owner2")
    io_type1 = "io_type1"
    io_type2 = "io_type2"
    unique_io_type1_1 = None
    unique_io_type1_2 = None
    unique_io_type2_1 = None

    #UnitTestComment("AtkIOConnectionLogs::get_io_type(): check that getting an unregistered io type raises exception")
    with pytest.raises(IndexError):
        io_connection_logs.get_io_type(io_type1, owner1)

    #UnitTestComment("AtkIOConnectionLogs::get_io_type(): check that getting a registered io type returns an id")
    io_connection_logs.add_io_logs(io_type1, owner1)
    unique_io_type1_1 = io_connection_logs.get_io_type(io_type1, owner1)
    assert unique_io_type1_1 is not None

    #UnitTestComment("AtkIOConnectionLogs::get_io_type(): check that registering a different io type on same owner returns a unique id")
    with pytest.raises(IndexError):
        io_connection_logs.get_io_type(io_type2, owner1)
    io_connection_logs.add_io_logs(io_type2, owner1)
    unique_io_type1_2 = io_connection_logs.get_io_type(io_type2, owner1)
    assert unique_io_type1_2 is not None
    assert unique_io_type1_2 != unique_io_type1_1

    #UnitTestComment("AtkIOConnectionLogs::get_io_type(): check that registering same io type on different owner returns a unique id")
    with pytest.raises(IndexError):
        io_connection_logs.get_io_type(io_type1, owner2)
    io_connection_logs.add_io_logs(io_type1, owner2)
    unique_io_type2_1 = io_connection_logs.get_io_type(io_type1, owner2)
    assert unique_io_type2_1 is not None
    assert unique_io_type2_1 != unique_io_type1_1
    assert unique_io_type2_1 != unique_io_type1_2


def testUT_AtkIOConnectionLogs_AddIOLogs(trace_test_start):
    io_connection_logs = AtkIOConnectionLogs()
    owner1 = Owner("owner1")
    owner2 = Owner("owner2")
    io_type1 = "io_type1"
    io_type2 = "io_type2"
    unique_io_type1_1 = None
    io_logs1_1 = None
    io_logs1_2 = None
    io_logs2_1 = None

    #UnitTestComment("AtkIOConnectionLogs::add_io_logs(): check that added io logs are accessible")
    io_logs1_1 = io_connection_logs.add_io_logs(io_type1, owner1)
    assert io_logs1_1 is not None
    unique_io_type1_1 = io_connection_logs.get_io_type(io_type1, owner1)
    assert io_logs1_1.io_type == unique_io_type1_1
    assert io_connection_logs[unique_io_type1_1] is io_logs1_1

    #UnitTestComment("AtkIOConnectionLogs::add_io_logs(): check that added same io logs on same owner fails")
    with pytest.raises(ValueError):
        io_connection_logs.add_io_logs(io_type1, owner1)

    #UnitTestComment("AtkIOConnectionLogs::get_io_type(): check that registering a different io type on same owner returns a new io logs")
    io_logs1_2 = io_connection_logs.add_io_logs(io_type2, owner1)
    assert io_logs1_2 is not None
    assert io_logs1_2 is not io_logs1_1
    assert io_connection_logs[io_logs1_2.io_type] is io_logs1_2

    #UnitTestComment("AtkIOConnectionLogs::get_io_type(): check that registering same io type on different owner returns a unique id")
    io_logs2_1 = io_connection_logs.add_io_logs(io_type1, owner2)
    assert io_logs2_1 is not None
    assert io_logs2_1 is not io_logs1_1
    assert io_logs2_1 is not io_logs1_2
    assert io_connection_logs[io_logs2_1.io_type] is io_logs2_1


# TODO faire le test avec des criterias
def testUT_AtkIOConnectionLogs_SelectByCriteria(trace_test_start):
    pass


def testUT_AtkIOConnectionLogs_AddIOs(trace_test_start):
    io_connection_logs = AtkIOConnectionLogs()
    owner = Owner("owner")
    io_type_input = "input"
    io_type_output = "output"

    #UnitTestComment("AtkIOConnectionLogs: test that io log can be edited")
    io_connection_logs.add_io_logs(io_type_input, owner)
    io_type_input = "owner_input"
    io_input1 = io_connection_logs[io_type_input].add_io("input1")
    assert io_connection_logs[io_type_input][0].data == "input1"

    #UnitTestComment("AtkIOConnectionLogs::add_io_logs(): create another io logs")
    io_connection_logs.add_io_logs(io_type_output, owner)
    io_type_output = "owner_output"
    io_output1 = io_connection_logs[io_type_output].add_io("output1")
    assert io_connection_logs[io_type_output][0] is io_output1
    assert io_connection_logs[io_type_input][0] is io_input1


# def test_UT_AtkIOSelector_CheckIOConnectionLogs(trace_test_start):
#     io_connection_logs = AtkIOConnectionLogs()

#     io_input1 = io_connection_logs["input"].add_io("input1")
#     io_input2 = io_connection_logs["input"].add_io("input2")
#     io_output1 = io_connection_logs["output"].add_io("output1")
    
#     #UnitTestComment("AtkIOConnectionLogs::check(): test that check expects one occurence by default")
#     assert AtkIOSelector(AtkIOCriteria(io_type="output")).check(io_connection_logs) is io_output1
#     with pytest.raises(AtkSelectorError):
#         AtkIOSelector(AtkIOCriteria(io_type="input")).check(io_connection_logs)
    
#     #UnitTestComment("AtkIOConnectionLogs::check(): test that number of expected occurences can be set")
#     found_ios = AtkIOSelector(AtkIOCriteria(io_type="input")).check(io_connection_logs, 2)
#     assert Counter(found_ios) == Counter([io_input1, io_input2])
#     with pytest.raises(AtkSelectorError):
#         AtkIOSelector(AtkIOCriteria(io_type="output")).check(io_connection_logs, 2)
    
#     #UnitTestComment("AtkIOConnectionLogs::check(): test that selector attributes are correctly used")
#     assert AtkIOSelector(AtkIOCriteria(io_type="input", first_rank=1)).check(io_connection_logs) is io_input2


 
# def testUT_AtkIOConnectionLogs_Select():
#     io_connection_logs = AtkIOConnectionLogs()
    
#     # We don't loop on IOTYPE_COMMAND_RESULT buffer type, because management of this type 
#     # of buffer is slightly different, but it is not relevant for select unit tests
#     for uiBufferType in range(Cio_connection_logs.IOTYPE_LOGS, Cio_connection_logs.IOTYPE_COMMAND + 1):
#         ui_start_time = time.time()
#         time.sleep(0.001)
#         io_connection_logs._addLog(uiBufferType, "123 test")
#         time.sleep(0.001)
#         ui_end_time_1 = time.time()
#         time.sleep(0.001)
#         io_connection_logs._addLog(uiBufferType, "12 test")
#         time.sleep(0.001)
#         ui_end_time_2 = time.time()
    
#         #UnitTestComment("Cio_connection_logs::select() without regexpr specifiers")
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType))
#         assert acc_matching_ios[0].data ==  "123 test"
#         assert acc_matching_ios[1].data ==  "12 test"
#         assert len(acc_matching_ios) == 2
        
#         #UnitTestComment("Cio_connection_logs::select() without time specifiers")
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*"))
#         assert acc_matching_ios[0].data ==  "123 test"
#         assert acc_matching_ios[1].data ==  "12 test"
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{3} [a-z]*"))
#         assert acc_matching_ios[0].data ==  "123 test"
#         assert len(acc_matching_ios) == 1
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "^[1-9]{2} [a-z]*"))
#         assert acc_matching_ios[0].data ==  "12 test"
#         assert len(acc_matching_ios) == 1
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "^[1-9] [a-z]*"))
#         assert len(acc_matching_ios) == 0

#         # TODO test with rank specifiers
        
#         #UnitTestComment("AtkIOConnectionLogs::select() with time specifiers")
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*", aui_start_time=ui_start_time, aui_end_time=ui_end_time_2))
#         assert acc_matching_ios[0].data ==  "123 test"
#         assert acc_matching_ios[1].data ==  "12 test"
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*", aui_start_time=ui_start_time, aui_end_time=ui_end_time_1))
#         assert acc_matching_ios[0].data ==  "123 test"
#         assert len(acc_matching_ios) == 1
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*", aui_start_time=ui_end_time_1, aui_end_time=ui_end_time_2))
#         assert acc_matching_ios[0].data ==  "12 test"
#         assert len(acc_matching_ios) == 1
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*", aui_start_time=ui_end_time_2))
#         assert len(acc_matching_ios) == 0
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*", aui_start_time=ui_end_time_2, aui_end_time=ui_end_time_2 + 10))
#         assert len(acc_matching_ios) == 0
#         acc_matching_ios = io_connection_logs.select(AtkIOSelector(uiBufferType, "[1-9]{2} [a-z]*", aui_start_time=ui_start_time - 10, aui_end_time=ui_start_time))
#         assert len(acc_matching_ios) == 0
