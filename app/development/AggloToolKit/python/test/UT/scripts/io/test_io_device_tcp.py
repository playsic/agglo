############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic. For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file. Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
## * Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in
## the documentation and/or other materials provided with the
## distribution.
## * Neither the name of the Agglo project nor the names of its
## contributors may be used to endorse or promote products derived
## from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time
import socket

import pytest

from agglo_tk.io import AtkIODeviceTCP

from agglo_tk.utils.test import DEFAULT_MIRROR_TCP_PORT
from agglo_tk.utils.test import mirroring_server
from fixtures import test_cleaner
from agglo_tt.fixtures import trace_test_start


    
def test_UT_AtkIODeviceTCP_Constructor(trace_test_start):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_MIRROR_TCP_PORT)

    #UnitTestComment("AtkIODeviceTCP::AtkIODeviceTCP():check that constructor doesn't open connection")
    assert not io_device_tcp.opened
    assert io_device_tcp.fd == -1
    

def test_UT_AtkIODeviceTCP_OpenClose(trace_test_start, mirroring_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_MIRROR_TCP_PORT)

    #UnitTestComment("AtkIODeviceTCP::open():test io device tcp opening failure")
    io_device_failed = AtkIODeviceTCP("localhost", DEFAULT_MIRROR_TCP_PORT + 1)
    with pytest.raises(socket.error):
        io_device_failed.open()
    assert not io_device_failed.opened
    
    for i in range (0, 2):
        #UnitTestComment("AtkIODeviceTCP::open():test io device tcp opening")
        io_device_tcp.open()
        test_cleaner.io_device_tcp = io_device_tcp
        assert io_device_tcp.opened

        #UnitTestComment("AtkIODeviceTCP::open():test io device tcp reopening")
        with pytest.raises(ValueError):
            io_device_tcp.open()

        #UnitTestComment("AtkIODeviceTCP::close()")
        io_device_tcp.close()
        test_cleaner.io_device_tcp = None
        assert not io_device_tcp.opened

        #UnitTestComment("AtkIODeviceTCP::open():test io device tcp closing while not opened")
        io_device_tcp.close()
        
        
def test_UT_AtkIODeviceTCP_ReadWrite(trace_test_start, mirroring_server, test_cleaner):
    io_device_tcp = AtkIODeviceTCP("localhost", DEFAULT_MIRROR_TCP_PORT)
    test_cleaner.io_device_tcp = io_device_tcp
    io_device_tcp.open()

    time.sleep(0.2)
    for i in range (0, 3):
        #UnitTestComment("AtkIODeviceTCP::write():test write and read including a \n")
        io_device_tcp.write("data " + str(i) + "\n")
        assert io_device_tcp.read(1) ==  "data " + str(i) + "\n"
    
        #UnitTestComment("AtkIODeviceTCP::write():test write and read including without \n")
        io_device_tcp.write("data " + str(i))
        assert io_device_tcp.read(1) ==  "data " + str(i)

