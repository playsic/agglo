############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

import pytest

from agglo_tk.io.io_logs import AtkIOLogs

from agglo_tt.fixtures import trace_test_start


def test_UT_AtkIOLogs_Constructor(trace_test_start):
    #UnitTestComment("AtkIOLogs::AtkIOLogs()")
    io_logs = AtkIOLogs(2)
    assert len(io_logs) == 0
    assert io_logs.io_type == 2
    
    
def test_UT_AtkIOLogs_BuiltIn(trace_test_start):
    io_logs = AtkIOLogs(2)

    #UnitTestComment("AtkIOLogs:__getitem__():test boolean")
    assert not io_logs
    io1 = io_logs.add_io("data1")
    assert io_logs

    #UnitTestComment("AtkIOLogs:__getitem__():test ios index")
    io2 = io_logs.add_io("data2")
    assert io_logs[0] is io1
    assert io_logs[-1] is io2
    with pytest.raises(IndexError):
        io_logs[2]
    with pytest.raises(IndexError):
        io_logs[-3]

    #UnitTestComment("AtkIOLogs:__getitem__():test length")
    assert len(io_logs) == 2

    #UnitTestComment("AtkIOLogs:__getitem__():test iteration")
    count_iter = 0
    for i, io in enumerate(io_logs):
        assert io.data == "data" + str(i + 1)
        count_iter += 1
    assert count_iter == len(io_logs)


def test_UT_AtkIOLogs_EditEntries(trace_test_start):
    io_logs = AtkIOLogs(2)    
    set_entry_time = time.time()

    for data_idx in range(0, 4):
        #UnitTestComment("AtkIOLogs:add_io()")
        time.sleep(0.001)
        io_logs.add_io("test" + str(data_idx))
        time.sleep(0.001)
        end_time1 = time.time()
        io = io_logs[data_idx]
        assert io.data == "test" + str(data_idx)
        assert set_entry_time < io.time < end_time1

        #UnitTestComment("Cio::append_last_io()")
        time.sleep(0.001)
        io_logs.append_last_io(".2")
        time.sleep(0.001)
        end_time2 = time.time()
        io = io_logs[data_idx]
        assert io.data ==  "test" + str(data_idx) + ".2"
        assert end_time1 < io.time < end_time2
    
        #UnitTestComment("AtkIOLogs:check that last entry edition doesn't modify former entries")
        for i in range(0, data_idx):
            io = io_logs[data_idx - 1 - i]
            assert io.data == "test" + str(data_idx - 1 - i) + ".2"
            assert io.time < set_entry_time
            set_entry_time = io.time

        set_entry_time = end_time2        
