############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic. For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file. Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
## * Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in
## the documentation and/or other materials provided with the
## distribution.
## * Neither the name of the Agglo project nor the names of its
## contributors may be used to endorse or promote products derived
## from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from time import sleep

import pytest
from pytest import mark

from scapy.all import IP
from scapy.all import ICMP
from scapy.all import TCP

from agglo_tk.io.scapy import AtkIOConnectionScapy
from agglo_tk.io.scapy import AtkIOScapyCriteria
from agglo_tk.io import AtkIOConnectionLogs
from agglo_tk.io import AtkIOHub

from agglo_tk.exceptions import AtkIODeviceError

from agglo_tt.fixtures import trace_test_start
from fixtures import test_cleaner
from agglo_tk.utils.test import *


def test_UT_AtkIOConnectionScapy_Constructor(trace_test_start):
    io_connection = AtkIOConnectionScapy("lo")
    io_logs = AtkIOConnectionLogs()
    io_hub = AtkIOHub()
    
    #UnitTestComment("AtkIOConnectionScapy::AtkIOConnectionScapy():check that constructor set defaults parameters")
    assert io_connection.io_logs is not None
    assert io_connection.io_hub is not None

    #UnitTestComment("AtkIOConnectionScapy::AtkIOConnectionScapy():check that constructor doesn't open connection")
    assert not io_connection.opened
    assert not io_connection.io_hub.is_opened()

    #UnitTestComment("AtkIOConnectionScapy::AtkIOConnectionScapy():check constructor with other parameters")
    io_connection = AtkIOConnectionScapy("lo", io_logs, io_hub)
    assert io_connection.io_logs is io_logs
    assert io_connection.io_hub is io_hub


@mark.sudo
def test_UT_AtkIOConnectionScapy_OpenClose(trace_test_start, test_cleaner):
    io_connection = AtkIOConnectionScapy("lo")
    
    for i in range (0, 2):
        #UnitTestComment("AtkIOConnectionScapy::open():test io connection opening")
        test_cleaner.io_connection = io_connection
        io_connection.open()
        assert io_connection.opened
        assert io_connection.io_hub.is_opened()

        #UnitTestComment("AtkIOConnectionScapy::open():test io connection reopening")
        with pytest.raises(AtkIODeviceError) as exception:
            io_connection.open()
        assert str(exception.value) == AtkIODeviceError.ALREADY_OPENED

        #UnitTestComment("AtkIOConnectionScapy::close()")
        io_connection.close()
        test_cleaner.io_connection = None
        assert not io_connection.opened
        assert not io_connection.io_hub.is_opened()

        #UnitTestComment("AtkIOConnectionScapy::open():test io connection closing while not opened")
        io_connection.close()


@mark.sudo
def test_UT_AtkIOConnectionScapy_ReadWrite(trace_test_start, test_cleaner, dump_failed):
    io_logs = AtkIOConnectionLogs()
    io_connection = AtkIOConnectionScapy("lo", io_logs)
    ip = IP(dst="127.0.0.1")
    sent_data_criteria = AtkIOScapyCriteria(io_type=io_connection.io_type_output, 
                                            match_io_data=lambda crit, io_data: ICMP in io_data)
    match_payload = lambda crit, io_data: ICMP in io_data and (str(io_data[ICMP].payload) == crit.payload)
    received_data_criteria = AtkIOScapyCriteria(io_type=io_connection.io_type_input, match_io_data=match_payload)
   
    test_cleaner.io_connection = io_connection
    dump_failed.io_logs = io_logs
    io_connection.open()

    for i in range(0, 3):
        payload = "data" + str(i)

        #UnitTestComment("AtkIOConnection::send():test payload emission on ICMP")
        sent_data_criteria.from_now(500)
        received_data_criteria.from_now(500)
        io_connection.send(ip/ICMP()/payload)
        sent = io_logs.check(sent_data_criteria)
        assert str(sent.data[ICMP].payload) == payload

        #UnitTestComment("AtkIOConnection::read():test payload reception on ICMP")
        sleep(0.1)
        received_data_criteria.payload = payload
        io_logs.check(received_data_criteria, nb_occurrences=2)


@mark.sudo
def test_UT_AtkIOConnectionScapy_OpenCloseTCPErrorCases(trace_test_start, test_cleaner, mirroring_server):
    io_connection = AtkIOConnectionScapy("lo")
   
    # TODO setter les exceptions attendues
    #UnitTestComment("AtkIOConnectionScapy::open_tcp():test tcp opening failure on closed connection")
    with pytest.raises(Exception):
        io_connection.open_tcp("127.0.0.1", DEFAULT_MIRROR_TCP_PORT)
        
    #UnitTestComment("AtkIOConnectionScapy::open_tcp():test tcp opening failure on bad tcp port")
    test_cleaner.io_connection = io_connection
    io_connection.open()
    with pytest.raises(Exception):
        io_connection.open_tcp("127.0.0.1", DEFAULT_MIRROR_TCP_PORT + 1)
   
    #UnitTestComment("AtkIOConnectionScapy::open_tcp():test tcp opening failure on timeout")
    with pytest.raises(Exception):
        io_connection.open_tcp("127.0.0.1", DEFAULT_MIRROR_TCP_PORT, timeout_sec=0.001)

    #UnitTestComment("AtkIOConnectionScapy::close_tcp():test tcp closing failure")
    with pytest.raises(Exception):
        io_connection.close_tcp((10000, DEFAULT_MIRROR_TCP_PORT))


@mark.sudo
def test_UT_AtkIOConnectionScapy_OpenCloseTCP(trace_test_start, test_cleaner, dump_failed, mirroring_server):
    SRC_PORT = 10000
    io_logs = AtkIOConnectionLogs()
    io_connection = AtkIOConnectionScapy("lo", io_logs)
    tcp_criteria = AtkIOScapyCriteria(io_type=io_connection.io_type_output, sport=SRC_PORT, 
                                      match_io_data=lambda crit, io_data: crit.match_field(io_data, TCP, "sport"))
   
    test_cleaner.io_connection = io_connection
    dump_failed.io_logs = io_logs
    io_connection.open()

    #UnitTestComment("AtkIOConnectionScapy::write():test tcp opening")
    tcp_criteria.from_now(2000)
    ack = io_connection.open_tcp("127.0.0.1", DEFAULT_MIRROR_TCP_PORT, SRC_PORT)
    assert ack.sport == SRC_PORT
    assert ack.dport == DEFAULT_MIRROR_TCP_PORT

    #UnitTestComment("AtkIOConnectionScapy::write(): check that 2 frames were sent : syn and ack")
    io_syn, io_ack = io_logs.check(tcp_criteria, nb_occurrences=2)
    assert io_syn.data[TCP].flags == 2
    assert io_ack.data[TCP].flags == 16
    
    #UnitTestComment("AtkIOConnectionScapy::write(): check that 1 frame was received : synack")
    tcp_criteria.sport = DEFAULT_MIRROR_TCP_PORT
    tcp_criteria.io_type = io_connection.io_type_input
    io_syn_ack = io_logs.check(tcp_criteria)
    assert io_syn_ack.data[TCP].flags == 18
    assert io_syn_ack.time > io_syn.time
    assert io_syn_ack.time < io_ack.time

    #UnitTestComment("AtkIOConnectionScapy::write():test tcp closing")
    tcp_criteria.from_now(2000)
    io_connection.close_tcp((io_ack.data.sport, io_ack.data.dport))

    #UnitTestComment("AtkIOConnectionScapy::write(): check that 2 frames were sent : fin and ack")
    tcp_criteria.sport = SRC_PORT
    tcp_criteria.io_type = io_connection.io_type_output
    io_fin, io_ack = io_logs.check(tcp_criteria, nb_occurrences=2)
    assert io_fin.data[TCP].flags == 17
    assert io_ack.data[TCP].flags == 16

    #UnitTestComment("AtkIOConnectionScapy::write(): check that 1 frame was received : finack")
    tcp_criteria.sport = DEFAULT_MIRROR_TCP_PORT
    tcp_criteria.io_type = io_connection.io_type_input
    io_fin_ack = io_logs.check(tcp_criteria)
    assert io_fin_ack.data[TCP].flags == 16
    assert io_fin_ack.time > io_fin.time
    assert io_fin_ack.time < io_ack.time
 

@mark.sudo
def test_UT_AtkIOConnectionScapy_ReadWriteTCP(trace_test_start, test_cleaner, dump_failed, mirroring_server):
    SRC_PORT = 15000
    io_logs = AtkIOConnectionLogs()
    io_connection = AtkIOConnectionScapy("lo", io_logs)
    ip = IP(dst="127.0.0.1")
    match_payload = lambda crit, io_data: (TCP in io_data) and (len(io_data[TCP].payload) > 0)
    sent_data_criteria = AtkIOScapyCriteria(io_type=io_connection.io_type_output, 
                                            match_io_data=lambda crit, io_data: match_payload(crit, io_data) and io_data[TCP].dport == DEFAULT_MIRROR_TCP_PORT)
    received_data_criteria = AtkIOScapyCriteria(io_type=io_connection.io_type_input, 
                                                match_io_data=lambda crit, io_data: match_payload(crit, io_data) and io_data[TCP].sport == DEFAULT_MIRROR_TCP_PORT)
   
    test_cleaner.io_connection = io_connection
    dump_failed.io_logs = io_logs
    io_connection.open()

    ack = io_connection.open_tcp("127.0.0.1", DEFAULT_MIRROR_TCP_PORT, SRC_PORT)
    tcp = TCP(sport=SRC_PORT, dport=DEFAULT_MIRROR_TCP_PORT, flags="PA", seq=ack.seq, ack=ack.ack)

    for i in range(0, 3):
        payload = "data" + str(i)

        #UnitTestComment("AtkIOConnection::write():test payload emission on TCP")
        sent_data_criteria.from_now(500)
        received_data_criteria.from_now(500)
        io_connection.send(ip/tcp/payload)
        sent = io_logs.check(sent_data_criteria)
        assert str(sent.data[TCP].payload) == payload

        #UnitTestComment("AtkIOConnection::write():test payload reception on TCP")
        sleep(0.1)
        received = io_logs.check(received_data_criteria).data[TCP]
        assert str(received.payload) == payload
        ack = ip/TCP(sport=SRC_PORT, dport=DEFAULT_MIRROR_TCP_PORT, flags="A", seq=received.ack, ack=received.seq + len(received.payload))
        io_connection.send(ack)
        tcp.seq = ack.seq
        tcp.ack = ack.ack


@mark.sudo
def test_UT_AtkIOConnectionScapy_WriteIP(trace_test_start, test_cleaner, dump_failed, mirroring_server):
    pass
