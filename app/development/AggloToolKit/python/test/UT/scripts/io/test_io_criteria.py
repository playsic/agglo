############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time
from collections import Counter

import pytest

from agglo_tk.io import AtkIOCriteria
from agglo_tk.io import AtkIO
from agglo_tk.io import AtkIOLogs
from agglo_tk.io import AtkIOConnectionLogs

from agglo_tk.exceptions import AtkSelectorError
from agglo_tk.exceptions import AtkIOSelectorError

from agglo_tk.utils.test.stubs import AtkStubIOLogs

from agglo_tt.fixtures import trace_test_start


def test_UT_AtkIOCriteria_Constructor(trace_test_start):
    IO_TYPE1 = 1

    #UnitTestComment("AtkIOCriteria: construct instance with default parameters")
    io_criteria = AtkIOCriteria()
    assert io_criteria.io_type is None
    assert io_criteria.first_rank is None
    assert io_criteria.last_rank is None
    assert io_criteria.start_time is None
    assert io_criteria.end_time is None

    #UnitTestComment("AtkIOCriteria: construct instance with parameters set")
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, first_rank=1 , last_rank=5, \
                                start_time=102.5, end_time=107.891)
    assert io_criteria.io_type == IO_TYPE1
    assert io_criteria.first_rank == 1
    assert io_criteria.last_rank == 5
    assert io_criteria.start_time == 102.5
    assert io_criteria.end_time == 107.891


def test_UT_AtkIOCriteria_Properties(trace_test_start):
    IO_TYPE1 = 1
    IO_TYPE2 = 2
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1)

    #UnitTestComment("AtkIOCriteria: set parameters")
    io_criteria.io_type = IO_TYPE2
    assert io_criteria.io_type == IO_TYPE2
    io_criteria.first_rank = 4
    assert io_criteria.first_rank == 4
    io_criteria.last_rank = 5
    assert io_criteria.last_rank == 5
    io_criteria.start_time = 2
    assert io_criteria.start_time == 2
    io_criteria.end_time = 12
    assert io_criteria.end_time == 12


def test_UT_AtkIOCriteria_MatchErrorCases(trace_test_start):
    io = AtkIO("123 test", 0)

    #UnitTestComment("AtkIOCriteria:match on negative ranks without specifying nb_ios")
    io_criteria = AtkIOCriteria(first_rank=-1)
    with pytest.raises(AtkIOSelectorError):
        io_criteria.match(io)
    io_criteria = AtkIOCriteria(last_rank=-2)
    with pytest.raises(AtkIOSelectorError):
        io_criteria.match(io)
    io_criteria = AtkIOCriteria(first_rank=-2, last_rank=-1)
    with pytest.raises(AtkIOSelectorError):
        io_criteria.match(io)
    
 
def test_UT_AtkIOCriteria_Match(trace_test_start):
    IO_TYPE1 = 1
    IO_TYPE2 = 2

    start_time = time.time()
    time.sleep(0.001)
    RANK_IO1 = 7
    io1 = AtkIO("123 test", RANK_IO1)
    time.sleep(0.001)
    end_time1 = time.time()
    time.sleep(0.001)
    # TODO changer en buffer binaire
    MIDDLE_RANK = 8
    RANK_IO2 = 9
    io2 = AtkIO("12 test", RANK_IO2)
    time.sleep(0.001)
    end_time2 = time.time()

    #UnitTestComment("AtkIOCriteria:matching without specifiers")
    io_criteria = AtkIOCriteria()
    assert io_criteria.match(io1)
    assert io_criteria.match(io2)

    #UnitTestComment("AtkIOCriteria:matching io_type specifiers")
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1)
    assert io_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_criteria.match(io2, io_type=IO_TYPE2)
    
    #UnitTestComment("AtkIOCriteria: matching with rank specifiers")
    io_criteria = AtkIOCriteria(first_rank=RANK_IO1 - 1, last_rank=RANK_IO2 + 1)
    assert io_criteria.match(io1)
    assert io_criteria.match(io2)
    io_criteria = AtkIOCriteria(first_rank=RANK_IO1 - 1, last_rank=MIDDLE_RANK)
    assert io_criteria.match(io1)
    assert not io_criteria.match(io2)
    io_criteria = AtkIOCriteria(first_rank=MIDDLE_RANK, last_rank=RANK_IO2 + 1)
    assert not io_criteria.match(io1)
    assert io_criteria.match(io2)
    io_criteria = AtkIOCriteria(first_rank=RANK_IO2 + 1)
    assert not io_criteria.match(io1)
    assert not io_criteria.match(io2)
    io_criteria = AtkIOCriteria(first_rank=RANK_IO2 + 1, last_rank=14)
    assert not io_criteria.match(io1)
    assert not io_criteria.match(io2)
    io_criteria = AtkIOCriteria(first_rank=1, last_rank=RANK_IO1 - 1)
    assert not io_criteria.match(io1)
    assert not io_criteria.match(io2)
    
    #UnitTestComment("AtkIOCriteria: matching with negative rank specifiers")
    io_criteria = AtkIOCriteria(first_rank=-1)
    assert io_criteria.match(io1, nb_ios=RANK_IO1 + 1)
    io_criteria = AtkIOCriteria(last_rank=-1)
    assert io_criteria.match(io2, nb_ios=RANK_IO2 + 1)
    io_criteria = AtkIOCriteria(first_rank=-3, last_rank=-2)
    assert not io_criteria.match(io1, nb_ios=RANK_IO2 + 2)
    assert io_criteria.match(io2, nb_ios=RANK_IO2 + 2)
    
    #UnitTestComment("AtkIOCriteria: matching with time specifiers")
    io_criteria = AtkIOCriteria(start_time=start_time, end_time=end_time2)
    assert io_criteria.match(io1)
    assert io_criteria.match(io2)
    io_criteria = AtkIOCriteria(start_time=start_time, end_time=end_time1)
    assert io_criteria.match(io1)
    assert not io_criteria.match(io2)
    io_criteria = AtkIOCriteria(start_time=end_time1, end_time=end_time2)
    assert not io_criteria.match(io1)
    assert io_criteria.match(io2)
    io_criteria = AtkIOCriteria(start_time=end_time2)
    assert not io_criteria.match(io1)
    assert not io_criteria.match(io2)
    io_criteria = AtkIOCriteria(start_time=end_time2, end_time=end_time2 + 10)
    assert not io_criteria.match(io1)
    assert not io_criteria.match(io2)
    io_criteria = AtkIOCriteria(start_time=start_time - 10, end_time=start_time)
    assert not io_criteria.match(io1)
    assert not io_criteria.match(io2)
    
    #UnitTestComment("AtkIOCriteria: matching with io_type, rank and time specifiers")
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=MIDDLE_RANK, last_rank=RANK_IO2 + 1, \
                                start_time=start_time, end_time=end_time2)
    assert not io_criteria.match(io1, io_type=IO_TYPE2)
    assert not io_criteria.match(io1, io_type=IO_TYPE1)
    assert io_criteria.match(io2, io_type=IO_TYPE1)
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=MIDDLE_RANK, last_rank=RANK_IO2 + 1, \
                                start_time=start_time, end_time=end_time1)
    assert not io_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_criteria.match(io2, io_type=IO_TYPE1)
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=RANK_IO1 - 1, last_rank=MIDDLE_RANK, \
                                start_time=end_time1, end_time=end_time2)
    assert not io_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_criteria.match(io2, io_type=IO_TYPE1)
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=RANK_IO1 - 1, last_rank=RANK_IO2 + 1, \
                                start_time=end_time2)
    assert not io_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_criteria.match(io2, io_type=IO_TYPE1)
    io_criteria = AtkIOCriteria(io_type=IO_TYPE1, 
                                first_rank=RANK_IO2 + 1, last_rank=14, \
                                start_time=start_time, end_time=end_time2)
    assert not io_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_criteria.match(io2, io_type=IO_TYPE1)

    
def test_UT_AtkIOCriteria_FromCurrentTail(trace_test_start):
    RANK_IO1 = 5
    RANK_IO2 = 10
    RANK_IO3 = 15
    io_logs = AtkStubIOLogs()
    io_logs.set_length(RANK_IO2)
    io1 = AtkIO("123 test", RANK_IO1)
    io2 = AtkIO("12 test", RANK_IO2)
    io3 = AtkIO("1 test", RANK_IO3)
    io_criteria1 = AtkIOCriteria().from_current_tail(io_logs)
    io_criteria2 = AtkIOCriteria().from_current_tail(io_logs, (RANK_IO3 - 1) - RANK_IO2)

    #UnitTestComment("AtkIOCriteria: match entries from last received io")
    assert not io_criteria1.match(io1)
    assert io_criteria1.match(io2)
    assert io_criteria1.match(io3)
    
    #UnitTestComment("AtkIOCriteria: match any entries from last received io, with a rank span for end value")
    assert not io_criteria2.match(io1)
    assert io_criteria2.match(io2)
    assert not io_criteria2.match(io3)


def test_UT_AtkIOCriteria_UntilCurrentTail(trace_test_start):
    RANK_IO1 = 5
    RANK_IO2 = 10
    RANK_IO3 = 15
    io_logs = AtkStubIOLogs()
    io_logs.set_length(RANK_IO2)
    io1 = AtkIO("123 test", RANK_IO1)
    io2 = AtkIO("123 test", RANK_IO2)
    io3 = AtkIO("123 test", RANK_IO3)
    io_criteria1 = AtkIOCriteria().until_current_tail(io_logs)
    io_criteria2 = AtkIOCriteria().until_current_tail(io_logs, RANK_IO2 - (RANK_IO1 + 1))

    #UnitTestComment("AtkIOCriteria: match any entries until last received io")
    assert io_criteria1.match(io1)
    assert io_criteria1.match(io2)
    assert not io_criteria1.match(io3)
    
    #UnitTestComment("AtkIOCriteria: match entries until last received io, with a rank span for start value")
    assert not io_criteria2.match(io1)
    assert io_criteria2.match(io2)
    assert not io_criteria2.match(io3)

    
def test_UT_AtkIOCriteria_FromNow(trace_test_start):
    RANK_IO1 = 1
    io1 = AtkIO("123 test", RANK_IO1)
    time.sleep(0.001)
    io_criteria1 = AtkIOCriteria().from_now()
    io_criteria2 = AtkIOCriteria().from_now(200)
    io2 = AtkIO("12 test", RANK_IO1 + 1)
    time.sleep(0.5)
    io3 = AtkIO("1 test", RANK_IO1 + 2)
    time.sleep(0.001)

    #UnitTestComment("AtkIOCriteria: match entries from this moment")
    assert not io_criteria1.match(io1)
    assert io_criteria1.match(io2)
    assert io_criteria1.match(io3)
    
    #UnitTestComment("AtkIOCriteria: match any entries from this moment, with a time span for end value")
    assert not io_criteria2.match(io1)
    assert io_criteria2.match(io2)
    assert not io_criteria2.match(io3)


def test_UT_AtkIOCriteria_UntilNow(trace_test_start):
    RANK_IO1 = 1
    io1 = AtkIO("123 test", RANK_IO1)
    time.sleep(0.5)
    io2 = AtkIO("12 test", RANK_IO1 + 1)
    io_criteria1 = AtkIOCriteria().until_now()
    io_criteria2 = AtkIOCriteria().until_now(200)
    time.sleep(0.001)
    io3 = AtkIO("1 test", RANK_IO1 + 2)
    time.sleep(0.001)

    #UnitTestComment("AtkIOCriteria: match any entries until this moment")
    assert io_criteria1.match(io1)
    assert io_criteria1.match(io2)
    assert not io_criteria1.match(io3)
    
    #UnitTestComment("AtkIOCriteria: match entries until this moment, with a time span for start value")
    assert not io_criteria2.match(io1)
    assert io_criteria2.match(io2)
    assert not io_criteria2.match(io3)


# TODO    
def test_UT_AtkIOCriteria_Rank(trace_test_start):
    pass
    
# TODO    
def test_UT_AtkIOCriteria_Next(trace_test_start):
    pass
    
# TODO    
def test_UT_AtkIOCriteria_Prev(trace_test_start):
    pass
