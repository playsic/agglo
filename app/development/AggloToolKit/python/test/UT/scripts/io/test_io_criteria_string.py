############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

import pytest

from agglo_tk.io import AtkIOStringCriteria
from agglo_tk.io import AtkIO
from agglo_tk.io import AtkIOLogs

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(6)
def test_UT_AtkIOStringCriteria_Constructor(trace_test_start):
    IO_TYPE1 = 1

    #UnitTestComment("AtkIOStringCriteria: construct instance with default parameters")
    io_string_criteria = AtkIOStringCriteria()
    assert io_string_criteria.io_type is None
    assert io_string_criteria.reg_expr is None
    assert io_string_criteria.first_rank is None
    assert io_string_criteria.last_rank is None
    assert io_string_criteria.start_time is None
    assert io_string_criteria.end_time is None

    #UnitTestComment("AtkIOStringCriteria: construct instance with parameters set")
    io_string_criteria = AtkIOStringCriteria(io_type=IO_TYPE1, reg_expr="test.*", 
                                             first_rank=1, last_rank=5, 
                                             start_time=102.5, end_time=107.891)
    assert io_string_criteria.io_type == IO_TYPE1
    assert io_string_criteria.reg_expr == "test.*"
    assert io_string_criteria.first_rank == 1
    assert io_string_criteria.last_rank == 5
    assert io_string_criteria.start_time == 102.5
    assert io_string_criteria.end_time == 107.891


def test_UT_AtkIOStringCriteria_Properties(trace_test_start):
    IO_TYPE1 = 1
    IO_TYPE2 = 2
    
    #UnitTestComment("AtkIOStringCriteria: set parameters")
    io_string_criteria = AtkIOStringCriteria(io_type=IO_TYPE1)
    io_string_criteria.io_type = IO_TYPE2
    assert io_string_criteria.io_type == IO_TYPE2
    io_string_criteria.reg_expr = "test.*2"
    assert io_string_criteria.reg_expr == "test.*2"    
 

def test_UT_AtkIOStringCriteria_Match(trace_test_start):
    IO_TYPE1 = 1
    start_time = time.time()
    time.sleep(0.001)
    RANK_IO1 = 7
    io1 = AtkIO("123 test", RANK_IO1)
    time.sleep(0.001)
    end_time1 = time.time()
    time.sleep(0.001)
    # TODO changer en buffer binaire
    MIDDLE_RANK = 8
    RANK_IO2 = 9
    io2 = AtkIO("12 test", RANK_IO2)
    time.sleep(0.001)
    end_time2 = time.time()

    #UnitTestComment("AtkIOStringCriteria: matching without specifiers")
    io_string_criteria = AtkIOStringCriteria()
    assert io_string_criteria.match(io1, io_type=IO_TYPE1)
    assert io_string_criteria.match(io2, io_type=IO_TYPE1)
    
    #UnitTestComment("AtkIOStringCriteria: matching with regular expression specifiers")
    io_string_criteria = AtkIOStringCriteria(io_type=IO_TYPE1, reg_expr="[1-9]{2,3} [a-z]*")
    assert io_string_criteria.match(io1, io_type=IO_TYPE1)
    assert io_string_criteria.match(io2, io_type=IO_TYPE1)
    io_string_criteria.reg_expr = "[1-9]{3} [a-z]*"
    assert io_string_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_string_criteria.match(io2, io_type=IO_TYPE1)
    
    #UnitTestComment("AtkIOStringCriteria: matching with regular expressions, rank and time specifiers")
    io_string_criteria = AtkIOStringCriteria(io_type=IO_TYPE1, reg_expr="[1-9]{3} [a-z]*", 
                                                 first_rank=MIDDLE_RANK, last_rank=RANK_IO2 + 1, 
                                                 start_time=start_time, end_time=end_time2)
    assert not io_string_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_string_criteria.match(io2, io_type=IO_TYPE1)
    io_string_criteria = AtkIOStringCriteria(io_type=IO_TYPE1, reg_expr="[1-9]{2,3} [a-z]*", 
                                                 first_rank=MIDDLE_RANK, last_rank=RANK_IO2 + 1, 
                                                 start_time=start_time, end_time=end_time1)
    assert not io_string_criteria.match(io1, io_type=IO_TYPE1)
    assert not io_string_criteria.match(io2, io_type=IO_TYPE1)
