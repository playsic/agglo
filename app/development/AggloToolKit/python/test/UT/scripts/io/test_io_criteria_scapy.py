############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import time

import pytest

from scapy.all import IP
from scapy.all import TCP
from scapy.all import ICMP
from scapy.all import UDP

from agglo_tk.io.scapy import AtkIOScapyCriteria
from agglo_tk.io import AtkIO
from agglo_tk.io import AtkIOLogs

from agglo_tt.fixtures import trace_test_start


@pytest.suite_rank(7)
def test_UT_AtkIOScapyCriteria_Constructor(trace_test_start):
    IO_TYPE1 = 1

    #UnitTestComment("AtkIOScapyCriteria: construct instance with default parameters")
    io_scapy_criteria = AtkIOScapyCriteria()
    assert io_scapy_criteria.io_type is None
    assert io_scapy_criteria.first_rank is None
    assert io_scapy_criteria.last_rank is None
    assert io_scapy_criteria.start_time is None
    assert io_scapy_criteria.end_time is None

    #UnitTestComment("AtkIOScapyCriteria: construct instance with parameters set")
    io_scapy_criteria = AtkIOScapyCriteria(io_type=IO_TYPE1, reg_expr="test.*", 
                                           first_rank=1, last_rank=5, 
                                           start_time=102.5, end_time=107.891,
                                           protocol=IP)
    assert io_scapy_criteria.io_type == IO_TYPE1
    assert io_scapy_criteria.first_rank == 1
    assert io_scapy_criteria.last_rank == 5
    assert io_scapy_criteria.start_time == 102.5
    assert io_scapy_criteria.end_time == 107.891
    assert io_scapy_criteria.protocol is IP


def test_UT_AtkIOScapyCriteria_Accessor(trace_test_start):
    IO_TYPE1 = 1
    IO_TYPE2 = 2
    
    #UnitTestComment("AtkIOScapyCriteria: set parameters")
    io_scapy_criteria = AtkIOScapyCriteria(io_type=IO_TYPE1, protocol=IP)
    io_scapy_criteria.io_type = IO_TYPE2
    assert io_scapy_criteria.io_type == IO_TYPE2
    io_scapy_criteria.protocol = TCP
    assert io_scapy_criteria.protocol is TCP


def test_UT_AtkIOScapyCriteria_MatchField(trace_test_start):
    #UnitTestComment("AtkIOScapyCriteria::match_field(): match with protocol, field and value passed as parameters)
    io_scapy_criteria = AtkIOScapyCriteria()
    assert io_scapy_criteria.match_field(IP(len=4), IP, "len", 4)
    assert not io_scapy_criteria.match_field(IP(len=4), TCP, "len", 4)
    assert not io_scapy_criteria.match_field(IP(len=4), IP, "len", 5)

    #UnitTestComment("AtkIOScapyCriteria::match_field(): match with field and value passed as kwarg)
    assert io_scapy_criteria.match_field(IP(len=4), IP, len=4)
    assert not io_scapy_criteria.match_field(IP(len=4), IP, len=5)

    #UnitTestComment("AtkIOScapyCriteria::match_field(): match with protocol as a boundary)
    io_scapy_criteria = AtkIOScapyCriteria(protocol=IP)
    assert io_scapy_criteria.match_field(IP(len=4), "protocol", len=4)
    io_scapy_criteria.protocol = TCP
    assert not io_scapy_criteria.match_field(IP(len=4), "protocol", len=4)
    
    #UnitTestComment("AtkIOScapyCriteria::match_field(): test match with field/value as a boundary)
    io_scapy_criteria = AtkIOScapyCriteria(dst="127.0.0.1")
    assert io_scapy_criteria.match_field(IP(dst="127.0.0.1"), IP, "dst")
    assert not io_scapy_criteria.match_field(IP(dst="127.0.0.2"), IP, "dst")

    #UnitTestComment("AtkIOScapyCriteria::match_field(): test match with matched field as a boundary)
    io_scapy_criteria = AtkIOScapyCriteria(matched_field="len")
    assert io_scapy_criteria.match_field(IP(len=4, dst="127.0.01"), IP, "matched_field", 4)
    assert not io_scapy_criteria.match_field(IP(len=4, dst="127.0.0.1"), IP, "matched_field", 5)
    io_scapy_criteria.matched_field = "dst"
    assert not io_scapy_criteria.match_field(IP(len=4, dst="127.0.0.1"), IP, "matched_field", 4)
    assert io_scapy_criteria.match_field(IP(len=4, dst="127.0.0.1"), IP, "matched_field", "127.0.0.1")
    assert not io_scapy_criteria.match_field(IP(len=4, dst="127.0.0.1"), IP, "matched_field", "127.0.0.2")

    #UnitTestComment("AtkIOScapyCriteria::match_field(): test match with expected value as a boundary)
    io_scapy_criteria = AtkIOScapyCriteria(expected_len=4)
    assert io_scapy_criteria.match_field(IP(len=4), IP, "len", "expected_len")
    assert not io_scapy_criteria.match_field(IP(len=5), IP, "len", "expected_len")
    io_scapy_criteria.expected_len = 5
    assert not io_scapy_criteria.match_field(IP(len=4), IP, "len", "expected_len")
    assert io_scapy_criteria.match_field(IP(len=5), IP, "len", "expected_len")

    #UnitTestComment("AtkIOScapyCriteria::match_field(): test match with protocol, matched field and expected value as boundaries)
    io_scapy_criteria = AtkIOScapyCriteria(protocol=IP, matched_field="len", expected_value=4)
    assert io_scapy_criteria.match_field(IP(len=4)/TCP(sport=22), "protocol", "matched_field", "expected_value")
    assert not io_scapy_criteria.match_field(IP(len=5)/TCP(sport=23), "protocol", "matched_field", "expected_value")
    io_scapy_criteria.protocol = TCP
    io_scapy_criteria.matched_field = "sport"
    io_scapy_criteria.expected_value = 23
    assert not io_scapy_criteria.match_field(IP(len=4)/TCP(sport=22), "protocol", "matched_field", "expected_value")
    assert io_scapy_criteria.match_field(IP(len=5)/TCP(sport=23), "protocol", "matched_field", "expected_value")

    #UnitTestComment("AtkIOScapyCriteria::match_field(): test that several boundaries can be used)
    io_scapy_criteria = AtkIOScapyCriteria(protocol1=TCP, protocol2=TCP, matched_field="len", expected_value=4)
    with pytest.raises(AttributeError):
        io_scapy_criteria.match_field(IP(len=5)/TCP(sport=22), "protocol1", "matched_field", "expected_value")
    io_scapy_criteria.protocol1 = IP
    assert io_scapy_criteria.match_field(IP(len=4)/TCP(sport=22), "protocol1", "matched_field", "expected_value")
    assert not io_scapy_criteria.match_field(IP(len=5)/TCP(sport=23), "protocol1", "matched_field", "expected_value")
    io_scapy_criteria.matched_field = "sport"
    io_scapy_criteria.expected_value = 23
    assert not io_scapy_criteria.match_field(IP(len=4)/TCP(sport=22), "protocol2", "matched_field", "expected_value")
    assert io_scapy_criteria.match_field(IP(len=5)/TCP(sport=23), "protocol2", "matched_field", "expected_value")


def test_UT_AtkIOScapyCriteria_Match(trace_test_start):
    match_protocols=lambda crit, io_data: any([protocol in io_data for protocol in crit.protocols])

    IO_TYPE1 = 1
    start_time = time.time()
    time.sleep(0.001)
    RANK_IO_TCP = 7
    io_tcp = AtkIO(IP()/TCP(), RANK_IO_TCP)
    time.sleep(0.001)
    end_time1 = time.time()
    time.sleep(0.001)
    MIDDLE_RANK = 8
    RANK_IO_ICMP = 9
    io_icmp = AtkIO(IP()/ICMP(), RANK_IO_ICMP)
    time.sleep(0.001)
    end_time2 = time.time()

    #UnitTestComment("AtkIOScapyCriteria: matching without specifiers")
    io_scapy_criteria = AtkIOScapyCriteria()
    assert io_scapy_criteria.match(io_tcp, io_type=IO_TYPE1)
    assert io_scapy_criteria.match(io_icmp, io_type=IO_TYPE1)
    
    #UnitTestComment("AtkIOScapyCriteria: matching with scapy protocol specifiers")
    io_scapy_criteria = AtkIOScapyCriteria(io_type=IO_TYPE1, protocols=[TCP], 
                                           match_io_data=match_protocols)
    assert io_scapy_criteria.match(io_tcp, io_type=IO_TYPE1)
    assert not io_scapy_criteria.match(io_icmp, io_type=IO_TYPE1)
    io_scapy_criteria.protocols.append(ICMP)
    assert io_scapy_criteria.match(io_tcp, io_type=IO_TYPE1)
    assert io_scapy_criteria.match(io_icmp, io_type=IO_TYPE1)
    
    #UnitTestComment("AtkIOScapyCriteria: matching with scapy protocol, rank and time specifiers")
    io_scapy_criteria = AtkIOScapyCriteria(first_rank=MIDDLE_RANK, last_rank=RANK_IO_ICMP + 1, 
                                           start_time=start_time, end_time=end_time2,
                                           io_type=IO_TYPE1, protocols=[TCP], 
                                           match_io_data=match_protocols)
    assert not io_scapy_criteria.match(io_tcp, io_type=IO_TYPE1)
    assert not io_scapy_criteria.match(io_icmp, io_type=IO_TYPE1)
    io_scapy_criteria = AtkIOScapyCriteria(first_rank=MIDDLE_RANK, last_rank=RANK_IO_ICMP + 1, 
                                           start_time=start_time, end_time=end_time1, 
                                           io_type=IO_TYPE1, protocols=[UDP], 
                                           match_io_data=match_protocols)
    assert not io_scapy_criteria.match(io_tcp, io_type=IO_TYPE1)
    assert not io_scapy_criteria.match(io_icmp, io_type=IO_TYPE1)
