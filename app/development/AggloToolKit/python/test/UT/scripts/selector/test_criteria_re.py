############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.selector import AtkCriteriaRe

from agglo_tt.fixtures import trace_test_start



class String(object):
    def __init__(self, string):
        self.str = string


def test_UT_AtkCriteriaRe_Constructor(trace_test_start):

    #UnitTestComment("AtkCriteriaRe::AtkCriteriaRe(): test constructor with defaults arguments)
    criteria = AtkCriteriaRe(reg_expr=".*")
    assert criteria.boundary == ".*"
    assert criteria.reg_expr == ".*"
    assert not criteria.wildcard

    #UnitTestComment("False, AtkCriteriaRe::AtkCriteriaRe(): test constructor with specific arguments)
    criteria = AtkCriteriaRe(reg_expr=".*", wildcard=True)
    assert criteria.reg_expr == ".*"
    assert criteria.wildcard


def test_UT_AtkCriteriaRe_Property(trace_test_start):
    #UnitTestComment("AtkCriteriaRe::boundary: test wildcard setter property)
    criteria = AtkCriteriaRe(reg_expr=".+")
    assert not criteria.wildcard
    criteria.wildcard = True
    assert criteria.wildcard


def test_UT_AtkCriteriaRe_Match(trace_test_start):
    #UnitTestComment("AtkCriteriaRe::match(): match with regular expression)
    criteria = AtkCriteriaRe(reg_expr="Test-[0-9]{1,2}-(end)*")
    assert criteria.match("Test-2-endend")
    assert criteria.match("Test-23-endend")
    assert not criteria.match("Test-23-enden")
    
    #UnitTestComment("AtkCriteriaRe::match(): test regular expression match after boundary change)
    criteria.reg_expr = "Test-[0-9]{1,2}-(end)*en"
    assert criteria.match("Test-23-enden")

    #UnitTestComment("AtkCriteriaRe::match(): match with wildcard)
    criteria.reg_expr = "Test*.exe"
    assert criteria.match("Tes4exe")
    assert not criteria.match("Test23.exe")
    criteria.wildcard = True
    assert not criteria.match("Tes4exe")
    assert criteria.match("Test23.exe")

    #UnitTestComment("AtkCriteriaRe::match(): match with data extractor)
    with pytest.raises(TypeError):
        criteria.match(String("Tes4exe"))
    criteria.change_behavior(extract_data=lambda crit, data:data.str)
    assert criteria.match(String("Test23.exe"))
    assert not criteria.match(String("Tes4exe.exe"))
