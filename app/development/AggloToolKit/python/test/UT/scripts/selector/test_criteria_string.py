############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.selector import AtkCriteriaString


class String(object):
    def __init__(self, string):
        self.str = string


def test_UT_AtkCriteriaString_Constructor():
    #UnitTestComment("AtkCriteriaString::AtkCriteriaString(): test default constructor)
    criteria = AtkCriteriaString(str="that is a string")
    assert criteria.boundary == "that is a string"
    assert criteria.str == "that is a string"
    assert criteria.match_case == True
    assert criteria.begin == 0
    assert criteria.end == None
    assert criteria.reverse_search == False
    assert criteria.exact_string == False


def test_UT_AtkCriteriaString_Match():
    #UnitTestComment("AtkCriteriaString::match(): match exact string)
    criteria = AtkCriteriaString(string="that is a string")
    assert criteria.match("that is a string")
    assert criteria.match("that is")
    criteria.exact_string = True
    assert criteria.match("that is a string")
    assert not criteria.match("that is")
    criteria.exact_string = False

    #UnitTestComment("AtkCriteriaString::match(): match case)
    assert not criteria.match("THAT IS A STRING")
    criteria.match_case = False
    assert criteria.match("THAT IS A STRING")
    criteria.match_case = True

    #UnitTestComment("AtkCriteriaString::match(): change start search index)
    assert criteria.match("is a string")
    criteria.begin = 5
    assert criteria.match("is a string")
    criteria.begin = 6
    assert not criteria.match("is a string")
    criteria.begin = 0

    #UnitTestComment("AtkCriteriaString::match(): change end search index)
    assert criteria.match("is")
    criteria.end = 7
    assert criteria.match("is")
    criteria.end = 6
    assert not criteria.match("is")
    criteria.end = None

    #UnitTestComment("AtkCriteriaString::match(): search boundary into data instead of data into boundary
    assert not criteria.match("and that is a string long")
    criteria.reverse_search = True
    assert criteria.match("and that is a string long")

    #UnitTestComment("AtkCriteriaString::match(): combine reverse search and begin property
    criteria.begin = 4
    assert criteria.match("and that is a string long")
    criteria.begin = 5
    assert not criteria.match("and that is a string long")
    criteria.begin = 0

    #UnitTestComment("AtkCriteriaString::match(): combine reverse search and end property
    criteria.end = 20
    assert criteria.match("and that is a string long")
    criteria.end = 19
    assert not criteria.match("and that is a string long")
    criteria.end = None
    criteria.reverse_search = False

    #UnitTestComment("AtkCriteriaString::match(): match with data extractor)
    with pytest.raises(TypeError):
        criteria.match(String("that is a string"))
    criteria.change_behavior(extract_data=lambda crit, data:data.str)
    assert criteria.match(String("that is a string"))
    assert not criteria.match(String("and that is a string"))
