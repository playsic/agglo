############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.selector import AtkCriteria


class Temperature(object):
    def __init__(self, temperature):
        self.temp = temperature


def test_UT_AtkCriteria_Constructor():

    #UnitTestComment("AtkCriteria::AtkCriteria(): test default constructor)
    criteria = AtkCriteria(temperature=10)
    assert criteria.boundary == 10
    assert criteria.temperature == 10
    assert len(criteria.boundaries) == 1
    assert criteria.boundaries["temperature"] == 10
    assert criteria.match(10)
    assert not criteria.match(11)

    #UnitTestComment("AtkCriteria::AtkCriteria(): test constructor with optional parameters)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, data : False, extract_data=lambda crit, data : data)
    assert criteria.boundary == 10
    assert criteria.temperature_min == 10
    assert criteria.boundaries["temperature_min"] == 10
    assert not criteria.match(10)


def test_UT_AtkCriteria_Properties():
    criteria = AtkCriteria(path="/var")

    #UnitTestComment("AtkCriteria::boundary: check that boundary modification also modifies path attribute)
    criteria.boundary = "/var/bin"
    assert criteria.path == "/var/bin"

    #UnitTestComment("AtkCriteria::boundary: check that path attribute modification also modifies boundary)
    criteria.path = "/var"
    assert criteria.boundary == "/var"


def test_UT_AtkCriteria_Match():
    #UnitTestComment("AtkCriteria::match(): check that match return result of match method given as parameter)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, data : True)
    assert criteria.match(5)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, data : False)
    assert not criteria.match(5)

    #UnitTestComment("AtkCriteria::match(): check that match can check inside the object passed as parameter)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, data : data > crit.temperature_min, 
                           extract_data=lambda crit, data : data.temp)
    assert criteria.match(Temperature(15))
    assert not criteria.match(Temperature(5))
    
    #UnitTestComment("AtkCriteria::match(): check that criteria match after boundary change)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, data : data > crit.temperature_min)
    assert criteria.match(15)
    criteria.temperature_min = 20
    assert not criteria.match(15)
    criteria.boundary = 10
    assert criteria.match(15)
