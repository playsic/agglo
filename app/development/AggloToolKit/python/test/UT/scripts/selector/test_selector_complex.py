############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.tree.tree_bool import *

from agglo_tk.selector import AtkComplexSelector
from agglo_tk.selector import AtkCriteria
from agglo_tk.exceptions import AtkUndecidedError


class PressureVolume(object):
    def __init__(self, pressure, volume):
        self.pressure = pressure
        self.volume = volume
class Temperature(object):
    def __init__(self, temperature):
        self.temperature = temperature
class Pressure(object):
    def __init__(self, pressure):
        self.pressure = pressure

TEMP_MIN = AtkCriteria(temperature_min=10, match=lambda crit, data: data >= crit.temperature_min, 
                       extract_data=lambda crit, data: data.temperature)
TEMP_MAX = AtkCriteria(temperature_max=20, match=lambda crit, data: data < crit.temperature_max, 
                       extract_data=lambda crit, data: data.temperature)
PRESSURE_MIN = AtkCriteria(pressure_min=30, match=lambda crit, data: data >= crit.pressure_min, 
                           extract_data=lambda crit, data: data.pressure)
PRESSURE_MAX = AtkCriteria(pressure_max=40, match=lambda crit, data: data < crit.pressure_max, 
                           extract_data=lambda crit, data: data.pressure)
VOLUME_MAX = AtkCriteria(volume_max=50, match=lambda crit, data: data < crit.volume_max, 
                         extract_data=lambda crit, data: data.volume)

LOW_TEMP = 5
GOOD_TEMP = 15
HIGH_TEMP = 25
LOW_PRESSURE = 25
GOOD_PRESSURE = 35
HIGH_PRESSURE = 45
GOOD_VOLUME = 45
BAD_VOLUME = 55

        
def test_UT_AtkComplexSelector_Constructor():
    #UnitTestComment("AtkComplexSelector::AtkComplexSelector(): test default constructor)
    selector = AtkComplexSelector()
    assert selector.criteria is None

    #UnitTestComment("AtkComplexSelector::AtkComplexSelector(): test constructor with a criteria parameter)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, _: True)
    selector = AtkComplexSelector(criteria)
    assert selector.criteria is not None
    assert selector["temperature_min"][0] is criteria

    #UnitTestComment("AtkComplexSelector::AtkComplexSelector(): test constructor with a tree node as criteria)
    selector = AtkComplexSelector(Not(criteria))
    assert selector["temperature_min"][0] is criteria


def test_UT_AtkComplexSelector_Properties():
    criteria1 = AtkCriteria(temperature_min=10)
    selector = AtkComplexSelector(criteria1)

    #UnitTestComment("AtkComplexSelector::criteria:test criteria property)
    assert selector.criteria.value is criteria1
    criteria2 = AtkCriteria(temperature_max=20)
    selector.criteria = criteria2
    assert selector.criteria.value is criteria2


def test_UT_AtkComplexSelector_BuiltIn():
    criteria1 = AtkCriteria(temperature_min=10)
    criteria2 = AtkCriteria(temperature_max=20)
    criteria3 = AtkCriteria(pressure_min=30)
    selector = AtkComplexSelector(And(Not(criteria1), Or(criteria2, criteria3)))

    #UnitTestComment("AtkComplexSelector::criteria:test get item)
    assert selector["temperature_min"][0] is criteria1
    assert selector["temperature_max"][0] is criteria2
    assert selector["pressure_min"][0] is criteria3

    #UnitTestComment("AtkComplexSelector::criteria:test get attributes)
    assert selector.temperature_min == 10
    assert selector.temperature_max == 20
    criteria1.temperature_min = 15
    assert selector.temperature_min == 15

    #UnitTestComment("AtkComplexSelector::criteria:test set attributes)
    selector.pressure_min = 10
    assert criteria3.pressure_min == 10
    

def test_UT_AtkComplexSelector_Match():
    #UnitTestComment("AtkComplexSelector::match(): check that match can check with data passed as values)
    selector = AtkComplexSelector(And(PRESSURE_MIN, PRESSURE_MAX, VOLUME_MAX))
    assert selector.match(pressure=GOOD_PRESSURE, volume=GOOD_VOLUME)
    assert not selector.match(pressure=LOW_PRESSURE, volume=GOOD_VOLUME)
    assert not selector.match(pressure=GOOD_PRESSURE, volume=BAD_VOLUME)
    
    #UnitTestComment("AtkComplexSelector::match(): check that match can check inside the object passed as parameter)
    assert selector.match(PressureVolume(GOOD_PRESSURE, GOOD_VOLUME))
    assert not selector.match(PressureVolume(LOW_PRESSURE, GOOD_VOLUME))
    assert not selector.match(PressureVolume(HIGH_PRESSURE, GOOD_VOLUME))

    #UnitTestComment("AtkComplexSelector::match(): check that default data name is criteria name)
    pressure = AtkCriteria(pressure=40, match=lambda crit, data: data <= crit.pressure, 
                           extract_data=lambda crit, data:data.pressure)
    volume = AtkCriteria(volume=50, match=lambda crit, data: data <= crit.volume, 
                         extract_data=lambda crit, data:data.volume)
    selector = AtkComplexSelector(And(pressure, volume))
    assert selector.match(pressure=GOOD_PRESSURE, volume=GOOD_VOLUME)
    assert not selector.match(PressureVolume(HIGH_PRESSURE, GOOD_VOLUME))

    #UnitTestComment("AtkComplexSelector::match(): check match with a complex criteria)
    selector = AtkComplexSelector(And(PRESSURE_MAX, Or(PRESSURE_MAX, PRESSURE_MIN), Not(VOLUME_MAX)))
    assert selector.match(PressureVolume(LOW_PRESSURE, BAD_VOLUME))
    assert not selector.match(PressureVolume(HIGH_PRESSURE, BAD_VOLUME))
    assert not selector.match(PressureVolume(GOOD_PRESSURE, GOOD_VOLUME))

    
def test_UT_AtkComplexSelector_PartialMatch():
    #UnitTestComment("AtkComplexSelector::match(): check that positive match works with partial values)
    selector = AtkComplexSelector(Or(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    assert selector.match(temperature=GOOD_TEMP)
    assert selector.match(pressure=GOOD_PRESSURE, volume=GOOD_VOLUME)
    
    #UnitTestComment("AtkComplexSelector::match(): check that negative match works with partial values)
    selector = AtkComplexSelector(And(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    assert not selector.match(pressure=HIGH_PRESSURE)
    assert not selector.match(pressure=GOOD_PRESSURE, volume=BAD_VOLUME)

    #UnitTestComment("AtkComplexSelector::match(): check that undecided match works with partial values)
    selector = AtkComplexSelector(Or(TEMP_MIN, PRESSURE_MAX, VOLUME_MAX))
    with pytest.raises(AtkUndecidedError):
        selector.match(temperature=LOW_TEMP)
    with pytest.raises(AtkUndecidedError):
        selector.match(pressure=HIGH_PRESSURE, volume=BAD_VOLUME)
    selector = AtkComplexSelector(And(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    with pytest.raises(AtkUndecidedError):
        selector.match(pressure=GOOD_PRESSURE)
    with pytest.raises(AtkUndecidedError):
        selector.match(temperature=GOOD_TEMP, volume=GOOD_VOLUME)
    
    #UnitTestComment("AtkComplexSelector::match(): check that positive match works with an object embedding partial values)
    selector = AtkComplexSelector(Or(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    assert selector.match(Pressure(GOOD_PRESSURE))
    assert selector.match(PressureVolume(GOOD_PRESSURE, GOOD_VOLUME))
    
    #UnitTestComment("AtkComplexSelector::match(): check that negative match works with an object embedding partial values)
    selector = AtkComplexSelector(And(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    assert not selector.match(Temperature(HIGH_TEMP))
    assert not selector.match(PressureVolume(GOOD_PRESSURE, BAD_VOLUME))
    
    #UnitTestComment("AtkComplexSelector::match(): check that undecided match works with an object embedding partial values)
    selector = AtkComplexSelector(Or(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    with pytest.raises(AtkUndecidedError):
        selector.match(Pressure(HIGH_PRESSURE))
    with pytest.raises(AtkUndecidedError):
        selector.match(PressureVolume(HIGH_PRESSURE, BAD_VOLUME))
    selector = AtkComplexSelector(And(TEMP_MIN, TEMP_MAX, PRESSURE_MAX, VOLUME_MAX))
    with pytest.raises(AtkUndecidedError):
        selector.match(Temperature(GOOD_TEMP))
    with pytest.raises(AtkUndecidedError):
        selector.match(PressureVolume(GOOD_PRESSURE, GOOD_VOLUME))
    

def test_UT_AtkComplexSelector_PartialMatchComplex():
    #UnitTestComment("AtkComplexSelector::match(): check partial match with a complex criteria base on Or)
    selector = AtkComplexSelector(Or(TEMP_MAX, Or(VOLUME_MAX, PRESSURE_MAX)))
    assert selector.match(temperature=GOOD_TEMP)
    assert selector.match(temperature=HIGH_TEMP, pressure=GOOD_PRESSURE)
    assert selector.match(pressure=GOOD_PRESSURE)
    with pytest.raises(AtkUndecidedError):
        selector.match(temperature=HIGH_TEMP, pressure=HIGH_PRESSURE)
    with pytest.raises(AtkUndecidedError):
        selector.match(volume=BAD_VOLUME, pressure=HIGH_PRESSURE)

    #UnitTestComment("AtkComplexSelector::match(): check partial match with a complex criteria base on Xor)
    selector = AtkComplexSelector(And(TEMP_MAX, Xor(VOLUME_MAX, TEMP_MIN, PRESSURE_MAX)))
    with pytest.raises(AtkUndecidedError):
        selector.match(volume=GOOD_VOLUME, temperature=LOW_TEMP)
    with pytest.raises(AtkUndecidedError):
        selector.match(volume=BAD_VOLUME, temperature=GOOD_TEMP)
    with pytest.raises(AtkUndecidedError):
        selector.match(volume=BAD_VOLUME, temperature=LOW_TEMP)
    assert not selector.match(volume=GOOD_VOLUME, pressure=GOOD_PRESSURE)
    assert not selector.match(volume=GOOD_VOLUME, temperature=HIGH_TEMP)
    

def test_UT_AtkComplexSelector_Select():
    selector = AtkComplexSelector(TEMP_MAX)
    good_temp1 = Temperature(GOOD_TEMP)
    good_temp2 = Temperature(GOOD_TEMP - 1)
    good_temp3 = Temperature(GOOD_TEMP + 1)
    bad_temp1 = Temperature(HIGH_TEMP)
    bad_temp2 = Temperature(HIGH_TEMP + 1)
    iterable = [good_temp1, bad_temp1, good_temp2, bad_temp2, good_temp3]
    
    #UnitTestComment("AtkComplexSelector::select(): filter a list)
    filtered = selector.select(iterable)
    assert good_temp1 in filtered
    assert good_temp2 in filtered
    assert good_temp3 in filtered
    assert 3 == len(filtered)
    
    #UnitTestComment("AtkComplexSelector::select(): filter a set)
    filtered = selector.select(set(iterable))
    assert 3 == len(filtered)
    
    #UnitTestComment("AtkComplexSelector::select(): filter a tuple)
    filtered = selector.select(tuple(iterable))
    assert 3 == len(filtered)
    
    #UnitTestComment("AtkComplexSelector::select(): filter a generator)
    def generate():
        for data in iterable:
            yield data
    filtered = selector.select(generate())
    assert 3 == len(filtered)
    

def test_UT_AtkComplexSelector_SelectPartialMatch():
    selector = AtkComplexSelector(Or(VOLUME_MAX, PRESSURE_MAX))
    good_data1 = PressureVolume(GOOD_PRESSURE, GOOD_VOLUME)
    good_data2 = PressureVolume(GOOD_PRESSURE - 1, GOOD_VOLUME - 1)
    bad_data1 = PressureVolume(HIGH_PRESSURE, BAD_VOLUME)
    bad_data2 = PressureVolume(HIGH_PRESSURE + 1, BAD_VOLUME + 1)
    incomplete_data1 = Pressure(GOOD_PRESSURE)
    incomplete_data2 = Pressure(GOOD_PRESSURE - 1)
    bad_incomplete_data1 = Pressure(HIGH_PRESSURE)
    bad_incomplete_data2 = Pressure(HIGH_PRESSURE + 1)
    iterable = [good_data1, incomplete_data1, bad_incomplete_data1, bad_data1, \
                good_data2, incomplete_data2, bad_incomplete_data2, bad_data2]

    #UnitTestComment("AtkComplexSelector::select(): select only matching datas with Or criteria)
    filtered = selector.select(iterable)
    assert good_data1 in filtered
    assert good_data2 in filtered
    assert incomplete_data1 in filtered
    assert incomplete_data2 in filtered
    assert 4 == len(filtered)

    #UnitTestComment("AtkComplexSelector::select(): select matching and undecided datas with Or criteria)
    filtered = selector.select(iterable, include_undecided=True)
    assert good_data1 in filtered
    assert good_data2 in filtered
    assert incomplete_data1 in filtered
    assert incomplete_data2 in filtered
    assert bad_incomplete_data1 in filtered
    assert bad_incomplete_data1 in filtered
    assert 6 == len(filtered)

    #UnitTestComment("AtkComplexSelector::select(): select matching with And criteria)
    selector = AtkComplexSelector(And(VOLUME_MAX, PRESSURE_MAX))
    filtered = selector.select(iterable)
    assert good_data1 in filtered
    assert good_data2 in filtered
    assert 2 == len(filtered)

    #UnitTestComment("AtkComplexSelector::select(): select matching and undecided datas with And criteria)
    selector = AtkComplexSelector(And(VOLUME_MAX, PRESSURE_MAX))
    filtered = selector.select(iterable, include_undecided=True)
    assert good_data1 in filtered
    assert good_data2 in filtered
    assert incomplete_data1 in filtered
    assert incomplete_data2 in filtered
    assert 4 == len(filtered)

# TODO TU check