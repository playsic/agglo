############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.selector import AtkSelector
from agglo_tk.selector import AtkCriteria

    
class PressureVolume(object):
    def __init__(self, pressure, volume):
        self.pressure = pressure
        self.volume = volume
class Temperature(object):
    def __init__(self, temperature):
        self.temperature = temperature
class Pressure(object):
    def __init__(self, pressure):
        self.pressure = pressure

TEMP_MIN = 10
TEMP_MAX = 20
PRESSURE_MIN = 30
PRESSURE_MAX = 40
VOLUME_MAX = 50
LOW_TEMP = 5
GOOD_TEMP = 15
HIGH_TEMP = 25
LOW_PRESSURE = 25
GOOD_PRESSURE = 35
HIGH_PRESSURE = 45
GOOD_VOLUME = 45
BAD_VOLUME = 55

        
def test_UT_AtkSelector_Constructor():
    #UnitTestComment("AtkSelector::AtkSelector(): test constructor with a criteria parameter)
    criteria = AtkCriteria(temperature_min=10, match=lambda crit, data: True)
    selector = AtkSelector(criteria)

    assert selector.criteria is criteria


def test_UT_AtkSelector_BuiltIn():
    criteria = AtkCriteria(temperature_min=10, temperature_max=20, pressure_min=30)
    selector = AtkSelector(criteria)

    #UnitTestComment("AtkSelector::criteria:test get attributes)
    assert selector.temperature_min == 10
    assert selector.temperature_max == 20
    assert selector.pressure_min == 30

    #UnitTestComment("AtkSelector::criteria:test set attributes)
    criteria.temperature_min = 15
    assert selector.temperature_min == 15
    selector.pressure_min = 10
    assert criteria.pressure_min == 10
    

def test_UT_AtkSelector_Match():
    def match(crit, data):
        return (data.pressure >= crit.press_min and \
                data.pressure <= crit.press_max and \
                data.volume <= crit.vol_max)
    criteria = AtkCriteria(press_min=PRESSURE_MIN, press_max=PRESSURE_MAX, vol_max=VOLUME_MAX, match=match)
    selector = AtkSelector(criteria)

    #UnitTestComment("AtkSelector::match(): check that match can check with data passed as values)
    assert selector.match(pressure=GOOD_PRESSURE, volume=GOOD_VOLUME)
    assert not selector.match(pressure=LOW_PRESSURE, volume=GOOD_VOLUME)
    assert not selector.match(pressure=GOOD_PRESSURE, volume=BAD_VOLUME)
    
    #UnitTestComment("AtkSelector::match(): check that match can check inside the object passed as parameter)
    assert selector.match(PressureVolume(GOOD_PRESSURE, GOOD_VOLUME))
    assert not selector.match(PressureVolume(LOW_PRESSURE, GOOD_VOLUME))
    assert not selector.match(PressureVolume(HIGH_PRESSURE, GOOD_VOLUME))
    

def test_UT_AtkSelector_Select():
    criteria = AtkCriteria(temp_max=TEMP_MAX, match=lambda crit, data: data.temperature <= crit.temp_max)
    selector = AtkSelector(criteria)
    good_temp1 = Temperature(GOOD_TEMP)
    good_temp2 = Temperature(GOOD_TEMP - 1)
    good_temp3 = Temperature(GOOD_TEMP + 1)
    bad_temp1 = Temperature(HIGH_TEMP)
    bad_temp2 = Temperature(HIGH_TEMP + 1)
    iterable = [good_temp1, bad_temp1, good_temp2, bad_temp2, good_temp3]
    
    #UnitTestComment("AtkSelector::select(): filter a list
    filtered = selector.select(iterable)
    assert good_temp1 in filtered
    assert good_temp2 in filtered
    assert good_temp3 in filtered
    assert 3 == len(filtered)


# TODO TU check