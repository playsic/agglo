############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest

from agglo_tk.selector.criteria_path import AtkCriteriaPath


class Pth(object):
    def __init__(self, path):
        self.pth = path


def test_UT_AtkCriteriaPath_Constructor():

    #UnitTestComment("AtkCriteriaPath::AtkCriteriaPath(): test constructor with defaults arguments)
    criteria = AtkCriteriaPath(path="/var")
    assert criteria.boundary == "/var"
    assert criteria.path == "/var"
    assert criteria.recursive

    #UnitTestComment("False, AtkCriteriaPath::AtkCriteriaPath(): test data_name=constructor with specific arguments)
    criteria = AtkCriteriaPath(path="/var", recursive=False)
    assert criteria.boundary == "/var"
    assert criteria.path == "/var"
    assert not criteria.recursive


def test_UT_AtkCriteriaPath_Property():
    #UnitTestComment("AtkCriteriaPath::boundary: test boundary property)
    criteria = AtkCriteriaPath(path="/var/bin")
    criteria.boundary = "/var/bin/usr"
    assert criteria.boundary == "/var/bin/usr"
    assert criteria.path == "/var/bin/usr"

    #UnitTestComment("AtkCriteriaPath::boundary: test recursive property)
    assert criteria.recursive
    criteria.recursive = False
    assert not criteria.recursive


def test_UT_AtkCriteriaPath_Match():
    #UnitTestComment("AtkCriteriaPath::AtkCriteriaPath(): match with recursivity and or_self)
    criteria = AtkCriteriaPath(path="/var/bin")
    assert criteria.match("/var/bin/usr")
    assert criteria.match("/var/bin/usr/fake")
    assert criteria.match("/var/bin")
    assert not criteria.match("/Var/bin")
    assert not criteria.match("/")
    assert not criteria.match("/bin")

    #UnitTestComment("AtkCriteriaPath::AtkCriteriaPath(): test match without or_self)
    criteria.or_self = False
    assert not criteria.match("/var/bin")
    assert criteria.match("/var/bin/usr")
    criteria.or_self = True

    #UnitTestComment("AtkCriteriaPath::AtkCriteriaPath(): test match without recursivity)
    criteria.recursive = False
    assert criteria.match("/var/bin")
    assert not criteria.match("/var/bin/usr")
    criteria.recursive = True

    #UnitTestComment("AtkCriteriaPath::AtkCriteriaPath(): test match after boundary change)
    criteria.boundary = "/usr"
    assert not criteria.match("/var/bin")
    assert criteria.match("/usr/bin")
    assert criteria.match("/usr/bin/fake")

    #UnitTestComment("AtkCriteriaPath::match(): match with data extractor)
    with pytest.raises(TypeError):
        criteria.match(Pth("/usr/bin"))
    criteria.change_behavior(extract_data=lambda crit, data:data.pth)
    assert criteria.match(Pth("/usr/bin"))
    assert not criteria.match(Pth("/var/bin"))
