############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest
from agglo_tk.utils.limited_access_proxy import AtkLimitedAccessProxy
from agglo_tk.exceptions import AtkLimitedAccessError
from agglo_tt.fixtures import trace_test_start


class Proxied(object):
    def __init__(self):
        self._proxied_attr1 = 1

    def get_proxied_attr1(self):
        return self._proxied_attr1

    def set_proxied_attr1(self, value):
        self._proxied_attr1 = value

    @property
    def proxied_attr1(self):
        return self._proxied_attr1

    @proxied_attr1.setter
    def proxied_attr1(self, value):
        self._proxied_attr1 = value

class ChildProxied(Proxied):
    def __init__(self):
        Proxied.__init__(self)
        self._proxied_attr2 = 10

    def set_proxied_attr2(self, value):
        self._proxied_attr2 = attr

class Proxy(AtkLimitedAccessProxy):
    def __init__(self, item, **kwargs):
        AtkLimitedAccessProxy.__init__(self, item, **kwargs)
        self.add_attr("_proxy_attr1", 10)

    def get_proxy_attr1(self):
        return self._proxy_attr1

    def set_proxy_attr1(self, value):
        self._proxy_attr1 = value

    @property
    def proxy_attr1(self):
        return self._proxy_attr1

    @proxy_attr1.setter
    def proxy_attr1(self, value):
        self._proxy_attr1 = value


def test_UT_AtkLimitedAccessProxy_ConstructorErrorCases(trace_test_start):
    #UnitTestComment("AtkLimitedAccessProxy::constructor: test that allowed and forbidden properties are exclusive")
    with pytest.raises(ValueError) as exception:
        proxy = Proxy(Proxied(), allowed=["_proxied_attr1"], forbidden=["set_proxied_attr1"])


def test_UT_AtkLimitedAccessProxy_Constructor(trace_test_start):
    proxied = Proxied()
    proxy = Proxy(proxied)

    #UnitTestComment("AtkLimitedAccessProxy::constructor: test proxy subject is proxied")
    assert proxied in [proxy]
    assert proxy in [proxied]
            
    #UnitTestComment("AtkLimitedAccessProxy::constructor: test that all proxied instance api are accessible by default")
    proxy._proxied_attr1 = 2
    assert proxy._proxied_attr1 == 2
    proxy.set_proxied_attr1(3)
    assert proxy.proxied_attr1 == 3
    
    #UnitTestComment("AtkLimitedAccessProxy::constructor: test that allowed property is correctly initialized")
    proxy = Proxy(proxied, allowed=["_proxied_attr1"])
    proxy._proxied_attr1 = 2
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy.set_proxied_attr1(3)
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    assert proxy._proxied_attr1 == 2

    #UnitTestComment("AtkLimitedAccessProxy::constructor: test that forbidden property is correctly initialized")
    proxy = Proxy(proxied, forbidden=["_proxied_attr1"])
    proxy.set_proxied_attr1(2)
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy._proxied_attr1 = 3
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    assert proxy.proxied_attr1 == 2


# TODO reprendre dans test_limited_access_proxy les test builtin et les tests d'acces aux attributes par .
def test_UT_AtkLimitedAccessProxy_BuiltIn(trace_test_start):
    proxied = ChildProxied()
    proxy = Proxy(proxied)
            
    #UnitTestComment("AtkLimitedAccessProxy::builtin: test that proxy attribute is accessible")
    assert hasattr(proxy, "_proxy_attr1")
    setattr(proxy, "_proxy_attr1", 2)
    assert getattr(proxy, "_proxy_attr1") == 2
    delattr(proxy, "_proxy_attr1")
    assert not hasattr(proxy, "_proxy_attr1")

    #UnitTestComment("AtkLimitedAccessProxy::builtin: test that proxy attribute is no seen by proxied")
    proxy.add_attr("_proxy_attr1", 1)
    assert not hasattr(proxied, "referenced")
    assert getattr(proxy, "_proxy_attr1") == 1
    assert not hasattr(proxied, "_proxy_attr1")
            
    #UnitTestComment("AtkLimitedAccessProxy::builtin: test that proxied attribute is accessible")
    assert hasattr(proxy, "_proxied_attr1")
    setattr(proxy, "_proxied_attr1", 2)
    assert getattr(proxy, "_proxied_attr1") == 2
    assert proxied._proxied_attr1 == 2
    delattr(proxy, "_proxied_attr1")
    assert not hasattr(proxied, "_proxied_attr1")
    assert not hasattr(proxy, "_proxied_attr1")
            
    #UnitTestComment("AtkLimitedAccessProxy::builtin: test that child proxied attribute is accessible")
    assert hasattr(proxy, "_proxied_attr2")
    setattr(proxy, "_proxied_attr2", 2)
    assert getattr(proxy, "_proxied_attr2") == 2
    assert proxied._proxied_attr2 == 2
    delattr(proxy, "_proxied_attr2")
    assert not hasattr(proxied, "_proxied_attr2")
    assert not hasattr(proxy, "_proxied_attr2")


def test_UT_AtkLimitedAccessProxy_AddAttr(trace_test_start):
    proxied = Proxied()
    proxy = Proxy(proxied, forbidden=["_proxied_attr1", "proxied_attr1", "get_proxied_attr1", "set_proxied_attr1"])

    #UnitTestComment("AtkLimitedAccessProxy::add_attr(): test that proxy attributes created in constructor are accessible")
    proxy._proxy_attr1 = 11
    assert proxy._proxy_attr1 == 11
    proxy.set_proxy_attr1(12)
    assert proxy.get_proxy_attr1() == 12
    proxy.proxy_attr1 = 13
    assert proxy.proxy_attr1 == 13

    #UnitTestComment("AtkLimitedAccessProxy::add_attr(): test that an attribute can be added to proxy")
    proxy.add_attr("_proxy_attr2", 20)
    assert proxy._proxy_attr2 == 20
    proxy._proxy_attr2 = 21
    assert proxy._proxy_attr2 == 21

    #UnitTestComment("AtkLimitedAccessProxy::add_attr(): test that an attribute with same name that proxied's attribute can be overriden by proxy")
    proxy.add_attr("_proxied_attr1", 30)
    assert proxy._proxied_attr1 == 30
    proxy._proxied_attr1 = 31
    assert proxy._proxied_attr1 == 31
    assert proxied._proxied_attr1 == 1


def test_UT_AtkLimitedAccessProxy_LimitedAccessUsage(trace_test_start):
    proxied = Proxied()
    
    #UnitTestComment("AtkLimitedAccessProxy::access rights: test that several acces rights can be defined")
    proxy = Proxy(proxied, allowed=["_proxied_attr1", "set_proxied_attr1"])
    proxy._proxied_attr1 = 2
    assert proxied._proxied_attr1 == 2
    proxy.set_proxied_attr1(3)
    assert proxied._proxied_attr1 == 3
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy.get_proxied_attr1()
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    
    #UnitTestComment("AtkLimitedAccessProxy::access rights: test that proxied method is accessible even if backward attribute is not")
    proxy = Proxy(proxied, allowed=["get_proxied_attr1"])
    assert proxy.get_proxied_attr1() == 3
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy._proxied_attr1 = 4
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    proxy = Proxy(proxied, forbidden=["_proxied_attr1"])
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy._proxied_attr1 = 4
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    assert proxy.get_proxied_attr1() == 3
    
    #UnitTestComment("AtkLimitedAccessProxy::access rights: test that proxied property is accessible even if backward attribute is not")
    proxy = Proxy(proxied, allowed=["proxied_attr1"])
    assert proxy.proxied_attr1 == 3
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy._proxied_attr1 = 4
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    proxy = Proxy(proxied, forbidden=["_proxied_attr1"])
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy._proxied_attr1 = 4
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    assert proxy.proxied_attr1 == 3


def test_UT_AtkLimitedAccessProxy_AllowAccess(trace_test_start):
    proxied = Proxied()

    #UnitTestComment("AtkLimitedAccessProxy::allow_access: test that allowed attributes list can be extended")
    proxy = Proxy(proxied, allowed=["_proxied_attr1"])
    proxy.allow_access("set_proxied_attr1")
    proxy.set_proxied_attr1(2)
    assert proxy._proxied_attr1 == 2

    #UnitTestComment("AtkLimitedAccessProxy::allow_access: test that an attribute can be removed from forbidden attributes list")
    proxy = Proxy(proxied, forbidden=["_proxied_attr1", "get_proxied_attr1"])
    proxy.allow_access("_proxied_attr1")
    proxy._proxied_attr1 = 3
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy.get_proxied_attr1() == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED

    #UnitTestComment("AtkLimitedAccessProxy::forbid_access: test that when last element is removed from forbidden list, all items are allowed")
    proxy.allow_access("get_proxied_attr1")
    assert proxy.get_proxied_attr1() == 3
    assert proxy.proxied_attr1 == 3

    #UnitTestComment("AtkLimitedAccessProxy::allow_access: test that allowing an allowed attribute doesn't change anything")
    proxy = Proxy(proxied)
    proxy.allow_access("_proxied_attr1")
    assert proxy._proxied_attr1 == 3
    assert proxy.proxied_attr1 == 3
    proxy = Proxy(proxied, allowed=["_proxied_attr1"])
    proxy.allow_access("_proxied_attr1")
    assert proxy._proxied_attr1 == 3
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy.proxied_attr1 == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED

def test_UT_AtkLimitedAccessProxy_ForbidAccess(trace_test_start):
    proxied = Proxied()

    #UnitTestComment("AtkLimitedAccessProxy::forbid_access: test that forbidden attributes list can be extended")
    proxy = Proxy(proxied, forbidden=["_proxied_attr1"])
    proxy.forbid_access("set_proxied_attr1")
    with pytest.raises(AtkLimitedAccessError) as exception:
        proxy.set_proxied_attr1(2)
    assert str(exception.value) == AtkLimitedAccessError.MASKED

    #UnitTestComment("AtkLimitedAccessProxy::forbid_access: test that an attribute can be removed from allowed attributes list")
    proxy = Proxy(proxied, allowed=["_proxied_attr1", "get_proxied_attr1"])
    proxy.forbid_access("get_proxied_attr1")
    proxy._proxied_attr1 = 3
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy.get_proxied_attr1() == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy.proxied_attr1 == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED

    #UnitTestComment("AtkLimitedAccessProxy::forbid_access: test that when last element is removed from allowed list, all items are forbidden")
    proxy.forbid_access("_proxied_attr1")
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy._proxied_attr1 == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy.proxied_attr1 == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED

    #UnitTestComment("AtkLimitedAccessProxy::forbid_access: test that forbidding a forbidden attribute doesn't change anything")
    proxy = Proxy(proxied, allowed=["proxied_attr1"])
    proxy.forbid_access("_proxied_attr1")
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy._proxied_attr1 == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    proxy = Proxy(proxied, forbidden=["_proxied_attr1"])
    proxy.forbid_access("_proxied_attr1")
    with pytest.raises(AtkLimitedAccessError) as exception:
        assert proxy._proxied_attr1 == 3 
    assert str(exception.value) == AtkLimitedAccessError.MASKED


def test_UT_AtkLimitedAccessProxy_ChildProxiedUsage(trace_test_start):
    child_proxied = Proxied()

    #UnitTestComment("AtkLimitedAccessProxy::access rights: test that access rights can be modified for inherited attributes")
    child_proxy = Proxy(child_proxied, forbidden=["_proxied_attr1", "_attr2"])
    with pytest.raises(AtkLimitedAccessError) as exception:
        child_proxy._proxied_attr1 = 1
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    with pytest.raises(AtkLimitedAccessError) as exception:
        child_proxy._attr2 = 1
    assert str(exception.value) == AtkLimitedAccessError.MASKED
