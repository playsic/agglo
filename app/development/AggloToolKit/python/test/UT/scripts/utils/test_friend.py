﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.utils.friend import *


class Friend:
    def __init__(self, private):
        self.private = private

    # TODO avec des partials
    def call_friend_private(self, method):
        getattr(self.private, method)(self.private)

        
class Friend1:
    def __init__(self, private):
        self.private = private

    def call_friend_private(self):
        return self.private.public1_1()

    def call_private(self):
        return self.private.__private()

    def call_friend_protected(self):
        return self.private.public1_2()

    def call_other_friend_private(self):
        return self.private.public2_1()

        
class Friend2:
    def __init__(self, private):
        self.private = private

    def call_friend_private(self):
        return self.private.public2_1()

    def call_other_friend_private(self):
        return self.private.private()

        
@has_friends
class Private:
    
    @friend("Friend1", "public1_1")
    @friend("Friend2", "public2_1")
    @friend("Friend3", "public2_1")
    @friend("Friend4")
    def __private(self):
        return True
    
    @friend("Friend1", "public1_2")
    @friend("Friend4")
    def _protected(self):
        return True

        
class Friend3:
    def __init__(self, private):
        self.private = private

    def call_friend_private(self):
        return self.private.public2_1()
        

class Friend4:
    def __init__(self, private):
        self.private = private

    def call_friend_private(self):
        return self.private.private()

    def call_friend_protected(self):
        return self.private.protected()

    def call_other_friend_private(self):
        return self.private.public1_1()
        

class NotFriend:
    def __init__(self, private):
        self.private = private

    def call_other_friend_private(self):
        self.private.public1_1()
        
        
def test_UT_AtkFriend():
    private = Private()
    friend1 = Friend1(private)
    friend2 = Friend2(private)
    friend3 = Friend3(private)
    friend4 = Friend4(private)
    not_friend = NotFriend(private)
    
    #UnitTestComment("friend: test calling a private method as friend)
    assert friend1.call_friend_private()
    assert friend2.call_friend_private()
    assert friend3.call_friend_private()
    assert friend4.call_friend_private()
    try:
        friend1.call_private()
        assert False
    except AttributeError:
        assert True
        
    #UnitTestComment("AtkSelector::AtkSelector(): test calling another friend's private method)
    try:
        friend1.call_other_friend_private()
        assert False
    except AtkNotFriendError:
        # TODO tester le message d'erreur
        assert True
    try:
        friend2.call_other_friend_private()
        assert False
    except AtkNotFriendError:
        assert True
    try:
        friend4.call_other_friend_private()
        assert False
    except AtkNotFriendError:
        assert True
    try:
        not_friend.call_other_friend_private()
        assert False
    except AtkNotFriendError:
        assert True
   
    #UnitTestComment("friend: test calling a protected method as friend)
    assert friend1.call_friend_protected()
    assert friend4.call_friend_protected()

    
# @has_friends
# class classB:
    
    # @friend("classC", "public1")
    # @friend("classD", "public2")
    # @friend("classF", "public2")
    # @friend("classG")
    # def __test(self):
        # print "YEAAAH !!"

        
# class classC:
    # def test(self):
        # testB = classB()
        
        # testB.public1()
        # testB.public2()
        # testB.public1()
        # testB.test()

# class classD:
    # def test(self):
        # testB = classB()
        
        # testB.public1()
        # testB.public2()
        # testB.public1()
        # testB.test()

# class classE:
    # def test(self):
        # testB = classB()
        
        # testB.public1()
        # testB.public2()
        # testB.public1()
        # testB.test()

# class classF:
    # def test(self):
        # testB = classB()
        
        # testB.public1()
        # testB.public2()
        # testB.public1()
        # testB.test()

# class classG:
    # def test(self):
        # testB = classB()
        
        # testB.public1()
        # testB.public2()
        # testB.public1()
        # testB.test()
        
# def testSelector_Constructor():
    # pass
        
# if __name__ == "__main__":
    # print
    # print "TEST CLASS C"
    # testC = classC()
    # testC.test()
    # print

    # print "TEST CLASS D"
    # testD = classD()
    # testD.test()
    # print

    # print "TEST CLASS E"
    # testE = classE()
    # testE.test()
    # print

    # print "TEST CLASS F"
    # testF = classF()
    # testF.test()
    # print

    # print "TEST CLASS G"
    # testG = classG()
    # testG.test()
    

