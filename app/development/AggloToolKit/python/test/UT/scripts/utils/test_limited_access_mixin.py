############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

import pytest
from agglo_tk.utils.limited_access_mixin import AtkLimitedAccessMixIn
from agglo_tk.exceptions import AtkLimitedAccessError
from agglo_tt.fixtures import trace_test_start


class Referenced(object):
    def __init__(self):
        self._referenced_attr = 1

    @property
    def referenced_attr(self):
        return self._referenced_attr

    @referenced_attr.setter
    def referenced_attr(self, value):
        self._referenced_attr = value

class Proxied(object):

    def __init__(self):
        self._proxied_attr = 1

    def get_proxied_attr(self):
        return self._proxied_attr

    def set_proxied_attr(self, val):
        self._proxied_attr = val

    @property
    def proxied_attr(self):
        return self._proxied_attr

    @proxied_attr.setter
    def proxied_attr(self, val):
        self._proxied_attr = val

class MixIn(AtkLimitedAccessMixIn):
    def __init__(self, item, **kwargs):
        AtkLimitedAccessMixIn.__init__(self, item, **kwargs)
        self.add_attr("_proxy_attr", 1)

    def get_proxy_attr(self):
        return self._proxy_attr

    def set_proxy_attr(self, val):
        self._proxy_attr = val

    @property
    def proxy_attr(self):
        return self._proxy_attr

    @proxy_attr.setter
    def proxy_attr(self, value):
        self._proxy_attr = value

# TODO reprendre dans test_limited_access_proxy les test builtin et les tests d'acces aux attributes par .
def test_UT_AtkLimitedAccessMixIn_BuiltIn(trace_test_start):
    proxied = Proxied()
    referenced = Referenced()
    mix_in = MixIn(proxied)
            
    #UnitTestComment("AtkLimitedAccessMixIn::builtin: test that reference is accessible")
    mix_in.reference(referenced, "referenced")
    assert hasattr(mix_in, "referenced")
    assert getattr(mix_in, "referenced") is referenced
    with pytest.raises(AtkLimitedAccessError) as exception:
        setattr(mix_in, "referenced", Referenced())
    assert str(exception.value) == AtkLimitedAccessError.CANNOT_SET_REF
    assert mix_in.referenced is referenced
    with pytest.raises(AtkLimitedAccessError) as exception:
        delattr(mix_in, "referenced")
    assert str(exception.value) == AtkLimitedAccessError.CANNOT_DEL_REF
    assert mix_in.referenced is referenced

    #UnitTestComment("AtkLimitedAccessMixIn::builtin: test that reference is no seen by proxied")
    assert not hasattr(proxied, "referenced")
            
    #UnitTestComment("AtkLimitedAccessMixIn::builtin: test that proxy attribute is accessible")
    assert hasattr(mix_in, "_proxy_attr")
    setattr(mix_in, "_proxy_attr", 2)
    assert getattr(mix_in, "_proxy_attr") == 2
    delattr(mix_in, "_proxy_attr")
    assert not hasattr(mix_in, "_proxy_attr")

    #UnitTestComment("AtkLimitedAccessMixIn::builtin: test that proxy attribute is no seen by proxied")
    mix_in.add_attr("_proxy_attr", 1)
    assert not hasattr(proxied, "referenced")
    assert getattr(mix_in, "_proxy_attr") == 1
    assert not hasattr(proxied, "_proxy_attr")
            
    #UnitTestComment("AtkLimitedAccessMixIn::builtin: test that proxied attribute is accessible")
    assert hasattr(mix_in, "_proxied_attr")
    setattr(mix_in, "_proxied_attr", 2)
    assert getattr(mix_in, "_proxied_attr") == 2
    assert proxied._proxied_attr == 2
    delattr(mix_in, "_proxied_attr")
    assert not hasattr(proxied, "_proxied_attr")
    assert not hasattr(mix_in, "_proxied_attr")


# TODO TU AddAttr + error cases

def test_UT_AtkLimitedAccessMixIn_ReferenceErrorCase(trace_test_start):
    proxied = Proxied()
    referenced = Referenced()
    mix_in = MixIn(proxied)
            
    #UnitTestComment("AtkLimitedAccessMixIn::reference(): test that a reference cannot be set with same name than a proxy's existing attribute")
    with pytest.raises(AtkLimitedAccessError) as exception:
        mix_in.reference(referenced, "_proxy_attr")
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert mix_in._proxy_attr == 1
            
    #UnitTestComment("AtkLimitedAccessMixIn::reference(): test that a reference cannot be set with same name than a proxied's existing attribute")
    with pytest.raises(AtkLimitedAccessError) as exception:
        mix_in.reference(referenced, "_proxied_attr")
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert mix_in._proxied_attr == 1


def test_UT_AtkLimitedAccessMixIn_ReferencedAttributesUsage(trace_test_start):
    proxied = Proxied()
    referenced = Referenced()
    mix_in = MixIn(proxied)
            
    #UnitTestComment("AtkLimitedAccessMixIn::reference(): test get")
    mix_in.reference(referenced, "referenced")
    assert mix_in.referenced is referenced


def test_UT_AtkLimitedAccessMixIn_ProxyAttributes(trace_test_start):
    proxied = Proxied()
    referenced = Referenced()
    mix_in = MixIn(proxied)
            
    mix_in.reference(referenced, "referenced")

    #UnitTestComment("AtkLimitedAccessMixIn::proxy: check that proxy attributes are accessible")
    assert mix_in._proxy_attr == 1
    mix_in._proxy_attr = 2
    assert mix_in._proxy_attr == 2

    #UnitTestComment("AtkLimitedAccessMixIn::proxy: check that proxy methods are accessible")
    mix_in.set_proxy_attr(2)
    assert mix_in.get_proxy_attr() == 2

    #UnitTestComment("AtkLimitedAccessMixIn::proxy: check that proxy properties are accessible")
    mix_in.proxy_attr = 3
    assert mix_in.proxy_attr == 3


def test_UT_AtkLimitedAccessMixIn_ProxiedAttributes(trace_test_start):
    proxied = Proxied()
    referenced = Referenced()
    mix_in = MixIn(proxied, forbidden=["set_proxied_attr"])
            
    mix_in.reference(referenced, "referenced")

    #UnitTestComment("AtkLimitedAccessMixIn::proxied: check that proxied attributes are accessible")
    assert mix_in._proxied_attr == 1
    mix_in._proxied_attr = 2
    assert mix_in._proxied_attr == 2

    #UnitTestComment("AtkLimitedAccessMixIn::proxied: check that proxied methods are accessible")
    with pytest.raises(AtkLimitedAccessError) as exception:
        mix_in.set_proxied_attr(3)
    assert str(exception.value) == AtkLimitedAccessError.MASKED
    assert mix_in.get_proxied_attr() == 2

    #UnitTestComment("AtkLimitedAccessMixIn::proxied: check that proxied properties are accessible")
    mix_in.proxied_attr = 2
    assert mix_in.proxied_attr == 2
