############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.utils.serializable import AtkSerializable
        

    
class SampleSerializable(AtkSerializable):

    def __init__(self):
        AtkSerializable.__init__(self)
        self.sAttr1 = "5"
        self.iAttr2 = 3
        self.sAttr3 = "test"
        self.iAttr4 = 19
        self.__ls_commands = []
 

    def _create_result(self):
        return [True]
        
        
    def _append_result(self, a_global_result, a_cmd_result):
        if not a_cmd_result:
            a_global_result.reset()


    @property
    def commands(self):
        return self.__ls_commands


    def serialize_attr1(self):
        self.__ls_commands.append("write attr1 " + self.sAttr1)

        return True


    def serialize_attr1_2(self):
        self.__ls_commands.append("write attr1 " + self.sAttr1 + " attr2 " + str(self.iAttr2))

        return True


    def serialize_attr2(self):
        self.__ls_commands.append("write attr2 " + str(self.iAttr2))

        return True
        
        
    def serialize_attr3(self):
        self.__ls_commands.append("write attr3 " + self.sAttr3)

        return True


    def serialize_attr3_4(self):
        self.__ls_commands.append("write attr3 " + self.sAttr3 + " attr4 " + str(self.iAttr4))

        return True


    def unserialize_attr1(self):
        self.__ls_commands.append("read attr1")

        return True


    def unserialize_attr1_2(self):
        self.__ls_commands.append("read attr1_2")

        return True


    def unserialize_attr2(self):
        self.__ls_commands.append("read attr2")

        return True


    def unserialize_attr3(self):
        self.__ls_commands.append("read attr3")

        return True


    def unserialize_attr3_4(self):
        self.__ls_commands.append("read attr3_4")

        return True



def test_UT_AtkSerializable_AddSerializeMethod():
    serializable = SampleSerializable()

    #UnitTestComment("serializable::add_serialize_method(): define a serialization method for an unknown attribute")
    assert not(serializable.add_serialize_method(SampleSerializable.serialize_attr1, "unknown"))
    
    #UnitTestComment("serializable::add_serialize_method(): define serialization method for one attribute")
    assert serializable.add_serialize_method(SampleSerializable.serialize_attr1, "sAttr1")
    
    #UnitTestComment("serializable::add_serialize_method(): define serialization method for several attributes")
    assert serializable.add_serialize_method(SampleSerializable.serialize_attr1_2, "sAttr1", "iAttr2")
    
    #UnitTestComment("serializable::add_serialize_method(): define serialization method in several steps")
    assert serializable.add_serialize_method(SampleSerializable.serialize_attr3_4, "sAttr3")
    assert serializable.add_serialize_method(SampleSerializable.serialize_attr3_4, "iAttr4")


def test_UT_AtkSerializable_AddUnserializeMethod():
    serializable = SampleSerializable()

    #UnitTestComment("serializable::add_unserialize_method(): define a unserialization method for an unknown attribute")
    assert not(serializable.add_unserialize_method(SampleSerializable.unserialize_attr1, "unknown"))
    
    #UnitTestComment("serializable::add_unserialize_method(): define unserialization method for one attribute")
    assert serializable.add_unserialize_method(SampleSerializable.unserialize_attr1, "sAttr1")
    
    #UnitTestComment("serializable::add_unserialize_method(): define unserialization method for several attributes")
    assert serializable.add_unserialize_method(SampleSerializable.unserialize_attr1_2, "sAttr1", "iAttr2")
    
    #UnitTestComment("serializable::add_unserialize_method(): define unserialization method in several steps")
    assert serializable.add_unserialize_method(SampleSerializable.unserialize_attr3_4, "sAttr3")
    assert serializable.add_unserialize_method(SampleSerializable.unserialize_attr3_4, "iAttr4")

    
def test_UT_AtkSerializable_IsSerializationNeeded():
    serializable = SampleSerializable()

    #UnitTestComment("serializable.serialize() : check that serialization is not needed for unknown attribute")
    assert not serializable.is_serialization_needed("unknown")

    #UnitTestComment("serializable.serialize() : check that serialization is not needed for non serializable attributes")
    assert not serializable.is_serialization_needed("sAttr1")
    assert not serializable.is_serialization_needed("iAttr2")

    #UnitTestComment("serializable.serialize() : check that serialization is needed when declaring an attribute serializable")
    serializable.add_serialize_method(SampleSerializable.serialize_attr1, "sAttr1")
    serializable.add_serialize_method(SampleSerializable.serialize_attr3, "sAttr3")
    assert serializable.is_serialization_needed("sAttr1")
    assert not serializable.is_serialization_needed("iAttr2")
    assert serializable.is_serialization_needed("sAttr3")
    
    #UnitTestComment("serializable.serialize() : check that serialization reset serialization needed property")
    serializable.serialize("sAttr3")
    assert serializable.is_serialization_needed("sAttr1")
    assert not serializable.is_serialization_needed("sAttr3")

    #UnitTestComment("serializable.serialize() : check that serialization is not needed when declaring an non serializable attribute as unserializable")
    serializable.add_unserialize_method(SampleSerializable.unserialize_attr2, "iAttr2")
    assert not serializable.is_serialization_needed("iAttr2")

    #UnitTestComment("serializable.serialize() : check that serialization is needed when declaring an serializable attribute as unserializable")
    serializable.serialize("sAttr3")
    assert not serializable.is_serialization_needed("sAttr3")
    serializable.add_serialize_method(SampleSerializable.unserialize_attr3, "sAttr3")
    assert serializable.is_serialization_needed("sAttr3")
    
    #UnitTestComment("serializable.serialize() : check that unserialization reset serialization needed property")
    serializable.add_unserialize_method(SampleSerializable.unserialize_attr3, "sAttr3")
    serializable.unserialize("sAttr3")
    assert serializable.is_serialization_needed("sAttr1")
    assert not serializable.is_serialization_needed("sAttr3")
    
    #UnitTestComment("serializable.serialize() : check that attribute modification sets serialization needed property")
    serializable.sAttr3 = "19"
    assert serializable.is_serialization_needed("sAttr3")
    
    #UnitTestComment("serializable.serialize() : check is_serialization_needed for all attributes")
    assert serializable.is_serialization_needed()
    serializable.serialize()
    assert not serializable.is_serialization_needed()

    
def test_UT_AtkSerializable_Serialize():
    serializable = SampleSerializable()

    #UnitTestComment("serializable::serialize(): serialize without defined commands")
    assert serializable.serialize()

    #UnitTestComment("serializable.serialize() : check that serialization is needed at init")
    serializable.add_serialize_method(SampleSerializable.serialize_attr1_2, "sAttr1", "iAttr2")
    assert serializable.serialize()
    assert serializable.commands[-1] ==  "write attr1 " + serializable.sAttr1 + " attr2 " + str(serializable.iAttr2)
    
    #UnitTestComment("serializable.serialize() : check that serialization is done only when needed")
    ui_nb_commands  = len(serializable.commands)
    assert serializable.serialize()
    assert len(serializable.commands) == ui_nb_commands
    
    #UnitTestComment("serializable.serialize() : check generation of several commands")
    serializable.add_serialize_method(SampleSerializable.serialize_attr3, "sAttr3")
    serializable.sAttr1 = "6"
    serializable.iAttr2 = 4
    assert serializable.serialize()
    assert serializable.commands[-2] ==  "write attr3 " + serializable.sAttr3
    assert serializable.commands[-1] ==  "write attr1 6 attr2 4"
    
    #UnitTestComment("serializable.serialize() : check the force serialization mode")
    assert serializable.serialize(ab_force=True)
    assert serializable.commands[-2] ==  "write attr1 6 attr2 4"
    assert serializable.commands[-1] ==  "write attr3 " + serializable.sAttr3
    
    #UnitTestComment("serializable.serialize() : check the serialization of all modified attributes")
    serializable.sAttr1 = "7"
    serializable.iAttr2 = 5
    serializable.sAttr3 = "test2"
    assert serializable.serialize(ab_force=True)
    assert serializable.commands[-2] ==  "write attr1 7 attr2 5"
    assert serializable.commands[-1] ==  "write attr3 test2"
    
    #UnitTestComment("serializable.serialize() : check the command is sent when only one of the attribute is modified")
    serializable.iAttr2 = 6
    assert serializable.serialize()
    assert serializable.commands[-1] ==  "write attr1 7 attr2 6"

    # TODO verifier que les commandes sont invquees dans l'ordre de modification des attributs, et non dans l'order de declaration des serializable
    
    #UnitTestComment("serializable.serialize() : check serialization of the same attribute with several commands")
    serializable.add_serialize_method(SampleSerializable.serialize_attr1, "sAttr1")
    serializable.sAttr1 = "8"
    assert serializable.serialize()
    assert serializable.commands[-2] ==  "write attr1 8 attr2 6"
    assert serializable.commands[-1] ==  "write attr1 8"

    #UnitTestComment("serializable.serialize() : check serialization of a subset of serializable attributes")
    serializable.add_serialize_method(SampleSerializable.serialize_attr2, "iAttr2")
    serializable.sAttr1 = "8"
    serializable.iAttr2 = 6
    serializable.sAttr3 = "test2"
    assert serializable.serialize("sAttr1", "iAttr2")
    assert serializable.commands[-3] ==  "write attr1 8 attr2 6"
    assert serializable.commands[-2] ==  "write attr1 8"
    assert serializable.commands[-1] ==  "write attr2 6"
    
    
def test_UT_AtkSerializable_Unserialize():
    serializable = SampleSerializable()

    #UnitTestComment("serializable::serialize(): unserialize without defined commands")
    assert serializable.unserialize()

    #UnitTestComment("serializable.serialize() : check that unserialization is needed at init")
    serializable.add_unserialize_method(SampleSerializable.unserialize_attr1_2, "sAttr1", "iAttr2")
    assert serializable.unserialize()
    assert serializable.commands[-1] ==  "read attr1_2"
    
    #UnitTestComment("serializable.serialize() : check that unserialization is done any time")
    ui_nb_commands  = len(serializable.commands)
    assert serializable.unserialize()
    assert len(serializable.commands) == (ui_nb_commands + 1)
    assert serializable.commands[-1] ==  "read attr1_2"
    
    #UnitTestComment("serializable.serialize() : check generation of several commands")
    serializable.add_unserialize_method(SampleSerializable.unserialize_attr3, "sAttr3")
    serializable.sAttr1 = "6"
    serializable.iAttr2 = 4
    serializable.sAttr1 = "test"
    assert serializable.unserialize()
    assert serializable.commands[-2] ==  "read attr1_2"
    assert serializable.commands[-1] ==  "read attr3"

    # TODO a completer