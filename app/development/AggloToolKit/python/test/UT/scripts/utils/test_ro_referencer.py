############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR
## PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


import pytest

from agglo_tk.utils.ro_referencer import AtkROReferencer
from agglo_tk.exceptions import AtkLimitedAccessError

from agglo_tt.fixtures import trace_test_start


class Referenced(object):
    def __init__(self):
        self._referenced_attr = 1

    @property
    def referenced_attr(self):
        return self._referenced_attr

    @referenced_attr.setter
    def referenced_attr(self, value):
        self._referenced_attr = value

class Referencer(AtkROReferencer):
    def __init__(self):
        AtkROReferencer.__init__(self)
        self._referencer_attr = 1

    @property
    def referencer_attr(self):
        return self._referencer_attr

    @referencer_attr.setter
    def referencer_attr(self, value):
        self._referencer_attr = value
    


def test_UT_AtkROReferencer_ReferenceErrorCase(trace_test_start):
    referencer = Referencer()
    referenced1 = Referenced()
    referenced2 = Referenced()
            
    #UnitTestComment("AtkROReferencer::reference(): test that a reference cannot be set with same name than a referencer's existing attribute")
    with pytest.raises(AtkLimitedAccessError) as exception:
        referencer.reference(referenced1, "_referencer_attr")
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT
    assert referencer._referencer_attr == 1

    #UnitTestComment("AtkROReferencer::reference(): test that a reference cannot be set with same name than another reference")
    referencer.reference(referenced1, "referenced1")
    with pytest.raises(AtkLimitedAccessError) as exception:
        referencer.reference(referenced2, "referenced1")
    assert str(exception.value) == AtkLimitedAccessError.CONFLICT


def test_UT_AtkROReferencer_Reference(trace_test_start):
    referencer = AtkROReferencer()
    referenced1 = Referenced()
    referenced2 = Referenced()
            
    #UnitTestComment("AtkROReferencer::reference(): test nominal case")
    referencer.reference(referenced1, "referenced1")
    assert referencer.referenced1 is referenced1
            
    #UnitTestComment("AtkROReferencer::reference(): test that referencing same object with same name has no effect")
    referencer.reference(referenced1, "referenced1")
    assert referencer.referenced1 is referenced1
            
    #UnitTestComment("AtkROReferencer::reference(): test that an instance can be referenced several times")
    referencer.reference(referenced1, "referenced1bis")
    assert referencer.referenced1bis is referenced1
    assert referencer.referenced1 is referenced1


def test_UT_AtkROReferencer_BuiltIn(trace_test_start):
    referenced = Referenced()
    referencer = Referencer()
            
    #UnitTestComment("AtkROReferencer::builtin: test that reference is accessible")
    referencer.reference(referenced, "referenced")
    assert hasattr(referencer, "referenced")
    assert getattr(referencer, "referenced") is referenced
    with pytest.raises(AtkLimitedAccessError) as exception:
        setattr(referencer, "referenced", Referenced())
    assert str(exception.value) == AtkLimitedAccessError.CANNOT_SET_REF
    assert referencer.referenced is referenced
    with pytest.raises(AtkLimitedAccessError) as exception:
        delattr(referencer, "referenced")
    assert str(exception.value) == AtkLimitedAccessError.CANNOT_DEL_REF
    assert referencer.referenced is referenced

    #UnitTestComment("AtkROReferencer::reference(): check that references are not defined at class level")
    # TODO util ca ?
    referencer2 = Referencer()
    assert not hasattr(referencer2, "referenced")


def test_UT_AtkROReferencer_AccessReferenceErrorCases(trace_test_start):
    referencer = AtkROReferencer()
    referenced = Referenced()
    
    #UnitTestComment("AtkROReferencer::reference(): check that access to unkown attribute fails")
    with pytest.raises(AttributeError) as exception:
    # with pytest.raises(AtkLimitedAccessError) as exception:
        referencer.referenced._referenced_attr
    # assert str(exception.value) == AtkLimitedAccessError.CANNOT_GET_REF

    #UnitTestComment("AtkROReferencer::reference(): check that a reference cannot be set")
    referencer.reference(referenced, "referenced")
    with pytest.raises(AtkLimitedAccessError) as exception:
        referencer.referenced = Referenced()
    assert str(exception.value) == AtkLimitedAccessError.CANNOT_SET_REF
    assert referencer.referenced is referenced

    #UnitTestComment("AtkROReferencer::reference(): check that a reference cannot be deleted")
    with pytest.raises(AtkLimitedAccessError) as exception:
        del referencer.referenced
    assert str(exception.value) == AtkLimitedAccessError.CANNOT_DEL_REF
    assert referencer.referenced is referenced


def test_UT_AtkROReferencer_AccessReference(trace_test_start):
    referencer = Referencer()
    referenced = Referenced()

    #UnitTestComment("AtkROReferencer::reference(): check that referenced usage fully works")
    referencer.reference(referenced, "referenced")
    referenced._referenced_attr = 2
    assert referencer.referenced._referenced_attr == 2
    referencer.referenced.referenced_attr = 3
    assert referenced.referenced_attr == 3
            
    #UnitTestComment("AtkROReferencer::reference(): test that referencer attributes are accessible")
    assert referencer._referencer_attr == 1
    referencer.referencer_attr = 2
    assert referencer.referencer_attr == 2
