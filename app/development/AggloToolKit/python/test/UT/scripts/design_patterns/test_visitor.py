﻿############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################

from agglo_tk.design_patterns.visitor import AtkVisitable


class SampleVisitable1(AtkVisitable):
    def __init__(self, attr):
        self.attr = attr

class SampleVisitable2(SampleVisitable1):
    def __init__(self, attr):
        SampleVisitable1.__init__(self, attr)
        
class GetAttrVisitor1(object):
    pass

class GetAttrVisitor2(GetAttrVisitor1):
    def visit(visitor, visitable, *args, **kwargs):
        return visitable.attr

class GetAttrVisitor3(GetAttrVisitor2):
    def visit_SampleVisitable1(visitor, visitable, *args, **kwargs):
        return visitable.attr + 1

    def visit_SampleVisitable2(visitor, visitable, *args, **kwargs):
        return visitable.attr + 2

class GetAttrVisitor4(GetAttrVisitor3):
    def visit_SampleVisitable2(visitor, visitable, *args, **kwargs):
        return visitable.attr + 3

class GetAttrVisitor5(GetAttrVisitor3):
    def visit_SampleVisitable2(visitor, visitable, *args, **kwargs):
        attr = visitable.attr
        sargs = ""
        
        for arg in args:
            attr += arg
        
        for kwarg, arg in kwargs.items():
            sargs += kwarg
            attr += arg
            
        return sargs, attr

        
def test_UT_AtkVisitable_Accept():
    visitable1 = SampleVisitable1(5)
    visitable2 = SampleVisitable2(5)
    visitor1 = GetAttrVisitor1()
    visitor2 = GetAttrVisitor2()
    visitor3 = GetAttrVisitor3()
    visitor4 = GetAttrVisitor4()
    visitor5 = GetAttrVisitor5()
    
    #UnitTestComment("AtkVisitable::accept(): test visit without visit method)
    try:
        visitable1.accept(visitor1)
        assert False
    except AttributeError:
        assert True
      
    #UnitTestComment("AtkVisitable::accept(): test visit with default visit method)
    assert visitable1.accept(visitor2) == 5
      
    #UnitTestComment("AtkVisitable::accept(): test visit with dedicated visit method)
    assert visitable1.accept(visitor3) == 6

    #UnitTestComment("AtkVisitable::accept(): test choice of visit method in visitor hierarchy)
    assert visitable1.accept(visitor4) == 6
    assert visitable2.accept(visitor4) == 8

    #UnitTestComment("AtkVisitable::accept(): test usage of *args/**kwargs by visitor)
    assert visitable1.accept(visitor5, 2, 3, 4, test=5, test2=6) == 6
    sargs, attr = visitable2.accept(visitor5, 2, 3, 4, test1=5, test2=6)
    assert sargs == "test1test2"
    assert attr == 25

        

# class Test(AtkVisitable):
    # def __init__(self):
        # self.name = "Test"

        
# class TestA(Test):
    # def __init__(self):
        # self.name = "TestA"

        
# class TestB(TestA):
    # def __init__(self):
        # self.name = "TestB"

        
# class TestC(Test):
    # def __init__(self):
        # self.name = "TestC"

        
# class TestD(Test):
    # def __init__(self):
        # self.name = "TestD"


# class Visitor(object)  :
    # def visit(self, visitable, *args, **kwargs):
        # print "0", visitable.name
        
    # def visit_TestA(self, visitable, *args, **kwargs):
        # print "1", visitable.name        
        
    # def visit_TestB(self, visitable, *args, **kwargs):
        # print "2", visitable.name
        
    # def visit_TestC(self, visitable, *args, **kwargs):
        # print "3", visitable.name


# if __name__ == '__main__':
    # a = TestA()
    # b = TestB()
    # c = TestC()
    # d = TestD()
    # v = Visitor()
    # a.accept(v)
    # b.accept(v)
    # c.accept(v)
    # d.accept(v)
    
    