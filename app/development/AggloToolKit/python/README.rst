agglo_tool_kit is a library embedding various utils:
* io management : generic, string, scapy
* generic data selector

you can install it with
<pre>
pip install agglo_tool_kit
<pre>