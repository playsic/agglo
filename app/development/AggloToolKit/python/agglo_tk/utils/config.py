############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################

from .module_loader import AtkModuleLoader


__all__ = ["AtkConfig", "AtkConfigModule"]

class AtkConfig:
    def __init__(self, path=None, **kwargs):
        self.config_file = AtkConfigModule(path) if path else None
        self._default_values = kwargs


    def __getattr__(self, attr_name):
        result = None

        if self.config_file and (attr_name in self.config_file):
            result = self.config_file.params[attr_name]
        elif attr_name in self._default_values:
            result = self._default_values[attr_name]
        else:
            raise AttributeError(f"'{self.__class__.__name__}' object '{attr_name}' attribute is not set")

        return result


    def __str__(self):
        params = "\n". join(f"  {name}: {getattr(self, name)}" for name in self._default_values)

        return f"{self.__class__.__name__}:\n{params}"


class AtkConfigModule:
    def __init__(self, path):
        self.path = path
        self._module = None
        self.load()


    def __contains__(self, item):
        return item in self.params
        

    def __getattr__(self, attr_name):
        result = None

        try:
            if attr_name == "params":
                module = object.__getattribute__(self, "_module")
                params_names = dir(module)
                params = vars(module)

                # Remove builtins from module variables
                params_names = [param_name for param_name in params_names 
                                        if not (param_name.startswith("__") and param_name.endswith("__"))]
                result = {param_name:params[param_name] for param_name in params_names}
            else:
              result = self.params[attr_name]
        except KeyError:
            raise AttributeError(f"'{self.__class__.__name__}' object '{attr_name}' attribute is not set")

        return result


    def __str__(self):
        params = "\n". join(f"  {name}: {value}" for name, value in self.params.items())

        return f"{self.__class__.__name__}:\n{params}"


    def load(self):
        self._module = AtkModuleLoader(self.path).load()

        return self.params
