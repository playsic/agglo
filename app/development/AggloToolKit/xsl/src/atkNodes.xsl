<?xml version="1.0" encoding="UTF-8"?>
<!--************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
*************************************************************************-->


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                              xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<!-- <xsl:output method="xml" indent="yes" encoding="UTF-8"/> -->


<!-- Global variables -->
<xsl:variable name="STR_Differs"><xsl:text>differs</xsl:text></xsl:variable>
<xsl:variable name="STR_Equals"><xsl:text>equals</xsl:text></xsl:variable>
<xsl:variable name="STR_Includes"><xsl:text>includes</xsl:text></xsl:variable>
<xsl:variable name="STR_IsIncluded"><xsl:text>isIncluded</xsl:text></xsl:variable>


<!-- TODO Cette fonction est en realite une fusion avec factorize -->
<xsl:template name="Merge">
    <xsl:param name="Node1"/>
    <xsl:param name="Node2"/>
    
	<!-- TODO comparer l'ensemble du noeud avec GetNodeRelation -->
    <xsl:if test="name($Node1)=name($Node2)">
		<!-- Copy root node (no attributes -->
        <xsl:copy>
			<!-- Copy attributes of Node1 -->
			<xsl:for-each select="@*">	
				<xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
			</xsl:for-each>
            <xsl:value-of select="$gsEndl"/>
            
			<!-- Copy all childs Node1 -->
            <xsl:value-of select="$STR_Indent1"/>
            <xsl:copy-of select="$Node1/*"/>
            <xsl:value-of select="$gsEndl"/>
            
			<!-- Copy all childs of Node2 -->
            <xsl:value-of select="$STR_Indent1"/>
            <xsl:copy-of select="$Node2/*"/>
            <xsl:value-of select="$gsEndl"/>
        </xsl:copy>
    </xsl:if>
</xsl:template>
  
  
<xsl:template name="Factorize">
    <xsl:param name="NodeList"/>
    <xsl:param name="FactorizationDepth" select="2"/>
    <xsl:param name="CurrentDepth" select="1"/>
    
    <xsl:for-each select="$NodeList">
		<xsl:variable name="FirstIdenticalNodesPosition">
			<xsl:call-template name="GetFirstIdenticalNodePosition">
				<xsl:with-param name="NodeList" select="$NodeList"/>
				<xsl:with-param name="NodeEtalon" select="."/>
			</xsl:call-template>
		</xsl:variable>

        <xsl:if test="position() = $FirstIdenticalNodesPosition">
			
			<xsl:variable name="IdenticalNodesList">
				<xsl:call-template name="BuildIdenticalNodesList">
					<xsl:with-param name="NodeList" select="$NodeList"/>
					<xsl:with-param name="NodeEtalon" select="."/>
				</xsl:call-template>
			</xsl:variable>
			
			<xsl:value-of select="$gsEndl"/>
			<xsl:call-template name="Indent">
				<xsl:with-param name="NbIndent" select="number($CurrentDepth)-1"/>
			</xsl:call-template>
			
			<!-- Copy current node name -->
            <xsl:copy>
				<!-- Copy current node attributes -->
				<xsl:for-each select="@*">	
					<xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
				</xsl:for-each>	
				
				<xsl:choose>
					<!-- If first node in NodeList equals NodeEtalon -->
					<xsl:when test="number($FactorizationDepth)>1">
						<!-- Factorise all the children of all the node in NodeList which avec CurrentChildName as name-->
						<xsl:call-template name="Factorize">
							<xsl:with-param name="NodeList" select="exsl:node-set($IdenticalNodesList)/*/*"/>
							<xsl:with-param name="FactorizationDepth" select="number($FactorizationDepth)-1"/>
							<xsl:with-param name="CurrentDepth" select="number($CurrentDepth)+1"/>
						</xsl:call-template>
					</xsl:when>

					<!-- Else look for an identical node in remaining nodes -->
					<xsl:otherwise>						
						<xsl:copy-of select="exsl:node-set($IdenticalNodesList)/*/*"/>
					</xsl:otherwise>
					
				</xsl:choose>
			</xsl:copy>
        </xsl:if>
    </xsl:for-each>
</xsl:template>


<xsl:template name="GetNodesRelation">
    <xsl:param name="Node1"/>
    <xsl:param name="Node2"/>
    
    <xsl:choose>
		
		<!-- If the 2 nodes have the same name -->
		<xsl:when test="name($Node1)=name($Node2)">
			
			<!-- Compare the attribute sets -->
			<xsl:call-template name="GetAttributeSetRelation_BiDir">
				<xsl:with-param name="AttributeSet1" select="$Node1/@*"/>
				<xsl:with-param name="AttributeSet2" select="$Node2/@*"/>
			</xsl:call-template>

		</xsl:when>
					
		<!-- If the 2 nodes have different names -->
		<xsl:otherwise>
			<xsl:value-of select="$STR_Differs"/>
		</xsl:otherwise>
    </xsl:choose>
    
</xsl:template>


<xsl:template name="GetAttributeSetRelation_BiDir">
    <xsl:param name="AttributeSet1"/>
    <xsl:param name="AttributeSet2"/>
    
	<xsl:variable name="RelationFromAttributeSet1ToAttributeSet2">
		<xsl:call-template name="GetAttributeSetRelation_MonoDir">
			<xsl:with-param name="AttributeSet1" select="$AttributeSet1"/>
			<xsl:with-param name="AttributeSet2" select="$AttributeSet2"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="RelationFromAttributeSet2ToAttributeSet1">
		<xsl:call-template name="GetAttributeSetRelation_MonoDir">
			<xsl:with-param name="AttributeSet1" select="$AttributeSet2"/>
			<xsl:with-param name="AttributeSet2" select="$AttributeSet1"/>
		</xsl:call-template>
	</xsl:variable>

    <xsl:choose>
		
		<!-- If one of the attributes set differs fom the other -->
		<xsl:when test="$RelationFromAttributeSet1ToAttributeSet2=$STR_Differs or $RelationFromAttributeSet2ToAttributeSet1=$STR_Differs">
			<xsl:value-of select="$STR_Differs"/>
		</xsl:when>
			
		<!-- If the attributes sets equals in both direction -->
		<xsl:when test="$RelationFromAttributeSet1ToAttributeSet2=$STR_Equals and $RelationFromAttributeSet2ToAttributeSet1=$STR_Equals">
			<xsl:value-of select="$STR_Equals"/>
		</xsl:when>
		
		<!-- If the attributes sets includes each others -->
		<!-- TODO on pourrait plutot retourner union -->
		<xsl:when test="$RelationFromAttributeSet1ToAttributeSet2=$STR_Includes and $RelationFromAttributeSet2ToAttributeSet1=$STR_Includes">
			<xsl:value-of select="$STR_Differs"/>
		</xsl:when>
		
		<!-- If AttributeSet1 includes AttributeSet2 -->
		<xsl:when test="$RelationFromAttributeSet1ToAttributeSet2=$STR_Includes">
			<xsl:value-of select="$STR_Includes"/>
		</xsl:when>
					
		<!-- If AttributeSet2 includes AttributeSet1 -->
		<xsl:otherwise>
			<xsl:value-of select="$STR_IsIncluded"/>
		</xsl:otherwise>
    </xsl:choose>
    
</xsl:template>


<xsl:template name="GetAttributeSetRelation_MonoDir">
    <xsl:param name="AttributeSet1"/>
    <xsl:param name="AttributeSet2"/>

    <xsl:choose>
		<!-- If AttributeSet1 has no element -->
		<xsl:when test="count($AttributeSet1)=0">
			<!-- The result is equals -->
			<xsl:value-of select="$STR_Equals"/>
		</xsl:when>
		
		<xsl:otherwise>
			<xsl:variable name="FirstAttributeName" select="name($AttributeSet1[1])"/>
			<xsl:variable name="FirstAttributeRelation">
				<xsl:choose>
					<!-- If AttributeSet2 has an attribute with same name than FirstAttributeName -->
					<xsl:when test="$AttributeSet2[name()=$FirstAttributeName]">
						<xsl:choose>
							<!-- TODO le nombre d'élément pour connaitre l'inclusion, et ne retourner equals que s'il y a reellement égalité -->
							<!-- If AttributeSet2 and AttributeSet1 have an attribute with same value -->
							<xsl:when test="$AttributeSet1[1]=$AttributeSet2[name()=$FirstAttributeName]">
								<xsl:value-of select="$STR_Equals"/>
							</xsl:when>
							
							<!-- If AttributeSet2 and AttributeSet1 have an attribute with different value -->
							<xsl:otherwise>
								<xsl:value-of select="$STR_Differs"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					
					<!-- If AttributeSet1 has an attribute which is not in AttributeSet2 -->
					<xsl:otherwise>
						<xsl:value-of select="$STR_Includes"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>	

			<xsl:choose>
				<!-- If AttributeSet1 has only one element, or there is a difference between AttributeSet1 first attribute value and AttributeSet2 -->
				<xsl:when test="count($AttributeSet1)=1 or $FirstAttributeRelation=$STR_Differs">
					<!-- The result has already been computed -->
					<xsl:value-of select="$FirstAttributeRelation"/>
				</xsl:when>

				<xsl:otherwise>
					<xsl:variable name="RemainingAttributesRelation">
						<xsl:call-template name="GetAttributeSetRelation_MonoDir">
							<xsl:with-param name="AttributeSet1" select="$AttributeSet1[position()>1]"/>
							<xsl:with-param name="AttributeSet2" select="$AttributeSet2"/>
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:choose>
						<!-- If AttributeSet1 remaining attributes is different from AttributeSet2 -->
						<xsl:when test="$RemainingAttributesRelation=$STR_Differs">
							<xsl:value-of select="$STR_Differs"/>
						</xsl:when>
						
						<!-- If AttributeSet1 remaining attributes has the same relation with AttributeSet2 than FirstAttributeRelation -->
						<xsl:when test="$RemainingAttributesRelation=$FirstAttributeRelation">
							<xsl:value-of select="$FirstAttributeRelation"/>
						</xsl:when>
						
						<!-- Otherwise it means that AttributeSet1 includes AttributeSet2-->
						<xsl:otherwise>
							<xsl:value-of select="$STR_Includes"/>
						</xsl:otherwise>
					</xsl:choose>			
				</xsl:otherwise>
				
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
	        
</xsl:template>



<xsl:template name="GetFirstIdenticalNodePosition">
	<xsl:param name="NodeList"/>
	<xsl:param name="NodeEtalon"/>
	<xsl:param name="CurrentPosition" select="1"/>

	<xsl:variable name="NodeRelation">
		<xsl:call-template name="GetNodesRelation">
			<xsl:with-param name="Node1" select="$NodeList[1]"/>
			<xsl:with-param name="Node2" select="$NodeEtalon"/>
		</xsl:call-template>
	</xsl:variable>

    <xsl:choose>
        <!-- If first node in NodeList equals NodeEtalon -->
        <xsl:when test="$NodeRelation=$STR_Equals">
			<xsl:value-of select="$CurrentPosition"/>
        </xsl:when>

        <!-- If there is no left node in NodeList -->
        <xsl:when test="count($NodeList)=1">
			<xsl:text>0</xsl:text>
        </xsl:when>
        
        <!-- Else look for an identical node in remaining nodes -->
        <xsl:otherwise>
            <xsl:call-template name="GetFirstIdenticalNodePosition">
				<xsl:with-param name="NodeList" select="$NodeList[position()>1]"/>
				<xsl:with-param name="NodeEtalon" select="$NodeEtalon"/>
				<xsl:with-param name="CurrentPosition" select="number($CurrentPosition)+1"/>
            </xsl:call-template>
        </xsl:otherwise>
        
    </xsl:choose>
    	
</xsl:template>


<xsl:template name="BuildIdenticalNodesList">
	<xsl:param name="NodeList"/>
	<xsl:param name="NodeEtalon"/>

    <xsl:for-each select="$NodeList">
        <xsl:variable name="NodeRelation">
			<xsl:call-template name="GetNodesRelation">
                <xsl:with-param name="Node1" select="."/>
                <xsl:with-param name="Node2" select="$NodeEtalon"/>
			</xsl:call-template>
        </xsl:variable>
        
        <xsl:if test="$NodeRelation=$STR_Equals">
			<xsl:value-of select="$STR_Indent1"/>
			<xsl:copy-of select="."/>
			<xsl:value-of select="$gsEndl"/>
		</xsl:if>
    </xsl:for-each>
	
</xsl:template>




<!-- TODO valider ce template -->
<xsl:template name="Copy">
    <xsl:param name="Node"/>
    <xsl:param name="ExtraChildNodes" select="''"/>
    <xsl:param name="CopyAttributes" select="yes"/>
    <xsl:param name="CopyChildNodes" select="no"/>
    
    <xsl:choose>
		
		<!-- If we must copy attributes and child nodes -->
		<xsl:when test="$CopyAttributes='yes' and $CopyChildNodes='yes'">
			<xsl:copy-of select="$Node">
				<xsl:value-of select="$ExtraChildNodes"/>
			</xsl:copy-of>
		</xsl:when>
		
		<!-- If we must copy attributes only -->
		<xsl:when test="$CopyAttributes='yes'">
			<xsl:copy select="$Node">
				<xsl:for-each select="$Node/@*">	
					<xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
				</xsl:for-each>
				
				<xsl:value-of select="$ExtraChildNodes"/>
			</xsl:copy>
		</xsl:when>
		
		<!-- If we must copy child nodes only -->
		<xsl:when test="$CopyChildNodes='yes'">
			<xsl:copy select="$Node">
				<xsl:for-each select="$Node/*">	
					<xsl:copy-of select="."/>
				</xsl:for-each>
				
				<xsl:value-of select="$ExtraChildNodes"/>
			</xsl:copy>
		</xsl:when>
					
		<!-- If we must copy neither attributes nor child nodes -->
		<xsl:otherwise>
			<xsl:copy select="$Node">
				<xsl:value-of select="$ExtraChildNodes"/>
			</xsl:copy>
		</xsl:otherwise>
    </xsl:choose>
        
</xsl:template>


<xsl:template name="isDescendant">
	<xsl:param name="ansParentNode"/>
	<xsl:param name="ansDescendantNode"/>
	<xsl:param name="bOrSelf"/>
    
    <xsl:choose>
        <xsl:when test="generate-id($ansParentNode)=generate-id($ansDescendantNode)">
			<xsl:value-of select="$bOrSelf"/>
		</xsl:when>
        <xsl:when test="count($ansParentNode|$ansDescendantNode/ancestor::*)=count($ansDescendantNode/ancestor::*)">true</xsl:when>
        <xsl:otherwise>false</xsl:otherwise>
    </xsl:choose>
	
</xsl:template>


<xsl:template name="isAncestor">
	<xsl:param name="ansParentNode"/>
	<xsl:param name="ansDescendantNode"/>
	<xsl:param name="bOrSelf"/>

    <xsl:call-template name="isDescendant">
        <xsl:with-param name="ansParentNode" select="$ansParentNode"/>
        <xsl:with-param name="ansDescendantNode" select="$ansDescendantNode"/>
        <xsl:with-param name="bOrSelf" select="$bOrSelf"/>
    </xsl:call-template>	
	
</xsl:template>


</xsl:stylesheet>

