<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                              xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl">

<!-- Global variables -->
<xsl:variable name="gsEndl"><xsl:text>
</xsl:text></xsl:variable>
<xsl:variable name="STR_Indent"><xsl:text>    </xsl:text></xsl:variable>
<xsl:variable name="STR_Indent0"><xsl:text></xsl:text></xsl:variable>
<xsl:variable name="STR_Indent1"><xsl:value-of select="$STR_Indent0"/><xsl:value-of select="$STR_Indent"/></xsl:variable>
<xsl:variable name="STR_Indent2"><xsl:value-of select="$STR_Indent1"/><xsl:value-of select="$STR_Indent"/></xsl:variable>
<xsl:variable name="STR_Indent3"><xsl:value-of select="$STR_Indent2"/><xsl:value-of select="$STR_Indent"/></xsl:variable>
<xsl:variable name="STR_Indent4"><xsl:value-of select="$STR_Indent3"/><xsl:value-of select="$STR_Indent"/></xsl:variable>
<xsl:variable name="STR_Indent5"><xsl:value-of select="$STR_Indent4"/><xsl:value-of select="$STR_Indent"/></xsl:variable>
<xsl:variable name="gsLowerCaseDictionnary" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="gsUpperCaseDictionnary" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

<xsl:param name="STR_CurrentDate"><!--<xsl:message terminate="yes">Error! Please set STR_CurrentDate param</xsl:message>--></xsl:param>



<!-- Template to insert indentation -->
<xsl:template name="Indent">
    <xsl:param name="NbIndent"/>

    <xsl:if test="number($NbIndent)>0">
        <xsl:value-of select="$STR_Indent"/>
    </xsl:if>
    
    <xsl:if test="number($NbIndent)>1">
        <xsl:call-template name="Indent">
            <xsl:with-param name="NbIndent" select="number($NbIndent)-1"/>
        </xsl:call-template>
    </xsl:if>
        
</xsl:template>


<!-- Template to insert indentation -->
<xsl:template name="incIndent">
    <xsl:param name="asIndent"/>

    <xsl:value-of select="$asIndent"/><xsl:value-of select="$STR_Indent"/>
        
</xsl:template>


<xsl:template name="upperCase">
    <xsl:param name="asString"/>
    <xsl:param name="auiFirstChar" select="1"/>
    <xsl:param name="aiNbChar" select="-1"/>

    <xsl:call-template name="translate">
        <xsl:with-param name="asString" select="$asString"/>
        <xsl:with-param name="auiFirstChar" select="$auiFirstChar"/>
        <xsl:with-param name="aiNbChar" select="$aiNbChar"/>
        <xsl:with-param name="asDicoSrc" select="$gsLowerCaseDictionnary"/>
        <xsl:with-param name="asDicoDest" select="$gsUpperCaseDictionnary"/>
    </xsl:call-template>
        
</xsl:template>


<xsl:template name="lowerCase">
    <xsl:param name="asString"/>
    <xsl:param name="auiFirstChar" select="1"/>
    <xsl:param name="aiNbChar" select="-1"/>

    <xsl:call-template name="translate">
        <xsl:with-param name="asString" select="$asString"/>
        <xsl:with-param name="auiFirstChar" select="$auiFirstChar"/>
        <xsl:with-param name="aiNbChar" select="$aiNbChar"/>
        <xsl:with-param name="asDicoSrc" select="$gsUpperCaseDictionnary"/>
        <xsl:with-param name="asDicoDest" select="$gsLowerCaseDictionnary"/>
    </xsl:call-template>
        
</xsl:template>

<!-- Generic translation template -->
<xsl:template name="translate">
    <xsl:param name="asString"/>
    <xsl:param name="auiFirstChar" select="1"/>
    <xsl:param name="aiNbChar" select="-1"/>
    <xsl:param name="asDicoSrc"/>
    <xsl:param name="asDicoDest"/>

    <xsl:variable name="uiStrLen" select="string-length($asString)"/>
    <xsl:variable name="uiNbChar">
        <xsl:choose>
            <xsl:when test="number($aiNbChar)&lt;0">
                <xsl:value-of select="$uiStrLen"/>
            </xsl:when>

            <xsl:otherwise>
                <xsl:value-of select="$aiNbChar"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="sPrefix" select="substring($asString, number('1'), number($auiFirstChar) - number('1'))"/>
    <xsl:variable name="sMiddle" select="substring($asString, number($auiFirstChar), number($uiNbChar))"/>
    <xsl:variable name="sSuffix" select="substring($asString, number($auiFirstChar) + number($uiNbChar))"/>

    <xsl:value-of select="$sPrefix"/>
    <xsl:value-of select="translate($sMiddle, $asDicoSrc, $asDicoDest)"/>
    <xsl:value-of select="$sSuffix"/>
        
</xsl:template>


<xsl:template name="repeatText">
    <xsl:param name="asText"/>
    <xsl:param name="auiNbTimes"/>

    <xsl:if test="number($auiNbTimes) &gt;= 1">
        <xsl:value-of select="$asText"/>

        <xsl:call-template name="repeatText">
            <xsl:with-param name="asText" select="$asText"/>
            <xsl:with-param name="auiNbTimes" select="number($auiNbTimes)-1"/>
        </xsl:call-template>
    </xsl:if>
        
</xsl:template>


<xsl:template name="printPaddledText">
    <xsl:param name="asText"/>
    <xsl:param name="ansTextSet"/>
    <xsl:param name="abPaddleRight" select="'true'"/>
    <xsl:param name="abAlignmentOnly" select="'false'"/>
    <xsl:param name="auiExtraTextLength" select="0"/>

    <xsl:variable name="uiMaxTextLength">
        <xsl:call-template name="getMaxTextLength">
            <xsl:with-param name="ansTextSet" select="$ansTextSet"/>
        </xsl:call-template>
    </xsl:variable>
    <!-- <xsl:message>uiMaxTextLength=<xsl:value-of select="$uiMaxTextLength"/></xsl:message> -->
    <xsl:variable name="uiTextLength" select="string-length($asText)"/>

    <!-- <xsl:message>repeatText(' ', <xsl:value-of select="$uiMaxTextLength + $auiExtraTextLength - $uiTextLength"/>)</xsl:message> -->
    <xsl:if test="($abAlignmentOnly = 'false') and ($abPaddleRight = 'true')">
        <xsl:value-of select="$asText"/>
    </xsl:if>
    <xsl:call-template name="repeatText">
        <xsl:with-param name="asText" select="' '"/>
        <xsl:with-param name="auiNbTimes" select="$uiMaxTextLength + $auiExtraTextLength - $uiTextLength"/>
    </xsl:call-template>
    <xsl:if test="($abAlignmentOnly = 'false') and ($abPaddleRight = 'false')">
        <xsl:value-of select="$asText"/>
    </xsl:if>

</xsl:template>


<xsl:template name="getMaxTextLength">
    <xsl:param name="ansTextSet"/>
    <xsl:param name="auiCurrentMaxLength" select="0"/>

    <xsl:choose>
        <xsl:when test="count($ansTextSet) = 0">
            <xsl:value-of select="$auiCurrentMaxLength"/>
        </xsl:when>

        <xsl:when test="string-length($ansTextSet[1]) &gt; $auiCurrentMaxLength">
            <xsl:call-template name="getMaxTextLength">
                <xsl:with-param name="ansTextSet" select="$ansTextSet[position() &gt; 1]"/>
                <xsl:with-param name="auiCurrentMaxLength" select="string-length($ansTextSet[1])"/>
            </xsl:call-template>
        </xsl:when>

        <xsl:otherwise>
            <xsl:call-template name="getMaxTextLength">
                <xsl:with-param name="ansTextSet" select="$ansTextSet[position() &gt; 1]"/>
                <xsl:with-param name="auiCurrentMaxLength" select="$auiCurrentMaxLength"/>
            </xsl:call-template>
        </xsl:otherwise>

    </xsl:choose>
</xsl:template>


<xsl:template name="nodesetToString">
    <xsl:param name="ansNodeSet"/>
    <xsl:param name="asSeparator"/>

    <xsl:for-each select="$ansNodeSet">
        <xsl:value-of select="."/>
        <xsl:if test="position() != last()">
            <xsl:value-of select="$asSeparator"/>
        </xsl:if>
    </xsl:for-each>
</xsl:template>


<xsl:template name="stringToXml">
    <xsl:param name="asText"/>
    <xsl:param name="asSeparator"/>

    <xsl:variable name="sResultXml">
        <xsl:choose>
            <xsl:when test="string-length($asText) = 0"/>
    
            <xsl:when test="contains($asText, $asSeparator)">
                <substring>
                    <xsl:value-of select="substring-before($asText, $asSeparator)"/>
                </substring>
        
                <xsl:call-template name="stringToXml">
                    <xsl:with-param name="asText" select="substring-after($asText, $asSeparator)"/>
                    <xsl:with-param name="asSeparator" select="$asSeparator"/>
                </xsl:call-template>
            </xsl:when>
    
            <xsl:otherwise>
                <substring>
                    <xsl:value-of select="$asText"/>
                </substring>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:copy-of select="$sResultXml"/>
</xsl:template>

</xsl:stylesheet>
<!-- 


<xsl:template match="/">
  <xsl:value-of select="translate(doc, $smallcase, $uppercase)" />
</xsl:template> -->