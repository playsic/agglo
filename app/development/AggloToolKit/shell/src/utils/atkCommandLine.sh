############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################


NONE="ATK_NONE"


function executeStandardBehaviors()
{
    local iResult=0

    parseCommandLine $@
    
    if  [ $(isFlagSet "help") == "true" ]; then
        # Display help
        sHelp=$(getHelp)
        echo -e $sHelp
        iResult=2
    elif  [ $(isFlagSet "register-completion") == "true" ]; then
        # TODO Register Command, ainsi que l'auto enregistrement
        registerCommandLine "./testCommandLine.sh"
        # registerCommandLine "$0"
        iResult=1
    else
        # If there is an error in command line
        sErrorMessage=$(checkCommandLine)
        if [ -n "$sErrorMessage" ]; then
            # Display error message
            echo -e $sErrorMessage
            iResult=3
        fi
    fi

    return $iResult
}


function getCommandFlags()
{
    sResult=""

    for sCurrentFlag in @; do
        # TODO attention le flag peut etre sette plusieurs fois
        if [ $(isFlagSet $sCurrentFlag) == "true" ]; then
            sResult=$sResult" $sCurrentFlag $(getFlagValue $sCurrentFlag)"
        fi
    done

    echo $sResult
}


function isValueExpected()
{
    asFlag=$1
    getDeclaredParam $asFlag
    iDeclaredParam=$?
    
    if [ $iDeclaredParam -eq 0 ]; then
        sResult="error"
    else
        sResult=${arFlagHasValue[iDeclaredParam]}
    fi

    echo $sResult
}


# function completeCommandLine()
# {
#     # local cmd="${1##*/}"
#     local word=${COMP_WORDS[COMP_CWORD]}
#     local line=${COMP_LINE}
#     local i=1
#     sValueCompletion=""
#     sFlagCompletion=""

#     # Parse seized command arguments
#     params=("${COMP_WORDS[@]}")
#     unset params[0]
#     params=("${params[@]}")
#     # echo "$params"
#     resetValues
#     parseCommandLine ${params[@]}
#     # echo "parseCommandLine done"

#     # Retrieve last flag
#     iFlagCount=${#gsFlags[*]}
#     sLastFlag=${gsFlags[iFlagCount]}
#     sLastValue=${gsFlagsValue[iFlagCount]}
#     # echo "Flag$sLastFlag""Value$sLastValue""Word$word"FIN

#     # If last flag expects a value
#     bIsValueExpected=$(isValueExpected $sLastFlag)
#     # echo "$bIsValueExpected"
#     if [ $bIsValueExpected == "true" ] && [[ ($NONE == "$sLastValue") || ("$word" != "") ]]; then
#       # Generate value completion
#       sValueCompletion="-f -u"
#     fi
#     # If a new flag can be seized ( last flag doesn't expect a value, or last value is not nul)
#     if [ $bIsValueExpected != "true" ] || [ $NONE != "$sLastValue" ]; then

#         # for index in "${!mon_tableau[@]}
#         i=1
#         for asCurrentDefinedFlag in "${arDefinedFlags[@]}"; do
#             sCurrentFlag=($asCurrentDefinedFlag)
#             sCurrentFlagShort=${sCurrentFlag[0]}
#             sCurrentFlagDetailed=${sCurrentFlag[1]}
#             iMinimumArity=${arFlagsMinArity[i]}
#             iMaximumArity=${arFlagsMaxArity[i]}
#             # echo "count"
#             (countFlag $sCurrentFlagShort)
#             iCurrentFlagCount=$?
#             # echo "flag completion $asCurrentDefinedFlag $sCurrentFlag $iMinimumArity $iMaximumArity $iCurrentFlagCount"
            
#             if [ $iCurrentFlagCount -lt $iMaximumArity ]; then
#                 sFlagCompletion=$sFlagCompletion" -$sCurrentFlagShort --$sCurrentFlagDetailed"
#             fi
            
#             i=$((i + 1))
#         done
#     fi

#     COMPREPLY=($(compgen -W "$sFlagCompletion" $sValueCompletion -- "${word}"))
# }

function registerCommandLine()
{
    sCommandName="testCommandLine"
    sCompletionFile="$sCommandName"
    sCompletionFile="completion"
    sCompletionFile="/usr/share/bash-completion/completions/$sCommandName"

    # Create completion file
    touch $sCompletionFile
    # chmod +x $sCompletionFile


    # echo "#!/bin/bash" > $sCompletionFile
    echo 'arDefinedFlags=("h help" "s sources-path" "t target" "r register")' > $sCompletionFile
    # echo 'arDefinedFlags=('${arDefinedFlags[@]}')' > $sCompletionFile
    echo 'arFlagHasValue=('${arFlagHasValue[@]}')' >> $sCompletionFile
    echo 'arFlagsMaxArity=('${arFlagsMaxArity[@]}')' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo 'function _complete_'$sCommandName'('')' >> $sCompletionFile
    echo '{' >> $sCompletionFile

    echo '    word=${COMP_WORDS[COMP_CWORD]}' >> $sCompletionFile
    echo '    local i=1' >> $sCompletionFile
    echo '    sValueCompletion=""' >> $sCompletionFile
    echo '    sFlagCompletion=""' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo '    # Parse seized command arguments' >> $sCompletionFile
    echo '    params=("${COMP_WORDS[@]}")' >> $sCompletionFile
    echo '    unset params[0]' >> $sCompletionFile
    echo '    params=("${params[@]}")' >> $sCompletionFile
    echo '    parseCommandLine ${params[@]}' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo '    # Retrieve last flag' >> $sCompletionFile
    echo '    iFlagCount=${#gsFlags[*]}' >> $sCompletionFile
    echo '    sLastFlag=${gsFlags[iFlagCount]}' >> $sCompletionFile
    echo '    sLastValue=${gsFlagsValue[iFlagCount]}' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo '    # If last flag expects a value' >> $sCompletionFile
    echo '    bIsValueExpected=$(isValueExpected $sLastFlag)' >> $sCompletionFile
    # echo '    echo $bIsValueExpected' >> $sCompletionFile
    echo '    if [ $bIsValueExpected == "true" ] && [[ ($NONE == "$sLastValue") || ("$word" != "") ]]; then' >> $sCompletionFile
    echo '        # Generate value completion' >> $sCompletionFile
    echo '        sValueCompletion="-f -u"' >> $sCompletionFile
    echo '    fi' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo '    # If a new flag can be seized ( last flag doesn t expect a value, or last value is not nul)' >> $sCompletionFile
    echo '    if [ $bIsValueExpected != "true" ] || [ $NONE != "$sLastValue" ]; then' >> $sCompletionFile
    # for index in "${!mon_tableau[@]}
    # TODO pourquoi 0 alors que le code d'origine est a 1?
    echo '        i=0' >> $sCompletionFile
    echo '        for asCurrentDefinedFlag in "${arDefinedFlags[@]}"; do' >> $sCompletionFile
    echo '            sCurrentFlag=($asCurrentDefinedFlag)' >> $sCompletionFile
    echo '            sCurrentFlagShort=${sCurrentFlag[0]}' >> $sCompletionFile
    echo '            sCurrentFlagDetailed=${sCurrentFlag[1]}' >> $sCompletionFile
    echo '            iMaximumArity=${arFlagsMaxArity[i]}' >> $sCompletionFile
    echo '            (countFlag $sCurrentFlagShort)' >> $sCompletionFile
    echo '            iCurrentFlagCount=$?' >> $sCompletionFile
    echo '' >> $sCompletionFile
            
    echo '            if [ $iCurrentFlagCount -lt $iMaximumArity ]; then' >> $sCompletionFile
    echo '                sFlagCompletion=$sFlagCompletion" -$sCurrentFlagShort --$sCurrentFlagDetailed"' >> $sCompletionFile
    echo '            fi' >> $sCompletionFile
    echo '' >> $sCompletionFile
            
    echo '            i=$((i + 1))' >> $sCompletionFile
    echo '        done' >> $sCompletionFile
    echo '    fi' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo '    COMPREPLY=($(compgen -W "$sFlagCompletion" $sValueCompletion -- "${word}"))' >> $sCompletionFile
    echo '}' >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo 'complete -F _complete_'$sCommandName' '$sCommandName'.sh' >> $sCompletionFile
    echo '' >> $sCompletionFile

    cat ./atkCommandLine.sh >> $sCompletionFile
    echo '' >> $sCompletionFile

    cat ./atkString.sh >> $sCompletionFile
    echo '' >> $sCompletionFile

    echo source $sCompletionFile
    source $sCompletionFile
}


# TODO ajouter completion et declaration param, avec arity mandatory, option, hasvalue et aide, et check automatique
function parseCommandLine()
{
    local iCurrentFlagIdx=1
    local iCurrentValueIdx=0
    
    # Reset values stored by previous command call
    resetValues

    for sParam in "$@"
    do
        # Retrieve flag and/or value from current parameter
        sCurrentFlag=$(getParsedFlag "$sParam")
        sCurrentValue=$(getParsedFlagValue "$sParam")
        
        # If current param contains a flag
        if [ $NONE != "$sCurrentFlag" ]; then
            gsFlags[iCurrentFlagIdx]=$sCurrentFlag
            iCurrentFlagIdx=$((iCurrentFlagIdx + 1))
            iCurrentValueIdx=$((iCurrentValueIdx + 1))
        fi
        # # If current param contains a value, and current value is set
        # if [ "NONE" != "$sCurrentValue" ] && [ $iCurrentFlagIdx -gt 0 ] ; then
        # If there is a flag being set
        if [ $iCurrentFlagIdx -gt 0 ] ; then
            gsFlagsValue[iCurrentValueIdx]=$sCurrentValue
        fi
    done
}


function defineParam()
{
    local asFlags=$1
    local asHelp=$2
    local abHasValue="false"
    local aiFlagMinArity=0
    local aiFlagMaxArity=1
    iNextParamIdx=$((${#arDefinedFlags[@]} + 1))

    if [ $iNextParamIdx -eq 1 ] && [ "$asFlags" != "h help" ]; then
        defineParam "h help" "Display this help"
        iNextParamIdx=$((iNextParamIdx + 1))
    fi    

    if [ $iNextParamIdx -eq 2 ] && [ "$asFlags" != "R register-completion" ]; then
        defineParam "R register-completion" "Register completion for this command"
        iNextParamIdx=$((iNextParamIdx + 1))
    fi    

    # If has value property is given
    if [ $# -gt 2 ]; then
        # Retrieve has value property from function parameter
        abHasValue=$3
    fi
    # If minimum flag arity is given
    if [ $# -gt 3 ]; then
        # Retrieve minimum flag arity from function parameter
        aiFlagMinArity=$4
    fi
    # If maximum flag arity is given
    if [ $# -gt 4 ]; then
        # Retrieve maximum flag arity from function parameter
        aiFlagMaxArity=$5
    # If maximum flag arity is not given
    else
        # If minimum flag arity is more than 0
        if [ $aiFlagMinArity -gt 0 ]; then
            # Maximum flag arity is equal to minimum arity (i.e. no optional flag)
            aiFlagMaxArity=$aiFlagMinArity
        fi
        # If minimum flag arity is 0, maximum flag arity is 1  (i.e. one optional flag)
    fi
    
    arDefinedFlags[$iNextParamIdx]=$asFlags
    arFlagHasValue[$iNextParamIdx]=$abHasValue
    arHelps[$iNextParamIdx]=$asHelp
    arFlagsMinArity[$iNextParamIdx]=$aiFlagMinArity
    arFlagsMaxArity[$iNextParamIdx]=$aiFlagMaxArity
}


function checkCommandLine()
{
    local sResult=""
    local i=1

    for sCurrentFlag in "${gsFlags[@]}"; do
        getDeclaredParam $sCurrentFlag
        iDeclaredParam=$?
        
        if [ $iDeclaredParam -eq 0 ]; then
            sResult=$sResult"- unknown flag: $sCurrentFlag\n"
        else
            sCurrentFlagValue=${gsFlagsValue[i]}
            bHasValue=${arFlagHasValue[iDeclaredParam]}
            
            if [ $NONE == "$sCurrentFlagValue" ] && [ "true" == $bHasValue ]; then
                sResult=$sResult"- flag $sCurrentFlag should have a value\n"
            elif [ $NONE != "$sCurrentFlagValue" ] && [ "true" != $bHasValue ]; then
                sResult=$sResult"- flag $sCurrentFlag shouldn't have a value\n"
            fi
        fi
        
        i=$((i + 1))
    done


    # TODO changer la syntaxe avec for index in "${!mon_tableau[@]}
    i=1
    for asCurrentDefinedFlag in "${arDefinedFlags[@]}"; do
        sCurrentFlag=($asCurrentDefinedFlag)
        sCurrentFlag=${sCurrentFlag[0]}
        iMinimumArity=${arFlagsMinArity[i]}
        iMaximumArity=${arFlagsMaxArity[i]}
        (countFlag $sCurrentFlag)
        iCurrentFlagCount=$?
        
        if [ $iCurrentFlagCount -lt $iMinimumArity ]; then
            sResult=$sResult"- flag $asCurrentDefinedFlag should be defined at least $iMinimumArity time(s)\n"
        elif [ $iCurrentFlagCount -gt $iMaximumArity ]; then
            sResult=$sResult"- flag $asCurrentDefinedFlag should be defined $iMaximumArity time(s) at most\n"
        fi
        
        i=$((i + 1))
    done
    
    if [ -n "$sResult" ]; then
        sResult="errors found: \n"$sResult"\nuse -h|--help to display command's help\n"
    fi
    echo $sResult
}


function resetCommand()
{
    unset arDefinedFlags
    unset arFlagHasValue
    unset arHelps
    unset arFlagsMinArity
    unset arFlagsMaxArity
}


function resetValues()
{
    unset gsFlags
    unset gsFlagsValue
}


function getHelp()
{
    local sResult=""
    local i=1
    
    # TODO generate command line
    for asCurrentDefinedFlag in "${arDefinedFlags[@]}"; do
        sResult=$sResult"- $asCurrentDefinedFlag: "${arHelps[i]}"\n"
        i=$((i + 1))
    done
    
    echo $sResult
}


function isFlagSet()
{
    local asFlagName=$1
    local sResult="true"
    
    getFlagIndex $asFlagName
    iFlagIndex=$?
    
    if [ $iFlagIndex -eq 0 ]; then
        sResult="false"
    fi
    
    echo $sResult
}


function getFlagValue()
{
    local asFlagName=$1
    
    getFlagIndex $asFlagName
    iFlagIndex=$?
    if [ $iFlagIndex -ge 0 ]; then
        sResult=${gsFlagsValue[iFlagIndex]}
    fi
    
    echo $sResult
}


function getFlagIndex()
{
    local asFlagName=$1
    local aiFirstIndex=1
    local iResult=0
    local i=1
    local iSearchedParam=0

    if [ $# -gt 1 ]; then
        aiFirstIndex=$2
    fi
    
    getDeclaredParam $asFlagName
    iSearchedParam=$?

    for sCurrentFlag in "${gsFlags[@]}"; do
        if [ $i -ge $aiFirstIndex ]; then
            getDeclaredParam $sCurrentFlag
            iCurrentParam=$?
            
            if [ "$asFlagName" == "$sCurrentFlag" ] || ([ $iSearchedParam -ne 0 ] && [ $iSearchedParam -eq $iCurrentParam ]); then
                iResult=$i
                break
            fi
        fi
        
        i=$((i + 1))
    done
    
    return $iResult
}


function countFlag()
{
    local asFlagName=$1
    local iResult=0
    local iFoundFlagIndex=1
    
    while [ $iFoundFlagIndex -gt 0 ]; do
        local iFirstIndex=$iFoundFlagIndex
        
        getFlagIndex $asFlagName $iFirstIndex
        iFoundFlagIndex=$?
        
        if [ $iFoundFlagIndex -gt 0 ]; then
            iResult=$((iResult + 1))
            iFoundFlagIndex=$((iFoundFlagIndex + 1))
        fi
    done
    
    return $iResult
    
}

function parseParam()
{
    local asParam=$1
    local sFlag=""
    local sFlagValue=""
    local sFlagStart="-"
    local sFlagValueStart="="
    local bHasValue="true"
    
    # If param starts with flag char
    if [ $sFlagStart == "${asParam:0:1}" ]; then
        # Remove "-" chars from flag
        asParam=${asParam##$sFlagStart}
        asParam=${asParam##$sFlagStart}
        
        # Look for a "=" char in param
        findString "$asParam" "$sFlagValueStart"
        iValueStart=$?
        
        # If param contains a value along with flag
        if [ $iValueStart -gt 0 ]; then
            iFlagEnd=$((iValueStart - 1))
            sFlag=FLAG${asParam:0:$iFlagEnd}
            sFlagValue=VALUE${asParam:$iValueStart}
        else 
            sFlag=FLAG$asParam
        fi
    else
        # Param is only a flag value
        sFlagValue=VALUE$asParam
    fi

    echo $sFlag$sFlagValue
}


function getParsedFlag()
{
    local asParam=$1
    local sResult=$NONE
    
    # Retrieve flag and/or value from current parameter
    sParsedParam=$(parseParam "$asParam")
    findString "$sParsedParam" "FLAG"
    iFlagStart=$?
    findString "$sParsedParam" "VALUE"
    iValueStart=$?
    
    # If param contains a flag
    if [ $iFlagStart -gt 0 ]; then
        if [ $iValueStart -gt 0 ]; then
            iFlagEnd=$((iValueStart - 1))
        else
            # TODO utiliser strLength
            iFlagEnd=$((${#sParsedParam}))
        fi
        iFlagLength=$((iFlagEnd - 4))
        
        sResult=${sParsedParam:4:$iFlagLength}
    fi
    
    echo $sResult
}


function getParsedFlagValue()
{
    local asParam=$1
    local sResult=$NONE

    # Retrieve or value from current parameter
    sParsedParam=$(parseParam "$asParam")
    findString "$sParsedParam" "VALUE"
    iValueStart=$?
        
    # If param contains a value
    if [ $iValueStart -gt 0 ]; then
        iValueStart=$((iValueStart + 4))
        sResult=${sParsedParam:$iValueStart}
    fi
    
    echo $sResult
}


function getDeclaredParam()
{
    local asFlag=$1
    local iResult=0
    local i=1
    
    # TODO les flags devraient etre separe par des | plutot que des " " : "h|help" au lieu de "h help"
    for asCurrentDefinedFlag in "${arDefinedFlags[@]}"; do
        asCurrentDefinedFlag=($asCurrentDefinedFlag)
        
        for asCurrentFlag in "${asCurrentDefinedFlag[@]}"; do
            if [ "$asCurrentFlag" == "$asFlag" ]; then
                iResult=$i
                break
            fi
        done
        if [ $iResult -ne 0 ]; then
            break
        fi
        
        i=$((i + 1))
    done
    
    return $iResult
}
