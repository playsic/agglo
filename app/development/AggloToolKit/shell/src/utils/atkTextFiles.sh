############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################


# isUTF8 PathFileIn
function isUTF8()
{
    PathFileIn=$1
    
    Path=$(getPath "$PathFileIn")
    NameFileIn=$(getFileName "$PathFileIn")
    isUTF8=1

    # Check with file command if $PathFileIn is UTF-8
    for currentFile in $(file $Path/* | grep "UTF-8" | grep "$NameFileIn" | cut -d":" -f1)
    do
        isUTF8=0
        break
    done

    return $isUTF8
}


# isISO8859 PathFileIn
function isISO8859()
{
    PathFileIn=$1
    Path=$(getPath "$PathFileIn")
    NameFileIn=$(getFileName "$PathFileIn")
    is8859=1

    # Check with file command if $PathFileIn is ISO-8859
    for currentFile in $(file $Path/* | grep "ISO-8859" | grep "$NameFileIn" | cut -d":" -f1)
    do
        is8859=0
        break
    done

    return $is8859
}

# isText PathFileIn
function isText()
{
    PathFileIn=$1
    Path=$(getPath "$PathFileIn")
    NameFileIn=$(getFileName "$PathFileIn")
    isText=1

    # Check with file command is $PathFileIn is text
    for currentFile in $(file $Path/* | grep "text" | grep "$NameFileIn" | cut -d":" -f1)
    do
        isText=0
        break
    done

    return $isText
}

# isText PathFileIn
function isBinaryText()
{
    PathFileIn=$1
    Extension=$(getFileExtension "$PathFileIn")
    isBinaryText=1

    # Check with file command is $PathFileIn is text
    if [ $Extension = "doc" ] || [ $Extension = "pdf" ];
    then
        isBinaryText=0
    fi

    return $isBinaryText
}

# recodeText PathFileIn PathFileOut
function recodeText()
{
    PathFileIn=$1
    PathFileOut=$2
    
    FileNameNoExt=$(getFileNameNoExtension "$PathFileIn")
    Extension=$(getFileExtension "$PathFileIn")

    # Convert PathFileIn to a text file
    if [ $Extension = "doc" ];
    then
        soffice --headless -convert-to rtf "$PathFileIn" --outdir "$TempDir" &>/dev/null
        sleep 10
        mv "$TempDir/$FileNameNoExt.rtf" "$PathFileOut"
        recodeUTF8 "$PathFileOut" "$PathFileOut"
    #~ elif [ $Extension = "pdf" ];
    #~ then
        #~ echo pdftotxt "$PathFileIn" > $PathFileOut
    fi
}


# recodeUTF8 PathFileIn PathFileOut
function recodeUTF8()
{
    PathFileIn=$1
    PathFileOut=$(echo $2 | sed 's/ /\\ /g')

    # Convert NameFileIn (ISO-8859 in a copy NameFileOut (UTF-8)
    vim +"se bomb | se fileencoding=utf-8 | wq! $PathFileOut" "$PathFileIn"
}


