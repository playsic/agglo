############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################

# Global variables
sThisShellPath=${0%/*}
source $sThisShellPath/../utils/atkFileSystem.sh


# Check the parameters
ParamOK=true
if [ $# != 3 ];
then
    echo "Command format : atkComputePatch asOriginalSourcePathOrArchiveFile asPatchedSourcePath asPatchOutputPath"
    echo "example : ../atkComputePatch.sh './OriginalArchive.zip' './PatchedSources' '../Patch'"
    ParamOK=false
fi

if $ParamOK ;
then
    # Init parameters
    asOriginalSourcePathOrArchiveFile=$1
    asPatchedSourcePath=$2
    # PatchedSourcePathSed=$(echo "$asPatchedSourcePath"|sed s/'\/'/'\\'/g)
    asPatchOutputPath=$3
    
    sOriginalSourcePath=$asOriginalSourcePathOrArchiveFile
    
    IFS=$'\n'

    for sCurrentFile in $(diff --new-file --brief --recursive --strip-trailing-cr "$sOriginalSourcePath" "$asPatchedSourcePath"|grep "Seulement\|diff.rents"|grep "$asPatchedSourcePath")
    # for sCurrentFile in $(diff --new-file --brief --recursive "$sOriginalSourcePath" "$asPatchedSourcePath"|grep "Seulement\|différents"|grep "$asPatchedSourcePath")
    do
        # TODO faire plutot une branche sed et renommer sCurrentFile en sPatchedFile
        # TODO quid des fichiers/reperoires existant uniquement d'un cote?
        # TODO quid des reperoires vide d'un cote ou de l'autre?
        sSourceFile=$(echo "$sCurrentFile"|sed s#".*\($sOriginalSourcePath.*\) et $asPatchedSourcePath.*"#'\1'#)
        sPatchedFile=$(echo "$sCurrentFile"|sed s#".*\($asPatchedSourcePath.*\) sont diff.*"#'\1'#)
        if [ $sPatchedFile == $sCurrentFile ];
        then
            sPatchedFile=$(echo "$sCurrentFile"|sed s#".*\($asPatchedSourcePath.*\): \(.*\)"#'\1\2'#)
        fi
        
        if [ "$(exists $sPatchedFile)" == "true" ]; then
        	sCurrentDiffName=$(getFileName "$sPatchedFile").diff
        	sPathFromTo=$(getPathFromTo "$asPatchedSourcePath" "$sPatchedFile" "true")

        	# Create directory with all intermediaries directories
        	mkdir -p "$asPatchOutputPath/$sPathFromTo"
        	
            # Convert original and patched file in unix format to avoid diff errrors
            # dos2unix --keepdate --quiet "$sSourceFile" "$sSourceFile"
            # dos2unix --keepdate --quiet "$sPatchedFile" "$sPatchedFile"
            
        	# Create diff file
        	diff --new-file --unified --strip-trailing-cr "$sSourceFile" "$sPatchedFile" > "$asPatchOutputPath/$sPathFromTo/$sCurrentDiffName"
            
            # If the diff file is empty, remove it. This case can occur if the only difference between original and patched file was the dos/unix format
            # iDiffFileSize=$(stat -c "%s" $asPatchOutputPath/$sPathFromTo/$sCurrentDiffName)
            # if [ $iDiffFileSize == 0 ]; then
                # rm $asPatchOutputPath/$sPathFromTo/$sCurrentDiffName
            # fi
        fi
    done    
    unset IFS
    
    # If the script has unzipped archive
    if [ "$bIsZip" == "true" ]; then
        # Remove unzipped archive
        rm -rf $sOriginalSourcePath
    fi

fi
