############################################################################
## 
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
## 
## This file is part of the Agglo project.
## 
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in 
## accordance with the commercial license agreement provided with the 
## Software or, alternatively, in accordance with the terms contained in 
## a written agreement between you and Plaisic.  For licensing terms and 
## conditions contact eti.laurent@gmail.com.
## 
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
## 
## In addition, the following conditions apply: 
##     * Redistributions in binary form must reproduce the above copyright 
##       notice, this list of conditions and the following disclaimer in 
##       the documentation and/or other materials provided with the 
##       distribution.
##     * Neither the name of the Agglo project nor the names of its  
##       contributors may be used to endorse or promote products derived 
##       from this software without specific prior written permission.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## AGGLO_END_LICENSE
## 
############################################################################


# Global variables
#equivalent $(dirname $0)
ShellPath=${0%/*}
source $ShellPath/../utils/atkFileSystem.sh


function applyPatch()
{
    asPatchPath="$1"
    asPatchedSourcesOutputPath="$2"

    #  If patched directory doesnt' exist
    if [ "$(exists $asPatchedSourcesOutputPath)" != "true" ];
    then
        # Create patched directory
        mkdir -p "$asPatchedSourcesOutputPath"
    fi

    # Iterate on input files list
    IFS=$'\n'
    for sCurrentDiff in $(find $asPatchPath -name *.diff -maxdepth 1 -type f -prune 2>/dev/null)
    do
        sPatchFileName=$(getFileName "$sCurrentDiff")
		sPatchedFileName=$(echo "$sPatchFileName"|sed s/"\.diff"//)
        sPatchedFileCompleteName="$asPatchedSourcesOutputPath/$sPatchedFileName"
        
        #  If patched file doesnt' exist
        if [ "$(exists $sPatchedFileCompleteName)" != "true" ]; then
            # Create patched file
            touch "$sPatchedFileCompleteName"
        else
            # Make sure the patched file is in unix format, or patch will fail
            dos2unix --keepdate --quiet "$sPatchedFileCompleteName" "$sPatchedFileCompleteName"
        fi
		
        # Apply patch
        patch --quiet --directory="$asPatchedSourcesOutputPath" "$sPatchedFileName" < "$sCurrentDiff"
    done

	# Iterate on sub directories (watch out, there can be a space in sub directories name)
	for sCurrentDir in $(ls -d $asPatchPath/*/ 2>/dev/null | sed 's/\/$//g')
	do
        # Invoke applyPatch in a subshell, to avoid recursive method call to modify local variables
		sCurrentDirName=$(getLastItem "$sCurrentDir")
        (applyPatch "$sCurrentDir" "$asPatchedSourcesOutputPath/$sCurrentDirName")
	done

	unset IFS
}


# Check the parameters
bParamOK=true
if [ $# != 2 ] && [ $# != 3 ];
then
    echo "Command format : atkApplyPatch asPatchPath asPatchedSourcesOutputPath asOriginalSourcePathOrArchiveFile "
    echo "example : ../atkApplyPatch.sh '../Patch' './PatchedSources' './OriginalArchive.zip'"
    bParamOK=false
fi


if $bParamOK ;
then
    # Init parameters
    asPatchPath=$1
    asPatchedSourcesOutputPath=$2
    asOriginalSourcePathOrArchiveFile=$3
    
    # Copy asOriginalSourcePathOrArchiveFile in asPatchedSourcesOutputPath
	if [ "$asOriginalSourcePathOrArchiveFile" != "" ]; then
        rm -rf "$asPatchedSourcesOutputPath"
        
        cp -R "$asOriginalSourcePathOrArchiveFile" "$asPatchedSourcesOutputPath"
	fi

    # Apply asPatchPath patches recursively
    applyPatch "$asPatchPath" "$asPatchedSourcesOutputPath"
fi

