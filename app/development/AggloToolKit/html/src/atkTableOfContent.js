$(document).ready(function() {
    var previousLinkBottom = 0;
    var asideTocTopMargin = 10;
    
    $("h1, h2, h3, h4").each(function(i)
    {
        var current = $(this);
        var linkClass="' class='smoothScroll " + current.attr("tagName");
        // TODO adapter par rapport a la taille du document, pas du content, et virer #content
        var linkTop = current.position().top / $("#content").height() * ($(window).height() - 2 * asideTocTopMargin);

        current.attr("id", "title" + i);
        if ($(current).hasClass( "Numeroted"))
        {
            linkClass=linkClass + " Numeroted";
        }
        
        // TODO ajouter le title avec le debut du paragrape
        if (current.attr("tagName") != "H4")
        {
            $("#toc").append("<a id='link" + i + "' href='#title" + i + linkClass + "'>" + current.html() + "</a>");
            
            // If new link top  position starts before bottom of previous link
            if (linkTop < previousLinkBottom)
            {
                // New link top position is forced to previous link bottom position
                linkTop = previousLinkBottom;
            }
            $("#link" + i).css("top", linkTop + asideTocTopMargin);
            
            // Register bottom position of newly created link
            previousLinkBottom = linkTop + $("#link" + i).outerHeight();
        }
        $("#tocInside").append("<a id='link" + i + "' href='#title" + i + linkClass + "'>" + current.html() + "</a><br/>");
    });
});
