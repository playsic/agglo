$(document).ready(function()
{
    // TODO ca provoque un bug d'affichage des liens cliques
    $('.smoothScroll').click( function() {
        var targetPage = $(this).attr('href');
        var animationTimeSpanMs = 750;
        
        // Scroll
        $('html, body').animate( { scrollTop: $(targetPage).offset().top }, animationTimeSpanMs );
        
        return false;
    });
});