--- ./official/cpp/src/cli_pattern.cpp	1970-01-01 01:00:00.000000000 +0100
+++ ./../cli/cpp/src/cli_pattern.cpp	2015-04-16 21:34:01.940326400 +0200
@@ -0,0 +1,146 @@
+/***************************************************************************
+**
+** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
+** Contact: eti.laurent@gmail.com
+**
+** This file is part of the Agglo project.
+**
+** AGGLO_BEGIN_LICENSE
+** Commercial License Usage
+** Licensees holding valid commercial Agglo licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and Plaisic.  For licensing terms and
+** conditions contact eti.laurent@gmail.com.
+**
+** GNU General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU
+** General Public License version 3.0 as published by the Free Software
+** Foundation and appearing in the file LICENSE.GPL included in the
+** packaging of this file.  Please review the following information to
+** ensure the GNU General Public License version 3.0 requirements will be
+** met: http://www.gnu.org/copyleft/gpl.html.
+**
+** In addition, the following conditions apply:
+**     * Redistributions in binary form must reproduce the above copyright
+**       notice, this list of conditions and the following disclaimer in
+**       the documentation and/or other materials provided with the
+**       distribution.
+**     * Neither the name of the Agglo project nor the names of its
+**       contributors may be used to endorse or promote products derived
+**       from this software without specific prior written permission.
+**
+** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
+** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
+** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
+** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
+** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
+** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
+** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
+** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
+** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+**
+** AGGLO_END_LICENSE
+**
+***************************************************************************/
+
+#include "cli/cli_pattern.h"
+
+#include "cli/cli.h"
+#include "cli/shell.h"
+
+
+CLI_NS_USE(cli)
+
+CliPattern::CliPattern(const Cli& CLI_CliModel)
+{
+	m_pcliCliModel = CLI_CliModel.Clone();
+}
+
+CliPattern::CliPattern(const CliPattern& CLI_CliPattern)
+{
+    m_pcliCliModel = NULL;
+    *this = CLI_CliPattern;
+}
+
+CliPattern::~CliPattern()
+{
+	delete m_pcliCliModel;
+}
+
+CliPattern& CliPattern::operator=(const CliPattern& CLI_CliPattern)
+{
+    if (NULL != m_pcliCliModel)
+    {
+        delete m_pcliCliModel;
+    }
+    m_pcliCliModel = CLI_CliPattern.GetCliModel()->Clone();
+    m_cliWelcomeMessage = CLI_CliPattern.GetWelcomeMessage();
+    m_cliByeMessage = CLI_CliPattern.GetByeMessage();
+}
+
+const Cli* CliPattern::GetCliModel() const
+{
+    return m_pcliCliModel;
+}
+
+Cli* CliPattern::GetNewCliInstance() const
+{
+    return m_pcliCliModel->Clone();
+}
+
+const bool CliPattern::SetWelcomeMessage(const char* S_WelcomeMessage, const ResourceString::LANG E_Lang)
+{
+	bool b_Res = (NULL != S_WelcomeMessage);
+	
+	if (b_Res)
+	{
+        m_cliWelcomeMessage.SetString(E_Lang, S_WelcomeMessage);
+	}
+	
+	return b_Res;
+}
+
+void CliPattern::SetWelcomeMessage(const ResourceString CLI_WelcomeMessage)
+{
+    m_cliWelcomeMessage = CLI_WelcomeMessage;
+}
+
+const ResourceString CliPattern::GetWelcomeMessage() const
+{
+    return m_cliWelcomeMessage;
+}
+
+const bool CliPattern::SetByeMessage(const char* S_ByeMessage, const ResourceString::LANG E_Lang)
+{
+	bool b_Res = (NULL != S_ByeMessage);
+	
+	if (b_Res)
+	{
+		m_cliByeMessage.SetString(E_Lang, S_ByeMessage);
+	}
+	
+	return b_Res;
+}
+
+void CliPattern::SetByeMessage(const ResourceString CLI_ByeMessage)
+{
+    m_cliByeMessage = CLI_ByeMessage;
+}
+
+const ResourceString CliPattern::GetByeMessage() const
+{
+    return m_cliByeMessage;
+}
+
+void CliPattern::EnableConfigMenu(const bool B_Enable)
+{
+	m_pcliCliModel->EnableConfigMenu(B_Enable);
+}
+
+const bool CliPattern::IsConfigMenuEnabled() const
+{
+    return m_pcliCliModel->IsConfigMenuEnabled();
+}
