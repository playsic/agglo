--- ./official/cpp/src/telnet_connection_qt.cpp	1970-01-01 01:00:00.000000000 +0100
+++ ./../cli/cpp/src/telnet_connection_qt.cpp	2015-06-22 22:18:18.616820300 +0200
@@ -0,0 +1,215 @@
+/***************************************************************************
+**
+** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
+** Contact: eti.laurent@gmail.com
+**
+** This file is part of the Agglo project.
+**
+** AGGLO_BEGIN_LICENSE
+** Commercial License Usage
+** Licensees holding valid commercial Agglo licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and Plaisic.  For licensing terms and
+** conditions contact eti.laurent@gmail.com.
+**
+** GNU General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU
+** General Public License version 3.0 as published by the Free Software
+** Foundation and appearing in the file LICENSE.GPL included in the
+** packaging of this file.  Please review the following information to
+** ensure the GNU General Public License version 3.0 requirements will be
+** met: http://www.gnu.org/copyleft/gpl.html.
+**
+** In addition, the following conditions apply:
+**     * Redistributions in binary form must reproduce the above copyright
+**       notice, this list of conditions and the following disclaimer in
+**       the documentation and/or other materials provided with the
+**       distribution.
+**     * Neither the name of the Agglo project nor the names of its
+**       contributors may be used to endorse or promote products derived
+**       from this software without specific prior written permission.
+**
+** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
+** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
+** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
+** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
+** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
+** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
+** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
+** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
+** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+**
+** AGGLO_END_LICENSE
+**
+***************************************************************************/
+
+
+#pragma GCC diagnostic ignored "-Wignored-qualifiers"
+#include "cli/telnet_connection_qt.h"
+
+#include <iostream>
+#include <QTcpSocket>
+
+
+#include "cli/translator_telnet.h"
+#include "cli/exec_context.h"
+#include "cli/telnet_server_qt.h"
+
+
+using namespace std;
+
+CLI_NS_USE(cli)
+
+TelnetConnection_Qt::TelnetConnection_Qt(TelnetServer_Qt* const pcli_TelnetServer)
+                                 :m_pcliTelnetServer(pcli_TelnetServer), IODevice("qt telnet connection", false)
+{
+    // Instantiate a translator dedicated to telnet communication
+    if (NULL != m_pcliDeviceTranslator)
+    {
+        delete m_pcliDeviceTranslator;
+    }
+    m_pcliDeviceTranslator = new TelnetTranslator();
+}
+
+TelnetConnection_Qt::~TelnetConnection_Qt()
+{
+    if (NULL != m_pqtClientConnection)
+    {
+        // Require later deletion of Qt socket
+        m_pqtClientConnection->deleteLater();
+        m_pqtClientConnection = NULL;
+   }
+}
+
+const tk::String TelnetConnection_Qt::GetClientAddress()const
+{
+    QHostAddress qt_ClientAddress = m_pqtClientConnection->peerAddress();
+    
+    return tk::String(128, qt_ClientAddress.toString().toLatin1().constData());
+}
+
+const bool TelnetConnection_Qt::OpenDevice(void)
+{
+    bool b_Res = (NULL != m_pcliTelnetServer);
+    
+    if (b_Res)
+    {
+        m_pqtClientConnection = m_pcliTelnetServer->nextPendingConnection();
+        b_Res = IsOpen();
+    }
+    
+    // If new Qt socket has been opened
+    if (b_Res)
+    {
+        // Associate Qt socket with TelnetConnection_Qt callbacks
+        connect(m_pqtClientConnection, SIGNAL(readyRead()), this, SLOT(SlotOnReadReady()));
+        connect(m_pqtClientConnection, SIGNAL(disconnected()), this, SLOT(SlotOnSocketDisconnected()));
+        connect(m_pqtClientConnection, SIGNAL(error()), this, SLOT(SlotOnSocketError()));
+    }
+    
+    return b_Res;
+}
+
+const bool TelnetConnection_Qt::CloseDevice(void)
+{
+    if (IsOpen())
+	{
+        // Close Qt socket
+        m_pqtClientConnection->close();
+	}
+    
+    return true;
+}
+
+const bool TelnetConnection_Qt::IsOpen(void) const
+{
+    // TODO checker si la socket est ouverte ?
+    // TODO mettre les isopen dans tout le projet cli
+	return (NULL != m_pqtClientConnection);
+}
+
+const bool TelnetConnection_Qt::IsReadReady(void) const
+{
+    // Read is ready if the connection is opened, and there are queued datas, either in
+    // m_pcliDeviceTranslator or in m_pqtClientConnection
+    // TODO il faut que les queued inputs ne soient pas incomplete sequence. Deplacer ce code dans IODevice
+    return (IsOpen() && ((m_pcliDeviceTranslator->GetNbQueuedInputs() > 0) ||
+                                                (m_pqtClientConnection->bytesAvailable() > 0)));
+}
+
+const bool TelnetConnection_Qt::Read(void) const
+{
+    bool b_Res = IsReadReady();
+    
+    // Make sure there is something to read
+    if (b_Res)
+    {
+        char arc_Buffer[CLI_TRANSLATOR_INPUT_BUFFER_SIZE];
+        const int i_LenMax = CLI_TRANSLATOR_INPUT_BUFFER_SIZE  - m_pcliDeviceTranslator->GetNbQueuedInputs();
+        int i_LenReceived = 0;
+
+        // Dequeue characters from the socket
+        i_LenReceived = m_pqtClientConnection->read(arc_Buffer, i_LenMax);
+        b_Res = (i_LenReceived >= 0) && (i_LenReceived <= i_LenMax);
+
+        if (b_Res)
+        {
+            // Queue characters in the internal fifo
+            m_pcliDeviceTranslator->PushInputs(arc_Buffer, i_LenReceived);
+        }
+    }
+
+    return b_Res;
+}
+
+void TelnetConnection_Qt::Write(const char* const STR_Out) const
+{
+    if (NULL != m_pqtClientConnection)
+    {
+        // Write the translated buffer ine the socket
+        m_pqtClientConnection->write(STR_Out, strlen(STR_Out));
+        m_pqtClientConnection->flush();
+    }
+}
+
+void TelnetConnection_Qt::SlotOnReadReady()
+{
+    const ExecutionContext* const pcli_ExecutionContext = GetExecutionContext();
+    
+    // Handle received chars
+    OnReadReady();
+    
+    // If shell is not running anymore
+    if ((NULL != pcli_ExecutionContext) && (!(pcli_ExecutionContext->IsRunning())))
+	{
+        // End the connection
+        Stop();
+    }
+}
+
+void TelnetConnection_Qt::SlotOnSocketDisconnected()
+{
+    // End the connection
+    Stop();
+}
+
+void TelnetConnection_Qt::SlotOnSocketError()
+{
+    // End the connection
+    Stop();
+}
+
+void TelnetConnection_Qt::Stop()
+{
+    if (NULL != m_pqtClientConnection)
+    {
+        // We are going to close connection, so avoid receiving further signals
+        m_pqtClientConnection->disconnect();
+    }
+    
+    // Delete this
+    m_pcliTelnetServer->CloseConnection(*this);
+}
