--- ./official/cpp/src/serial_device.cpp	1970-01-01 01:00:00.000000000 +0100
+++ ./../cli/cpp/src/serial_device.cpp	2015-04-17 19:38:52.584393400 +0200
@@ -0,0 +1,520 @@
+/***************************************************************************
+**
+** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
+** Contact: eti.laurent@gmail.com
+**
+** This file is part of the Agglo project.
+**
+** AGGLO_BEGIN_LICENSE
+** Commercial License Usage
+** Licensees holding valid commercial Agglo licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and Plaisic.  For licensing terms and
+** conditions contact eti.laurent@gmail.com.
+**
+** GNU General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU
+** General Public License version 3.0 as published by the Free Software
+** Foundation and appearing in the file LICENSE.GPL included in the
+** packaging of this file.  Please review the following information to
+** ensure the GNU General Public License version 3.0 requirements will be
+** met: http://www.gnu.org/copyleft/gpl.html.
+**
+** In addition, the following conditions apply:
+**     * Redistributions in binary form must reproduce the above copyright
+**       notice, this list of conditions and the following disclaimer in
+**       the documentation and/or other materials provided with the
+**       distribution.
+**     * Neither the name of the Agglo project nor the names of its
+**       contributors may be used to endorse or promote products derived
+**       from this software without specific prior written permission.
+**
+** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
+** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
+** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
+** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
+** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
+** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
+** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
+** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
+** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+**
+** AGGLO_END_LICENSE
+**
+***************************************************************************/
+
+
+#include "cli/pch.h"
+
+#include "cli/serial_device.h"
+#include "cli/traces.h"
+
+#include <serial.h>
+#include <dirent.h>
+#include <sys/ioctl.h>
+#include <asm/ioctls.h>
+#include <termios.h>
+
+#include <stdio.h>
+#include <string.h>
+#include <fcntl.h>
+#include <unistd.h>
+
+CLI_NS_USE(cli)
+
+
+SerialDevice::SerialDevice(const SERIAL_PORT E_SerialPort, const bool B_AutoDelete): FileDescriptorDevice("serial", B_AutoDelete)
+{
+	m_ePort = E_SerialPort;
+	m_eBitRate = B115200;
+	m_eParity = NONE;
+	m_bIsBitStopEnabled = true;
+	m_bIsCanonicalModeEnabled = false;
+	m_bIsEchoOn = false;
+	m_uiDataBitNumber = CS8;
+}
+
+SerialDevice::~SerialDevice(void)
+{
+    CloseDevice();
+}
+
+const SERIAL_PORT SerialDevice::getPort() const
+{
+    return m_ePort;
+}
+
+const speed_t SerialDevice::getBitRate() const
+{
+    return m_eBitRate;
+}
+
+const PARITY SerialDevice::getParity() const
+{
+    return m_eParity;
+}
+
+const bool SerialDevice::isBitStopEnabled() const
+{
+    return m_bIsBitStopEnabled;
+}
+
+const bool SerialDevice::isCanonicalModeEnabled() const
+{
+    return m_bIsCanonicalModeEnabled;
+}
+
+const bool SerialDevice::isEchoOn() const
+{
+    return m_bIsEchoOn;
+}
+
+const unsigned int getDataBitNumber() const
+{
+    return m_iDataBitNumber;
+}
+
+
+const bool SerialDevice::setPort(const SERIAL_PORT E_Port)
+{
+    bool b_Res = (E_Port >= COM1) && (E_Port <= COM5);
+    
+    if (b_Res && (E_Port != m_ePort))
+    {
+        bRes = !IsOpen();
+        
+        if (b_Res)
+        {
+            m_ePort = E_Port;
+        }
+    }
+    
+    return b_Res;
+}
+
+const bool SerialDevice::setBitRate(const speed_t E_BitRate)
+{
+	bool b_Res = true;
+
+    switch (E_BitRate)
+    {
+        case B57600:
+        case B115200:
+        case B230400:
+        case B460800:
+        case B500000:
+        case B576000:
+        case B921600:
+        case B1000000:
+        case B1152000:
+        case B1500000:
+        case B2000000:
+        case B2500000:
+        case B3000000:
+        case B3500000:
+        case B4000000:
+            b_Res = true;
+            break;
+        default:
+            b_Res = false;
+            break;
+    }
+    
+    if (b_Res && (E_BitRate != m_eBitRate))
+    {
+        const speed_t e_SavedBitRate = m_eBitRate;
+        
+        // Store new bit rate in SerialDevice instance
+        m_eBitRate = E_BitRate;
+        
+        // If SerialDevice is already opened
+        if (IsOpen())
+        {
+            // Activate the actual device new configuration
+            b_Res = ActivateConfiguration()
+
+            // In case of activation failure
+            if (!b_Res)
+            {
+                // Restore old bit rate
+                m_eBitRate = e_SavedBitRate;
+            }
+        }
+    }
+	
+	return b_Res;
+}
+
+const bool SerialDevice::setParity(const PARITY E_Parity)
+{
+	bool b_Res = (E_Parity >= NONE) && (E_Parity <= EVEN);
+
+    if (b_Res && (E_Parity != m_eParity))
+    {
+        const PARITY e_SavedParity = m_eParity;
+        
+        // Store new parity in SerialDevice instance
+        m_eParity = E_Parity;
+        
+        // If SerialDevice is already opened
+        if (IsOpen())
+        {
+            // Activate the actual device new configuration
+            b_Res = ActivateConfiguration()
+
+            // In case of activation failure
+            if (!b_Res)
+            {
+                // Restore old parity
+                m_eParity = e_SavedParity;
+            }
+        }
+    }
+ 	
+	return b_Res;
+}
+
+const bool SerialDevice::enableBitStop(const bool B_Enable)
+{
+	bool b_Res = true;
+
+    if (B_Enable != m_bIsBitStopEnabled)
+    {
+        const bool b_SavedBitStopMode = m_bIsBitStopEnabled;
+        
+        // Store new bit stop mode in SerialDevice instance
+        m_bIsBitStopEnabled = B_Enable;
+        
+        // If SerialDevice is already opened
+        if (IsOpen())
+        {
+            // Activate the actual device new configuration
+            b_Res = ActivateConfiguration()
+
+            // In case of activation failure
+            if (!b_Res)
+            {
+                // Restore old bit stop mode
+                m_bIsBitStopEnabled = b_SavedBitStopMode;
+            }
+        }
+    }
+	
+	return b_Res;
+}
+
+const bool SerialDevice::enableCanonicalMode(const bool B_Enable)
+{
+	bool b_Res = true;
+
+    if (B_Enable != m_bIsCanonicalModeEnabled)
+    {
+        const bool b_SavedCanonicalMode = m_bIsCanonicalModeEnabled;
+        
+        // Store new canonical mode in SerialDevice instance
+        m_bIsCanonicalModeEnabled = B_Enable;
+        
+        // If SerialDevice is already opened
+        if (IsOpen())
+        {
+            // Activate the actual device new configuration
+            b_Res = ActivateConfiguration()
+
+            // In case of activation failure
+            if (!b_Res)
+            {
+                // Restore old canonical mode
+                m_bIsCanonicalModeEnabled = b_SavedCanonicalMode;
+            }
+        }
+    }
+	
+	return b_Res;
+}
+
+const bool SerialDevice::enableEcho(const bool B_Enable)
+{
+	bool b_Res = true;
+
+    if (B_Enable != m_bIsEchoOn)
+    {
+        const bool b_SavedEchoMode = m_bIsEchoOn;
+        
+        // Store new echo mode in SerialDevice instance
+        m_bIsEchoOn = B_Enable;
+        
+        // If SerialDevice is already opened
+        if (IsOpen())
+        {
+            // Activate the actual device new configuration
+            b_Res = ActivateConfiguration()
+
+            // In case of activation failure
+            if (!b_Res)
+            {
+                // Restore old echo mode
+                m_bIsEchoOn = b_SavedEchoMode;
+            }
+        }
+    }
+	
+	return b_Res;	
+}
+
+const bool SerialDevice::setDataBitNumber(const unsigned int UI_DataBitNumber)
+{
+	bool b_Res = (CS5 == I_DataBitNumber) || (CS6 == I_DataBitNumber) || 
+                 (CS7 == I_DataBitNumber) || (CS8 == I_DataBitNumber);
+
+    if (b_Res && (I_DataBitNumber != m_uiDataBitNumber))
+    {
+        const int i_SavedBitNumber = m_uiDataBitNumber;
+        
+        // Store new bit number in SerialDevice instance
+        m_uiDataBitNumber = I_DataBitNumber;
+        
+        // If SerialDevice is already opened
+        if (IsOpen())
+        {
+            // Activate the actual device new configuration
+            b_Res = ActivateConfiguration()
+
+            // In case of activation failure
+            if (!b_Res)
+            {
+                // Restore old bit number 
+                m_uiDataBitNumber = i_SavedBitNumber;
+            }
+        }
+    }
+
+	return b_Res;
+}
+
+const bool SerialDevice::OpenDevice(void)
+{
+	bool b_Res = false;
+	const char* s_SerialDevicePath = NULL;
+    Queue<char*> cli_SerialDevicesList = GetSerialDeviceFiles();
+
+    b_Res = (m_eSerialPort <= cli_SerialDevicesList.GetCount());
+	if (b_Res)
+	{
+        Queue<char*>::Iterator cli_IterSerialDevices = cli_SerialDevicesList.GetIterator();
+        
+        for (int i = COM1; (i < m_eSerialPort) && (cli_SerialDevicesList.IsValid(cli_IterSerialDevices)); i++)
+        {
+            cli_SerialDevicesList.MoveNext(cli_IterSerialDevices)
+        }
+        
+		s_SerialDevicePath = cli_SerialDevicesList.GetIterator(cli_IterSerialDevices);
+        b_Res = (s_SerialDevicePath != NULL);
+	}
+	
+	if (b_Res)
+	{
+		m_iFileDescriptor = open(s_SerialDevicePath, O_RDWR);
+		b_Res = IsOpen();
+	}
+
+	if (b_Res)
+	{
+        // Activate the actual device new configuration
+        b_Res = ActivateConfiguration();
+	}
+
+	return b_Res;
+}
+
+const bool SerialDevice::ActivateConfiguration() const
+{
+	bool b_Res = IsOpen();
+    struct termios st_NewSerialConfiguration;
+        
+    if (b_Res)
+    {
+        // Get the actual serial device configuration
+        b_Res = (tcgetattr(m_iFileDescriptor, &st_NewSerialConfiguration) >= 0);
+    }
+            
+    if (b_Res)
+    {
+        // Set the bit rate
+        cfsetospeed (&st_NewSerialConfiguration, E_BitRate);
+        
+        // Set the parity
+        switch (E_Parity)
+        {
+            case NONE:
+                st_NewSerialConfiguration.c_cflag &= ~PARENB;
+                break;
+            case ODD:
+                st_NewSerialConfiguration.c_cflag |= PARENB;
+                st_NewSerialConfiguration.c_cflag |= PARODD;
+                break;
+            case EVEN:
+                st_NewSerialConfiguration.c_cflag |= PARENB;
+                st_NewSerialConfiguration.c_cflag &= ~PARODD;
+                break;
+            default:
+                b_Res = false;
+                break;
+        }
+        
+        // Set bit stop mode
+        if (m_bIsBitStopEnabled)
+        {
+            st_NewSerialConfiguration.c_cflag &= ~CSTOPB;
+        }
+        else
+        {
+            st_NewSerialConfiguration.c_cflag |= CSTOPB;
+        }
+        
+        // Set canonical mode
+        if (m_bIsCanonicalModeEnabled)
+        {
+            st_NewSerialConfiguration.c_lflag |= ICANON;
+        }
+        else
+        {
+            st_NewSerialConfiguration.c_lflag &= ~ICANON;
+        }
+        
+        // Set echo mode
+        if (m_bIsEchoOn)
+        {
+            st_NewSerialConfiguration.c_lflag |= ECHO;
+        }
+        else
+        {
+            st_NewSerialConfiguration.c_lflag &= ~ECHO;
+        }
+        
+        // Set the bit number
+        st_NewSerialConfiguration.c_cflag &= ~CSIZE;
+        st_NewSerialConfiguration.c_cflag |= m_uiDataBitNumber;
+
+        // Activate the new configuration
+        b_Res = (tcsetattr(m_iFileDescriptor, TCSANOW, &st_NewSerialConfiguration) >= 0);
+    }
+    
+    return b_Res;
+}
+const Queue<char*> SerialDevice::GetSerialDeviceFiles(void)
+{
+    Queue<char*> cli_Res;
+	DIR* devicesDir = opendir ("/dev/");;
+	
+	if (dir != NULL) 
+	{
+        struct dirent* st_CurrentDevice = readdir(dir);
+        char s_SerialDevicePath[256];
+        std::list<char*>::iterator it;
+        
+		while (st_CurrentDevice != NULL) 
+		{
+			if (strncmp("ttyS", st_CurrentDevice->d_name, 4) == 0)
+			{
+                snprintf(s_SerialDevicePath, sizeof(s_SerialDevicePath), "/dev/%s", st_CurrentDevice->d_name);
+
+				if (IsSerialDeviceValid(s_SerialDevicePath))
+				{	
+                    char* ps_SavedSerialDevicePath = (char*)malloc(strlen(s_SerialDevicePath));
+                    
+                    strcpy(ps_SavedSerialDevicePath, s_SerialDevicePath);
+                    deviceList.push_back(ps_SavedSerialDevicePath);
+				}
+			}
+            
+            st_CurrentDevice = readdir(dir);
+		}
+        
+		closedir (dir);
+	}
+    
+    return cli_Res;
+}
+
+const bool SerialDevice::IsSerialDeviceValid(const char* S_SerialDevicePath)
+{
+	struct serial_struct st_SerialDeviceInfo;
+	int	i_SerialDeviceFd = open(S_SerialDevicePath, O_RDWR|O_NONBLOCK));
+	bool b_Res = (i_SerialDeviceFd >= 0);
+
+	if (b_Res)
+    {
+        st_SerialDeviceInfo.reserved_char[0] = 0;
+        b_Res = (ioctl(i_SerialDeviceFd, TIOCGSERIAL, &st_SerialDeviceInfo) >= 0);
+	}
+	
+	if (b_Res)
+    {
+        b_Res = (st_SerialDeviceInfo.irq != 0) && (st_SerialDeviceInfo.type != 0);
+    }
+
+    if (i_SerialDeviceFd >= 0)
+    {
+        close(i_SerialDeviceFd);
+    }
+    
+	return b_Res;
+}
+
+const KEY SerialDevice::WaitKey() const
+{
+	KEY e_Res = NULL_KEY;
+
+	if (!IsOpen())
+	{
+		UseInstance(__CALL_INFO__);
+		e_Res = GetKey();
+		FreeInstance(__CALL_INFO__);
+	}
+	
+	return e_Res;
+}
+
