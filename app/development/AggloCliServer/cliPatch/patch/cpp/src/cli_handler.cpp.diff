--- ./official/cpp/src/cli_handler.cpp	1970-01-01 01:00:00.000000000 +0100
+++ ./../cli/cpp/src/cli_handler.cpp	2018-07-24 21:51:59.166929487 +0200
@@ -0,0 +1,142 @@
+/***************************************************************************
+** 
+** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
+** Contact: eti.laurent@gmail.com
+** 
+** This file is part of the Agglo project.
+** 
+** AGGLO_BEGIN_LICENSE
+** Commercial License Usage
+** Licensees holding valid commercial Agglo licenses may use this file in 
+** accordance with the commercial license agreement provided with the 
+** Software or, alternatively, in accordance with the terms contained in 
+** a written agreement between you and Plaisic.  For licensing terms and 
+** conditions contact eti.laurent@gmail.com.
+** 
+** GNU General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU
+** General Public License version 3.0 as published by the Free Software
+** Foundation and appearing in the file LICENSE.GPL included in the
+** packaging of this file.  Please review the following information to
+** ensure the GNU General Public License version 3.0 requirements will be
+** met: http://www.gnu.org/copyleft/gpl.html.
+** 
+** In addition, the following conditions apply: 
+**     * Redistributions in binary form must reproduce the above copyright 
+**       notice, this list of conditions and the following disclaimer in 
+**       the documentation and/or other materials provided with the 
+**       distribution.
+**     * Neither the name of the Agglo project nor the names of its  
+**       contributors may be used to endorse or promote products derived 
+**       from this software without specific prior written permission.
+** 
+** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
+** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
+** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
+** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
+** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
+** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
+** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
+** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
+** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
+** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
+** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+** 
+** AGGLO_END_LICENSE
+** 
+***************************************************************************/
+
+#include "cli/cli_handler.h"
+
+#include <stdio.h>
+
+#include "cli/io_device.h"
+#include "cli/shell.h"
+
+
+#define NB_CHAR_OUTPUT_MAX 1024
+
+CLI_NS_USE(cli)
+
+const bool CliHandler::WriteToCli(const char* const as_Format, ...) const
+{
+    bool bRes = true;
+    va_list tListArguments;
+    
+    // Get the variable arguments list
+    va_start(tListArguments, as_Format);
+    
+    // Write the output on associated shell
+    bRes = WriteToCli(mcli_OutputShell.GetStream(OUTPUT_STREAM), as_Format, &tListArguments);
+    va_end(tListArguments);
+    
+    return (bRes);
+}
+
+
+const bool CliHandler::WriteToCli(const Shell& acli_OutputShell, const char* const as_Format, ...)
+{
+    bool bRes = true;
+    va_list tListArguments;
+    
+    // Get the variable arguments list
+    va_start(tListArguments, as_Format);
+    
+    // Write the output on cliOutputDevice
+    bRes = WriteToCli(acli_OutputShell.GetStream(OUTPUT_STREAM), as_Format, &tListArguments);
+    va_end(tListArguments);
+    
+    return (bRes);
+}
+
+
+const bool CliHandler::WriteToCli(const OutputDevice& acli_OutputDevice, const char* const as_Format, ...)
+{
+    bool bRes = true;
+    va_list tListArguments;
+    
+    // Get the variable arguments list
+    va_start(tListArguments, as_Format);
+    
+    // Write the output on cliOutputDevice
+    bRes = WriteToCli(acli_OutputDevice, as_Format, &tListArguments);
+    va_end(tListArguments);
+    
+    return (bRes);
+}
+
+
+const bool CliHandler::WriteToCli(const OutputDevice& acli_OutputDevice, const char* const as_Format, 
+                                  va_list* const at_ListArguments)
+{
+    bool bRes = true;
+    bool bOpenDevice = (0 == acli_OutputDevice.GetOpenUsers());
+    char sOutWriteToCli[NB_CHAR_OUTPUT_MAX];
+    
+    // Compute the string to display
+    vsnprintf(sOutWriteToCli, sizeof(sOutWriteToCli), as_Format, *at_ListArguments);
+
+    // If the OutputDevice has to be opened
+    if (bOpenDevice)
+    {
+        // Open the OutputDevice, it will be closed at the end of method call
+        bRes = (const_cast<OutputDevice&>(acli_OutputDevice)).OpenUp(__CALL_INFO__);
+    }
+    
+    if (bRes)
+    {
+      // Write the string on the OutputDevice
+      acli_OutputDevice << sOutWriteToCli;
+      
+      // If the OutputDevice has been opened by ShellCommandRunner::WriteToCli method call
+      if (bOpenDevice)
+      {
+          // Restore the OutputDevice in its initial state
+          (const_cast<OutputDevice&>(acli_OutputDevice)).CloseDown(__CALL_INFO__);
+      }
+    }
+    
+    return (bRes);
+}
+
+
