--- ./official/cpp/include/cli/translator_file.h	1970-01-01 01:00:00.000000000 +0100
+++ ./../cli/cpp/include/cli/translator_file.h	2015-02-13 21:52:03.248000000 +0100
@@ -0,0 +1,105 @@
+/***************************************************************************
+**
+** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
+** Contact: eti.laurent@gmail.com
+**
+** This file is part of the Agglo project.
+**
+** AGGLO_BEGIN_LICENSE
+** Commercial License Usage
+** Licensees holding valid commercial Agglo licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and Plaisic.  For licensing terms and
+** conditions contact eti.laurent@gmail.com.
+**
+** GNU General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU
+** General Public License version 3.0 as published by the Free Software
+** Foundation and appearing in the file LICENSE.GPL included in the
+** packaging of this file.  Please review the following information to
+** ensure the GNU General Public License version 3.0 requirements will be
+** met: http://www.gnu.org/copyleft/gpl.html.
+**
+** In addition, the following conditions apply:
+**     * Redistributions in binary form must reproduce the above copyright
+**       notice, this list of conditions and the following disclaimer in
+**       the documentation and/or other materials provided with the
+**       distribution.
+**     * Neither the name of the Agglo project nor the names of its
+**       contributors may be used to endorse or promote products derived
+**       from this software without specific prior written permission.
+**
+** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
+** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
+** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
+** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
+** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
+** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
+** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
+** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
+** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
+** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
+** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+**
+** AGGLO_END_LICENSE
+**
+***************************************************************************/
+
+#ifndef _CLI_TRANSLATOR_FILE_H_
+#define _CLI_TRANSLATOR_FILE_H_
+
+#include "cli/namespace.h"
+#include "cli/translator_default.h"
+
+//#include "sys/types.h"
+
+
+CLI_NS_BEGIN(cli)
+
+//! @brief Console intput/output device class.
+class FileTranslator: public IODeviceTranslator
+{
+	public:
+		FileTranslator();
+		virtual ~FileTranslator(void);
+
+	public:
+		//! @brief Translate an input characters sequence into cli character.
+		const KEY PopKey();
+		
+		//! @brief Translate a string in cli format into output device string.
+        void TranslateOutput(const char* const str_In, char*& str_Out);
+
+        //! @brief Special character enabling.
+       void EnableSpecialCharacters(const bool B_EnableSpecialCharacters);
+
+		//! @brief Current line accessor.
+		//! @return Current line, starting from 0.
+		const int GetCurrentLine(void) const;
+
+		//! @brief Current column accessor.
+		//! @return Current column, starting from 0.
+		const int GetCurrentColumn(void) const;
+
+	private:	
+		//! Special character enabling.
+		bool m_bEnableSpecialCharacters;
+		
+		//! Current Line.
+		int m_iCurrentLine;
+
+		//! Current column.
+		int m_iCurrentColumn;
+
+		//! Next character line.
+		int m_iNextLine;
+
+		//! Next column.
+		int m_iNextColumn;
+};
+
+CLI_NS_END(cli)
+
+#endif // _CLI_TRANSLATOR_FILE_H_
+
