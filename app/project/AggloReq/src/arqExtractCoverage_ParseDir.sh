############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


# Global variables
#equivalent $(dirname $0)
ExtractReqShellPath=${0%/*}

source $ExtractReqShellPath/../../../development/AggloToolKit/shell/src/utils/atkTextFiles.sh
source $ExtractReqShellPath/../../../development/AggloToolKit/shell/src/utils/atkFileSystem.sh

# Check the parameters
if [ $# != 6 ] && [ $# != 7 ];
then
    echo "format de la commande : arqExtractCoverage_ParseDir RegExprCoveringReq ReqExprCoveredReqList SubRegExprCoveredReq PathInput PathFilesSelector PathReqFileOutput [Recursive]"
    echo "example : ./arqExtractCoverage_ParseDir.sh '.*function\s*:\s*([^\r\n]*)' '.*requirement\s*:\s*([^\r\n]*)' '[[:blank:],]*([^[:blank:],]*)' './Directory' '*.cpp' './req.xml' Recursive"
else

    # Init parameters
    RegExprCoveringReq=$1
    ReqExprCoveredReqList=$2
    SubRegExprCoveredReq=$3
    PathInput=$4
    PathFilesSelector=$5
    PathReqFileOutput=$6

    if [ $# = 7 ] && [ $7 = "Recursive" ];
    then
        Recursive=true
    else
        Recursive=false
    fi

    # Iterate on input files list
    IFS=$'\n'
    for currentFile in $(ls "$PathInput"/$PathFilesSelector 2>/dev/null)
    do
        # Extract requirements from current file in a temporary xml file
        $ExtractReqShellPath/arqExtractBottomUpCoverage_ParseFile.sh "$RegExprCoveringReq" "$ReqExprCoveredReqList" "$SubRegExprCoveredReq" "$currentFile" "$TempDir/$currentFile.xml"

        # Append requirements to output file
        sed 's/^/\t/' "$TempDir/$currentFile.xml" >"$TempDir/$currentFile.xml2"
        cat "$TempDir/$currentFile.xml2" >> "$PathReqFileOutput"
        echo -e "\r" >> "$PathReqFileOutput"

        # Delete temp file
        rm "$TempDir/$currentFile.xml"
        rm "$TempDir/$currentFile.xml2"
    done
    unset IFS

    if $Recursive ;
    then
        # Iterate on sub directories (watch out, there can be a space in sub directories name)
        IFS=$'\n'
        for currentDir in $(ls -d $PathInput/*/ 2>/dev/null | sed 's/\/$//g')
        do
            $ExtractReqShellPath/arqExtractCoverage_ParseDir.sh "$RegExprCoveringReq" "$ReqExprCoveredReqList" "$SubRegExprCoveredReq" "$currentDir" "$PathFilesSelector" "$PathReqFileOutput" $7
        done
        unset IFS
    fi
fi

