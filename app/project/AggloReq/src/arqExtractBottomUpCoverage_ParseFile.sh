############################################################################
##
## Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
## Contact: eti.laurent@gmail.com
##
## This file is part of the Agglo project.
##
## AGGLO_BEGIN_LICENSE
## Commercial License Usage
## Licensees holding valid commercial Agglo licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and Plaisic.  For licensing terms and
## conditions contact eti.laurent@gmail.com.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3.0 as published by the Free Software
## Foundation and appearing in the file LICENSE.GPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU General Public License version 3.0 requirements will be
## met: http://www.gnu.org/copyleft/gpl.html.
##
## In addition, the following conditions apply:
##     * Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in
##       the documentation and/or other materials provided with the
##       distribution.
##     * Neither the name of the Agglo project nor the names of its
##       contributors may be used to endorse or promote products derived
##       from this software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
## TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
## PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
## LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
## NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
## SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##
## AGGLO_END_LICENSE
##
############################################################################


# Global variables
#equivalent $(dirname $0)
ExtractReqShellPath=${0%/*}
source $ExtractReqShellPath/../../../development/AggloToolKit/shell/src/utils/atkTextFiles.sh
source $ExtractReqShellPath/../../../development/AggloToolKit/shell/src/utils/atkFileSystem.sh
TempReqFileOutput=$TempDir/TempFile.xml

# Check the parameters
IsCoveringReq_ParseFile=true
if [ $# != 5 ];
then
    echo "format de la commande : arqExtractBottomUpCoverage_ParseFile RegExprCoveringReq ReqExprCoveredReqList SubRegExprCoveredReq PathFileInput PathReqFileOutput"
    echo "example : arqExtractBottomUpCoverage_ParseFile '.*function\s*:\s*([^\r\n]*)' '.*requirement\s*:\s*([^\r\n]*)' '[[:blank:],]*([^[:blank:],\r\n]*)' .\test.cpp .\req.xml"
    IsCoveringReq_ParseFile=false
fi


if $IsCoveringReq_ParseFile ;
then

    # Init parameters
    RegExprCoveringReq=$1
    ReqExprCoveredReqList=$2
    SubRegExprCoveredReq=$3
    PathFileInput=$4
    PathReqFileOutput=$5

	FileName=$(getFileName "$PathFileInput")

    # Ensure input file is in UTF-8 (necessary for sed)
    PathFileInputConverted=$PathFileInput
    ConvertFileToTxt=false
    if isUTF8 "$PathFileInput" ;
    then
        IsCoveringReq_ParseFile=true
    elif isISO8859 "$PathFileInput" ;
    then
        ConvertFileToTxt=true
        PathFileInputConverted=$TempDir/Temp_$FileName
        recodeUTF8 "$PathFileInput" "$PathFileInputConverted"
    elif isBinaryText "$PathFileInput" ;
    then
        ConvertFileToTxt=true;
        PathFileInputConverted=$TempDir/Temp_$FileName
        recodeText "$PathFileInput" "$PathFileInputConverted"
    elif ! isText "$PathFileInput" ;
    then
        echo "IsCoveringReq_ParseFile=false"
        IsCoveringReq_ParseFile=false
    fi
fi


if $IsCoveringReq_ParseFile ;
then
    # Extract covering requirements and covered requirements list
    # TODO : le tag de fermeture </CoveringReq> devrait être posé sur détection d'un coveringreq et non d'un covered req list. Sinon risque de bug si coveringReq sans CoveredeqList
    sed -r '
    /'"$RegExprCoveringReq"'/ b CoveringReq
    /'"$ReqExprCoveredReqList"'/ b CoveredReqList
    s/^.*$/\r/
    :CoveringReq
    s/'"$RegExprCoveringReq"'/AGGLO_DUMP\r\n\t<CoveringReq name="\1">\r\nAGGLO_DUMP/
    :CoveredReqList
    s/'"$ReqExprCoveredReqList"'/AGGLO_DUMP\r\n\1\n\t<\/CoveringReq>\r\nAGGLO_DUMP/
    ' <"$PathFileInputConverted" >"$TempReqFileOutput"

    # Extract covered requirements by applying sub regular expression
    sed -r '
    /AGGLO_DUMP/ b AggloDump
    /^\t<[\/]{0,1}CoveringReq.*>$/ b CoveringReq
    /^\r*$/ b EmptyLine
    :CoveredReq
    s/'"$SubRegExprCoveredReq"'/\t\t<CoveredReq name="\1"\/>\r\n/g
    :AggloDump
    s/^.*AGGLO_DUMP.*$/\r\n/
    :CoveringReq
    :EmptyLine
    ' <$TempReqFileOutput >"$PathReqFileOutput"

    # Add xml tag for file
    sed -i '1i<file name=\"'"$PathFileInput"\"'>\r' "$PathReqFileOutput"
    echo -e "</file>\r" >>"$PathReqFileOutput"

    # Delete empty lines
    sed -r -i '/^\r*$/d' "$PathReqFileOutput"

    # Delete temp file
    rm "$TempReqFileOutput"
    if $ConvertFileToTxt ;
    then
        rm "$PathFileInputConverted"
    fi
fi

