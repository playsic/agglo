<?xml version="1.0" encoding="UTF-8"?>
<!--************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
*************************************************************************-->


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:exsl="http://exslt.org/common"
                              extension-element-prefixes="exsl">
<xsl:output method="text" encoding="UTF-8" indent="no"/>

<!-- Utilities -->
<xsl:include href="../../../../doc/licence/aggInsertLicence.xsl"/>
<xsl:include href="../../../development/AggloToolKit/xsl/src/atkLayout.xsl"/>

<!-- Global variables -->
<xsl:param name="gsAggloPath"><!--<xsl:message terminate="yes">Error! Please set gsAggloPath param</xsl:message>--></xsl:param>
<xsl:param name="gsGeneratedShellPath"><!--<xsl:message terminate="yes">Error! Please set gsGeneratedShellPath param</xsl:message>--></xsl:param>
<xsl:param name="gsCoverageTreeFromShellPath"><!--<xsl:message terminate="yes">Error! Please set gsCoverageTreeFromShellPath param</xsl:message>--></xsl:param>

<!-- Constants for generation of global shell variables -->
<xsl:variable name="gsAggloPathVarName"><xsl:text>AggloPath</xsl:text></xsl:variable>
<xsl:variable name="gsAggloReqPathVarName"><xsl:text>AggloReqPath</xsl:text></xsl:variable>
<xsl:variable name="gsAggloXslPathVarName"><xsl:text>AggloToolKitXslPath</xsl:text></xsl:variable>
<xsl:variable name="gsAggloToolKitPathVarName"><xsl:text>AggloToolKitShellPath</xsl:text></xsl:variable>
<xsl:variable name="gsAggloHtmlPathVarName"><xsl:text>AggloToolKitHtmlPath</xsl:text></xsl:variable>

<!-- Constants for generation of traceability constants -->
<xsl:variable name="gsRawDataPath"><xsl:text>RawDataPath</xsl:text></xsl:variable>
<xsl:variable name="gsHtmlReportPath"><xsl:text>HtmlReportPath</xsl:text></xsl:variable>
<xsl:variable name="gsCustomizationFile"><xsl:text>CustomizationFile</xsl:text></xsl:variable>
<xsl:variable name="gsCoverageTreeConstantsPath"><xsl:value-of select="/coverageTree/@name"/>_TraceabilityConstants.sh</xsl:variable>
<xsl:variable name="gsCoverageTreeInitPath"><xsl:value-of select="/coverageTree/@name"/>_TraceabilityInit.sh</xsl:variable>
<xsl:variable name="gsCoverageTreeUpdatePath"><xsl:value-of select="/coverageTree/@name"/>_TraceabilityUpdate.sh</xsl:variable>
<xsl:variable name="gsSourcePath"><xsl:text>Path</xsl:text></xsl:variable>
<xsl:variable name="gsSourceSelector"><xsl:text>Selector</xsl:text></xsl:variable>
<xsl:variable name="gsRequirementsFile"><xsl:text>RequirementsFile</xsl:text></xsl:variable>
<xsl:variable name="gsCoverageFile"><xsl:text>CoverageFile</xsl:text></xsl:variable>
<xsl:variable name="gsCoverageReportFile"><xsl:text>CoverageReportFile</xsl:text></xsl:variable>
<xsl:variable name="gsProgressFile"><xsl:text>ProgressFile</xsl:text></xsl:variable>
<xsl:variable name="gsProgressReportFile"><xsl:text>ProgressReportFile</xsl:text></xsl:variable>
<xsl:variable name="gsLevelRegExpr"><xsl:text>RegExprLevelReq</xsl:text></xsl:variable>
<xsl:variable name="gsUpstreamListRegExpr"><xsl:text>RegExprUpstreamReqList</xsl:text></xsl:variable>
<xsl:variable name="gsUpstreamRegExpr"><xsl:text>RegExprUpstreamReq</xsl:text></xsl:variable>

<!-- Constants for generation of shell names -->
<xsl:variable name="gsRelativePathVarName"><xsl:text>sThisShellPath</xsl:text></xsl:variable>
<xsl:variable name="gsRequirementExtractShell"><xsl:text>RequirementsExtract</xsl:text></xsl:variable>
<xsl:variable name="gsCoverageExtractShell"><xsl:text>CoverageExtract</xsl:text></xsl:variable>
<xsl:variable name="gsCoverageReportShell"><xsl:text>CoverageReport</xsl:text></xsl:variable>
<xsl:variable name="gsProgressInitShell"><xsl:text>ProgressInit</xsl:text></xsl:variable>
<xsl:variable name="gsProgressUpdateShell"><xsl:text>ProgressUpdate</xsl:text></xsl:variable>
<xsl:variable name="gsProgressReportShell"><xsl:text>ProgressReport</xsl:text></xsl:variable>
<xsl:variable name="geVarName"><xsl:text>eVarName</xsl:text></xsl:variable>
<xsl:variable name="geVarEval"><xsl:text>eVarEval</xsl:text></xsl:variable>
<xsl:variable name="geShellName"><xsl:text>eShellName</xsl:text></xsl:variable>
<xsl:variable name="geShellCall"><xsl:text>eShellCall</xsl:text></xsl:variable>
<xsl:variable name="geFullPath"><xsl:text>eFullPath</xsl:text></xsl:variable>


<!-- Template to handle coverage tree file -->
<xsl:template match="/coverageTree">

    <!-- Generate top level shells -->
    <!-- TODO gerer une arborescence de shells, avec seulement 2 shell public, constante et update (avec param obligatoire INIT ou UPDATE), et tous les autres en privés -->
    <xsl:call-template name="generateConstantsShell"/>
    <xsl:call-template name="generateInitShell"/>
    <xsl:call-template name="generateUpdateShell"/>
    <!-- TODO creer un shell de sauvegarde -->
    
    <!-- For each coverage level in coverage tree, call the appropriate template -->
    <xsl:apply-templates select="./coverageLevel"/>

</xsl:template>


<!-- Template to handle a level in coverage tree -->
<xsl:template match="coverageLevel">

    <xsl:variable name="nsCurrentLevel" select="."/>
    <xsl:variable name="sCurrentLevelName" select="./@name"/>
    
    <xsl:call-template name="generateTraceabilityShell">
        <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
        <xsl:with-param name="asTypeShell" select="$gsRequirementExtractShell"/>
    </xsl:call-template>
    
    <!-- for each downstream level -->
    <xsl:for-each select="/coverageTree/coverageLevel[./coveredLevels/coveredLevel/@name = $sCurrentLevelName]">
        <xsl:call-template name="generateTraceabilityShell">
            <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
            <xsl:with-param name="asTypeShell" select="$gsCoverageExtractShell"/>
        </xsl:call-template>
        <xsl:call-template name="generateTraceabilityShell">
            <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
            <xsl:with-param name="asTypeShell" select="$gsCoverageReportShell"/>
        </xsl:call-template>
        
        <xsl:call-template name="generateTraceabilityShell">
            <!-- TODO creer les scripts de creation de fichiers de progress meme pour les coveragelevl qui ne sont pas couverts ? de maniere a ce que l'utilisateur puisse utiliser ces fichiers a la main sans passer par agglo -->
            <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
            <xsl:with-param name="asTypeShell" select="$gsProgressInitShell"/>
        </xsl:call-template>
        <xsl:call-template name="generateTraceabilityShell">
            <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
            <xsl:with-param name="asTypeShell" select="$gsProgressUpdateShell"/>
        </xsl:call-template>
        <xsl:call-template name="generateTraceabilityShell">
            <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
            <xsl:with-param name="asTypeShell" select="$gsProgressReportShell"/>
        </xsl:call-template>
    </xsl:for-each>
    
</xsl:template>


<xsl:template name="generateConstantsShell">

    <xsl:variable name="sShellPath">
        <xsl:value-of select="$gsGeneratedShellPath"/><xsl:text>/</xsl:text><xsl:value-of select="$gsCoverageTreeConstantsPath"/>
    </xsl:variable>

    <exsl:document href="{$sShellPath}" method="text">

        <xsl:variable name="RawDataPath">
            <xsl:choose>
                <xsl:when test="/coverageTree/export[@format='raw']">
                    <xsl:value-of select="/coverageTree/export[@format='raw']/@path"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>.</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="HtmlReportPath">
            <xsl:value-of select="/coverageTree/export[@format='html']/@path"/>
        </xsl:variable>
        <xsl:variable name="CoverageTreePathVarName">
            <xsl:call-template name="generateCoverageTreeVarName">
                <xsl:with-param name="typeVar" select="$gsSourcePath"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="RawDataPathVarName">
            <xsl:call-template name="generateCoverageTreeVarName">
                <xsl:with-param name="typeVar" select="$gsRawDataPath"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="CustomizationFileVarName">
            <xsl:call-template name="generateCoverageTreeVarName">
                <xsl:with-param name="typeVar" select="$gsCustomizationFile"/>
            </xsl:call-template>
        </xsl:variable>
        
        <xsl:call-template name="insertCopyRight">
            <xsl:with-param name="aeTypeTarget" select="$geShell"/>
        </xsl:call-template>
    
        <!-- Agglo constants -->
        <!-- TODO corriger et supprimer -->
        <!-- <xsl:variable name="sCoverageTreeFromShellPath"><xsl:text>..</xsl:text></xsl:variable> -->
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Path to </xsl:text><xsl:value-of select="./@name"/> coverage tree<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$CoverageTreePathVarName"/>="<xsl:value-of select="$gsCoverageTreeFromShellPath"/>"<xsl:value-of select="$gsEndl"/>
        
        <!-- Agglo constants -->
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Paths to agglo sources</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsAggloPathVarName"/>="<xsl:value-of select="$gsAggloPath"/>"<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsAggloReqPathVarName"/>="$<xsl:value-of select="$gsAggloPathVarName"/>/project/AggloReq/src"<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsAggloXslPathVarName"/>="$<xsl:value-of select="$gsAggloPathVarName"/>/development/AggloToolKit/xsl/src"<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsAggloToolKitPathVarName"/>="$<xsl:value-of select="$gsAggloPathVarName"/>/development/AggloToolKit/shell/src/utils"<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsAggloHtmlPathVarName"/>="$<xsl:value-of select="$gsAggloPathVarName"/>/development/AggloToolKit/html/src"<xsl:value-of select="$gsEndl"/>

        <!-- Coverage tree constants -->
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># </xsl:text><xsl:value-of select="./@name"/> coverage tree global constants<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$RawDataPathVarName"/>="$<xsl:value-of select="$CoverageTreePathVarName"/>/<xsl:value-of select="$RawDataPath"/>"<xsl:value-of select="$gsEndl"/>
        <xsl:if test="$HtmlReportPath!=''">
            
            <xsl:variable name="HtmlReportPathVarName">
                <xsl:call-template name="generateCoverageTreeVarName">
                    <xsl:with-param name="typeVar" select="$gsHtmlReportPath"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="CoverageReportFileVarName">
                <xsl:call-template name="generateCoverageTreeVarName">
                    <xsl:with-param name="typeVar" select="$gsCoverageReportFile"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="ProgressReportFileVarName">
                <xsl:call-template name="generateCoverageTreeVarName">
                    <xsl:with-param name="typeVar" select="$gsProgressReportFile"/>
                </xsl:call-template>
            </xsl:variable>
            
            <xsl:value-of select="$HtmlReportPathVarName"/>="$<xsl:value-of select="$CoverageTreePathVarName"/>/<xsl:value-of select="$HtmlReportPath"/>"<xsl:value-of select="$gsEndl"/>
            <!-- TODO gerer des fichiers de reporting globaux -->
<!--             <xsl:value-of select="$CoverageReportFileVarName"/>='$<xsl:value-of select="$HtmlReportPathVarName"/>/<xsl:value-of select="/coverageTree/@name"/>_Coverage.html'<xsl:value-of select="$gsEndl"/>
            <xsl:value-of select="$ProgressReportFileVarName"/>='$<xsl:value-of select="$HtmlReportPathVarName"/>/<xsl:value-of select="/coverageTree/@name"/>_Progress.html'<xsl:value-of select="$gsEndl"/>
 -->        </xsl:if>
        <xsl:value-of select="$CustomizationFileVarName"/>="$<xsl:value-of select="$CoverageTreePathVarName"/>/<xsl:value-of select="/coverageTree/@customizationFile"/>"<xsl:value-of select="$gsEndl"/>

        <!-- For each coverage level of the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel">

            <!-- Generate current coverage level constants -->
            <xsl:call-template name="generateCoverageLevelConstants"/>
            
        </xsl:for-each>
        
    </exsl:document>

</xsl:template>


<!-- Template to handle a level in coverage tree -->
<xsl:template name="generateCoverageLevelConstants">

    <xsl:variable name="nsCurrentLevel" select="."/>
    <xsl:variable name="sCurrentLevelName" select="./@name"/>
    <xsl:variable name="sRawDataPathVarName">
        <xsl:call-template name="generateCoverageTreeVarName">
            <xsl:with-param name="typeVar" select="$gsRawDataPath"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="sHtmlReportPathVarName">
        <xsl:call-template name="generateCoverageTreeVarName">
            <xsl:with-param name="typeVar" select="$gsHtmlReportPath"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="sRequirementsDestFileVarName">
        <xsl:call-template name="generateTraceabilityVar">
            <xsl:with-param name="ansUpstreamLevel" select="."/>
            <xsl:with-param name="asTypeVar" select="$gsRequirementsFile"/>
            <xsl:with-param name="aeUsage" select="$geVarName"/>
        </xsl:call-template>
    </xsl:variable>
    
    <xsl:value-of select="$gsEndl"/>
    <xsl:text># </xsl:text><xsl:value-of select="$sCurrentLevelName"/><xsl:text> coverage extraction constants</xsl:text><xsl:value-of select="$gsEndl"/>
    
    <!-- Extract output files constants -->
    <xsl:value-of select="$sRequirementsDestFileVarName"/><xsl:text>="$</xsl:text><xsl:value-of select="$sRawDataPathVarName"/>/<xsl:value-of select="$sRequirementsDestFileVarName"/><xsl:text>.xml"</xsl:text><xsl:value-of select="$gsEndl"/>
    
    <!-- for each downstream level -->
    <xsl:for-each select="/coverageTree/coverageLevel[./coveredLevels/coveredLevel/@name = $sCurrentLevelName]">
        <xsl:variable name="sCoverageFileVarName">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsCoverageFile"/>
                <xsl:with-param name="aeUsage" select="$geVarName"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="sCoverageReportFileVarName">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsCoverageReportFile"/>
                <xsl:with-param name="aeUsage" select="$geVarName"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="sProgressFileVarName">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressFile"/>
                <xsl:with-param name="aeUsage" select="$geVarName"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="sProgressReportFileVarName">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressReportFile"/>
                <xsl:with-param name="aeUsage" select="$geVarName"/>
            </xsl:call-template>
        </xsl:variable>
        
        <!-- Init coverage matrix and progress files constants -->
        <xsl:value-of select="$sCoverageFileVarName"/><xsl:text>="$</xsl:text><xsl:value-of select="$sRawDataPathVarName"/>/<xsl:value-of select="$sCoverageFileVarName"/><xsl:text>.xml"</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$sCoverageReportFileVarName"/><xsl:text>="$</xsl:text><xsl:value-of select="$sHtmlReportPathVarName"/>/<xsl:value-of select="$sCoverageReportFileVarName"/><xsl:text>.html"</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$sProgressFileVarName"/><xsl:text>="$</xsl:text><xsl:value-of select="$sRawDataPathVarName"/>/<xsl:value-of select="$sProgressFileVarName"/><xsl:text>.xml"</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$sProgressReportFileVarName"/><xsl:text>="$</xsl:text><xsl:value-of select="$sHtmlReportPathVarName"/>/<xsl:value-of select="$sProgressReportFileVarName"/><xsl:text>.html"</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:for-each>
    
    <!-- For each fileset in current coverage level -->
    <xsl:for-each select="./fileset">
        
        <!-- Generate current fileset constants -->
        <xsl:call-template name="generateFilesetConstants"/>
        
    </xsl:for-each>

</xsl:template>


<!-- Template to handle a set of file in coverage tree -->
<xsl:template name="generateFilesetConstants">

    <xsl:variable name="CoverageTreePathVarName">
        <xsl:call-template name="generateCoverageTreeVarName">
            <xsl:with-param name="typeVar" select="$gsSourcePath"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SourcePathVarName">
        <xsl:call-template name="generateFilesetVarName">
            <xsl:with-param name="fileset" select="."/>
            <xsl:with-param name="typeVar" select="$gsSourcePath"/>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SourceSelectorVarName">
        <xsl:call-template name="generateFilesetVarName">
            <xsl:with-param name="fileset" select="."/>
            <xsl:with-param name="typeVar" select="$gsSourceSelector"/>
        </xsl:call-template>
    </xsl:variable>

    <!-- Generate fileset parameters -->
    <xsl:value-of select="$SourcePathVarName"/>="$<xsl:value-of select="$CoverageTreePathVarName"/>/<xsl:value-of select="./@path"/>"<xsl:value-of select="$gsEndl"/>
    <xsl:value-of select="$SourceSelectorVarName"/>="<xsl:value-of select="./@selector"/>"<xsl:value-of select="$gsEndl"/>

    <!-- Generate regexpr constants -->
    <xsl:call-template name="generateReqParserConstants">
        <xsl:with-param name="ansReqParser" select="./reqParser"/>
    </xsl:call-template>

</xsl:template>


<!-- Template to handle a set of file in coverage tree -->
<xsl:template name="generateReqParserConstants">
    <xsl:param name="ansReqParser"/>

    <xsl:variable name="DownstreamRegExprVarName">
        <xsl:call-template name="generateReqParserVarName">
            <xsl:with-param name="reqParser" select="$ansReqParser"/>
            <xsl:with-param name="typeVar" select="$gsLevelRegExpr"/>
        </xsl:call-template>
    </xsl:variable>
                
    <!-- extract downstream expression -->
    <xsl:value-of select="$DownstreamRegExprVarName"/>="<xsl:value-of select="$ansReqParser/@downstreamRegExpr"/>"<xsl:value-of select="$gsEndl"/>
    
    <!-- if there is an upstream level -->                    
    <xsl:if test="$ansReqParser/../../coveredLevels/coveredLevel">
        
        <xsl:variable name="UpstreamListRegExprVarName">
            <xsl:call-template name="generateReqParserVarName">
                <xsl:with-param name="reqParser" select="$ansReqParser"/>
                <xsl:with-param name="typeVar" select="$gsUpstreamListRegExpr"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="UpstreamRegExprVarName">
            <xsl:call-template name="generateReqParserVarName">
                <xsl:with-param name="reqParser" select="$ansReqParser"/>
                <xsl:with-param name="typeVar" select="$gsUpstreamRegExpr"/>
            </xsl:call-template>
        </xsl:variable>
        
        <!-- extract upstream regular expression -->                    
        <xsl:value-of select="$UpstreamListRegExprVarName"/>="<xsl:value-of select="$ansReqParser/@upstreamRegExprList"/>"<xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$UpstreamRegExprVarName"/>="<xsl:value-of select="$ansReqParser/@upstreamSubRegExpr"/>"<xsl:value-of select="$gsEndl"/>
    </xsl:if>
    
</xsl:template>


<xsl:template name="generateInitShell">

    <xsl:variable name="sShellPath">
        <xsl:value-of select="$gsGeneratedShellPath"/><xsl:text>/</xsl:text><xsl:value-of select="$gsCoverageTreeInitPath"/>
    </xsl:variable>

     <exsl:document href="{$sShellPath}" method="text">
               
        <xsl:call-template name="insertCopyRight">
            <xsl:with-param name="aeTypeTarget" select="$geShell"/>
        </xsl:call-template>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Global variables</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:text># equivalent $(dirname $0)</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsRelativePathVarName"/>=${0%/*}<xsl:value-of select="$gsEndl"/>
        <!-- TODO creer un shell de backup, dans 2 rep de backup, coverage et progress, et invoquer le script au traceabilityUpdate -->
        <xsl:text>BackupInitialTraceabilityDir=$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/initialTraceability</xsl:text><xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text>source "$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/</xsl:text><xsl:value-of select="$gsCoverageTreeConstantsPath"/><xsl:text>"</xsl:text><xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Extract requirements</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- For each coverage level of the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel">

        <!-- Generate call to current level extract requirement shell -->
        <xsl:call-template name="generateTraceabilityVar">
            <xsl:with-param name="ansUpstreamLevel" select="."/>
            <xsl:with-param name="asTypeVar" select="$gsRequirementExtractShell"/>
            <xsl:with-param name="aeUsage" select="$geShellCall"/>
        </xsl:call-template>
        <xsl:value-of select="$gsEndl"/>
        </xsl:for-each>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Create progress files</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- TODO creer les fichiers de progress meme pour les coveragelevl qui ne sont pas couverts ? de maniere a ce que l'utilisateur puisse utiliser ces fichiers a la main sans passer par agglo -->
        <!-- For each traceability link in the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel/coveredLevels/coveredLevel">
            <xsl:variable name="sUpstreamLevelName" select="./@name"/>

            <!-- Generate call to current level progress init shell -->
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="/coverageTree/coverageLevel[./@name = $sUpstreamLevelName]"/>
                <xsl:with-param name="ansDownstreamLevel" select="../.."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressInitShell"/>
                <xsl:with-param name="aeUsage" select="$geShellCall"/>
            </xsl:call-template>
            <xsl:value-of select="$gsEndl"/>
        </xsl:for-each>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Extract coverage, update progress files and create initial reports</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- TODO passer par generateTraceabilityVar -->
        <xsl:text>$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/</xsl:text><xsl:value-of select="$gsCoverageTreeUpdatePath"/><xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Backup initial requirements, coverage and progress files</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:text>mkdir -p "$BackupInitialTraceabilityDir"</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- For each coverage level of the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel">

            <xsl:variable name="nsCurrentUpstreamLevel" select="."/>
            <xsl:variable name="sCurrentUpstreamLevelName" select="./@name"/>
            <xsl:variable name="sRequirementFile">
                <xsl:call-template name="generateTraceabilityVar">
                    <xsl:with-param name="ansUpstreamLevel" select="."/>
                    <xsl:with-param name="asTypeVar" select="$gsRequirementsFile"/>
                    <xsl:with-param name="aeUsage" select="$geFullPath"/>
                </xsl:call-template>
            </xsl:variable>
            
            <!-- Generate call to current level requirement file backup -->
            <xsl:text>cp -t "$BackupInitialTraceabilityDir"</xsl:text><xsl:value-of select="$sRequirementFile"/><xsl:value-of select="$gsEndl"/>
            
            <!-- for each traceability link to nsCurrentUpstreamLevel -->
            <xsl:for-each select="/coverageTree/coverageLevel[./coveredLevels/coveredLevel/@name = $sCurrentUpstreamLevelName]">
                <xsl:variable name="sCoverageFile">
                    <xsl:call-template name="generateTraceabilityVar">
                        <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentUpstreamLevel"/>
                        <xsl:with-param name="ansDownstreamLevel" select="."/>
                        <xsl:with-param name="asTypeVar" select="$gsCoverageFile"/>
                        <xsl:with-param name="aeUsage" select="$geFullPath"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="sCoverageReportFile">
                    <xsl:call-template name="generateTraceabilityVar">
                        <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentUpstreamLevel"/>
                        <xsl:with-param name="ansDownstreamLevel" select="."/>
                        <xsl:with-param name="asTypeVar" select="$gsCoverageReportFile"/>
                        <xsl:with-param name="aeUsage" select="$geFullPath"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="sProgressFile">
                    <xsl:call-template name="generateTraceabilityVar">
                        <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentUpstreamLevel"/>
                        <xsl:with-param name="ansDownstreamLevel" select="."/>
                        <xsl:with-param name="asTypeVar" select="$gsProgressFile"/>
                        <xsl:with-param name="aeUsage" select="$geFullPath"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="sProgressReportFile">
                    <xsl:call-template name="generateTraceabilityVar">
                        <xsl:with-param name="ansUpstreamLevel" select="$nsCurrentUpstreamLevel"/>
                        <xsl:with-param name="ansDownstreamLevel" select="."/>
                        <xsl:with-param name="asTypeVar" select="$gsProgressReportFile"/>
                        <xsl:with-param name="aeUsage" select="$geFullPath"/>
                    </xsl:call-template>
                </xsl:variable>
                
                <!-- Generate call to current traceability file backup -->
                <xsl:text>cp -t "$BackupInitialTraceabilityDir"</xsl:text><xsl:value-of select="$sCoverageFile"/><xsl:value-of select="$gsEndl"/>
                <xsl:text>cp -t "$BackupInitialTraceabilityDir"</xsl:text><xsl:value-of select="$sCoverageReportFile"/><xsl:value-of select="$gsEndl"/>
                <xsl:text>cp -t "$BackupInitialTraceabilityDir"</xsl:text><xsl:value-of select="$sProgressFile"/><xsl:value-of select="$gsEndl"/>
                <xsl:text>cp -t "$BackupInitialTraceabilityDir"</xsl:text><xsl:value-of select="$sProgressReportFile"/><xsl:value-of select="$gsEndl"/>
            </xsl:for-each>
        </xsl:for-each>

    </exsl:document>

</xsl:template>


<xsl:template name="generateUpdateShell">

    <xsl:variable name="sShellPath">
        <xsl:value-of select="$gsGeneratedShellPath"/><xsl:text>/</xsl:text><xsl:value-of select="$gsCoverageTreeUpdatePath"/>
    </xsl:variable>

    <exsl:document href="{$sShellPath}" method="text">
        
        <xsl:call-template name="insertCopyRight">
            <xsl:with-param name="aeTypeTarget" select="$geShell"/>
        </xsl:call-template>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Global variables</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:text># equivalent $(dirname $0)</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsRelativePathVarName"/>=${0%/*}<xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text>source "$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/</xsl:text><xsl:value-of select="$gsCoverageTreeConstantsPath"/><xsl:text>"</xsl:text><xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Extract coverage</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- For each traceability link in the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel/coveredLevels/coveredLevel">
            
            <!-- Generate call to current level extract coverage shell -->
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="/coverageTree/coverageLevel[@name=./@name]"/>
                <xsl:with-param name="ansDownstreamLevel" select="../.."/>
                <xsl:with-param name="asTypeVar" select="$gsCoverageExtractShell"/>
                <xsl:with-param name="aeUsage" select="$geShellCall"/>
            </xsl:call-template>
            <xsl:value-of select="$gsEndl"/>
        </xsl:for-each>
            
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Update progress files using new coverage matrix files</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- For each traceability link in the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel/coveredLevels/coveredLevel">

            <!-- Generate call to current level progress update shell -->
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="/coverageTree/coverageLevel[@name=./@name]"/>
                <xsl:with-param name="ansDownstreamLevel" select="../.."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressUpdateShell"/>
                <xsl:with-param name="aeUsage" select="$geShellCall"/>
            </xsl:call-template>
            <xsl:value-of select="$gsEndl"/>
        </xsl:for-each>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Update coverage report files using new coverage matrix files</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- For each traceability link in the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel/coveredLevels/coveredLevel">

            <!-- Generate call to current level coverage report shell -->
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="/coverageTree/coverageLevel[@name=./@name]"/>
                <xsl:with-param name="ansDownstreamLevel" select="../.."/>
                <xsl:with-param name="asTypeVar" select="$gsCoverageReportShell"/>
                <xsl:with-param name="aeUsage" select="$geShellCall"/>
            </xsl:call-template>
            <xsl:value-of select="$gsEndl"/>
        </xsl:for-each>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Update progress report files using new progress files</xsl:text><xsl:value-of select="$gsEndl"/>
        <!-- For each traceability link in the coverage tree -->
        <xsl:for-each select="/coverageTree/coverageLevel/coveredLevels/coveredLevel">
        
            <!-- Generate call to current level progress report shell -->
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="/coverageTree/coverageLevel[@name=./@name]"/>
                <xsl:with-param name="ansDownstreamLevel" select="../.."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressReportShell"/>
                <xsl:with-param name="aeUsage" select="$geShellCall"/>
            </xsl:call-template>
            <xsl:value-of select="$gsEndl"/>
        </xsl:for-each>

    </exsl:document>

</xsl:template>


<xsl:template name="generateTraceabilityShell">
    <xsl:param name="ansUpstreamLevel"/>
    <xsl:param name="asTypeShell"/>
    
    <xsl:variable name="sCurrentShellPath">
        <xsl:value-of select="$gsGeneratedShellPath"/><xsl:text>/</xsl:text>
        <!-- TODO unifier les appel a generateTraceabilityVar? -->
        <xsl:choose>
            <xsl:when test="($asTypeShell=$gsRequirementExtractShell)">
                <xsl:call-template name="generateTraceabilityVar">
                    <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                    <xsl:with-param name="asTypeVar" select="$asTypeShell"/>
                    <xsl:with-param name="aeUsage" select="$geShellName"/>
                </xsl:call-template>
            </xsl:when>
            
            <xsl:otherwise>
                <xsl:call-template name="generateTraceabilityVar">
                    <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                    <xsl:with-param name="ansDownstreamLevel" select="."/>
                    <xsl:with-param name="asTypeVar" select="$asTypeShell"/>
                    <xsl:with-param name="aeUsage" select="$geShellName"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- Export shell in a file dedicated to Upstream/Downstream coverage -->
    <exsl:document href="{$sCurrentShellPath}" method="text">
    
        <xsl:variable name="sAggloReqPath">
            <xsl:text>$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/>/$<xsl:value-of select="$gsAggloReqPathVarName"/>
        </xsl:variable>
        
        <xsl:variable name="CustomizationFile">
            <xsl:text> "$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/$</xsl:text>
            <xsl:call-template name="generateCoverageTreeVarName">
                <xsl:with-param name="typeVar" select="$gsCustomizationFile"/>
            </xsl:call-template>
            <xsl:text>"</xsl:text>
        </xsl:variable>
        
        <xsl:variable name="sRequirementFile">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                <xsl:with-param name="asTypeVar" select="$gsRequirementsFile"/>
                <xsl:with-param name="aeUsage" select="$geFullPath"/>
            </xsl:call-template>
        </xsl:variable>
    
        <xsl:variable name="sCoverageFile">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsCoverageFile"/>
                <xsl:with-param name="aeUsage" select="$geFullPath"/>
            </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="sProgressFile">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressFile"/>
                <xsl:with-param name="aeUsage" select="$geFullPath"/>
            </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="sProgressReportFile">
            <xsl:call-template name="generateTraceabilityVar">
                <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                <xsl:with-param name="ansDownstreamLevel" select="."/>
                <xsl:with-param name="asTypeVar" select="$gsProgressReportFile"/>
                <xsl:with-param name="aeUsage" select="$geFullPath"/>
            </xsl:call-template>
        </xsl:variable>
        
        <xsl:call-template name="insertCopyRight">
            <xsl:with-param name="aeTypeTarget" select="$geShell"/>
        </xsl:call-template>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Global variables</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:text># equivalent $(dirname $0)</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$gsRelativePathVarName"/>=${0%/*}<xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text>source "$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/>/<xsl:value-of select="$gsCoverageTreeConstantsPath"/>"<xsl:value-of select="$gsEndl"/>
        
        <xsl:choose>
            <xsl:when test="($asTypeShell=$gsRequirementExtractShell) or ($asTypeShell=$gsCoverageExtractShell)">
                <xsl:call-template name="generateMergedFilesetShell">
                    <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                    <xsl:with-param name="asTypeShell" select="$asTypeShell"/>
                </xsl:call-template>
            </xsl:when>
            
            <xsl:when test="$asTypeShell=$gsCoverageReportShell">
            
                <xsl:variable name="sCoverageReportFile">
                    <xsl:call-template name="generateTraceabilityVar">
                        <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                        <xsl:with-param name="ansDownstreamLevel" select="."/>
                        <xsl:with-param name="asTypeVar" select="$gsCoverageReportFile"/>
                        <xsl:with-param name="aeUsage" select="$geFullPath"/>
                    </xsl:call-template>
                </xsl:variable>
                
                <xsl:value-of select="$gsEndl"/>
                <xsl:text># Create coverage report from former xml progress file and up to date coverage file</xsl:text><xsl:value-of select="$gsEndl"/>
                <xsl:value-of select="$sAggloReqPath"/>/arqReportCoverage.sh <xsl:value-of select="$sCoverageFile"/><xsl:value-of select="$sRequirementFile"/><xsl:value-of select="$sCoverageReportFile"/><xsl:value-of select="$CustomizationFile"/><xsl:value-of select="$gsEndl"/>
            </xsl:when>
            
            <xsl:when test="$asTypeShell=$gsProgressInitShell">
                <xsl:value-of select="$gsEndl"/>
                <xsl:text># Init progress file from xml requirements file</xsl:text><xsl:value-of select="$gsEndl"/>
                <xsl:value-of select="$sAggloReqPath"/>/arqInitCoverageProgress.sh <xsl:value-of select="$sProgressFile"/><xsl:value-of select="$sRequirementFile"/><xsl:value-of select="$gsEndl"/>
            </xsl:when>
            
            <xsl:when test="$asTypeShell=$gsProgressUpdateShell">
                <xsl:value-of select="$gsEndl"/>
                <xsl:text># Update progress file from former xml progress file and up to date coverage file</xsl:text><xsl:value-of select="$gsEndl"/>
                <xsl:value-of select="$sAggloReqPath"/>/arqUpdateCoverageProgress.sh <xsl:value-of select="$sProgressFile"/><xsl:value-of select="$sProgressFile"/><xsl:value-of select="$sCoverageFile"/><xsl:value-of select="$CustomizationFile"/><xsl:value-of select="$gsEndl"/>
            </xsl:when>

            <xsl:when test="$asTypeShell=$gsProgressReportShell">
                <xsl:value-of select="$gsEndl"/>
                <xsl:text># Report progress from xml progress file and requirement file</xsl:text><xsl:value-of select="$gsEndl"/>
                <xsl:value-of select="$sAggloReqPath"/>/arqReportCoverageProgress.sh <xsl:value-of select="$sProgressFile"/><xsl:value-of select="$sProgressReportFile"/><xsl:value-of select="$sRequirementFile"/><xsl:value-of select="$CustomizationFile"/><xsl:value-of select="$gsEndl"/>
            </xsl:when>
        </xsl:choose>

    </exsl:document>
        
</xsl:template>


<xsl:template name="generateMergedFilesetShell">
    <xsl:param name="ansUpstreamLevel"/>
    <xsl:param name="asTypeShell"/>

    <xsl:variable name="sShellPath">
        <!-- <xsl:value-of select="$asTypeShell"/><xsl:value-of select="$gsRelativePathVarName"/> -->
        <xsl:value-of select="$gsRelativePathVarName"/>
    </xsl:variable>
        
    <xsl:variable name="sAggloReqPath">
        <xsl:text>$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/>/$<xsl:value-of select="$gsAggloReqPathVarName"/>
    </xsl:variable>
        
    <xsl:variable name="sAggloXslPath">
        <xsl:text>$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/>/$<xsl:value-of select="$gsAggloXslPathVarName"/>
    </xsl:variable>
    
    <xsl:variable name="sFinalDestFile">
        <xsl:text> "$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/$</xsl:text>
        <xsl:choose>
            <xsl:when test="$asTypeShell=$gsRequirementExtractShell">
                <xsl:call-template name="generateTraceabilityVar">
                    <xsl:with-param name="ansUpstreamLevel" select="."/>
                    <xsl:with-param name="asTypeVar" select="$gsRequirementsFile"/>
                    <xsl:with-param name="aeUsage" select="$geVarName"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$asTypeShell=$gsCoverageExtractShell">
                <xsl:call-template name="generateTraceabilityVar">
                    <xsl:with-param name="ansUpstreamLevel" select="$ansUpstreamLevel"/>
                    <xsl:with-param name="ansDownstreamLevel" select="."/>
                    <xsl:with-param name="asTypeVar" select="$gsCoverageFile"/>
                    <xsl:with-param name="aeUsage" select="$geVarName"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
        <xsl:text>"</xsl:text>
    </xsl:variable>
    
    <xsl:variable name="sTempFile">
        <xsl:text>Temp</xsl:text><xsl:value-of select="$asTypeShell"/><xsl:text>File</xsl:text>
    </xsl:variable>                

    <xsl:if test="count(./fileset)>1">
        <xsl:variable name="sAggloShellPath">
            <xsl:text>$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/>/$<xsl:value-of select="$gsAggloToolKitPathVarName"/>
        </xsl:variable>
        
        <xsl:text>source "</xsl:text><xsl:value-of select="$sAggloShellPath"/>/atkFileSystem.sh"<xsl:value-of select="$gsEndl"/>
        
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Declare temp file for extracted datas merging</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:value-of select="$sTempFile"/><xsl:text>="$TempDir/Temp</xsl:text><xsl:value-of select="$asTypeShell"/>.xml"<xsl:value-of select="$gsEndl"/>
    </xsl:if>
    
    <xsl:for-each select="./fileset">
    
        <xsl:variable name="sDownstreamRegExpr">
            <xsl:text> "$</xsl:text>
            <xsl:call-template name="generateReqParserVarName">
                <xsl:with-param name="reqParser" select="./reqParser"/>
                <xsl:with-param name="typeVar" select="$gsLevelRegExpr"/>
            </xsl:call-template>
            <xsl:text>"</xsl:text>
        </xsl:variable>
        
        <xsl:variable name="sSourcePath">
            <xsl:text> "$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/$</xsl:text>
            <xsl:call-template name="generateFilesetVarName">
                <xsl:with-param name="fileset" select="."/>
                <xsl:with-param name="typeVar" select="$gsSourcePath"/>
            </xsl:call-template>
            <xsl:text>"</xsl:text>
        </xsl:variable>
        
        <xsl:variable name="sSourceSelector">
            <xsl:text> "$</xsl:text>
            <xsl:call-template name="generateFilesetVarName">
                <xsl:with-param name="fileset" select="."/>
                <xsl:with-param name="typeVar" select="$gsSourceSelector"/>
            </xsl:call-template>
            <xsl:text>"</xsl:text>
        </xsl:variable>
        
        <xsl:variable name="sRecursive">
            <xsl:if test="./@recursive='yes'">
                <xsl:text> Recursive</xsl:text>
            </xsl:if>
        </xsl:variable>
        
        <xsl:variable name="sDestFile">
            <xsl:choose>
                <xsl:when test="position()=1">
                    <xsl:value-of select="$sFinalDestFile"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text> "$</xsl:text><xsl:value-of select="$sTempFile"/><xsl:text>"</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>                

        <xsl:value-of select="$gsEndl"/>
        <xsl:choose>
            <xsl:when test="$asTypeShell=$gsRequirementExtractShell">
                
                <xsl:text># Extract requirements from </xsl:text><xsl:value-of select="./@name"/><xsl:text> </xsl:text><xsl:value-of select="$ansUpstreamLevel/@name"/><xsl:value-of select="$gsEndl"/>
                <!-- TODO ajouter la gestion du recursif sur l'extraction des requirements -->
                <xsl:value-of select="$sAggloReqPath"/><xsl:text>/arqExtractRequirements.sh</xsl:text><xsl:value-of select="$sDownstreamRegExpr"/><xsl:value-of select="$sSourcePath"/><xsl:value-of select="$sSourceSelector"/><xsl:value-of select="$sDestFile"/><xsl:value-of select="$gsEndl"/>
            </xsl:when>

            <xsl:when test="$asTypeShell=$gsCoverageExtractShell">
                
               <xsl:variable name="sUpstreamListRegExpr">
                    <xsl:text> "$</xsl:text>
                    <xsl:call-template name="generateReqParserVarName">
                        <xsl:with-param name="reqParser" select="./reqParser"/>
                        <xsl:with-param name="typeVar" select="$gsUpstreamListRegExpr"/>
                    </xsl:call-template>
                    <xsl:text>"</xsl:text>
                </xsl:variable>
                <xsl:variable name="sUpstreamRegExpr">
                    <xsl:text> "$</xsl:text>
                    <xsl:call-template name="generateReqParserVarName">
                        <xsl:with-param name="reqParser" select="./reqParser"/>
                        <xsl:with-param name="typeVar" select="$gsUpstreamRegExpr"/>
                    </xsl:call-template>
                    <xsl:text>"</xsl:text>
                </xsl:variable>
                
                <xsl:text># Extract coverage from </xsl:text><xsl:value-of select="./@name"/><xsl:text> </xsl:text><xsl:value-of select="$ansUpstreamLevel/@name"/><xsl:value-of select="$gsEndl"/>
                <xsl:value-of select="$sAggloReqPath"/><xsl:text>/arqExtractCoverage.sh</xsl:text><xsl:value-of select="$sDownstreamRegExpr"/><xsl:value-of select="$sUpstreamListRegExpr"/><xsl:value-of select="$sUpstreamRegExpr"/><xsl:value-of select="$sSourcePath"/><xsl:value-of select="$sSourceSelector"/><xsl:value-of select="$sDestFile"/><xsl:value-of select="$sRecursive"/><xsl:value-of select="$gsEndl"/>
                
            </xsl:when>            
        </xsl:choose>
        
        <xsl:if test="position()>1">
            <xsl:value-of select="$sAggloXslPath"/><xsl:text>/atkMergeXml.sh</xsl:text><xsl:value-of select="$sFinalDestFile"/><xsl:value-of select="$sDestFile"/><xsl:value-of select="$sFinalDestFile"/><xsl:value-of select="$gsEndl"/>
        </xsl:if>
    </xsl:for-each>
    
    <xsl:if test="count(./fileset)>1">
        <xsl:value-of select="$gsEndl"/>
        <xsl:text># Clean temp file</xsl:text><xsl:value-of select="$gsEndl"/>
        <xsl:text>rm "$</xsl:text><xsl:value-of select="$sTempFile"/><xsl:text>"</xsl:text><xsl:value-of select="$gsEndl"/>
    </xsl:if>                        
    
</xsl:template>


<xsl:template name="generateCoverageTreeVarName">
    <xsl:param name="typeVar"/>
    
    <!-- Generate name of variable for the coverage tree -->
    <xsl:value-of select="/coverageTree/@name"/>CoverageTree_<xsl:value-of select="$typeVar"/>
 </xsl:template>


<xsl:template name="generateTraceabilityVar">
    <xsl:param name="ansUpstreamLevel"/>
    <xsl:param name="ansDownstreamLevel" select="''"/>
    <xsl:param name="asTypeVar"/>
    <xsl:param name="aeUsage"/>
    
    <!-- Generate name of variable for a coverage level -->
    <xsl:choose>
        <xsl:when test="$aeUsage=$geVarEval">
            <xsl:text>$</xsl:text>
        </xsl:when>
        
        <xsl:when test="($aeUsage=$geFullPath) or ($aeUsage=$geShellCall)">
            <xsl:if test="$aeUsage=$geFullPath">
                <xsl:text> "</xsl:text>
            </xsl:if>
            
            <xsl:text>$</xsl:text><xsl:value-of select="$gsRelativePathVarName"/><xsl:text>/</xsl:text>
            <xsl:if test="$aeUsage=$geFullPath">
                <xsl:text>$</xsl:text>
            </xsl:if>
        </xsl:when>
    </xsl:choose>
    
    <xsl:value-of select="/coverageTree/@name"/><xsl:value-of select="$ansUpstreamLevel/@name"/><xsl:text>_</xsl:text>
    <!-- TODO plutot tester asTypeVar? -->
    <xsl:if test="$ansDownstreamLevel!=''">
        <xsl:text>CoverageBy</xsl:text><xsl:value-of select="$ansDownstreamLevel/@name"/><xsl:text>_</xsl:text>
    </xsl:if>
    <xsl:value-of select="$asTypeVar"/>

    <xsl:choose>
        <xsl:when test="($aeUsage=$geShellName) or ($aeUsage=$geShellCall)">
            <xsl:text>.sh</xsl:text>
        </xsl:when>
        
        <xsl:when test="$aeUsage=$geFullPath">
            <xsl:text>"</xsl:text>
        </xsl:when>
    </xsl:choose>
    
</xsl:template>


<xsl:template name="generateFilesetVarName">
    <xsl:param name="fileset"/>
    <xsl:param name="typeVar"/>
    
    <!-- Generate name of variable for source documents path -->
    <xsl:value-of select="/coverageTree/@name"/><xsl:value-of select="$fileset/../@name"/><xsl:text>_</xsl:text><xsl:value-of select="$fileset/@name"/><xsl:text>_</xsl:text><xsl:value-of select="$typeVar"/>
</xsl:template>


<xsl:template name="generateReqParserVarName">
    <xsl:param name="reqParser"/>
    <xsl:param name="typeVar"/>
    
    <!-- Generate name of variable for downstream requirement regexpr -->
    <xsl:value-of select="/coverageTree/@name"/><xsl:value-of select="$reqParser/../../@name"/><xsl:text>_</xsl:text><xsl:value-of select="$reqParser/../@name"/><xsl:text>_</xsl:text><xsl:value-of select="$typeVar"/>
</xsl:template>


</xsl:stylesheet>

