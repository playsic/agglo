<?xml version="1.0" encoding="UTF-8"?>
<!--************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
*************************************************************************-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" version="4.0"/>

<!-- Utilities -->
<xsl:include href="../../../development/AggloToolKit/xsl/src/atkLayout.xsl"/>
<xsl:include href="arqUtils.xsl"/>


<!-- Template to handle progress file -->
<xsl:template match="/">

    <html lang="fr">
        <head>
            <meta HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=utf-8"/>
            <title>Progress Report</title>
            <link rel='stylesheet' href='css/structure.css' type='text/css'/>
            <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css"/>
            <link rel="stylesheet" type="text/css" href="css/basic.css" media='screen'/>
            <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script><xsl:value-of select="$gsEndl"/>
            <script type="text/javascript" src="js/jquery.jqplot.min.js"></script><xsl:value-of select="$gsEndl"/>
            <script type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script><xsl:value-of select="$gsEndl"/>
            <script type="text/javascript" src="js/jqplot.donutRenderer.min.js"></script><xsl:value-of select="$gsEndl"/>
            <script type="text/javascript" src="js/jquery.simplemodal.1.4.2.min.js"></script><xsl:value-of select="$gsEndl"/>
            <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script><xsl:value-of select="$gsEndl"/>
            <!-- IE6 "fix" for the close png image -->
            <!--[if lt IE 7]>
            <link type='text/css' href='css/basic_ie.css' rel='stylesheet' media='screen' />
            <![endif]-->
        </head>

        <body>
            <script class="code" type="text/javascript"> 
                /* 
                 * When page loading
                 * - Apply jQuery tablesorter for the requirement table
                 */
                $(document).ready(function() {

                    // jQuery sort table
                    $("#RequirementsList").tablesorter();
                });

                /*
                 * Function for display popup (table with method-function details)
                 */
                jQuery(function ($) {
                    $('.basic').click(function (e) {
                        var htmlStr = $(this).next().html();
                        $('#basic-modal-content').html(htmlStr);
                        $('#basic-modal-content').modal();

                        return false;
                    });
                });

                /*
                 * Function who display block or none a content (by Id)
                 */
                function swap(obj) 
                {
                    var o = document.getElementById(obj).style.display;
                    if (o == "block") 
                    {
                        document.getElementById(obj).style.display = "none";
                        document.getElementById("btn_" + obj).innerHTML = "Show all requirements";
                    }
                    else
                    {
                        document.getElementById(obj).style.display = "block";
                        document.getElementById("btn_" + obj).innerHTML = "Hide all requirements";
                    }
                }
            </script><xsl:value-of select="$gsEndl"/>

            <div id="global">
                <div class="content">
                    <h1>Progress Report - <xsl:value-of select="/AvancementProjet/@date"/></h1>

                    <h3>Resume and Average</h3>
                    <div class="resume">
                        <table class="resume" style="float:left">

                            <xsl:variable name="NbRequirements">
                                <xsl:call-template name="ComputeNbRequirements"/>
                            </xsl:variable>

                            <tr>
                                <th>Metric type</th><th>Value</th>
                            </tr>
                            <tr>
                                <td>Progress weithed by criticity</td>
                                <td>
                                    <xsl:call-template name="ComputeWeightedProgress">
				                        <xsl:with-param name="RequirementList" select="/AvancementProjet/Requirement"/>
				                        <xsl:with-param name="GlobalProgressValue" select="0"/>
				                        <xsl:with-param name="GlobalWeight" select="0"/>
				                        <xsl:with-param name="WeightType" select="'Criticity'"/>
                                    </xsl:call-template>
                                </td>
                            </tr>
                            <tr>
                                <td>Progress weithed by complexity</td>
                                <td>
                                    <xsl:call-template name="ComputeWeightedProgress">
				                        <xsl:with-param name="RequirementList" select="/AvancementProjet/Requirement"/>
				                        <xsl:with-param name="GlobalProgressValue" select="0"/>
				                        <xsl:with-param name="GlobalWeight" select="0"/>
				                        <xsl:with-param name="WeightType" select="'Complexity'"/>
                                    </xsl:call-template>
                                </td>
                            </tr>
                            <tr>
                                <td>Number of requirements</td>
                                <td>
                                    <xsl:value-of select="$NbRequirements"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Pourcentage of started requirements</td>
                                <td>
                                    <xsl:variable name="NbStartedRequirements">
                                        <xsl:call-template name="ComputeNbRequirements">
                                           <xsl:with-param name="Progress" select="1"/>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:value-of select="format-number($NbStartedRequirements div $NbRequirements, '###.#%')"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Pourcentage of finished requirements</td>
                                <td>
                                    <xsl:variable name="NbFinishedRequirements">
                                        <xsl:call-template name="ComputeNbRequirements">
                                           <xsl:with-param name="Progress" select="100"/>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:value-of select="format-number($NbFinishedRequirements div $NbRequirements, '###.#%')"/>
                                </td>
                            </tr>
    
                        </table>
                    </div><br class="clear"/>

                    <br/><h3>Per requirement progress</h3>
                    <div class="details">
                        <a href="javascript:;" onclick="return swap('RequirementsList')" id="btn_RequirementsList" class="show_details">Show all requirements</a><br class="clear"/>
                        
                        <div style="margin-top:20px">
                            <table id="RequirementsList" class="tablesorter detail" style="float:left;display:none">
                                 <thead>
                                    <tr>
                                        <th>Requirement Id</th><th>Requirement Criticity</th><th>Requirement Complexity</th><th>Progress</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <xsl:apply-templates select="/AvancementProjet/Requirement"/>
                                 </tbody>
                            </table>
                        </div><br class="clear"/>

                    </div>
                    <br/>

                </div>
            </div>
        </body>

    </html>

</xsl:template>



<!-- Template to handle a requirement -->
<xsl:template match="/AvancementProjet/Requirement">

    <xsl:variable name="RequirementName" select="./@name"/>
    <xsl:variable name="Color">
        <xsl:call-template name="GetColor">
           <xsl:with-param name="Progress" select="./AvancementReq/@value"/>
        </xsl:call-template>
    </xsl:variable>

    <tr class="file_detail">
        <td class="title"><xsl:value-of select="./@name"/></td>
        <td><xsl:value-of select="document($STR_RequirementFile)/Requirements//file/Requirement[@name=$RequirementName]/@Criticity"/></td>
        <td><xsl:value-of select="document($STR_RequirementFile)/Requirements//file/Requirement[@name=$RequirementName]/@Complexity"/></td>
        <xsl:element name="td">
            <xsl:attribute name="style">background-color:<xsl:value-of select="$Color"/></xsl:attribute>
            <xsl:value-of select="./AvancementReq/@value"/>
        </xsl:element>
    </tr>

</xsl:template>

</xsl:stylesheet>

