<?xml version="1.0" encoding="UTF-8"?>
<!--************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
*************************************************************************-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="UTF-8"/>

<!-- Utilities -->
<xsl:include href="../../../development/AggloToolKit/xsl/src/atkLayout.xsl"/>

<!-- XSL arguments -->
<xsl:param name="STR_CurrentDate"><xsl:message terminate="yes">Error! Please set STR_CurrentDate param</xsl:message></xsl:param>


<!-- Template to handle initial requirements file -->
<xsl:template match="/">

    <!-- Beginning of new xml progress file -->
    <xsl:element name="AvancementProjet">
        <xsl:attribute name="date"><xsl:value-of select="$STR_CurrentDate"/></xsl:attribute>
        <xsl:value-of select="$gsEndl"/>

        <!-- For all requirements, call the appropriate template -->
        <xsl:apply-templates select="//Requirement"/>

    <!-- End of new xml progress file -->
    </xsl:element><xsl:value-of select="$gsEndl"/>

</xsl:template>


<!-- Template to handle a requirement -->
<xsl:template match="//Requirement">

    <xsl:value-of select="$STR_Indent1"/>
    <xsl:element name="Requirement">
        <xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
        <xsl:value-of select="$gsEndl"/>

        <xsl:value-of select="$STR_Indent2"/>
        <xsl:element name="AvancementReq">
            <xsl:attribute name="value">0</xsl:attribute>
            
        </xsl:element><xsl:value-of select="$gsEndl"/><xsl:value-of select="$STR_Indent1"/>
        
    </xsl:element><xsl:value-of select="$gsEndl"/>

</xsl:template>

</xsl:stylesheet>

