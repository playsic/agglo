<?xml version="1.0" encoding="UTF-8"?>
<!--************************************************************************
** 
** Copyright (C) 2025 Plaisic and/or its subsidiary(-ies).
** Contact: eti.laurent@gmail.com
** 
** This file is part of the Agglo project.
** 
** AGGLO_BEGIN_LICENSE
** Commercial License Usage
** Licensees holding valid commercial Agglo licenses may use this file in 
** accordance with the commercial license agreement provided with the 
** Software or, alternatively, in accordance with the terms contained in 
** a written agreement between you and Plaisic.  For licensing terms and 
** conditions contact eti.laurent@gmail.com.
** 
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
** 
** In addition, the following conditions apply: 
**     * Redistributions in binary form must reproduce the above copyright 
**       notice, this list of conditions and the following disclaimer in 
**       the documentation and/or other materials provided with the 
**       distribution.
**     * Neither the name of the Agglo project nor the names of its  
**       contributors may be used to endorse or promote products derived 
**       from this software without specific prior written permission.
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
** 
** AGGLO_END_LICENSE
** 
*************************************************************************-->


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- XSL arguments -->
<xsl:param name="STR_RequirementParamFile"><!--<xsl:message terminate="yes">Error! Please set STR_RequirementParamFile param</xsl:message>--></xsl:param>
<xsl:param name="STR_RequirementFile"><!--<xsl:message terminate="yes">Error! Please set STR_RequirementFile param</xsl:message>--></xsl:param>
<xsl:param name="STR_CoverageFile"><!--<xsl:message terminate="yes">Error! Please set STR_CoverageFile param</xsl:message>--></xsl:param>
<xsl:param name="STR_ProgressFile"><!--<xsl:message terminate="yes">Error! Please set STR_ProgressFile param</xsl:message>--></xsl:param>


<!-- Template to compute number of requirements -->
<xsl:template name="ComputeNbRequirements">
    <xsl:param name="WeightType" select="''"/>
    <xsl:param name="Weight" select="''"/>
    <xsl:param name="Progress" select="0"/>

	<xsl:variable name="AllRequirements" select="document($STR_RequirementFile)/Requirements/file/Requirement"/>

	<xsl:choose>
		<!-- If the progress is not set -->
		<xsl:when test="$Progress=0">
			<xsl:choose>
				<!-- If the type is not set -->
				<xsl:when test="$WeightType=''">
					<!-- Count all requirements -->
					<xsl:value-of select="count($AllRequirements)"/>
				</xsl:when>

				<!-- If the type is set -->
				<xsl:otherwise>
					<!-- Count requirements which weight type equals given weight -->
					<xsl:value-of select="count($AllRequirements/@*[name()=$WeightType and .=$Weight])"/>
				</xsl:otherwise>

			</xsl:choose>
		</xsl:when>

        <!-- If the type is set -->
		<xsl:otherwise>
			<xsl:variable name="ProgressedRequirements" select="document($STR_ProgressFile)/AvancementProjet/Requirement/AvancementReq[@value&gt;=$Progress]/../@name"/>
			
			<xsl:choose>
				<!-- If the type is not set -->
				<xsl:when test="$WeightType=''">
					<!-- Count all requirements -->
					<xsl:value-of select="count($ProgressedRequirements)"/>
				</xsl:when>

				<!-- If the type is set -->
				<xsl:otherwise>
					<!-- Count requirements which weight type equals given weight -->
					<xsl:value-of select="count($AllRequirements[@name=$ProgressedRequirements]/@*[name()=$WeightType and .=$Weight])"/>
				</xsl:otherwise>

			</xsl:choose>
		</xsl:otherwise>
		
	</xsl:choose>


</xsl:template>


<!-- Template to compute progress weighted by complexity or criticity -->
<xsl:template name="ComputeWeightedProgress">
	<xsl:param name="RequirementList"/>
	<xsl:param name="GlobalProgressValue"/>
    <xsl:param name="GlobalWeight"/>
    <xsl:param name="WeightType"/>

    <!-- Compute current requirement weight -->
    <xsl:variable name="RequirementWeight">
		<xsl:call-template name="GetRequirementWeight" >
			<xsl:with-param name="RequirementName" select="$RequirementList[1]/@name"/>
			<xsl:with-param name="WeightType" select="$WeightType"/>
		</xsl:call-template>
    </xsl:variable>
    <!-- Compute current requirement weighted progress -->
    <xsl:variable name="RequirementWeightedProgress">
		<xsl:call-template name="GetRequirementWeightedProgress" >
			<xsl:with-param name="RequirementName" select="$RequirementList[1]/@name"/>
			<xsl:with-param name="WeightType" select="$WeightType"/>
		</xsl:call-template>
    </xsl:variable>
		
	<xsl:choose>
        <!-- If there is several requirement left -->
		<xsl:when test="$RequirementList[position()>1]">

            <!-- Recursive call to ComputeWeightedProgress -->
    		<xsl:call-template name = "ComputeWeightedProgress" >
				<xsl:with-param name="RequirementList" select="$RequirementList[position()>1]"/>
				<xsl:with-param name="GlobalProgressValue" select="$GlobalProgressValue + number($RequirementWeightedProgress)"/>
				<xsl:with-param name="GlobalWeight" select="$GlobalWeight + number($RequirementWeight)"/>
    			<xsl:with-param name="WeightType" select="$WeightType"/>
			</xsl:call-template>

		</xsl:when>
	  
        <!-- If the current requirement is the last one -->
		<xsl:otherwise>

            <!-- Result is the global progress divided by global weight -->
            <xsl:variable name="GlobalMetrics" select="($GlobalProgressValue + number($RequirementWeightedProgress)) div ($GlobalWeight + number($RequirementWeight))"/>
            <xsl:value-of select="format-number($GlobalMetrics div 100, '###.#%')"/>
		</xsl:otherwise>

	</xsl:choose>
</xsl:template>


<!-- Template to compute the numerical value of the weight (by criticity or complexity) of a given requirement -->
<xsl:template name="GetRequirementWeight">
	<xsl:param name="RequirementName"/>
	<xsl:param name="WeightType"/>

    <!-- Convert the weight stored in $STR_RequirementFile into a numerical value -->
    <xsl:variable name="RequirementWeight">
        <xsl:call-template name="GetWeight">
            <xsl:with-param name="WeightType" select="$WeightType"/>
            <xsl:with-param name="WeightName" select="document($STR_RequirementFile)/Requirements//file/Requirement[@name=$RequirementName]/@*[name()=$WeightType]"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="$RequirementWeight"/>
</xsl:template>


<!-- Template to compute the weighted (by criticity or complexity) progress of a given requirement -->
<xsl:template name="GetRequirementWeightedProgress">
	<xsl:param name="RequirementName"/>
	<xsl:param name="WeightType"/>

    <!-- Get the numerical value of the weight of the requirement -->
    <xsl:variable name="RequirementWeight">
        <xsl:call-template name="GetRequirementWeight">
            <xsl:with-param name="RequirementName" select="$RequirementName"/>
            <xsl:with-param name="WeightType" select="$WeightType"/>
        </xsl:call-template>
    </xsl:variable>
    <!-- Get the progress of the requirement -->
    <xsl:variable name="RequirementProgress"><xsl:value-of select="/AvancementProjet/Requirement[@name=$RequirementName]/AvancementReq/@value"/></xsl:variable>

    <xsl:value-of select="number($RequirementProgress) * number($RequirementWeight)"/>
</xsl:template>


<!-- Template to get the default value for progress, complexity or criticity -->
<xsl:template name="GetDefaultValue">
	<xsl:param name="ValueType"/>

    <xsl:value-of select="document($STR_RequirementParamFile)//*[name()=$ValueType]/Default/@value"/>

</xsl:template>


<!-- Template to compute the numerical value of the weight of a given criticity/complexity -->
<xsl:template name="GetWeight">
	<xsl:param name="WeightType"/>
	<xsl:param name="WeightName"/>

    <!-- The numerical value of the weight is stored in $STR_RequirementParamFile -->
    <xsl:value-of select="document($STR_RequirementParamFile)//*[name()=$WeightType]/Value[@name=$WeightName]/@weight"/>

</xsl:template>


<!-- Template to compute the color to display a requirement -->
<xsl:template name="GetColor">
	<xsl:param name="Progress"/>

    <!-- TODO si STR_RequirementParamFile not set or not found : 0->red else green -->
    <!-- TODO il peut y avoir un bug si les valeurs ne sont pas triées -->
    <xsl:value-of select="document($STR_RequirementParamFile)/RequirementsParam/Progress/Value[@value&gt;=$Progress]/@color"/>

</xsl:template>

</xsl:stylesheet>
