/* method:Pkg1Class1_f1
** requirement : ARQSAMPLE_RequirementTextFuncD_31
*/
f1()
{
}

/* method : Pkg1Class1_f2
** requirement : ARQSAMPLE_RequirementTextFuncD_31 , ARQSAMPLE_RequirementTextFuncE_42
*/
f2()
{
}

/* method : Pkg1Class1_f3
** requirement : 
** description : traceability "error"(but normal in code) : no requirement is covered by this method
*/
f3()
{
}

/* method : Pkg1Class1_f4
** requirement : ARQSAMPLE_RequirementTextFuncE_42
*/
f4()
{
}

/* method : Pkg1Class1_f5
** description : traceability "error"(but normal in code) : no requirement is covered by this method,  no requirement field. TODO: delete the requirement field. So far,
**               its absence generate a xml coverage tag error
** requirement : 
*/
f5()
{
}
